﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;

namespace day20
{
    class Field
    {
        public Vector2 Pos { get; set; }

    }
    class Wall : Field
    {
        public bool IsWall { get; set; }
    }

    class Path : Field { }

    class PortalField : Path
    {
        public string Portal { get; set; }
        public Vector2 PortalDest { get; set; }
    }

    class DonutMaze
    {
        public void Start()
        {
            var area = new Dictionary<Vector2, Field>();
            var y = 0;
            foreach (var line in File.ReadAllLines("INPUT.txt"))
            {
                for (int x = 0; x < line.Length; x++)
                {
                    var c = line[x];
                    var pos = new Vector2(x, y);
                    switch (c)
                    {
                        case '#':
                            area.Add(pos, new Wall()
                            {
                                IsWall = true,
                                Pos = pos
                            });
                            break;
                        case '.':
                            area.Add(pos, new Path()
                            {
                                Pos = pos
                            });
                            break;
                        case ' ':
                            break;
                        default:
                            
                            
                            break;
                    }
                }
                y++;
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            new DonutMaze().Start();
        }
    }
}
