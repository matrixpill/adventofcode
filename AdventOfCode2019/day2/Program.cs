﻿using System;
using System.IO;
using IntCodeComputer;

namespace day2
{
    class Program
    {
        
        static void Main(string[] args)
        {
            const long FINAL = 19690720;

            string inputText = File.ReadAllText("program.txt");
            string[] splittedText = inputText.Split(',');

            long[] baseprogram = Array.ConvertAll(splittedText, (string x) => long.Parse(x));
            
            var computer = new Computer();
            var tmpPrg = (long[])baseprogram.Clone();

            bool foundIt = false;

            for(int noun = 0; noun < 100; noun++)
            {
                for(int verb = 0; verb < 100; verb++)
                {
                    tmpPrg[1] = noun;
                    tmpPrg[2] = verb;
                    computer.installProgram(tmpPrg);

                    computer.run();
                    if (tmpPrg[0] == FINAL)
                    {
                        Console.WriteLine(calcResult(noun, verb));
                        foundIt = true;
                        break;
                    }
                    else
                        tmpPrg = (long[])baseprogram.Clone();

                }
                if (foundIt) break;
            }


            //Console.WriteLine(program[0]);
            Console.WriteLine("FINITO");
            Console.Read();
        }

        static int calcResult(int noun, int verb)
        {
            return 100 * noun + verb;
        }

        
    }
    /*
    enum STATE
    {
        NEXT,
        FIN,
        WUT
    }
    class Computer
    {
        static int[] program;

        public void installProgram(int[] prg)
        {
            program = prg;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns>Program at end</returns>
        public int[] run()
        {
            int curPos = 0;
            STATE state;
            do
            {
                state = performInstrunction(new int[] { program[curPos], program[curPos + 1], program[curPos + 2], program[curPos + 3] });
                curPos += 4;
            } while (state == STATE.NEXT);
            return program;
        }

        private STATE performInstrunction(int[] instruction)
        {
            switch (instruction[0])
            {
                case 1:
                    program[instruction[3]] = add(instruction[1], instruction[2]);
                    return STATE.NEXT;
                case 2:
                    program[instruction[3]] = mult(instruction[1], instruction[2]);
                    return STATE.NEXT;
                case 99:
                    //Console.WriteLine("ENDE");
                    return STATE.FIN;
                default:
                    //Console.WriteLine("WUT");
                    return STATE.WUT;
            }
        }

        private int add(int posA, int posB)
        {
            return program[posA] + program[posB];
        }
        private int mult(int posA, int posB)
        {
            return program[posA] * program[posB];
        }
    }
    */
}
