﻿// Learn more about F# at http://fsharp.org

open System.IO

[<EntryPoint>]
let main argv =    
    let lines = File.ReadLines("TEST.txt");
    
    let findAstroids row y =        
        row 
        |> Seq.indexed
        |> Seq.map (fun (i,ch) -> if ch = '#' then Some((i, y)) else None )        

    let astroids = 
        lines
            |> Seq.indexed
            |> Seq.map (fun (i,r) -> findAstroids r i)
            |> Seq.reduce Seq.append
    

    let dic =  dict[
       for i in astroids do 
            match i with 
            | Some(x) -> 
                yield (x,0)
            | _ -> ()
    ]

    let testIfHidden (fX, fY) (x, y) = 
        let deltaX, deltaY = (fX - x, fY - y)
        let blub = 
            dic.Keys
            |> Seq.map (fun (hX, hY) -> 
                    let distX, distY = (fX - hX, fY - hY)
                    let xt = 
                        if (distX <> 0) then
                            (abs deltaX % abs distX) <> 0
                        else 
                            true

                    let yt = 
                        if (distY <> 0) then
                            (abs deltaY % abs distY) <> 0
                        else 
                            true
                    if(yt && xt) then
                        Some(hX,hY)
                    else  
                        None
                    )
        blub



    let canSee (fX,fY) =         
        let boop = seq{
            for x in dic.Keys do 
                match x with 
                | (x,y) when x = fX && y = fY -> ()
                | (x,y) -> yield Seq.length (testIfHidden (fX,fY) (x,y))                        
        }
        boop

    let wut = 
        let d = seq {
            for i in dic.Keys do
                yield canSee i
        }
        d
        
    let arr = [|
        for d in wut do
            d
    |]

    printfn "%A " wut

    printfn "%A " dic
    0
