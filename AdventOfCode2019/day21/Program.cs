﻿using System;
using System.IO;
using IntCodeComputer;

namespace day21
{

    /*
     * Part1 
     (!A & !B & !C & D) | (!A & !B & C & D) | (!A & B & !C & D) | (!A & B & C & D) | (A & !B & !C & D) | (A & !B & C & D) | (A & B & !C & D) = 1

    (!A | !B | !C) & D = JUMP
    (A & B & C) | !D = DONT


        "NOT A J\n" +
        "NOT B T\n" +
        "OR T J\n" +
        "NOT C T\n" +
        "OR T J\n" +
        "AND D J\n" +
        "WALK\n" 
     */

    class JMP
    {
        public void Start(long[] prg)
        {
            var jmp = new Computer();
            jmp.installProgram(prg);

            // Guilty of cheating for Part2 :((((( I was completly stuck on one Thought which turned out to be false
            var input = Array.ConvertAll((
                "OR B J\n" +
                "AND C J\n" +
                "NOT J J\n" +
                "AND D J\n" +
                "AND H J\n" +
                "NOT A T\n" +
                "OR T J\n" +
                "RUN\n"

                ).ToCharArray(), x => (long)x);
            var state = STATE.NEXT;
            while (state != STATE.FIN)
            {
                var res = jmp.run(true, false, input);
                state = res.Item1;
                if (state == STATE.FIN)
                    Console.WriteLine(res.Item2);
                else
                    Console.Write((char)res.Item2);

            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var prg = Array.ConvertAll(File.ReadAllText("INPUT.txt").Split(','), x => long.Parse(x));
            new JMP().Start(prg);
        }
    }
}
