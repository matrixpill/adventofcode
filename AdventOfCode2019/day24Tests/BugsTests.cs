﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using day24;
using System;
using System.Collections.Generic;
using System.Text;

namespace day24.Tests
{
    [TestClass()]
    public class BugsTests
    {
        [TestMethod()]
        public void GetBiodiversityTest()
        {
            var bugs = new Bugs();

            //0
            string inp = "0000000000";
            var res = bugs.GetBiodiversity(inp);
            Assert.AreEqual(0, res);


            //0
            inp = "1000000000";
            res = bugs.GetBiodiversity(inp);
            Assert.AreEqual(1, res);

            //0
            inp = "1111111111"; //1023
            res = bugs.GetBiodiversity(inp);
            Assert.AreEqual(1023, res);
        }

    }
}