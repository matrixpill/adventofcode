﻿// Learn more about F# at http://fsharp.org

open System
open System.IO

[<EntryPoint>]
let main argv =
    let input = File.ReadAllText("INPUT.txt")
    let width = 25
    let height = 6

    let items = [|
        for i in 0 .. input.Length - 1 do 
            yield input.[i].ToString() |> int
    |]  
  
    let smallestFun items = 
        items 
        |> Array.chunkBySize (width * height)
        |> Array.map (Array.countBy id)
        |> Array.map (Array.sortBy (fun (k,v) -> k))
        |> Array.minBy (Array.find (fun (k,v) -> k = 0))

    let smallest = smallestFun items

    printfn "Part 1: %A \n" (snd smallest.[1] * snd smallest.[2])

    let properArrayFun items = 
        items 
        |> Array.chunkBySize (width * height)
        |> Array.map (Array.chunkBySize width)
       
    let properArray = properArrayFun items


    let muster h w (arra:int[][][])= 
        arra
        |> Array.map (fun s -> s.[h].[w])
        |> Array.tryFind (fun s -> s = 0 || s = 1)
        

   
   //|> Array.map (fun arr -> arr)
    for h in 0..height-1 do
        for w in 0 .. width-1 do            
             let blub = muster h w properArray
             match blub with
             | Some(c) -> if c = 0 then printf "_" else printf "%A" c
             | None -> printf "2"
             if w = width-1 then printfn ""
             

    


    0 



(*
    let seqChunks = Array.chunkBySize (width * height) items 
    let cntAll = Array.map (Array.countBy id) seqChunks
    let zeros = Array.map (Array.find (fun (k,_) -> k = 0)) cntAll |> Array.map (fun (k,c) -> c)

    let minIndex = Array.tryFindIndex (fun x -> x = (Array.min zeros)) zeros
    let min = Array.get cntAll minIndex.Value
    let sorted = Array.sortBy (fun (_,c) -> c) min
    let flattened = Array.map (fun (k,v) -> v) sorted
    printf "%A " (flattened.[1] * flattened.[2])*)