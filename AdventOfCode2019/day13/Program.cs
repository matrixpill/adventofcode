﻿using System;
using System.IO;
using System.Numerics;
using System.Threading;
using IntCodeComputer;

namespace day13
{

    class Game
    {
        enum TILES
        {
            EMPTY,
            WALL,
            BLOCK,
            HORIZONTAL_PADDLE,
            BALL
        }

        int ballX = 0;
        int playerX = 0;

        public void Start()
        {
            var console = new Computer();
            var game = Array.ConvertAll(File.ReadAllText("INPUT.txt").Split(","), s => long.Parse(s));
            game[0] = 2;

            console.installProgram(game);
            var state = STATE.NEXT;
            var i = 0;
            var cntWalls = 0;
            Vector2 pos = new Vector2();
            while (state != STATE.FIN && state != STATE.WUT)
            {
                var move = whereToMove();

                var res = console.run(true, para: move);
                state = res.Item1;
                if (state == STATE.FIN) break;

                switch ((i + 1) % 3)
                {
                    case 1:
                        pos.X = res.Item2;
                        break;
                    case 2:
                        pos.Y = res.Item2;
                        break;
                    case 0:
                        //paint or update Score
                        if (pos.X == -1 && pos.Y == 0)
                        {
                            drawAt(new Vector2(40, 0), res.Item2, true);
                        }
                        else
                        {
                            drawAt(pos, res.Item2);
                        }
                        break;
                }
                i++;
            }
            Console.SetCursorPosition(0, 25);
            Console.WriteLine("Part1: " + cntWalls);
        }

        int whereToMove()
        {
            if (ballX > playerX)
                return 1;
            if (ballX < playerX)
                return -1;
            return 0;
        }

        void drawAt(Vector2 pos, long value, bool isScore = false)
        {
            var prevPos = new Vector2(Console.CursorLeft, Console.CursorTop);
            Console.SetCursorPosition((int)pos.X, (int)pos.Y);
            if (isScore)
            {
                Console.Write(value);
            }
            else
            {
                var tile = (TILES)value;
                switch (tile)
                {
                    case TILES.EMPTY:
                        Console.WriteLine(" ");
                        break;
                    case TILES.WALL:
                        Console.WriteLine("■");
                        break;
                    case TILES.BLOCK:
                        Console.WriteLine("#");
                        break;
                    case TILES.HORIZONTAL_PADDLE:
                        Console.WriteLine("P");
                        playerX = (int)pos.X;
                        break;
                    case TILES.BALL:                        
                        Console.WriteLine("O");
                        ballX = (int)pos.X;
                        //Thread.Sleep(10);

                        break;
                    default:
                        break;
                }
            }
            Console.SetCursorPosition((int)prevPos.X, (int)prevPos.Y);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            new Game().Start();
            Console.Read();
        }
    }
}
