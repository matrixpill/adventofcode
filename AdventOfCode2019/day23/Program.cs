﻿using System;
using IntCodeComputer;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace day23
{
    class Network
    {
        
        public void Start2(long[] prg)
        {
            var inputQue = new Dictionary<int, List<long>>();
            var tmpRecDict = new Dictionary<int, List<long>>();

            var PcIdling = new bool[50];
            long lastY = 0;
            var NAT = new long[2];

            void setIdling(bool idl, int ID)
            {
                PcIdling[ID] = idl;
            }

            for (int i = 0; i < 50; i++)
            {
                var inpList = new List<long>();
                inpList.Add(i);
                inputQue.Add(i, inpList);
                tmpRecDict[i] = new List<long>();
            }

            Action<long, Computer> recieveValue = new Action<long, Computer>((x, pc) =>
            {
                var ID = pc.GetID();

                setIdling(false, ID);

                if (tmpRecDict[ID].Count < 3)
                {
                    tmpRecDict[ID].Add(x);
                    if (tmpRecDict[ID].Count != 3)
                        pc.run(true);
                }


                if (tmpRecDict[ID].Count == 3)
                {
                    if (tmpRecDict[ID][0] == 255)
                    {
                        NAT[0] = tmpRecDict[ID][1];
                        NAT[1] = tmpRecDict[ID][2];
                        //Console.WriteLine($"{NAT[0]} {NAT[1]}");
                        tmpRecDict[ID].Clear();
                    }
                    else
                    {
                        inputQue[(int)tmpRecDict[ID][0]].AddRange(tmpRecDict[ID].GetRange(1, 2));
                        tmpRecDict[ID].Clear();
                    }
                }
            });

            Func<Computer, long> giveInput = new Func<Computer, long>((pc) =>
            {
                var ID = pc.GetID();
                var list = inputQue[ID];
                long nextInp = -1;
                if (list.Count > 0)
                {
                    nextInp = list[0];
                    list.RemoveAt(0);
                    setIdling(false, ID);
                }
                else
                {
                    setIdling(true, ID);
                }
                return nextInp;
            });

            var pcs = new Computer[50];

            for (int i = 0; i < 50; i++)
            {
                var newPC = new Computer(i);
                newPC.installProgram((long[])prg.Clone());
                newPC.SetINOUT(recieveValue, giveInput);
                pcs[i] = newPC;
            }

            long cnt = 0;
            while (true)
            {
                foreach (var PC in pcs)
                {
                    PcIdling[PC.GetID()] = false;
                    var res = PC.RunWhileSTATENot(s => s != STATE.INNEXT && s != STATE.OUTNEXT);
                }
                var idle = cnt == 1 ? false: true;
                foreach (var PCtry in PcIdling)
                {
                    idle = idle & PCtry;
                }
                if (idle)
                {
                    if (lastY == NAT[1])
                    {
                        Console.WriteLine("Part2 : " + NAT[1]);
                        break;
                    }
                    lastY = NAT[1];
                    inputQue[0].AddRange(NAT);
                }
                    
                cnt++;
            }
        }


        public void Start1_ButFaster(long[] prg)
        {
            var inputQue = new Dictionary<int, List<long>>();
            var tmpRecDict = new Dictionary<int, List<long>>();

            for (int i = 0; i < 50; i++)
            {
                var inpList = new List<long>();
                inpList.Add(i);
                inputQue.Add(i, inpList);
                tmpRecDict[i] = new List<long>();
            }

            Action<long, Computer> recieveValue = new Action<long, Computer>((x, pc) =>
            {
                var ID = pc.GetID();
                if (tmpRecDict[ID].Count != 3)
                {
                    tmpRecDict[ID].Add(x);
                }

                if (tmpRecDict[ID].Count == 3)
                {
                    if (tmpRecDict[ID][0] == 255)
                        Console.Write("Part1: " + tmpRecDict[ID][2]);
                    else
                    {
                        inputQue[(int)tmpRecDict[ID][0]].AddRange(tmpRecDict[ID].GetRange(1, 2));
                        tmpRecDict[ID].Clear();
                    }
                }
            });


            Func<Computer, long> giveInput = new Func<Computer, long>((pc) =>
            {
                var ID = pc.GetID();
                var list = inputQue[ID];
                long nextInp = -1;
                if (list.Count > 0)
                {
                    nextInp = list[0];
                    list.RemoveAt(0);
                }
                return nextInp;
            });

            var pcs = new Computer[50];

            for (int i = 0; i < 50; i++)
            {
                var newPC = new Computer(i);
                newPC.installProgram((long[])prg.Clone());
                newPC.SetINOUT(recieveValue, giveInput);
                pcs[i] = newPC;
            }

            while (true)
                foreach (var PC in pcs)
                {
                    PC.RunOnce();
                }
        }

        class ThreadObj
        {
            public long[] prg { get; set; }
            public int NR { get; set; }
        }
        public void Start1(long[] prg)
        {
            Dictionary<int, List<long>> inputQue = new Dictionary<int, List<long>>();
            var tmpRecDict = new Dictionary<int, List<long>>();


            List<long> inpList;
            for (int i = 0; i < 50; i++)
            {
                inpList = new List<long>();
                inpList.Add(i);
                inputQue.Add(i, inpList);
                tmpRecDict[i] = new List<long>();
            }


            Action<long, Computer> recieveValue = new Action<long, Computer>((x, pc) =>
            {
                lock (tmpRecDict)
                {

                    var ID = pc.GetID();
                    if (tmpRecDict[ID].Count != 3)
                    {
                        tmpRecDict[ID].Add(x);
                    }

                    if (tmpRecDict[ID].Count == 3)
                    {
                        lock (inputQue)
                        {
                            if (tmpRecDict[ID][0] == 255)
                                Console.Write("Part1: " + tmpRecDict[ID][2]);
                            else
                            {
                                inputQue[(int)tmpRecDict[ID][0]].AddRange(tmpRecDict[ID].GetRange(1, 2));
                                tmpRecDict[ID].Clear();
                            }
                        }
                    }
                }
            });


            Func<Computer, long> giveInput = new Func<Computer, long>((pc) =>
            {
                var ID = pc.GetID();
                lock (inputQue)
                {
                    var list = inputQue[ID];
                    long nextInp = -1;
                    if (list.Count > 0)
                    {
                        nextInp = list[0];
                        list.RemoveAt(0);
                    }
                    return nextInp;
                }
            });


            Action<object> intAction = (Object inp) =>
            {
                var inpO = (ThreadObj)inp;
                var prg = inpO.prg;
                var pc = new Computer(inpO.NR);
                pc.installProgram(prg);
                pc.SetINOUT(recieveValue, giveInput);
                var r = pc.RunWhileSTATENot(s => true);
            };


            var tasks = new Task[50];
            for (int i = 0; i < 50; i++)
            {
                tasks[i] = new Task(intAction, new ThreadObj()
                {
                    NR = i,
                    prg = (long[])prg.Clone()
                });
                tasks[i].Start();
            }

            Task.WaitAll(tasks);
            Console.Read();
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            var prg = Array.ConvertAll(File.ReadAllText("INPUT.txt").Split(","), x => long.Parse(x));


            new Network().Start2(prg);
        }
    }
}
