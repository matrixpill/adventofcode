﻿using System;
using System.IO;

namespace day1
{
    class Program
    {
        static void Main(string[] args)
        {
            double sum = 0;
            var allMass = File.ReadAllLines("mass.txt");
            
            
            foreach (var mass in allMass)
            {
                var calculatedFuel = calculateFuel(long.Parse(mass));
                sum += calculatedFuel;
                while (true)
                {
                    var tmp = calculateFuel(calculatedFuel);
                    if (tmp > 0)
                    {
                        sum += tmp;
                        calculatedFuel = tmp;
                    }
                    else
                        break;
                }
            }

            Console.WriteLine("SUM: " + sum);


            Console.Read();
        }

        static long calculateFuel(long mass)
        {           
            decimal div3 = mass / 3;
            long rounded = long.Parse(Math.Floor(div3).ToString());
            return rounded - 2;
        }
    }
}
