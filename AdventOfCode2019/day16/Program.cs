﻿using System;
using System.Collections.Generic;
using System.IO;

namespace day16
{
    class FFT
    {
        public void Start()
        {
            const int TIMES = 10000;
            var input = File.ReadAllText("INPUT.txt");
            var f = input.Length * TIMES;
            var allNumbers = new int[f];
            var numbers = Array.ConvertAll(input.ToCharArray(), c => (int)char.GetNumericValue(c));
            string offsetString = "";
            for (int i = 0; i < 7; i++)
                offsetString += numbers[i];

            var offset = int.Parse(offsetString);

            for (int i = 0; i < TIMES; i++)
            {
                numbers.CopyTo(allNumbers, i * numbers.Length);
            }
            var lastNumbersList = new List<int>();
            for (int i = offset; i < allNumbers.Length; i++)
            {
                lastNumbersList.Add(allNumbers[i]);
            }

            var lastNumbers = lastNumbersList.ToArray();


            for (int phase = 0; phase < 100; phase++)
            {
                for (int i = lastNumbers.Length - 1; i >= 0; i--)
                {
                    var tmp = i + 1 == lastNumbers.Length ? 0 : lastNumbers[i + 1];
                    lastNumbers[i] = Math.Abs(tmp + lastNumbers[i]) % 10;
                }
            }

            for (int i = 0; i < 8; i++)
            {
                Console.Write(lastNumbers[i]);
            }

        }

        class Program
        {
            static void Main(string[] args)
            {
                new FFT().Start();
            }
        }
    }
}
