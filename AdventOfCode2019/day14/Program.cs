﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Numerics;

namespace day14
{
    class Ingredient
    {
        public Ingredient(string ing)
        {
            var spl = ing.Trim().Split(' ');
            Name = spl[1];
            Count = BigInteger.Parse(spl[0]);
        }

        public string Name { get; set; }
        public BigInteger Count { get; set; }
    }

    class Recipe
    {
        public Ingredient Result { get; set; }
        public Ingredient[] Ingredients { get; set; }
    }

    class FUELCalculator
    {
        List<Recipe> recipes = new List<Recipe>();

        public BigInteger Start()
        {
            Recipe FUEL = recipes.Find(x => x.Result.Name == "FUEL");

            BigInteger ores = Calculate(FUEL, FUEL.Result.Count);
            return ores;
        }

        


        public BigInteger FindMaxFUEL()
        {
            BigInteger MAXORE = 1000000000000;
            BigInteger ores = MAXORE;
            BigInteger maxFuel = long.MaxValue;
            BigInteger minFuel = 1;
            BigInteger FUELCnt = 1;

            Recipe FUEL;
            do
            {
                if(ores > MAXORE)
                {
                    maxFuel = FUELCnt;
                }
                else if(ores < MAXORE)
                {
                    minFuel = FUELCnt;
                }
                FUELCnt = minFuel + ((maxFuel - minFuel) / 2);
                if (maxFuel - minFuel == 1)
                    break;

                FUEL = recipes.Find(x => x.Result.Name == "FUEL");
                FUEL.Result.Count = FUELCnt;
                ores = Calculate(FUEL, FUEL.Result.Count);

            } while (ores < MAXORE || ores > MAXORE);

            
            return minFuel;
        }



        BigInteger Calculate(Recipe rec, BigInteger times, Dictionary<string, BigInteger> rest = null)
        {
            if (rest == null)
                rest = new Dictionary<string, BigInteger>();

            BigInteger ores = 0;
            foreach (var ingredient in rec.Ingredients)
            {
                if (ingredient.Name == "ORE")
                {
                    return times * ingredient.Count;                    
                }

                var nextRecipe = recipes.Find(x => x.Result.Name == ingredient.Name);
                BigInteger goalTimes = times * ingredient.Count;
                if (rest.ContainsKey(ingredient.Name))
                {
                    if (goalTimes >= rest[ingredient.Name])
                    {
                        goalTimes -= rest[ingredient.Name];
                        rest.Remove(ingredient.Name);
                    }
                    else
                    {
                        rest[ingredient.Name] -= goalTimes;
                        goalTimes = 0;
                    }
                    if (goalTimes == 0)
                        continue;
                }


                BigInteger nTimes = 1;
                if (goalTimes > nextRecipe.Result.Count)
                {
                    BigInteger rem = 0;
                    var tmp = BigInteger.DivRem(goalTimes, nextRecipe.Result.Count, out rem);
                    if (rem > 0)
                        tmp += 1;
                    nTimes = tmp;
                }

                if (nTimes * nextRecipe.Result.Count > goalTimes)
                {
                    if (rest.ContainsKey(ingredient.Name))
                    {
                        rest[ingredient.Name] += nTimes * nextRecipe.Result.Count - goalTimes;
                    }
                    else
                    {
                        rest.Add(ingredient.Name, nTimes * nextRecipe.Result.Count - goalTimes);
                    }
                }
                ores += Calculate(nextRecipe, nTimes, rest);
            }
            return ores;
        }

        public void ClearRecipes()
        {
            recipes.Clear();
        }

        public void ReadRecepie(string recepie)
        {
            var splitted = recepie.Split("=>");
            var rawIngredients = splitted[0].Split(",");


            recipes.Add(new Recipe()
            {
                Ingredients = Array.ConvertAll(rawIngredients, x => new Ingredient(x)),
                Result = new Ingredient(splitted[1])
            });
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var calc = new FUELCalculator();

            calc.ClearRecipes();
            foreach (var recipe in File.ReadAllLines("INPUT.txt"))
            {
                calc.ReadRecepie(recipe);
            }
            Console.WriteLine("Test1: " + calc.Start());
            calc.ClearRecipes();
            foreach (var recipe in File.ReadAllLines("INPUT2.txt"))
            {
                calc.ReadRecepie(recipe);
            }
            Console.WriteLine("Part1: " + calc.Start());

            Console.WriteLine("Part2: " + calc.FindMaxFUEL());
        }
    }
}
