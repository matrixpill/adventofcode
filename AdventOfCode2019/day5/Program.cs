﻿using System;
using System.IO;
using IntCodeComputer;

namespace day5
{
    class Program
    {
        static void Main(string[] args)
        {
            string inputText = File.ReadAllText("INPUT.txt");
            string[] splittedText = inputText.Split(',');

            long[] baseprogram = Array.ConvertAll(splittedText, (string x) => long.Parse(x));

            var computer = new Computer();
            computer.installProgram(baseprogram);
            Console.WriteLine(computer.run().Item2);
        }
    }

    /*
    enum STATE
    {
        NEXT,
        FIN,
        WUT
    }

    class Computer
    {
        int[] program;
        int curPos = 0;

        public void installProgram(int[] prg)
        {
            program = prg;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns>Program at end</returns>
        public int[] run()
        {
            //int curPos = 0;
            STATE state;
            do
            {
                state = performInstrunction(new int[] { tryGet(curPos), tryGet(curPos + 1), tryGet(curPos + 2), tryGet(curPos + 3) });

            } while (state == STATE.NEXT);
            return program;
        }

        private int tryGet(int pos)
        {
            if (pos >= program.Length)
            {
                Console.WriteLine("Out of Reach: " + pos);
                return 0;
            }
            else
            {
                return program[pos];
            }
        }

        private STATE performInstrunction(int[] instruction)
        {
            var tmpInst = new int[instruction.Length];
            instruction.CopyTo(tmpInst, 0);
            analyse(instruction);
            switch (instruction[0])
            {
                case 1:
                    program[instruction[3]] = add(instruction[1], instruction[2]);
                    curPos += 4;
                    return STATE.NEXT;
                case 2:
                    program[instruction[3]] = mult(instruction[1], instruction[2]);
                    curPos += 4;
                    return STATE.NEXT;
                case 3:
                    program[instruction[1]] = input();
                    curPos += 2;
                    return STATE.NEXT;
                case 4:
                    output(instruction[1]);
                    curPos += 2;
                    return STATE.NEXT;
                case 5:
                    //jmp if true
                    if (instruction[1] != 0)
                        curPos = instruction[2];
                    else
                        curPos += 3;
                    return STATE.NEXT;
                case 6:
                    //jmp if false
                    if (instruction[1] == 0)
                        curPos = instruction[2];
                    else
                        curPos += 3;
                    return STATE.NEXT;
                case 7:
                    //less than
                    if (instruction[1] < instruction[2])
                        program[instruction[3]] = 1;
                    else
                        program[instruction[3]] = 0;
                    curPos += 4;
                    return STATE.NEXT;
                case 8:
                    //equals
                    if (instruction[1] == instruction[2])
                        program[instruction[3]] = 1;
                    else
                        program[instruction[3]] = 0;
                    curPos += 4;
                    return STATE.NEXT;
                case 99:
                    Console.WriteLine("ENDE");
                    return STATE.FIN;
                default:
                    Console.WriteLine("WUT");
                    return STATE.WUT;
            }
        }

        private void analyse(int[] instruction)
        {
            var opt = instruction[0] % 100;
            int ABC = (instruction[0] - opt) / 100;

            var modes = ABC.ToString().PadLeft(3, '0');

            instruction[0] = opt;
            if (opt != 3 && opt != 99)
            {
                instruction[1] = modes[2] == '1' ? instruction[1] : program[instruction[1]];     //1 2   4 5 6 7 8 
                if (opt != 4)
                    instruction[2] = modes[1] == '1' ? instruction[2] : program[instruction[2]]; //1 2     5 6 7 8 
                //instruction[3] = modes[0] == '1' ? instruction[3] : program[instruction[3]];   //
            }
        }

        private int add(int posA, int posB)
        {
            return posA + posB;
        }
        private int mult(int posA, int posB)
        {
            return posA * posB;
        }

        private int input()
        {
            Console.Write("Please Input something: ");
            return int.Parse(Console.ReadLine());
        }
        private void output(int output)
        {

            Console.WriteLine("OUT: " + output);
        }
    }*/
}
