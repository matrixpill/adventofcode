﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using IntCodeComputer;

namespace day7
{
    class Program
    {
        static void Main()
        {
            long highest = 0;
            string inputText = File.ReadAllText("INPUT.txt");
            string[] splittedText = inputText.Split(',');

            long[] baseprogram = Array.ConvertAll(splittedText, (string x) => long.Parse(x));

            List<int[]> inputs = new List<int[]>();

            Computer[] amps = new Computer[5];
            for (int i = 0; i < 5; i++)
            {
                amps[i] = new Computer();
                amps[i].installProgram((long[])baseprogram.Clone());
            }

            var cpu = new Computer();
            for (int i = 56789; i <= 98765; i++)
            {
                var number = i.ToString().PadLeft(5, '0');
                if (Regex.Match(number, "([5-9]).*\\1").Success == false && Regex.Match(number, "[0-4]").Success == false) //Regex.Match(number, "([0-4]).*\\1").Success == false && Regex.Match(number, "[5-9]").Success == false
                {

                    inputs.Add(new int[]{
                        (int)char.GetNumericValue(number[0]),
                        (int)char.GetNumericValue(number[1]),
                        (int)char.GetNumericValue(number[2]),
                        (int)char.GetNumericValue(number[3]),
                        (int)char.GetNumericValue(number[4])
                    }
                    );
                }
            }
            foreach (var inp in inputs)
            {
                var firstRun = true;
                amps = new Computer[5];
                for (int i = 0; i < 5; i++)
                {
                    amps[i] = new Computer();
                    amps[i].installProgram((long[])baseprogram.Clone());

                }
                (STATE, long) tmpRes = (STATE.NEXT, 0);
                while (tmpRes.Item1 != STATE.FIN)
                {
                    tmpRes = amps[0].run(true, true, buildInput(firstRun, inp[0], tmpRes.Item2).ToArray());
                    tmpRes = amps[1].run(true, true, buildInput(firstRun, inp[1], tmpRes.Item2).ToArray());
                    tmpRes = amps[2].run(true, true, buildInput(firstRun, inp[2], tmpRes.Item2).ToArray());
                    tmpRes = amps[3].run(true, true, buildInput(firstRun, inp[3], tmpRes.Item2).ToArray());
                    tmpRes = amps[4].run(true, true, buildInput(firstRun, inp[4], tmpRes.Item2).ToArray());
                    firstRun = false;
                }

                if (tmpRes.Item2 > highest)
                    highest = tmpRes.Item2;


            }
            Console.WriteLine("Part 2: " + highest);
        }

        static List<long> buildInput(bool first, int inp, long lastValue)
        {
            var inpP = new List<long>();
            inpP.Clear();
            if (first)
            {
                inpP.Add(inp);
            }
            inpP.Add(lastValue);
            return inpP;
        }
    }

    /*
    enum STATE
    {
        NEXT,
        OUTNEXT,
        INNEXT,
        FIN,
        WUT
    }
    class Computer
    {
        int timesCalled = 0;
        int[] program;
        int curPos = 0;
        int lastOut = 0;

        public void installProgram(int[] prg)
        {
            curPos = 0;
            program = prg;
        }
        (STATE, int) res = (STATE.NEXT, 0);

        public (STATE,int) run(params int[] para)
        {
            
            timesCalled += 1;
            

            int inputPos = 0;

            do
            {
                res = performInstrunction(new int[] { tryGet(curPos), tryGet(curPos + 1), tryGet(curPos + 2), tryGet(curPos + 3) });
                if (res.Item1 == STATE.OUTNEXT)
                {
                    lastOut = res.Item2;
                    return res;
                    //if (res.Item1 != STATE.FIN) break;
                }
                if (res.Item1 == STATE.INNEXT && para.Length > 0)
                {
                    //Console.WriteLine("IN: " + para[inputPos]);
                    if (timesCalled != 1)
                    {
                        inputPos = 1;
                    }
                    program[tryGet(curPos + 1)] = para[inputPos++];
                    
                    curPos += 2;
                }

            } while (res.Item1 != STATE.WUT && res.Item1 != STATE.FIN);
            return res;
        }

        private int tryGet(int pos)
        {
            if (pos >= program.Length)
            {
                //Console.WriteLine("Out of Reach: " + pos);
                return 0;
            }
            else
            {
                return program[pos];
            }
        }

        private (STATE, int) performInstrunction(int[] instruction)
        {
            var tmpInst = new int[instruction.Length];
            instruction.CopyTo(tmpInst, 0);
            analyse(instruction);
            (STATE, int) res;
            switch (instruction[0])
            {
                case 1:
                    program[instruction[3]] = add(instruction[1], instruction[2]);
                    curPos += 4;
                    res = (STATE.NEXT, 0);
                    break;

                case 2:
                    program[instruction[3]] = mult(instruction[1], instruction[2]);
                    curPos += 4;
                    res = (STATE.NEXT, 0);
                    break;

                case 3:
                    //program[instruction[1]] = input();
                    //curPos += 2;
                    res = (STATE.INNEXT, 0);
                    break;

                case 4:
                    output(instruction[1]);
                    curPos += 2;
                    res = (STATE.OUTNEXT, instruction[1]);
                    break;

                case 5:
                    //jmp if true
                    if (instruction[1] != 0)
                        curPos = instruction[2];
                    else
                        curPos += 3;
                    res = (STATE.NEXT, 0);
                    break;

                case 6:
                    //jmp if false
                    if (instruction[1] == 0)
                        curPos = instruction[2];
                    else
                        curPos += 3;
                    res = (STATE.NEXT, 0);
                    break;
                case 7:
                    //less than
                    if (instruction[1] < instruction[2])
                        program[instruction[3]] = 1;
                    else
                        program[instruction[3]] = 0;
                    curPos += 4;
                    res = (STATE.NEXT, 0);
                    break;
                case 8:
                    //equals
                    if (instruction[1] == instruction[2])
                        program[instruction[3]] = 1;
                    else
                        program[instruction[3]] = 0;
                    curPos += 4;
                    res = (STATE.NEXT, 0);
                    break;
                case 99:
                    Console.WriteLine("ENDE");
                    res = (STATE.FIN, lastOut);
                    break;
                default:
                    Console.WriteLine("WUT");
                    res = (STATE.WUT, 0);
                    break;
            }
            return res;
        }

        private void analyse(int[] instruction)
        {
            var opt = instruction[0] % 100;
            int ABC = (instruction[0] - opt) / 100;

            var modes = ABC.ToString().PadLeft(3, '0');

            instruction[0] = opt;
            if (opt != 3 && opt != 99)
            {
                instruction[1] = modes[2] == '1' ? instruction[1] : program[instruction[1]];     //1 2   4 5 6 7 8 
                if (opt != 4)
                    instruction[2] = modes[1] == '1' ? instruction[2] : program[instruction[2]]; //1 2     5 6 7 8 
                //instruction[3] = modes[0] == '1' ? instruction[3] : program[instruction[3]];   //
            }
        }

        private int add(int posA, int posB)
        {
            return posA + posB;
        }
        private int mult(int posA, int posB)
        {
            return posA * posB;
        }

        private int input()
        {
            Console.Write("Please Input something: ");
            return int.Parse(Console.ReadLine());
        }
        private void output(int output)
        {

            Console.WriteLine("OUT: " + output);
        }
    }*/
}
