﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;

namespace day12
{
    class MoonTracking
    {
        public void Start(string[] input)
        {
           
            //Item1 = Position | Item2 = Velocity
            var moons = new (Vector3, Vector3)[4];

            for (int i = 0; i < 4; i++)
            {
                var values = input[i][1..^1].Split(',');
                moons[i].Item1.X = int.Parse(values[0].Substring(values[0].IndexOf('=') + 1));
                moons[i].Item1.Y = int.Parse(values[1].Substring(values[1].IndexOf('=') + 1));
                moons[i].Item1.Z = int.Parse(values[2].Substring(values[2].IndexOf('=') + 1));
            }

            bool fX = false, fY = false, fZ = false;
            var listX = new List<(int, int)[]>();
            listX.Add(new (int, int)[] {
                ((int)moons[0].Item1.X, (int)moons[0].Item2.X),
                ((int)moons[1].Item1.X, (int)moons[1].Item2.X),
                ((int)moons[2].Item1.X, (int)moons[2].Item2.X),
                ((int)moons[3].Item1.X, (int)moons[3].Item2.X),
            });
            /*var ex = containsState(listX, new (int, int)[] {
                ((int)moons[0].Item1.X, (int)moons[0].Item2.X),
                ((int)moons[1].Item1.X, (int)moons[1].Item2.X),
                ((int)moons[2].Item1.X, (int)moons[2].Item2.X),
                ((int)moons[3].Item1.X, (int)moons[3].Item2.X),
            });*/
            var listY = new List<(int, int)[]>();
            listY.Add(new (int, int)[] {
                ((int)moons[0].Item1.Y, (int)moons[0].Item2.Y),
                ((int)moons[1].Item1.Y, (int)moons[1].Item2.Y),
                ((int)moons[2].Item1.Y, (int)moons[2].Item2.Y),
                ((int)moons[3].Item1.Y, (int)moons[3].Item2.Y),
            });
            var listZ = new List<(int, int)[]>();
            listZ.Add(new (int, int)[] {
                ((int)moons[0].Item1.Z, (int)moons[0].Item2.Z),
                ((int)moons[1].Item1.Z, (int)moons[1].Item2.Z),
                ((int)moons[2].Item1.Z, (int)moons[2].Item2.Z),
                ((int)moons[3].Item1.Z, (int)moons[3].Item2.Z),
            });

            //for (int c = 0; c < 1000; c++) //Part 1
            while(!fX || !fY || !fZ) //Part 2
            {
                for (int m = 0; m < 4; m++)
                {
                    var currMoon = moons[m];
                    for (int i = m + 1; i < 4; i++)
                    {
                        var targetMoon = moons[i];
                        if (!fX)
                            //X
                            if (currMoon.Item1.X < targetMoon.Item1.X)
                            {
                                currMoon.Item2.X += 1;
                                targetMoon.Item2.X -= 1;
                            }
                            else if (currMoon.Item1.X > targetMoon.Item1.X)
                            {
                                currMoon.Item2.X -= 1;
                                targetMoon.Item2.X += 1;
                            }

                        if (!fY)
                            //Y
                            if (currMoon.Item1.Y < targetMoon.Item1.Y)
                            {
                                currMoon.Item2.Y += 1;
                                targetMoon.Item2.Y -= 1;
                            }
                            else if (currMoon.Item1.Y > targetMoon.Item1.Y)
                            {
                                currMoon.Item2.Y -= 1;
                                targetMoon.Item2.Y += 1;
                            }

                        if (!fZ)
                            //Z
                            if (currMoon.Item1.Z < targetMoon.Item1.Z)
                            {
                                currMoon.Item2.Z += 1;
                                targetMoon.Item2.Z -= 1;
                            }
                            else if (currMoon.Item1.Z > targetMoon.Item1.Z)
                            {
                                currMoon.Item2.Z -= 1;
                                targetMoon.Item2.Z += 1;
                            }
                        moons[i] = targetMoon;
                    }
                    moons[m] = currMoon;
                }
                (int, int)[] newStateX = new (int, int)[4];
                (int, int)[] newStateY = new (int, int)[4];
                (int, int)[] newStateZ = new (int, int)[4];

                for (int i = 0; i < 4; i++)
                {
                    moons[i].Item1 += moons[i].Item2;
                    newStateX[i] = ((int)moons[i].Item1.X, (int)moons[i].Item2.X);
                    newStateY[i] = ((int)moons[i].Item1.Y, (int)moons[i].Item2.Y);
                    newStateZ[i] = ((int)moons[i].Item1.Z, (int)moons[i].Item2.Z);
                }

                if (containsState(listX, newStateX))
                {
                    fX = true;
                }
                else listX.Add(newStateX);

                if (containsState(listY, newStateY))
                {
                    fY = true;
                }
                else listY.Add(newStateY);
                
                if (containsState(listZ, newStateZ))
                {
                    fZ = true;
                }
                else listZ.Add(newStateZ);

                
            }
            Console.WriteLine("Part1: " + calcTotalEnergy(moons));

            Console.WriteLine("Part2: " + findCommon(listX.Count, listY.Count, listZ.Count));
        }

        long findCommon(int x, int y, int z)
        {
            var max = Math.Max(x, Math.Max(y, z));
            long cur = max;
            while(cur % x != 0 || cur % y != 0 || cur % z != 0)
            {
                cur += max;
            }
            return cur;
        }

        bool containsState(List<(int, int)[]> list, (int, int)[] find)
        {
            for (int i = 0; i < list.Count; i++)
            {
                bool has = true;
                
                if (list[i][0] != find[0])
                {
                    has = false;
                    break;
                }
                if (list[i][1] != find[1])
                {
                    has = false;
                    break;
                }
                if (list[i][2] != find[2])
                {
                    has = false;
                    break;
                }
                if (list[i][3] != find[3])
                {
                    has = false;
                    break;
                }
                return has;
            }
            return false;
        }

        int calcTotalEnergy((Vector3, Vector3)[] moons)
        {
            int sum = 0;
            foreach (var m in moons)
            {
                sum += VectorSum(Vector3.Abs(m.Item1)) * VectorSum(Vector3.Abs(m.Item2));
            }
            return sum;
        }

        int VectorSum(Vector3 vec)
        {
            return (int)vec.X + (int)vec.Y + (int)vec.Z;
        }
    }

    class Program
    {
        static void Main()
        {
            var moons = File.ReadAllLines("INPUT.txt");
            new MoonTracking().Start(moons);
        }
    }
}
