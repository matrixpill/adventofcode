﻿using System;
using System.Collections.Generic;

namespace day4
{
    class Program
    {
        static void Main()
        {
            const int START = 109165;
            const int END = 576723;
            List<int> possiblePwds = new List<int>();

            for (int i = START; i <= END; i++)
            {
                var doubleDigits = false;
                var decrease = false;
                var cnt = 1;
                char last = i.ToString()[0];
                if (i == 111222) ;
                for (int l = 1; l < i.ToString().Length; l++)
                {

                    char c = i.ToString()[l];
                    if (c < last)
                    {
                        decrease = true;
                        break;
                    }

                    if (last == c)
                    {
                        cnt++;
                        if (l == i.ToString().Length - 1 && cnt == 2)
                            doubleDigits = true;

                    }
                    else
                    {
                        if(cnt == 2)
                        {
                            doubleDigits = true;
                        }
                        cnt = 1;
                    }


                    last = c;

                }
                if (!decrease && doubleDigits)
                {
                    possiblePwds.Add(i);
                }
            }

            Console.WriteLine(possiblePwds.Count);
        }
    }
}
