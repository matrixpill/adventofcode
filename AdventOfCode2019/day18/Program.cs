﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;

namespace day18
{
    class Node
    {
        public Vector2 Pos { get; set; }
        public int F { get; set; }
    }

    class AStar
    {
        //https://de.wikipedia.org/wiki/A*-Algorithmus
        List<Node> openList = new List<Node>();
        HashSet<Node> closedList = new HashSet<Node>();
        public void AStarStart(Vector2 startNode, Vector2 endNode)
        {
            openList.Clear();
            closedList.Clear();

            addToOpenList(startNode, 0);
            Node currentNode = null;

            do
            {
                currentNode = openList[0];
                openList.RemoveAt(0);
                if (currentNode.Pos == endNode)
                {
                    //Found It
                    return;
                }
                closedList.Add(currentNode);
                expandNode(currentNode);

            } while (openList.Count > 0);

            //no Path Found

        }

        void expandNode(Node currentNode)
        {
            Node successor = null;
            for (int d = 0; d < 4; d++)
            {
                successor = getNextNode(d);
                if (listContainsNode(closedList, successor.Pos))
                {
                    continue;
                }



            }
        }

        bool listContainsNode(IEnumerable<Node> list, Vector2 current)
        {
            foreach (var item in list)
            {
                if (item.Pos == current)
                {
                    return true;
                }
            }
            return false;
        }

        Node getNextNode(int d)
        {
            //TODO: get next Node
            switch (d)
            {
                //UP
                case 0:
                    break;
                //RIGHT
                case 1:
                    break;
                //DOWN
                case 2:
                    break;
                //LEFT
                case 3:
                    break;
            }
            return new Node();
        }

        void addToOpenList(Vector2 pos, int f)
        {
            for (int i = 0; i < openList.Count; i++)
            {
                if (openList[i].F > f)
                {
                    openList.Insert(i, new Node()
                    {
                        Pos = pos,
                        F = f
                    });
                    return;
                }
            }
            openList.Add(new Node()
            {
                Pos = pos,
                F = f
            });
        }

    }

    class Vault
    {
        char[,] ReadArea()
        {
            var fieldLines = File.ReadAllLines("INPUT.txt");

            var height = fieldLines.Length;
            var width = fieldLines[0].Length;

            var area = new char[width, height];

            var y = 0;
            foreach (var line in fieldLines)
            {
                var x = 0;
                foreach (var c in line)
                {
                    area[x, y] = c;

                    x++;
                }
                y++;
            }
            return area;
        }


        public void Start()
        {
            //var area = ReadArea();
            AStar a = new AStar();
            a.AStarStart(new Vector2(), new Vector2());

        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            new Vault().Start();
        }
    }
}
