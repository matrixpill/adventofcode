﻿using System;
using System.Collections.Generic;
using System.IO;
namespace day3
{
    class YOO
    {
        static void Main123(string[] args)
        {
            List<CornerNode> intersections = new List<CornerNode>();
            var paths = File.ReadAllLines("pathsTest.txt");
            var cableA = new Cable(paths[0]);
            var cableB = new Cable(paths[1]);
            cableA.findIntersectionsWith(cableB, (posX, posY) => intersections.Add(new CornerNode(posX, posY)));

            Console.WriteLine(GetClosest(intersections));
            Console.WriteLine(GetFastest(intersections, cableA, cableB));
        }

        static int GetClosest(List<CornerNode> intersections)
        {
            int closest = int.MaxValue;
            foreach (CornerNode node in intersections)
            {
                if (Math.Abs(node.PosX) + Math.Abs(node.PosY) < closest)
                    closest = Math.Abs(node.PosX) + Math.Abs(node.PosY);
            }
            return closest;
        }

        class pos
        {
            public int x { get; set; }
            public int y { get; set; }
        }

        static void buildField(CornerNode cn, Dictionary<int, Dictionary<int, string>> path)
        {
            int from = cn.PosX, to = cn.nextNode.PosX;

            if (from > to)
            {
                var tmp = from;
                from = to - 1;
                to = tmp + 1;
            }
            for (int i = from + 1; i <= to; i++)
            {
                if (!path.ContainsKey(i))
                {
                    path.Add(i, new Dictionary<int, string>());
                }
                path[i][cn.PosY] = "";
                if (!path[i][cn.PosY].Contains("a"))
                {
                    path[i][cn.PosY] += "a";
                }
                //Console.Write(i + " ");
            }

            int fromY = cn.PosY, toY = cn.nextNode.PosY;

            if (fromY > toY)
            {
                var tmp = fromY;
                fromY = toY - 1;
                toY = tmp + 1;
            }

            for (int i = fromY + 1; i <= toY; i++)
            {
                if (!path[cn.PosX].ContainsKey(i))
                {
                    path[cn.PosX].Add(i, "");
                    //path[cn.PosX][i] = "";
                }

                if (!path[cn.PosX][i].Contains("a"))
                {
                    path[cn.PosX][i] += "a";
                }
                //Console.Write(i + " ");
            }
        }

        static int GetFastest(List<CornerNode> intersections, Cable cableA, Cable cableB)
        {
            Dictionary<int, Dictionary<int, string>> path = new Dictionary<int, Dictionary<int, string>>();
            cableA.rootNode.GoThrough((cn) =>
            {
                buildField(cn, path);

            });

            cableB.rootNode.GoThrough((cn) =>
            {
                buildField(cn, path);

            });
            ;
            //foreach (CornerNode intersection in intersections)
            //{

                
            //    ;
            //}
            return 0;
        }

        //private void addIntersect(int posX, int posY)
        //{
        //    intersections.Add(new CornerNode(posX, posY));
        //}
    }

    public class Cable
    {
        public CornerNode rootNode { get; private set; }
        //List<CornerNode> intersections = new List<CornerNode>();

        public Cable(string input)
        {
            rootNode = new CornerNode(0, 0);
            build(input.Split(','));
        }

        public void findIntersectionsWith(Cable otherCable, Action<int, int> addIntersect)
        {
            otherCable.WUT((x) => rootNode.TestWithThisCorner(x, addIntersect));
        }

        private void WUT(Action<CornerNode> p)
        {
            rootNode.GoThrough(p);
        }


        private void build(string[] directions)
        {
            for (int i = 0; i < directions.Length; i++)
            {
                addCable(directions[i]);
            }
        }

        private void addCable(string dir)
        {
            rootNode.addNode(dir[0], int.Parse(dir.Substring(1)));
        }
    }

    public enum Orientation
    {
        HORIZONTAL,
        VERTICAL
    }
    public class CornerNode
    {
        public Orientation Orientation { get; private set; }
        public int PosX { get; private set; }
        public int PosY { get; private set; }

        public CornerNode nextNode;
        public CornerNode(int x, int y)
        {
            PosX = x;
            PosY = y;
        }

        public void addNode(char dir, int length)
        {
            if (nextNode == null)
            {
                CornerNode newNode;
                switch (dir)
                {
                    case 'U':
                        Orientation = Orientation.VERTICAL;
                        newNode = new CornerNode(PosX, PosY + length);

                        break;
                    case 'R':
                        Orientation = Orientation.HORIZONTAL;
                        newNode = new CornerNode(PosX + length, PosY);
                        break;
                    case 'D':
                        Orientation = Orientation.VERTICAL;
                        newNode = new CornerNode(PosX, PosY - length);
                        break;
                    case 'L':
                        Orientation = Orientation.HORIZONTAL;
                        newNode = new CornerNode(PosX - length, PosY);
                        break;
                    default:
                        throw new Exception("OH BOI");
                }

                nextNode = newNode;
            }
            else
            {
                nextNode.addNode(dir, length);
            }
        }

        public void GoThrough(Action<CornerNode> p)
        {
            if (nextNode != null)
            {
                p(this);
                nextNode.GoThrough(p);
            }
        }

        public bool TestWithThisCorner(CornerNode x, Action<int, int> addIntersect)
        {

            if (nextNode != null)
            {
                checkIntersection(x, addIntersect);
                nextNode.TestWithThisCorner(x, addIntersect);
            }
            return false;
        }

        public void checkIntersection(CornerNode otherNode, Action<int, int> addIntersect)
        {
            if (otherNode.Orientation == Orientation || nextNode == null)
                return;

            switch (Orientation)
            {
                case Orientation.HORIZONTAL:
                    //other.posX zwischen this.posX und next.posX 
                    if (checkBetween(PosX, nextNode.PosX, otherNode.PosX))
                        if (checkBetween(otherNode.PosY, otherNode.nextNode.PosY, PosY))
                            addIntersect(otherNode.PosX, PosY);
                    break;
                case Orientation.VERTICAL:
                    //other.posY zwischen this.posY und next.posY
                    if (checkBetween(PosY, nextNode.PosY, otherNode.PosY))
                        if (checkBetween(otherNode.PosX, otherNode.nextNode.PosX, PosX))
                            addIntersect(PosX, otherNode.PosY);
                    break;
                default:
                    throw new Exception("EYE");
            }
        }

        private bool checkBetween(int a, int b, int c)
        {
            if (a > b)
            {
                if (c < a && c > b)
                {
                    return true;
                }
            }
            else
            {
                if (c < b && c > a)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
