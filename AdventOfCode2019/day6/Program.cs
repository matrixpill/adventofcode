﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace day6
{
    class Program
    {
        static void Main()
        {
            var orbits = new List<Orbit>();
            var planets = new Dictionary<string, Planet>();
            var lines = File.ReadAllLines("INPUT.txt");
            foreach (var line in lines)
            {
                var plnts = line.Split(')');
                var trg = new Planet() { Name = plnts[0] };
                var orbtr = new Planet() { Name = plnts[1] };
                planets.TryAdd(trg.Name, trg);
                planets.TryAdd(orbtr.Name, orbtr);
                orbits.Add(new Orbit()
                {
                    Target = trg,
                    Orbiter = orbtr
                });
            }
            var start = orbits.Find(o => o.Target.Name == "COM");
            //var finish = false;
            var currentPlanet = start.Target;

            PART1(currentPlanet, orbits, 0);
            Console.WriteLine(cntP1);

            var YOU = orbits.Find(o => o.Orbiter.Name == "YOU");
            PART2(YOU.Orbiter, orbits, -0, YOU.Orbiter);

            Console.WriteLine("FINITO");
        }

        static int cntP1 = 0;
        static void PART1(Planet currentPlanet, List<Orbit> orbits, int l)
        {
            currentPlanet.CntOrbit = l;
            cntP1 += l;
            foreach (var orbit in orbits.FindAll(o => o.Target.Name == currentPlanet.Name))
            {
                var pl = orbit;
                PART1(pl.Orbiter, orbits, currentPlanet.CntOrbit + 1);
            }

        }

        static void PART2(Planet currentPlanet, List<Orbit> orbits, int depth, Planet from)
        {
            if (orbits.Exists(o => o.Orbiter.Name == "SAN" && o.Target.Name == currentPlanet.Name))
            {
                Console.WriteLine(depth - 1);
                return;
            }
            foreach (var orbit in orbits.FindAll(o => o.Target.Name == currentPlanet.Name || o.Orbiter.Name == currentPlanet.Name))
            {
                //var pl = orbit;
                Planet next;
                if (currentPlanet.Name == orbit.Target.Name)
                    next = orbit.Orbiter;
                else
                    next = orbit.Target;

                if (next.Name != from.Name)
                    PART2(next, orbits, depth + 1, currentPlanet);

            }
        }
    }

    class Orbit
    {
        public Planet Target { get; set; }
        public Planet Orbiter { get; set; }
    }

    class Planet
    {
        public int CntOrbit { get; set; }
        public string Name { get; set; }
    }
}
