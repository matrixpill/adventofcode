﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using IntCodeComputer;

namespace day15
{

    class RepairDroid
    {
        enum DIRECTS
        {
            NORTH = 1,
            SOUTH = 2,
            WEST = 3,
            EAST = 4
        }

        enum OBJECTS
        {
            WALL = 0,
            MOVED = 1,
            MOVED_AND_FOUND = 2
        }

        Computer droid;
        public void Start()
        {
            var droidProgram = Array.ConvertAll(File.ReadAllText("INPUT.txt").Split(","), s => long.Parse(s));

            droid = new Computer();
            droid.installProgram(droidProgram);

            var area = new Dictionary<Vector2, string>();
            move(area, new Vector2(0, 0), 0);

            var startLoc = new Vector2(-14, 15);
            List<Vector2> spreadingOxigens = new List<Vector2>();
            spreadingOxigens.Add(startLoc);
            area.Remove(startLoc);


            var keys = area.Keys;
            foreach (var key in keys)
            {
                if (area[key] == "#")
                    area.Remove(key);
            }
            int minutes = 0;

            while (spreadingOxigens.Count > 0)
            {
                minutes++;
                var tmp = spreadingOxigens.Count;

                for (int i = 0; i < tmp; i++)
                {

                    var curr = spreadingOxigens[i];
                    for (int d = 1; d <= 4; d++)
                    {
                        if (area.ContainsKey(vectorInDirection(curr, d)))
                        {
                            spreadingOxigens.Add(vectorInDirection(curr, d));
                            area.Remove(vectorInDirection(curr, d));
                        }
                    }
                }

                spreadingOxigens.RemoveRange(0, tmp);
            }
            Console.WriteLine("Minutes: " + minutes);
        }


        void move(Dictionary<Vector2, string> area, Vector2 position, int steps)
        {
            if (!area.ContainsKey(position))
                area.Add(position, ".");
            for (int d = 1; d <= 4; d++)
            {
                if (area.ContainsKey(vectorInDirection(position, d)))
                {
                    continue;
                }

                var res = (OBJECTS)droid.run(true, para: new long[] { d }).Item2;

                switch (res)
                {
                    case OBJECTS.WALL:
                        if (!area.ContainsKey(vectorInDirection(position, d)))
                            area.Add(vectorInDirection(position, d), "#");
                        break;
                    case OBJECTS.MOVED:

                        move(area, vectorInDirection(position, d), steps + 1);

                        droid.run(true, para: new long[] { opposide(d) }); //back
                        break;
                    case OBJECTS.MOVED_AND_FOUND:
                        Console.WriteLine("PART1: " + (steps + 1));
                        Console.WriteLine("At Location: " + position);
                        move(area, vectorInDirection(position, d), steps + 1);
                        droid.run(true, para: new long[] { opposide(d) });
                        break;
                    default:
                        break;
                }
            }
        }


        Vector2 vectorInDirection(Vector2 pos, int d)
        {
            var dir = (DIRECTS)d;

            switch (dir)
            {
                case DIRECTS.NORTH:
                    return new Vector2(pos.X, pos.Y + 1);
                case DIRECTS.SOUTH:
                    return new Vector2(pos.X, pos.Y - 1);
                case DIRECTS.WEST:
                    return new Vector2(pos.X - 1, pos.Y);
                case DIRECTS.EAST:
                    return new Vector2(pos.X + 1, pos.Y);
            }
            return new Vector2();
        }

        int opposide(int direct)
        {
            switch ((DIRECTS)direct)
            {
                case DIRECTS.NORTH:
                    return (int)DIRECTS.SOUTH;
                case DIRECTS.SOUTH:
                    return (int)DIRECTS.NORTH;
                case DIRECTS.WEST:
                    return (int)DIRECTS.EAST;
                case DIRECTS.EAST:
                    return (int)DIRECTS.WEST;

                default:
                    return -1;
            }
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            new RepairDroid().Start();
        }
    }
}
