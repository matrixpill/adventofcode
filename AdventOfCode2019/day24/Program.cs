﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace day24
{

    public class Bugs
    {
        const int FIELDWIDTH = 5;
        const int FIELDHEIGHT = 5;
        bool multiLayer = false;

        Dictionary<int, char[,]> state;

        Dictionary<int, char[,]> BuidStateFromInput()
        {
            char[,] nLayer = new char[FIELDWIDTH, FIELDHEIGHT];
            var y = 0;
            foreach (var line in File.ReadAllLines("INPUT.txt"))
            {
                var x = 0;
                foreach (var c in line)
                {
                    nLayer[x, y] = c;
                    x++;
                }
                y++;
            }
            var nState = new Dictionary<int, char[,]>
            {
                { 0, nLayer }
            };
            return nState;
        }


        private string LayerToBinary(int layer)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var c in state[layer])
            {
                sb.Append(c == '#' ? "1" : "0");
            }
            return sb.ToString();
        }

        char GetField(int x, int y, int layer)
        {
            if (state.ContainsKey(layer))
                if (x < FIELDWIDTH && x >= 0)
                {
                    if (y < FIELDHEIGHT && y >= 0)
                        return state[layer][x, y];
                }
            return '.';
        }

        int CountAdjacent(int x, int y, int layer)
        {
            int cnt = 0;
            if (!multiLayer)
            {
                //Top
                cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                //Right
                cnt += GetField(x + 1, y, layer) == '#' ? 1 : 0;
                //Bottom
                cnt += GetField(x, y + 1, layer) == '#' ? 1 : 0;
                //Left
                cnt += GetField(x - 1, y, layer) == '#' ? 1 : 0;
            }
            else
            {
                //Top
                if (y == 0)
                {
                    cnt += GetField(2, 1, layer - 1) == '#' ? 1 : 0;
                }
                else
                {
                    if (x == 2 && y == 3)
                    {
                        cnt += GetField(0, 4, layer + 1) == '#' ? 1 : 0;
                        cnt += GetField(1, 4, layer + 1) == '#' ? 1 : 0;
                        cnt += GetField(2, 4, layer + 1) == '#' ? 1 : 0;
                        cnt += GetField(3, 4, layer + 1) == '#' ? 1 : 0;
                        cnt += GetField(4, 4, layer + 1) == '#' ? 1 : 0;
                    }
                    else
                    {
                        cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    }
                }
                //right
                if (x == 4)
                {
                    cnt += GetField(3, 2, layer - 1) == '#' ? 1 : 0;
                }
                else
                {
                    if (x == 1 && y == 2)
                    {
                        cnt += GetField(0, 0, layer + 1) == '#' ? 1 : 0;
                        cnt += GetField(0, 1, layer + 1) == '#' ? 1 : 0;
                        cnt += GetField(0, 2, layer + 1) == '#' ? 1 : 0;
                        cnt += GetField(0, 3, layer + 1) == '#' ? 1 : 0;
                        cnt += GetField(0, 4, layer + 1) == '#' ? 1 : 0;
                    }
                    else
                    {
                        cnt += GetField(x + 1, y, layer) == '#' ? 1 : 0;

                    }
                }
                //Bottom
                if (y == 4)
                {
                    cnt += GetField(2, 3, layer - 1) == '#' ? 1 : 0;
                }
                else
                {
                    if (x == 2 && y == 1)
                    {
                        cnt += GetField(0, 0, layer + 1) == '#' ? 1 : 0;
                        cnt += GetField(1, 0, layer + 1) == '#' ? 1 : 0;
                        cnt += GetField(2, 0, layer + 1) == '#' ? 1 : 0;
                        cnt += GetField(3, 0, layer + 1) == '#' ? 1 : 0;
                        cnt += GetField(4, 0, layer + 1) == '#' ? 1 : 0;
                    }
                    else
                    {
                        cnt += GetField(x, y + 1, layer) == '#' ? 1 : 0;
                    }
                }
                //Left
                if (x == 0)
                {
                    cnt += GetField(1, 2, layer - 1) == '#' ? 1 : 0;
                }
                else
                {
                    if (x == 3 && y == 2)
                    {
                        cnt += GetField(4, 0, layer + 1) == '#' ? 1 : 0;
                        cnt += GetField(4, 1, layer + 1) == '#' ? 1 : 0;
                        cnt += GetField(4, 2, layer + 1) == '#' ? 1 : 0;
                        cnt += GetField(4, 3, layer + 1) == '#' ? 1 : 0;
                        cnt += GetField(4, 4, layer + 1) == '#' ? 1 : 0;
                    }
                    else
                    {
                        cnt += GetField(x - 1, y, layer) == '#' ? 1 : 0;
                    }
                }


                #region maybe but much work
                /*
                if (y == 0) //top Row -> get bottom center of layer -1
                {
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                }
                else if (x == 4) //right collumn --> get left center of layer -1
                {
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                }
                else if (y == 4) //Bottom Row --> get top center of layer -1
                {
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                }
                else if (x == 0) //Left column --> get right center of layer -1
                {
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                }
                else if (x == 2 && y == 1) //second row center --> get top row of layer +1
                {
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                }
                else if (x == 3 && y == 2) //secend to last column --> get left column of layer +1
                {
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                }
                else if (x == 2 && y == 3) //secend to last row --> get bottom row of layer +1
                {
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                }
                else if (x == 1 && y == 2) //second column center --> get right column of layer +1
                {
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                }
                else
                {
                    //Top
                    cnt += GetField(x, y - 1, layer) == '#' ? 1 : 0;
                    //Right
                    cnt += GetField(x + 1, y, layer) == '#' ? 1 : 0;
                    //Bottom
                    cnt += GetField(x, y + 1, layer) == '#' ? 1 : 0;
                    //Left
                    cnt += GetField(x - 1, y, layer) == '#' ? 1 : 0;
                }
                */
                #endregion 

            }
            return cnt;
        }

        char[,] EmptyLayer()
        {
            var layer = new char[FIELDWIDTH, FIELDHEIGHT];
            for (int y = 0; y < FIELDHEIGHT; y++)
            {
                for (int x = 0; x < FIELDWIDTH; x++)
                {
                    layer[x, y] = '.';
                }
            }
            return layer;
        }
        public long GetBiodiversity(string layerBinary)
        {
            long rating = 0;
            for (int i = 0; i < layerBinary.Length; i++)
            {
                long b = (long)char.GetNumericValue(layerBinary[i]);
                long pow = (long)Math.Pow(2, i);
                rating += b * pow;
            }
            return rating;
        }


        public void Start2()
        {
            multiLayer = true;
            state = BuidStateFromInput();

            for (int i = -101; i < 101; i++)
            {
                if (i != 0)
                    state.Add(i, EmptyLayer());
            }
            var nextState = new Dictionary<int, char[,]>();

            for (int i = 0; i < 200; i++)
            {
                nextState.Clear();

                foreach (var layer in state)
                {
                    var nextLayer = new char[FIELDWIDTH, FIELDHEIGHT];
                    for (int y = 0; y < FIELDHEIGHT; y++)
                    {
                        for (int x = 0; x < FIELDWIDTH; x++)
                        {
                            if (x == 2 && y == 2)
                                continue;
                            var cntAdj = CountAdjacent(x, y, layer.Key);
                            var current = GetField(x, y, layer.Key);
                            switch (current)
                            {
                                case '#':
                                    if (cntAdj != 1)
                                        current = '.';
                                    break;
                                case '.':
                                    if (cntAdj == 1 || cntAdj == 2)
                                        current = '#';
                                    break;
                            }
                            nextLayer[x, y] = current;
                        }
                    }
                    nextState.Add(layer.Key, nextLayer);
                }
                foreach (var l in nextState)
                {
                    state[l.Key] = (char[,])l.Value.Clone();
                }
            }
            var cnt = 0;
            foreach (var l in state)
            {
                foreach (var f in l.Value)
                {
                    if (f == '#')
                        cnt++;
                }
            }
            Console.WriteLine("Part2: " + cnt);
        }

        public void Start()
        {
            multiLayer = false;
            state = BuidStateFromInput();
            List<string> prevLayerBinarys = new List<string>
            {
                LayerToBinary(0)
            };
            var nextLayer = new char[FIELDWIDTH, FIELDHEIGHT];
            StringBuilder nextBinary = new StringBuilder();
            while (true)
            {
                nextBinary.Clear();

                for (int y = 0; y < FIELDHEIGHT; y++)
                {
                    for (int x = 0; x < FIELDWIDTH; x++)
                    {
                        var cntAdj = CountAdjacent(x, y, 0);
                        var current = GetField(x, y, 0);
                        switch (current)
                        {
                            case '#':
                                if (cntAdj != 1)
                                    current = '.';
                                break;
                            case '.':
                                if (cntAdj == 1 || cntAdj == 2)
                                    current = '#';
                                break;
                        }

                        if (current == '#')
                            nextBinary.Append('1');
                        else
                            nextBinary.Append('0');

                        nextLayer[x, y] = current;
                    }
                }
                if (prevLayerBinarys.Contains(nextBinary.ToString()))
                    break;
                prevLayerBinarys.Add(nextBinary.ToString());
                state[0] = (char[,])nextLayer.Clone();
            }

            Console.WriteLine(GetBiodiversity(nextBinary.ToString()));
        }
    }


    class Program
    {
        static void Main()
        {
            new Bugs().Start2();
        }
    }
}
