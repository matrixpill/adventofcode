﻿using System;
using System.Collections.Generic;
using System.IO;
using IntCodeComputer;

namespace day11
{
    class Program
    {
        static void Main()
        {
            string inputText = File.ReadAllText("INPUT.txt");
            string[] splittedText = inputText.Split(',');
            long[] baseprogram = Array.ConvertAll(splittedText, (string x) => long.Parse(x));
            var cpu = new Computer();
            cpu.installProgram(baseprogram);

            Dictionary<(int, int), int> steps = new Dictionary<(int, int), int>();

            int x = 0, y = 0, xmax = 0, xmin = 0, ymax = 0, ymin = 0;
            var o = new Orientation(0);

            while (true)
            {
                int color = 1;
                if (steps.ContainsKey((x, y)))
                    color = steps[(x, y)];
                var res = cpu.run(true, para: (long)color);

                if (res.Item1 == STATE.FIN)
                    break;
                //Feld malen
                if (steps.ContainsKey((x, y)))
                    steps[(x, y)] = (int)res.Item2;
                else
                    steps.Add((x, y), (int)res.Item2);

                res = cpu.run(true);
                //Console.WriteLine("OUT2: " + res.Item2);
                //bewegen
                if ((int)res.Item2 == 1)
                {
                    //rechts
                    o.turnRight();
                }
                else
                {
                    //links
                    o.turnLeft();
                }
                switch (o.CurrOrientation)
                {
                    //up
                    case 0:
                        y--;
                        if (y < ymin) ymin = y;
                        break;
                    //right
                    case 1:
                        x++;
                        if (x > xmax) xmax = x;
                        break;
                    //down
                    case 2:
                        y++;
                        if (y > ymax) ymax = y;
                        break;
                    //left
                    case 3:
                        x--;
                        if (x < xmin) xmin = x;
                        break;
                }
            }

            Console.WriteLine($"{steps.Count}");
            for (int yi = 0; yi <= ymax; yi++)
            {
                for (int xi = 0; xi <= xmax; xi++)
                {
                    steps.TryGetValue((xi, yi), out int col);
                    Console.Write(col == 1 ? col.ToString() : "_");
                }
                Console.WriteLine();
            }
        }

        class Orientation
        {
            public int CurrOrientation { get; private set; }
            public Orientation(int startOrient)
            {
                CurrOrientation = startOrient;
            }
            //up right down left
            //int[] orientations = new int[] { 0, 1, 2, 3 };

            public int turnRight()
            {
                CurrOrientation += 1;
                if (CurrOrientation == 4)
                    CurrOrientation = 0;
                return CurrOrientation;
            }
            public int turnLeft()
            {
                CurrOrientation -= 1;
                if (CurrOrientation == -1)
                    CurrOrientation = 3;
                return CurrOrientation;
            }
        }
    }

    /*enum STATE
    {
        NEXT,
        OUTNEXT,
        INNEXT,
        FIN,
        WUT
    }
    class Computer
    {
        int timesCalled = 0;
        long[] program;
        long curPos = 0;
        long lastOut = 0;
        long offset = 0;

        public void installProgram(long[] prg)
        {
            curPos = 0;
            program = prg;
        }
        (STATE, long) res = (STATE.NEXT, 0);

        
        public (STATE, long) run(bool runUntilOUT = false, params long[] para)
        {
            do
            {
                res = performInstrunction(new long[] { tryGet(curPos), tryGet(curPos + 1), tryGet(curPos + 2), tryGet(curPos + 3) }, para);
                if (runUntilOUT && res.Item1 == STATE.OUTNEXT)
                    break;
                    

            } while (res.Item1 != STATE.WUT && res.Item1 != STATE.FIN);


            return res;
        }

        private long tryGet(long pos)
        {
            if (pos >= program.Length)
            {
                return 0;
            }
            else
            {
                return readFromProgram(pos);
            }
        }

        private (STATE, long) performInstrunction(long[] instruction, params long[] para)
        {
            var tmpInst = new long[instruction.Length];
            instruction.CopyTo(tmpInst, 0);

            analyse(instruction);
            (STATE, long) res;
            switch (instruction[0])
            {
                case 1:
                    writeToProgram(instruction[3], add(instruction[1], instruction[2]));
                    curPos += 4;
                    res = (STATE.NEXT, 0);
                    break;

                case 2:
                    writeToProgram(instruction[3], mult(instruction[1], instruction[2]));
                    curPos += 4;
                    res = (STATE.NEXT, 0);
                    break;

                case 3:
                    writeToProgram(instruction[1], input());
                    curPos += 2;
                    res = (STATE.INNEXT, 0);
                    break;

                case 4:
                    output(instruction[1]);
                    curPos += 2;
                    res = (STATE.OUTNEXT, instruction[1]);
                    break;

                case 5:
                    //jmp if true
                    if (instruction[1] != 0)
                        curPos = instruction[2];
                    else
                        curPos += 3;
                    res = (STATE.NEXT, 0);
                    break;

                case 6:
                    //jmp if false
                    if (instruction[1] == 0)
                        curPos = instruction[2];
                    else
                        curPos += 3;
                    res = (STATE.NEXT, 0);
                    break;
                case 7:
                    //less than
                    if (instruction[1] < instruction[2])
                        writeToProgram(instruction[3], 1);
                    else
                        writeToProgram(instruction[3], 0);
                    curPos += 4;
                    res = (STATE.NEXT, 0);
                    break;
                case 8:
                    //equals
                    if (instruction[1] == instruction[2])
                        writeToProgram(instruction[3], 1);
                    else
                        writeToProgram(instruction[3], 0);
                    curPos += 4;
                    res = (STATE.NEXT, 0);
                    break;

                case 9:
                    offset += instruction[1];
                    curPos += 2;
                    res = (STATE.NEXT, 0);
                    break;

                case 99:
                    Console.WriteLine("ENDE");
                    res = (STATE.FIN, lastOut);
                    break;
                default:
                    Console.WriteLine("WUT");
                    res = (STATE.WUT, 0);
                    break;
            }
            return res;
        }

        private void writeToProgram(long pos, long value)
        {
            checkPrgLength(pos);
            program[pos] = value;
        }
        private long readFromProgram(long pos)
        {
            checkPrgLength(pos);
            return program[pos];
        }

        private long modeSwitch(char mode, long readfrom)
        {
            long value = 0;
            switch (mode)
            {
                case '0':
                    value = readFromProgram(readfrom);
                    break;
                case '1':
                    value = readfrom;
                    break;
                case '2':
                    value = readFromProgram(offset + readfrom);

                    break;
            }

            return value;
        }

        private void checkPrgLength(long where)
        {
            if (where > program.Length - 1)
            {
                var tmpPrg = new long[where + 1];
                program.CopyTo(tmpPrg, 0);
                program = tmpPrg;
            }
        }

        private void analyse(long[] instruction)
        {
            var opt = instruction[0] % 100;
            long ABC = (instruction[0] - opt) / 100;

            var modes = ABC.ToString().PadLeft(3, '0');

            instruction[0] = opt;
            switch (instruction[0])
            {
                case 1:
                case 2:
                case 5:
                case 6:
                case 7:
                case 8:
                    instruction[3] = modes[0] == '2' ? offset + instruction[3] : instruction[3];
                    instruction[2] = modeSwitch(modes[1], instruction[2]);
                    // THIS IS FINE 🔥
                    goto case 4;
                case 4:
                case 9:
                    instruction[1] = modeSwitch(modes[2], instruction[1]);
                    break;
                case 3:
                    if (modes[2] == '2')
                    {
                        instruction[1] = offset + instruction[1];
                    }
                    break;
                default: break;
            }
        }

        private long add(long posA, long posB)
        {
            return posA + posB;
        }
        private long mult(long posA, long posB)
        {
            return posA * posB;
        }

        private long input(params long[] para)
        {
            long input;
            if (para.Length == 0)
            {
                Console.Write("Please Input something: ");
                input = long.Parse(Console.ReadLine());
            } 
            else
            {
                input = para[0];
            }
            return input;
        }
        private void output(long output)
        {

            Console.WriteLine("OUT: " + output);
        }
    }*/
}
