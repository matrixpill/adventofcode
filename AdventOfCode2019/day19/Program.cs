﻿using System;
using System.Drawing;
using System.IO;
using System.Numerics;
using System.Text;
using IntCodeComputer;

namespace day19
{
    class TractorBeam
    {
        public void Start()
        {
            var prg = Array.ConvertAll(File.ReadAllText("INPUT.txt").Split(","), s => long.Parse(s));
            var drone = new Computer();

            part2(drone, prg);
        }

        void part1(Computer drone, long[] prg)
        {
            var points = 0;
            for (int y = 0; y < 50; y++)
            {
                for (int x = 0; x < 50; x++)
                {
                    drone.installProgram((long[])prg.Clone());

                    var res = drone.run(false, true, x, y);
                    Console.Write(res.Item2 == 1 ? "#" : ".");
                    if (res.Item2 == 1)
                        points++;
                }
                Console.WriteLine();
            }
            Console.WriteLine(points);
        }

        void part2(Computer drone, long[] prg)
        {
            long tPosX = 3, tPosY = 7;
            var fitting = false;
            while (!fitting)
            {
                drone.installProgram((long[])prg.Clone());
                //top right
                if (drone.run(false, true, tPosX, tPosY).Item2 == 1)
                {
                    if (tPosX - 99 > 0)
                    {
                        //top Left
                        drone.installProgram((long[])prg.Clone());

                        if (drone.run(false, true, tPosX - 99, tPosY).Item2 == 1)
                        {
                            drone.installProgram((long[])prg.Clone());
                            //bottom Left
                            if (drone.run(false, true, tPosX - 99, tPosY + 99).Item2 == 1)
                            {
                                fitting = true;
                                Console.WriteLine($"{tPosX - 99}, {tPosY}");
                                break;
                            }
                        }
                    }
                    tPosX++;
                    tPosY++;
                }
                else
                    tPosY++;

            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            new TractorBeam().Start();
        }
    }
}
