﻿using System;
using System.Collections.Generic;
using System.IO;
using IntCodeComputer;

namespace day25
{
    class Cryostasis
    {
        public void Start(long[] prg)
        {
            var cpu = new Computer();
            cpu.installProgram(prg);
            var inputQue = new List<long>();

            Func<Computer, long> input = new Func<Computer, long>(x =>
            {
                long inp = 10;
                if (inputQue.Count == 0)
                {
                    var readInput = Console.ReadLine();
                    foreach (var c in readInput)
                        inputQue.Add(c);
                    inputQue.Add(10);
                }

                inp = inputQue[0];
                inputQue.RemoveAt(0);



                return inp;
            });
            Action<long, Computer> print = new Action<long, Computer>((x, c) => Console.Write((char)x));

            cpu.SetINOUT(print, input);
            cpu.RunWhileSTATENot(x => true);
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            var prg = Array.ConvertAll(File.ReadAllText("INPUT.txt").Split(","), x => long.Parse(x));
            while (true)
                new Cryostasis().Start((long[])prg.Clone());
        }
    }
}
