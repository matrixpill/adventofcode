﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;

namespace day10
{
    class Program
    {

        
        static void Main(string[] args)
        {
            var astroidMap = new AstroidMap();
            astroidMap.Start();

        }


    }

    class AstroidMap
    {
        int maxX;
        int maxY;
        Dictionary<(int, int), List<(int, int)>> astroids;//= new Dictionary<(int, int), List<(int, int)>>();
        List<(int, int)> astroKeys;
        public void Start()
        {
            var input = File.ReadAllLines("INPUT.txt");
            astroids = new Dictionary<(int, int), List<(int, int)>>();

            maxX = input[0].Length;
            maxY = input.Length;

            for (int y = 0; y < input.Length; y++)
            {
                for (int x = 0; x < input[y].Length; x++)
                {
                    if (input[y][x] == '#')
                    {
                        astroids.Add((x, y), null);
                    }
                }
            }
            astroKeys = new List<(int, int)>(astroids.Keys);

            foreach (var key in astroKeys)
            {
                testAstroids(key);
            }

            var best = 0;
            (int, int) von = (0, 0);
            foreach (var a in astroids)
            {
                if (a.Value.Count > best)
                {
                    best = a.Value.Count;
                    von = a.Key;
                }
            }
            Console.WriteLine("Part 1: " + best + " von: " + von);
            var testAst = von;

            var cnt = 0;
            while (cnt < 200)
            {
                var currentSeen = new List<((int, int), double)>();
                for (int i = 0; i < astroids[testAst].Count; i++)
                {
                    var target = astroids[testAst][i];
                    currentSeen.Add((target, tan(testAst, target))); 
                }
                currentSeen = currentSeen.OrderBy(x => x.Item2).ToList();

                var startIndex = 0;
                var first = currentSeen.Find(x => x.Item2 >= -90);
                startIndex = currentSeen.IndexOf(first);
                for (int i = 0; i < currentSeen.Count; i++)
                {
                    startIndex = startIndex % (currentSeen.Count);
                    astroids.Remove(currentSeen[startIndex].Item1);
                    cnt++;
                    if (cnt == 200) Console.WriteLine("The 200th -->" + currentSeen[startIndex].Item1);
                    startIndex++;
                }

                testAstroids(testAst);
            }

        }

        private void testAstroids((int, int) key)
        {
            astroids[key] = new List<(int, int)>(astroids.Keys);
            astroids[key].Remove(key);
            var hidden = new List<(int, int)>();
            foreach (var otherKey in astroKeys)
            {
                if (otherKey != key)
                {
                    var delta = subKeys(otherKey, key);
                    var step = findSmallestStep(delta);

                    var next = addKeys(otherKey, step);
                    while (inRange(next, (maxX, maxY)))
                    {
                        if (astroids.ContainsKey(next) && !hidden.Contains(next))
                        {
                            hidden.Add(next);
                            astroids[key].Remove(next);
                        }
                        next = addKeys(next, step);
                    }
                }
            }
        }

        private double RadianToDegree(double angle)
        {
            return angle * (180.0 / Math.PI);
        }
        private double tan((int, int) start, (int, int) target)
        {
            return RadianToDegree(Math.Atan2(target.Item2 - start.Item2, target.Item1 - start.Item1));
        }

        private (int, int) findSmallestStep((int, int) delta)
        {
            int x = Math.Abs(delta.Item1), y = Math.Abs(delta.Item2);
            if (x == 0)
                return (0, delta.Item2 / y);
            if (y == 0)
                return (delta.Item1 / x, 0);
            if (x == y) return (delta.Item1 / x, delta.Item2 / y);

            var divisors = new List<int>();
            var smallest = x > y ? y : x;

            for (int i = 1; i <= smallest; i++)
            {
                if (x % i == 0 && y % i == 0)
                {
                    divisors.Add(i);
                }
            }
            divisors.Sort();

            var greatest = divisors.Count > 0 ? divisors[divisors.Count - 1] : 1;
            return (delta.Item1 / greatest, delta.Item2 / greatest);
        }

        private (int, int) subKeys((int, int) o, (int, int) t)
        {
            return (o.Item1 - t.Item1, o.Item2 - t.Item2);
        }

        private (int, int) addKeys((int, int) o, (int, int) t)
        {
            return (o.Item1 + t.Item1, o.Item2 + t.Item2);
        }

        private bool inRange((int, int) a, (int, int) target)
        {
            int aX = a.Item1, aY = a.Item2;
            int tX = target.Item1, tY = target.Item2;
            return (aX >= 0 && aX < tX) && (aY >= 0 && aY < tY);

        }
    }
}
