﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using System.Text;
using System.Threading;
using IntCodeComputer;

namespace day17
{
    class ASCII
    {
        List<Vector2> scaffolding = new List<Vector2>();

        Vector2 printPosition = new Vector2();

        enum DIRECT
        {
            UP,
            RIGHT,
            DOWN,
            LEFT
        }

        public void Start()
        {
            var prg = Array.ConvertAll(File.ReadAllText("INPUT.txt").Split(","), s => long.Parse(s));
            var asci = new Computer();

            prg[0] = 2;
            asci.installProgram(prg);
            //Part1(asci);
            Part2(asci);
        }

        void Part1(Computer asci)
        {
            var state = STATE.NEXT;
            while (state != STATE.FIN)
            {
                var res = asci.run(true);
                state = res.Item1;
                print(res.Item2);
            }
            long sum = 0;
            foreach (var scaff in scaffolding)
            {
                var isIntersection = true;
                for (int d = 0; d < 4; d++)
                {
                    if (!scaffolding.Contains(vectorInDirection(scaff, d)))
                    {
                        isIntersection = false;
                        break;
                    }
                }
                if (isIntersection)
                {
                    sum += (int)scaff.X * (int)scaff.Y;
                }
            }
            Console.WriteLine("Part1: " + sum);

        }

        void Part2(Computer asci)
        {
            char[] MAIN = "A,B,A,B,A,C,A,C,B,C\n".ToCharArray();
            char[] A = "R,6,L,10,R,10,R,10\n".ToCharArray();
            char[] B = "L,10,L,12,R,10\n".ToCharArray();
            char[] C = "R,6,L,12,L,10\n".ToCharArray();
            char YoN = 'y';
            char[] YN = new char[] { YoN, '\n' };
            var combined = new char[MAIN.Length + A.Length + B.Length + C.Length + 2];
            MAIN.CopyTo(combined, 0);
            A.CopyTo(combined, MAIN.Length);
            B.CopyTo(combined, MAIN.Length + A.Length);
            C.CopyTo(combined, MAIN.Length + A.Length + B.Length);

            combined[combined.Length - 2] = YoN;
            combined[combined.Length - 1] = '\n';


            var result = (long)0;
            long lastOut = (char)'r';
            StringBuilder sb = new StringBuilder();
            while (true)
            {
                var res = asci.run(true, false, Array.ConvertAll(combined, x => (long)x));
                result = res.Item2;
                if (res.Item1 == STATE.FIN) break;
                print(result, sb);
                if (lastOut == '\n' && result == '\n')
                {
                    //Console.Clear();
                    Console.SetCursorPosition(0, 0);
                    Console.WriteLine(sb.ToString());
                    //Thread.Sleep(50);
                    sb.Clear();
                }
                lastOut = result;
            }

            Console.WriteLine("Part2: " + result);
        }

        Vector2 vectorInDirection(Vector2 pos, int dir)
        {
            switch ((DIRECT)dir)
            {
                case DIRECT.UP:
                    return new Vector2(pos.X, pos.Y - 1);
                case DIRECT.RIGHT:
                    return new Vector2(pos.X + 1, pos.Y);
                case DIRECT.DOWN:
                    return new Vector2(pos.X, pos.Y + 1);
                case DIRECT.LEFT:
                    return new Vector2(pos.X - 1, pos.Y);
                default:
                    throw new Exception("Unknown Direction");
            }
        }

        void print(long code, StringBuilder sb = null)
        {
            if (sb != null)
                sb.Append((char)code);
            else
                Console.Write((char)code);

            switch (code)
            {
                case 35:
                    //Console.Write("#");
                    scaffolding.Add(printPosition);
                    printPosition.X++;
                    break;
                case 46:
                    //Console.Write(".");
                    printPosition.X++;
                    break;
                case 10:
                    //Console.WriteLine();
                    printPosition.X = 0;
                    printPosition.Y++;
                    break;
                default:
                    //Console.Write((char)code);
                    printPosition.X++;
                    break;
            }
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            new ASCII().Start();
        }
    }
}
