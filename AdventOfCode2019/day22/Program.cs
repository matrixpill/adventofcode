﻿using System;
using System.IO;
using System.Numerics;

namespace day22
{
    class Shuffle
    {

        Instruction[] compactInput(string[] input)
        {
            var instructions = new Instruction[input.Length];
            var i = 0;

            foreach (var line in input)
            {
                instructions[i++] = Instruction.Build(line);
            }

            return instructions;
        }

        int[] createBaseArray(long max)
        {
            var array = new int[max];
            for (int i = 0; i < max; i++)
            {
                array[i] = i;
            }
            return array;
        }

        public void Part1(string[] input, long max)
        {
            var instructions = compactInput(input);
            var baseArray = createBaseArray(max);

            var tmpArray = new int[max];

            foreach (var inst in instructions)
            {
                switch (inst.Type)
                {
                    case INSTR.DWI:
                        for (int i = 0; i < max; i++)
                        {
                            tmpArray[(inst.N * i) % max] = baseArray[i];
                        }
                        tmpArray.CopyTo(baseArray, 0);
                        break;
                    case INSTR.CUT:
                        if (inst.N > 0)
                        {
                            Array.Copy(baseArray, 0, tmpArray, max - inst.N, inst.N);
                            Array.Copy(baseArray, inst.N, tmpArray, 0, max - inst.N);
                        }
                        else
                        {
                            var abs = Math.Abs(inst.N);
                            Array.Copy(baseArray, max - abs, tmpArray, 0, abs);
                            Array.Copy(baseArray, 0, tmpArray, abs, max - abs);
                        }
                        tmpArray.CopyTo(baseArray, 0);
                        break;
                    case INSTR.REVERSE: //REV
                        Array.Reverse(baseArray);
                        break;
                }
            }
            Console.WriteLine("PART1 " + Array.IndexOf(baseArray, 2019));
        }


        public void Part2(string[] input, long maxL, long finalPos, long times)
        {
            //https://www.reddit.com/r/adventofcode/comments/ee0rqi/2019_day_22_solutions/fbnkaju/
            var instructions = compactInput(input);

            BigInteger max = maxL;
            BigInteger increment_mul = 1;
            BigInteger offset_diff = 0;
             
            BigInteger inv(BigInteger n)
            {
                return BigInteger.ModPow(n, max - 2, max);
            }

            foreach (var inst in instructions)
            {
                switch (inst.Type)
                {
                    case INSTR.DWI:
                        increment_mul *= inv(inst.N);
                        increment_mul %= max;
                        break;
                    case INSTR.CUT:
                        offset_diff += inst.N * increment_mul;
                        offset_diff %= max;
                        break;
                    case INSTR.REVERSE:
                        increment_mul *= -1;
                        increment_mul %= max;

                        offset_diff += increment_mul;
                        offset_diff %= max;
                        break;
                }

            }

            (BigInteger, BigInteger) getSequence(BigInteger iterations)
            {
                var increment = BigInteger.ModPow(increment_mul, iterations, max);
                var offset = offset_diff * (1 - increment) * inv((1 - increment_mul) % max);
                offset %= max;
                return (increment, offset);
            }

            BigInteger get(BigInteger offset, BigInteger increment, BigInteger i)
            {
                return (offset + i * increment) % max;
            }

            (BigInteger inc, BigInteger off) = getSequence(times);
            Console.Write(get(off, inc, 2020));

        }

        /*public long find(long max, long currentPos, long N)
        {
            long i = 0;
            var found = false;
            long res = 0;
            while (!found)
            {
                res = (i++ * max + currentPos);
                if (res % N == 0)
                    found = true;
            }
            var y = res / N;
            return y;
        }*/
    }


    enum INSTR
    {
        DWI, //deal with increment
        CUT,
        REVERSE //deal into new stack
    }

    class Instruction
    {
        public INSTR Type { get; set; }
        public long N { get; set; }


        public static Instruction Build(string line)
        {
            var splitted = line.Split(" ");
            return (splitted[0], splitted[1]) switch
            {
                ("deal", "with") => new Instruction()
                {
                    Type = INSTR.DWI,
                    N = int.Parse(splitted[3])
                },
                ("deal", "into") => new Instruction()
                {
                    Type = INSTR.REVERSE
                },
                ("cut", _) => new Instruction()
                {
                    Type = INSTR.CUT,
                    N = int.Parse(splitted[1])
                },
                (_, _) => throw new Exception("Not valid Instruction")
            };
        }

    }


    class Program
    {
        static void Main(string[] args)
        {
            var input = File.ReadAllLines("INPUT.txt");
            var m = 119315717514047;

            var sh = new Shuffle();
            sh.Part1(input, 10007);
            sh.Part2(input, m, 2020, 101741582076661);
        }
    }
}
