package days23

import (
	"aoc/solutions/etc"
	"fmt"
	"os"
	"strings"
)

var Day20 = etc.Solution{Solve1: solve20_1}

var flipFlop = "%"
var conjunction = "&"
var broadcast = "broadcaster"
var button = "button"

type LowHigh struct {
	low  int
	high int
}

func (lh *LowHigh) Add(high bool) {
	if high {
		lh.high++
	} else {
		lh.low++
	}
}

var mainLH = LowHigh{}

func solve20_1() string {
	os.Remove("20.txt")
	var sb strings.Builder
	var modules = parseInput20()
	for i := 0; i < 1000; i++ {
		sb.WriteString(fmt.Sprintf("%v\n", (i + 1)))
		nextTargets := []Module{modules[button]}
		for len(nextTargets) != 0 {
			tmpTargets := []string{}
			for _, t := range nextTargets {
				tmpTargets = append(tmpTargets, t.Name(true))
			}
			nextTargets = []Module{}
			for _, t := range tmpTargets {
				tmpTargets := modules[t].SendPulses()
				nextTargets = append(nextTargets, tmpTargets...)
				for _, mt := range tmpTargets {
					text := fmt.Sprintf("%v -%v -> %v\n", modules[t].Name(false), modules[t].State(), modules[mt.Name(true)].Name(false))
					sb.WriteString(text)
				}
			}
			for _, t := range nextTargets {
				modules[t.Name(true)] = t
			}
		}
		sb.WriteString("\n")
	}
	os.WriteFile("20.txt", []byte(sb.String()), os.ModeAppend)

	return fmt.Sprintf("low %v, high %v mult %v", mainLH.low, mainLH.high, mainLH.low*mainLH.high)
}

func lowHigh(s bool) string {
	if s {
		return "high"
	} else {
		return "low"
	}
}

func parseInput20() map[string]Module {
	modules := map[string]Module{}
	for _, line := range strings.Split(testInput20_2, "\n") {
		split := strings.Split(line, "->")
		name := strings.Trim(split[0], " ")
		moduleType := broadcast
		if name != broadcast {
			moduleType = string(name[0])
			name = name[1:]
		}
		targets := strings.Split(split[1], ",")
		for i, t := range targets {
			targets[i] = strings.Trim(t, " ")
			_, exi := modules[targets[i]]
			if !exi {
				modules[targets[i]] = &Broadcast{state: false, modules: modules, targets: []string{}, name: targets[i]}
			}
		}
		switch moduleType {
		case broadcast:
			modules[name] = &Broadcast{state: false, modules: modules, targets: targets, name: name}
		case flipFlop:
			modules[name] = &FlipFlop{state: false, modules: modules, targets: targets, name: name}
		case conjunction:
			modules[name] = &Conjunction{state: false, modules: modules, targets: targets, inputs: []string{}, name: name, inputStates: map[string]bool{}}
		}
	}

	for k, v := range modules {
		for _, outp := range v.Targets() {
			module := modules[outp]
			conj, isConj := module.(*Conjunction)

			if isConj {
				conj.inputs = append(conj.inputs, k)
				conj.inputStates[k] = false
			}
		}
	}

	modules[button] = &Broadcast{state: false, modules: modules, targets: []string{broadcast}, name: "button"}

	return modules
}

func copyModules(modues map[string]Module) map[string]Module {
	newMap := map[string]Module{}
	for k, v := range modues {
		newMap[k] = v.Copy()
	}
	return newMap
}

type Module interface {
	ReceivePulse(pulse bool, from string) Module
	SendPulses() []Module
	State() bool
	Targets() []string
	GetPulsCount() (int, int)
	Name(raw bool) string
	Copy() Module
}

type Broadcast struct {
	state   bool
	modules map[string]Module
	targets []string
	low     int
	high    int
	name    string
}

func (ff Broadcast) Copy() Module {
	return &Broadcast{
		state:   ff.state,
		modules: ff.modules,
		targets: ff.targets,
		low:     ff.low,
		high:    ff.high,
		name:    ff.name,
	}
}
func (b Broadcast) Name(raw bool) string {
	if raw {
		return b.name
	}
	return fmt.Sprintf("(%v) %v", "B", b.name)
}

func (b Broadcast) Targets() []string {
	return b.targets
}

func (s Broadcast) State() bool {
	return s.state
}

func (ff Broadcast) ReceivePulse(pulse bool, _ string) Module {
	newM := ff.Copy().(*Broadcast)
	newM.state = pulse
	return newM
}

func (ff Broadcast) SendPulses() []Module {
	next := []Module{}
	for _, m := range ff.targets {
		next = append(next, ff.modules[m].ReceivePulse(ff.state, ff.name))

		mainLH.Add(ff.state)
	}

	return next
}

func (ff Broadcast) GetPulsCount() (int, int) {
	return ff.low, ff.high
}

type FlipFlop struct {
	state   bool
	modules map[string]Module
	targets []string
	low     int
	high    int
	locked  bool
	name    string
}

func (ff FlipFlop) Copy() Module {
	return &FlipFlop{
		state:   ff.state,
		modules: ff.modules,
		targets: ff.targets,
		low:     ff.low,
		high:    ff.high,
		locked:  ff.locked,
		name:    ff.name,
	}
}

func (f FlipFlop) Name(raw bool) string {
	if raw {
		return f.name
	}
	return fmt.Sprintf("(%v) %v", "F", f.name)
}

func (f FlipFlop) Targets() []string {
	if f.locked {
		return []string{}
	}
	return f.targets
}

func (s FlipFlop) State() bool {
	return s.state
}

func (ff FlipFlop) ReceivePulse(pulse bool, _ string) Module {
	newSt := ff.Copy().(*FlipFlop)
	if !pulse {
		newSt.locked = false
		newSt.state = !ff.state
	} else {
		newSt.locked = true
	}
	return newSt
}

func (ff FlipFlop) SendPulses() []Module {
	next := []Module{}
	if ff.locked {
		return next
	}
	for _, m := range ff.targets {
		next = append(next, ff.modules[m].ReceivePulse(ff.state, ff.name))

		mainLH.Add(ff.state)
	}
	return next
}

func (ff FlipFlop) GetPulsCount() (int, int) {
	return ff.low, ff.high
}

type Conjunction struct {
	state       bool
	modules     map[string]Module
	targets     []string
	inputs      []string
	low         int
	high        int
	name        string
	inputStates map[string]bool
}

func (ff Conjunction) Copy() Module {
	states := map[string]bool{}
	for s, v := range ff.inputStates {
		states[s] = v
	}

	return &Conjunction{
		state:       ff.state,
		modules:     ff.modules,
		targets:     ff.targets,
		low:         ff.low,
		high:        ff.high,
		name:        ff.name,
		inputStates: states,
		inputs:      ff.inputs,
	}
}

func (f Conjunction) Name(raw bool) string {
	if raw {
		return f.name
	}
	return fmt.Sprintf("(%v) %v", "C", f.name)
}

func (c Conjunction) Targets() []string {
	return c.targets
}

func (s Conjunction) State() bool {
	return s.state
}

func (ff Conjunction) ReceivePulse(pulse bool, source string) Module {
	newSt := ff.Copy().(*Conjunction)
	newSt.inputStates[source] = pulse
	return newSt.Copy()
}

func (ff Conjunction) SendPulses() []Module {
	next := []Module{}
	tmpSt := false
	for _, i := range ff.inputs {
		// state := ff.inputStates[i]
		state := ff.modules[i].State()
		if !state {
			tmpSt = true
			break
		}
	}

	for _, m := range ff.targets {
		next = append(next, ff.modules[m].ReceivePulse(tmpSt, ff.name))

		mainLH.Add(tmpSt)
	}
	return next
}

func (ff Conjunction) GetPulsCount() (int, int) {
	return ff.low, ff.high
}

var testInput20 = ``

var testInput20_2 = ``

var realInput20 = ``
