package days23

import (
	"aoc/solutions/etc"
	"fmt"
	"sort"
	"strconv"
	"strings"
)

var Day7 = etc.Solution{Solve1: solve7_1, Solve2: solve7_2}

type hand struct {
	cards         string
	bid           int
	handTypeValue int
	handValue     int
	hexHandVal    string
}

var cardValueMap = map[rune]byte{
	'A': 'E',
	'K': 'D',
	'Q': 'C',
	'J': 'B',
	'T': 'A',
	'9': '9',
	'8': '8',
	'7': '7',
	'6': '6',
	'5': '5',
	'4': '4',
	'3': '3',
	'2': '2'}

var handValueMap = map[string]int{"50000": 70, "41000": 60, "32000": 50, "31100": 40, "22100": 30, "21110": 20, "11111": 10}

func (hand *hand) calcValues(jokerRule bool) {

	cntedC := map[rune]int{}
	v := ""
	for _, c := range hand.cards {
		if jokerRule && c == 'J' {
			v += "1"
		} else {
			v += string(cardValueMap[c])
		}
		_, ok := cntedC[c]
		if ok {
			continue
		}
		cntedC[c] = strings.Count(hand.cards, string(c))
	}
	hand.hexHandVal = v
	tmpV, _ := strconv.ParseInt(v, 16, 64)
	hand.handValue = int(tmpV)

	if jokerRule {
		j, ok := cntedC['J']

		if ok {
			maxV := 0
			maxC := ' '
			for c, v := range cntedC {
				if v > maxV && c != 'J' {
					maxV = v
					maxC = c
				}
			}
			cntedC[maxC] += j
			delete(cntedC, 'J')
		}
	}

	cnt := []int{0, 0, 0, 0, 0}
	idx := 0
	for _, val := range cntedC {
		cnt[idx] = val
		idx++
	}
	sort.Slice(cnt, func(i, j int) bool { return cnt[i] > cnt[j] })

	var numbStrings []string
	for _, i := range cnt {
		numbStrings = append(numbStrings, strconv.Itoa(i))
	}
	hand.handTypeValue = handValueMap[strings.Join(numbStrings, "")]
}

func getHands(inp string, jokerRule bool) []hand {
	lines := strings.Split(inp, "\n")
	hands := []hand{}
	for _, line := range lines {
		splt := strings.Split(line, " ")
		bid, _ := strconv.Atoi(splt[1])
		hand := hand{cards: splt[0], bid: bid}
		hand.calcValues(jokerRule)
		hands = append(hands, hand)
	}

	sort.Slice(hands, func(i, j int) bool {
		if hands[i].handTypeValue != hands[j].handTypeValue {
			return hands[i].handTypeValue < hands[j].handTypeValue
		}
		return hands[i].handValue < hands[j].handValue
	})

	return hands
}

func solve7_1() string {
	winnings := 0
	for i, hand := range getHands(realInput7, false) {
		winnings += hand.bid * (i + 1)
	}

	return fmt.Sprint(winnings)
}

func solve7_2() string {
	winnings := 0
	for i, hand := range getHands(realInput7, true) {
		winnings += hand.bid * (i + 1)
	}

	return fmt.Sprint(winnings)
}

var testInput7 = ``

var realInput7 = ``
