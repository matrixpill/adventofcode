package days23

import (
	"aoc/solutions/etc"
	"fmt"
	"strings"
)

var Day16 = etc.Solution{Solve1: solve16_1, Solve2: solve16_2}

const vertical = '|'
const horizontal = '-'
const tippedLeft = '\\'
const tippedRight = '/'

func solve16_1() string {
	contraption := strings.Split(realInput16, "\n")
	return fmt.Sprint(solve16(Light{direction: east, position: Pos{X: -1, Y: 0}}, contraption))
}

func solve16_2() string {
	max := 0
	contraption := strings.Split(realInput16, "\n")
	// top row south
	for x := 0; x < len(contraption[0]); x++ {
		tmp := solve16(Light{direction: south, position: Pos{X: x, Y: -1}}, contraption)
		if tmp > max {
			max = tmp
		}
	}
	// bottom row north
	for x := 0; x < len(contraption[0]); x++ {
		tmp := solve16(Light{direction: north, position: Pos{X: x, Y: len(contraption)}}, contraption)
		if tmp > max {
			max = tmp
		}
	}
	// left col east
	for y := 0; y < len(contraption); y++ {
		tmp := solve16(Light{direction: east, position: Pos{X: -1, Y: y}}, contraption)
		if tmp > max {
			max = tmp
		}
	}
	// right col west
	for y := 0; y < len(contraption); y++ {
		tmp := solve16(Light{direction: west, position: Pos{X: len(contraption[0]), Y: y}}, contraption)
		if tmp > max {
			max = tmp
		}
	}
	return fmt.Sprint(max)
}

func solve16(startLight Light, contraption []string) int {

	lightsToCheck := []Light{startLight}
	contraptionSize := Pos{X: len(contraption[0]) - 1, Y: len(contraption) - 1}

	energizedSpaces := map[Pos]EnergizedPos{}

	for len(lightsToCheck) != 0 {
		currentLight := lightsToCheck[0]
		lightsToCheck = lightsToCheck[1:]

		nextPos := currentLight.position.Add(currentLight.direction)

		if !inSpace(nextPos, contraptionSize) {
			continue
		}
		pos, energized := energizedSpaces[nextPos]
		light, isLight := pos.(Light)
		if energized && isLight && currentLight.EqualDirection(light) {
			continue
		}

		switch contraption[nextPos.Y][nextPos.X] {
		case vertical:
			if currentLight.direction != north && currentLight.direction != south {
				lightsToCheck = append(lightsToCheck, Light{direction: north, position: nextPos})
				lightsToCheck = append(lightsToCheck, Light{direction: south, position: nextPos})
			} else {
				lightsToCheck = append(lightsToCheck, Light{direction: currentLight.direction, position: nextPos})
			}
			if !energized {
				energizedSpaces[nextPos] = Mirror{}
			}
		case horizontal:
			if currentLight.direction != east && currentLight.direction != west {
				lightsToCheck = append(lightsToCheck, Light{direction: east, position: nextPos})
				lightsToCheck = append(lightsToCheck, Light{direction: west, position: nextPos})
			} else {
				lightsToCheck = append(lightsToCheck, Light{direction: currentLight.direction, position: nextPos})
			}
			if !energized {
				energizedSpaces[nextPos] = Mirror{}
			}
		case tippedLeft:
			switch currentLight.direction {
			case north:
				lightsToCheck = append(lightsToCheck, Light{direction: west, position: nextPos})
			case east:
				lightsToCheck = append(lightsToCheck, Light{direction: south, position: nextPos})
			case south:
				lightsToCheck = append(lightsToCheck, Light{direction: east, position: nextPos})
			case west:
				lightsToCheck = append(lightsToCheck, Light{direction: north, position: nextPos})
			}
			if !energized {
				energizedSpaces[nextPos] = Mirror{}
			}
		case tippedRight:
			switch currentLight.direction {
			case north:
				lightsToCheck = append(lightsToCheck, Light{direction: east, position: nextPos})
			case east:
				lightsToCheck = append(lightsToCheck, Light{direction: north, position: nextPos})
			case south:
				lightsToCheck = append(lightsToCheck, Light{direction: west, position: nextPos})
			case west:
				lightsToCheck = append(lightsToCheck, Light{direction: south, position: nextPos})
			}
			if !energized {
				energizedSpaces[nextPos] = Mirror{}
			}
		default:
			lightsToCheck = append(lightsToCheck, Light{direction: currentLight.direction, position: nextPos})
			if !energized {
				energizedSpaces[nextPos] = Light{direction: currentLight.direction, position: nextPos}
			}
		}
	}

	return len(energizedSpaces)
}

type Light struct {
	direction Pos
	position  Pos
}

type Mirror struct {
}

type EnergizedPos interface{}

func (p Light) EqualDirection(p2 Light) bool {
	switch p.direction {
	case north:
		return p2.direction == north || p2.direction == south
	case south:
		return p2.direction == north || p2.direction == south
	case east:
		return p2.direction == east || p2.direction == west
	case west:
		return p2.direction == east || p2.direction == west
	default:
		return false
	}
}

var testInput16 = ``

var realInput16 = ``
