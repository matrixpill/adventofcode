package days23

import (
	"aoc/solutions/etc"
	"fmt"
	"math"
	"slices"
	"strings"
)

var Day14 = etc.Solution{Solve1: solve14_1, Solve2: solve14_2}

var rollingStone = 'O'
var cubeStone = '#'
var nothing = ' '

func solve14_1() string {
	input := strings.Split(realInput14, "\n")
	sum := 0
	maxPossible := len(input)
	for x := 0; x < len(input[0]); x++ {
		maxPos := maxPossible
		for y := 0; y < len(input); y++ {
			r := input[y][x]
			switch r {
			case byte(rollingStone):
				sum += maxPos
				maxPos--
			case byte(cubeStone):
				maxPos = maxPossible - y - 1
			case byte(nothing):
			}
		}
	}
	return fmt.Sprint(sum)
}

func solve14_2() string {
	input := strings.Split(realInput14, "\n")
	baseSlice := []string{}
	for _, line := range input {
		tmp := ""
		for _, r := range line {
			if r == cubeStone {
				tmp += string(cubeStone)
			} else {
				tmp += "."
			}
		}
		baseSlice = append(baseSlice, tmp)
	}

	pastCycles := map[string]int{}
	stones := slices.Clone(input)
	toCycleCnt := 1000000000
	for c := 1; true; c++ {
		newStonePos := slices.Clone(baseSlice)
		tiltNorth(stones, newStonePos)
		stones = newStonePos
		newStonePos = slices.Clone(baseSlice)
		tiltWest(stones, newStonePos)
		stones = newStonePos
		newStonePos = slices.Clone(baseSlice)
		tiltSouth(stones, newStonePos)
		stones = newStonePos
		newStonePos = slices.Clone(baseSlice)
		tiltEast(stones, newStonePos)
		stones = newStonePos

		stonesAsString := strings.Join(stones, "")
		prev, found := pastCycles[stonesAsString]
		if found {
			cLen := c - prev
			cycleCnt := int(math.Floor(float64(toCycleCnt-c) / float64(cLen)))
			remain := toCycleCnt - (c + cycleCnt*cLen)
			lastStoneC := prev + remain
			for k, v := range pastCycles {
				if v == lastStoneC {
					return fmt.Sprint(calcWeightFromString(len(input), len(input[0]), k))
				}
			}
			break
		} else {
			pastCycles[stonesAsString] = c
		}
	}
	panic("no solution")
}

func calcWeightFromString(rows, rLen int, k string) int {
	sum := 0
	for i, r := range k {
		if r == rollingStone {
			y := math.Floor(float64(i) / float64(rLen))
			sum += rows - int(y)
		}
	}
	return sum
}

func tiltNorth(stones []string, newStonePos []string) {
	for x := 0; x < len(stones[0]); x++ {
		freeIdx := 0
		for y := 0; y < len(stones); y++ {
			r := stones[y][x]
			switch r {
			case byte(rollingStone):
				newStonePos[freeIdx] = replaceAtIndex(newStonePos[freeIdx], rollingStone, x)
				freeIdx++
			case byte(cubeStone):
				freeIdx = y + 1
			case byte(nothing):
			}
		}
	}
}

func tiltWest(stones []string, newStonePos []string) {
	for y := 0; y < len(stones); y++ {
		freeIdx := 0
		for x := 0; x < len(stones[0]); x++ {
			r := stones[y][x]
			switch r {
			case byte(rollingStone):
				newStonePos[y] = replaceAtIndex(newStonePos[y], rollingStone, freeIdx)
				freeIdx++
			case byte(cubeStone):
				freeIdx = x + 1
			case byte(nothing):
			}
		}
	}
}

func tiltSouth(stones []string, newStonePos []string) {
	for x := 0; x < len(stones[0]); x++ {
		freeIdx := len(stones) - 1
		for y := len(stones) - 1; y >= 0; y-- {
			r := stones[y][x]
			switch r {
			case byte(rollingStone):
				newStonePos[freeIdx] = replaceAtIndex(newStonePos[freeIdx], rollingStone, x)
				freeIdx--
			case byte(cubeStone):
				freeIdx = y - 1
			case byte(nothing):
			}
		}
	}
}

func tiltEast(stones []string, newStonePos []string) {
	for y := 0; y < len(stones); y++ {
		freeIdx := len(stones[0]) - 1
		for x := len(stones[0]) - 1; x >= 0; x-- {
			r := stones[y][x]
			switch r {
			case byte(rollingStone):
				newStonePos[y] = replaceAtIndex(newStonePos[y], rollingStone, freeIdx)
				freeIdx--
			case byte(cubeStone):
				freeIdx = x - 1
			case byte(nothing):
			}
		}
	}
}

func printStones(stones []string, cycle int) {
	fmt.Printf("Cycle %d \n", cycle)
	for _, line := range stones {
		fmt.Println(line)
	}
	fmt.Println()
}

func replaceAtIndex(in string, r rune, i int) string {
	out := []rune(in)
	out[i] = r
	return string(out)
}

var testInput14 = ``

var realInput14 = ``
