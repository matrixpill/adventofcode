package days23

import (
	"fmt"
	"testing"
)

func BenchmarkPart1(b *testing.B) {
	for n := 0; n < b.N; n++ {
		solve4_1()
	}

}

func TestPart1(t *testing.T) {
	result := solve4_1()
	if result != fmt.Sprint(28750) {
		t.Fatal("wrong result")
	}
}
