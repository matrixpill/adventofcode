package days23

import (
	"aoc/solutions/etc"
	"fmt"
	"math"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

var Day5 etc.Solution = etc.Solution{Solve1: solve5_1, Solve2: solve5_2frfr}

func solve5_1() string {
	input := strings.Split(realInput5, "\n")

	seedRegx := regexp.MustCompile(`\d+`)
	min := math.MaxInt
	for _, seedStr := range seedRegx.FindAllString(input[0], -1) {
		seednum, _ := strconv.Atoi(seedStr)
		foundRange := true
		for _, line := range input[2:] {
			if strings.HasSuffix(line, "map:") {
				foundRange = false
				continue
			}
			if foundRange {
				continue
			}
			if seedRegx.MatchString(line) {
				mapping := etc.ToIntArray(seedRegx.FindAllString(line, -1))
				if seednum >= mapping[1] && seednum < mapping[1]+mapping[2] {
					seednum = seednum - mapping[1] + mapping[0]
					foundRange = true
				}
			}
		}
		if seednum < min {
			min = seednum
		}
	}
	return fmt.Sprint(min)
}

type mapStep struct {
	part      string
	rangeMaps []rangeMap
}

type rangeMap struct {
	source aRange
	target aRange
}

type aRange struct {
	min int
	max int
}

func (a aRange) copy() aRange {
	return aRange{min: a.min, max: a.max}
}

func (mapStep mapStep) SortBySource() {
	sort.Slice(mapStep.rangeMaps, func(i, j int) bool {
		return mapStep.rangeMaps[i].source.min < mapStep.rangeMaps[j].source.min
	})
}

func (mapStep mapStep) SortByTarget() {
	sort.Slice(mapStep.rangeMaps, func(i, j int) bool {
		return mapStep.rangeMaps[i].target.min < mapStep.rangeMaps[j].target.min
	})
}

func (aRange aRange) overlaps(oherRange aRange) bool {
	return oherRange.min <= aRange.max && oherRange.max >= aRange.min
}

func solve5_2frfr() string {
	input := strings.Split(realInput5, "\n")
	seedRegx := regexp.MustCompile(`\d+`)
	foundRange := true
	rangeMaps := []mapStep{}
	var currentStep mapStep
	for _, line := range input[2:] {
		if strings.HasSuffix(line, "map:") {
			if currentStep.part != "" {
				rangeMaps = append(rangeMaps, currentStep)
			}
			currentStep = mapStep{part: line, rangeMaps: []rangeMap{}}
			foundRange = false
			continue
		}
		if foundRange {
			continue
		}
		if seedRegx.MatchString(line) {
			mapping := etc.ToIntArray(seedRegx.FindAllString(line, -1))

			rMap := rangeMap{
				source: aRange{min: mapping[1], max: mapping[1] + mapping[2] - 1},
				target: aRange{min: mapping[0], max: mapping[0] + mapping[2] - 1},
			}

			currentStep.rangeMaps = append(currentStep.rangeMaps, rMap)
		}
	}
	if currentStep.part != "" {
		rangeMaps = append(rangeMaps, currentStep)
	}
	seeds := seedRanges(etc.ToIntArray(seedRegx.FindAllString(input[0], -1)))
	heights := combine(rangeMaps, seeds)

	return fmt.Sprint(heights[0].min)
}

func seedRanges(seeds []int) []aRange {
	ranges := []aRange{}
	for i := 0; i < len(seeds)-1; i += 2 {
		ranges = append(ranges, aRange{min: seeds[i], max: seeds[i] + seeds[i+1] - 1})
	}
	return ranges
}

func add(r []aRange, a aRange) []aRange {
	if a.min == 0 {
		fmt.Print()
	}
	return append(r, a)
}

func combine(mappings []mapStep, mainRanges []aRange) []aRange {
	for _, step := range mappings {
		oldRanges := copyAll(mainRanges)
		newRanges := []aRange{}

		for _, main := range oldRanges {
			remains := []aRange{main.copy()}
			for len(remains) > 0 {
				remain := remains[0]
				remains = remains[1:]
				found := false
				for _, mapping := range step.rangeMaps {
					if !mapping.source.overlaps(remain) {
						continue
					}

					found = true
					if mapping.source.min < remain.min && mapping.source.max > remain.max {
						newRanges = add(newRanges,
							aRange{
								min: remain.min - mapping.source.min + mapping.target.min,
								max: remain.max - mapping.source.max + mapping.target.max,
							},
						)
						break
					}

					if mapping.source.min >= remain.min && mapping.source.max <= remain.max {
						if mapping.source.min > remain.min {
							remains = add(remains, aRange{min: remain.min, max: mapping.source.min - 1})
						}
						if mapping.source.max < remain.max {
							remains = add(remains, aRange{min: mapping.source.max + 1, max: remain.max})
						}
						newRanges = add(newRanges, mapping.target.copy())
						break
					}

					if mapping.source.min < remain.min {
						if mapping.source.max < remain.max {
							remains = add(remains, aRange{min: mapping.source.max + 1, max: remain.max})
						}
						newRanges = add(newRanges,
							aRange{
								min: remain.min - mapping.source.min + mapping.target.min,
								max: mapping.target.max,
							},
						)
						break
					}

					if mapping.source.max > remain.max {
						if mapping.source.min > remain.min {
							remains = add(remains, aRange{min: remain.min, max: mapping.source.min - 1})
						}
						newRanges = add(newRanges,
							aRange{
								min: mapping.target.min,
								max: remain.max - mapping.source.max + mapping.target.max,
							},
						)
						break
					}
				}

				if !found {
					newRanges = add(newRanges, remain.copy())
				}
			}
		}

		sort.Slice(newRanges, func(i, j int) bool {
			return newRanges[i].min < newRanges[j].min
		})

		mainRanges = newRanges
	}
	return mainRanges
}

func copyAll(ranges []aRange) []aRange {
	newRanges := []aRange{}

	for _, r := range ranges {
		newRanges = append(newRanges, r.copy())
	}
	return newRanges
}

//lint:ignore U1000 Ignore unused function temporarily for debugging
func solve5_2() string {
	input := strings.Split(realInput5, "\n")
	seedRegx := regexp.MustCompile(`\d+`)
	var min int = math.MaxInt
	seeds := etc.ToIntArray(seedRegx.FindAllString(input[0], -1))
	for i := 0; i < len(seeds)-1; i += 2 {
		var l int = 0
		for l = 0; l < seeds[i+1]; l++ {
			seedNum := seeds[i] + l
			foundRange := true
			for _, line := range input[2:] {
				if strings.HasSuffix(line, "map:") {
					foundRange = false
					continue
				}
				if foundRange {
					continue
				}
				if seedRegx.MatchString(line) {
					mapping := etc.ToIntArray(seedRegx.FindAllString(line, -1))
					if seedNum >= mapping[1] && seedNum < mapping[1]+mapping[2] {
						seedNum = seedNum - mapping[1] + mapping[0]
						foundRange = true
					}
				}

			}
			if seedNum < min {
				min = seedNum
			}
		}

	}

	return fmt.Sprint(min)
}

var testInput5 string = ``

var realInput5 string = ``
