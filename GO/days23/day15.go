package days23

import (
	"aoc/solutions/etc"
	"fmt"
	"slices"
	"strconv"
	"strings"
)

var Day15 = etc.Solution{Solve1: solve15_1, Solve2: solve15_2}

func solve15_1() string {
	input := realInput15
	seq := strings.Split(input, ",")
	sum := 0

	for _, r := range seq {
		sum += hash15(r)
	}
	return fmt.Sprint(sum)
}

func solve15_2() string {
	input := realInput15
	boxes := make([][]lense, 256)
	seq := strings.Split(input, ",")
	// opReg := regexp.MustCompile(`([a-z]+)([=-])([0-9]*)`)

	for _, inst := range seq {
		// match := opReg.FindAllStringSubmatch(inst, -1)
		var label string
		var focal int = 0
		var op string
		if inst[len(inst)-1] == '-' {
			op = "-"
			label = inst[:len(inst)-1]
		} else {
			op = "="
			splt := strings.Split(inst, "=")
			label = splt[0]
			focal, _ = strconv.Atoi(splt[1])
		}

		// label := match[0][1]
		boxNum := hash15(label)
		// op := match[0][2]
		idx := slices.IndexFunc(boxes[boxNum], func(l lense) bool { return l.label == label })
		if op == "=" {
			// focal, _ := strconv.Atoi(match[0][3])
			lens := lense{label: label, focal: focal}

			if idx != -1 {
				boxes[boxNum][idx] = lens
			} else {
				boxes[boxNum] = append(boxes[boxNum], lens)
			}
		} else {
			if idx != -1 {
				start := boxes[boxNum][:idx]
				end := boxes[boxNum][idx+1:]
				boxes[boxNum] = append(start, end...)
			}
		}
	}
	power := 0
	for bi, box := range boxes {
		for li, l := range box {
			power += (bi + 1) * (li + 1) * l.focal
		}
	}
	return fmt.Sprint(power)
}

type lense struct {
	label string
	focal int
}

func hash15(s string) int {
	val := 0
	for _, r := range s {
		val += int(r)
		val *= 17
		val = val % 256
	}
	return val
}

var testInput15 = ``

var realInput15 = ``
