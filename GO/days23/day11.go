package days23

import (
	"aoc/solutions/etc"
	"fmt"
	"strings"
)

var Day11 = etc.Solution{Solve1: solve11_1, Solve2: solve11_2}

type wurstbrot []int

func solve11_1() string {
	return solve11(2)
}

func solve11_2() string {
	return solve11(1000000)
}

func solve11(rowExpansion int) string {
	input := strings.Split(realInput11, "\n")

	expandedRows := make(wurstbrot, len(input))
	expandedColumns := make(wurstbrot, len(input[0]))

	for y, line := range input {
		if !strings.Contains(line, "#") {
			expandedRows[y] = rowExpansion - 1
		}
	}
	for x := 0; x < len(input[0]); x++ {
		shouldExpand := true
		for y := 0; y < len(input); y++ {
			if input[y][x] == '#' {
				shouldExpand = false
				break
			}
		}
		if shouldExpand {
			expandedColumns[x] = rowExpansion - 1
		}
	}

	galaxys := []Pos{}
	for y := 0; y < len(input); y++ {
		for x := 0; x < len(input[y]); x++ {
			if input[y][x] == '#' {
				posX := x
				for _, e := range expandedColumns[:x] {
					posX += e
				}
				posY := y
				for _, e := range expandedRows[:y] {
					posY += e
				}

				galaxys = append(galaxys, Pos{X: posX, Y: posY})
			}
		}
	}
	sum := 0
	for i, galaxy := range galaxys {
		for _, galaxy2 := range galaxys[i+1:] {
			normalDistance := etc.Abs(galaxy.X-galaxy2.X) + etc.Abs(galaxy.Y-galaxy2.Y)
			sum += normalDistance
		}
	}

	return fmt.Sprint(sum)
}

var testInput11 = ``

var realInput11 = ``
