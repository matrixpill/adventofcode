package days23

import (
	"aoc/solutions/etc"
	"fmt"
	"regexp"
	"slices"
	"strings"
	"time"
)

var Day12 = etc.Solution{Solve1: solve12_1, Solve2: solve12_2}

type guess struct {
	Guess rune
	Idx   int
	group *group
}

func (guess *guess) CanBeChanged() bool {
	return guess.Guess == defectRune && guess.Idx == guess.group.startIdx
}

const unknwonRune = '?'
const defectRune = '#'
const workingRune = '.'

type group struct {
	startIdx    int
	targetSize  int
	currentSize int
}

func (group group) IsFull() bool {
	return group.currentSize == group.targetSize
}

func (group *group) Add1(idx int) {
	if group.currentSize == 0 {
		group.startIdx = idx
	}
	group.currentSize++
}

func (group *group) ResetSize() {
	group.currentSize = 0
}

func (group *group) ResetToIdx(idx int) {
	if idx < group.startIdx {
		group.currentSize = 0
		group.startIdx = -1
	} else {
		group.currentSize = idx - group.startIdx //+1 ???
	}
}

func solve12_1() string {
	lines := strings.Split(realInput12, "\n")
	return solve12(lines)
}

func solve12_2() string {
	lines := strings.Split(realInput12, "\n")
	for i, line := range lines {
		split := strings.Split(line, " ")
		opString := split[0]
		opString2 := opString
		groupString := split[1]
		groupString2 := groupString
		for l := 0; l < 4; l++ {
			opString2 += "?" + opString
			groupString2 += "," + groupString
		}
		newLine := opString2 + " " + groupString2

		lines[i] = newLine
	}
	return solve12(lines)
}

func solve12(lines []string) string {
	start := time.Now()
	reg := regexp.MustCompile(`\d+`)
	cnt := 0
	for lIdx, line := range lines {
		proz := float64(lIdx+1) / float64(len(lines)) * 100

		end := time.Now()
		elapsed := (end.UnixMilli() - start.UnixMilli()) / 1000
		fmt.Println(proz, " in ", elapsed, " sek")
		split := strings.Split(line, " ")
		opString := split[0]
		groupString := split[1]
		//fmt.Println(line)
		unknownCnt := strings.Count(opString, string(unknwonRune))
		knownDefectCnt := strings.Count(opString, string(defectRune))
		knownWorkingCnt := strings.Count(opString, string(workingRune))
		groups := etc.ToIntArray(reg.FindAllString(groupString, -1))

		toBeDefective := 0
		for _, group := range groups {
			toBeDefective += group
		}
		toGuess := toBeDefective - knownDefectCnt
		UNUSED(knownWorkingCnt)

		foundGroups := []group{}
		for _, g := range groups {
			foundGroups = append(foundGroups, group{startIdx: -1, targetSize: g, currentSize: 0})
		}
		groupIdx := 0
		constructedLine := ""
		guesses := []guess{}
		retryFrom := 0
		for {
			broken := false
			for i := retryFrom; i < len(opString); i++ {
				r := opString[i]
				switch r {
				case defectRune:
					if foundGroups[groupIdx].IsFull() {
						//fmt.Println("group full")
						//reverse last defect guess
						lastGuess, idx := lastMovableDefectGuess(guesses)
						if idx == -1 {
							broken = true
							break
						}
						guesses = guesses[:idx+1]
						i = lastGuess.Idx
						constructedLine = constructedLine[:i]
						if lastGuess.Guess == defectRune {
							lastGuess.Guess = workingRune
							constructedLine += string(workingRune)
						} else {
							lastGuess.Guess = defectRune
							constructedLine += string(defectRune)
						}
						lastGuess.group.ResetSize()
						groupIdx = slices.Index(foundGroups, *lastGuess.group)
						for i := groupIdx + 1; i < len(foundGroups); i++ {
							foundGroups[i].ResetSize()
						}
					} else {
						constructedLine += string(defectRune)
						foundGroups[groupIdx].Add1(i)
					}
				case unknwonRune:
					if foundGroups[groupIdx].IsFull() {
						//fmt.Println("closed group")
						constructedLine += string(workingRune)
						guesses = append(guesses, guess{Guess: workingRune, Idx: i, group: nil})
						if foundGroups[groupIdx].currentSize != 0 {
							if groupIdx == len(groups)-1 {
								broken = true
								break
							}
							groupIdx++
						}
					} else {
						if toGuess == guessedDefectCnt(guesses) {
							//fmt.Println("already guessed to much")
							constructedLine += string(workingRune)
							guesses = append(guesses, guess{Guess: workingRune, Idx: i, group: nil})
							if foundGroups[groupIdx].currentSize != 0 {
								if groupIdx == len(groups)-1 {
									broken = true
									break
								}
								groupIdx++
							}
						} else {
							constructedLine += string(defectRune)
							foundGroups[groupIdx].Add1(i)
							guesses = append(guesses, guess{Guess: defectRune, Idx: i, group: &foundGroups[groupIdx]})
						}
					}
				case workingRune:
					constructedLine += string(workingRune)

					if foundGroups[groupIdx].currentSize != 0 {
						if groupIdx == len(groups)-1 {
							broken = true
							break
						}
						groupIdx++
					}
				}
				if broken {
					break
				}
			}

			full := true
			for i := len(foundGroups) - 1; i >= 0; i-- {
				if !foundGroups[i].IsFull() {
					full = false
					break
				}
			}
			if full {
				cnt++
				//fmt.Println(constructedLine)
			}
			lastGuess, idx := lastMovableDefectGuess(guesses)
			if idx == len(guesses)-1 && unknownCnt == len(guesses) {
				lastGuess, idx = lastMovableDefectGuess(guesses[:idx])
			}
			if idx < 0 {
				break
			}

			guesses = guesses[:idx+1]
			retryFrom = lastGuess.Idx + 1
			constructedLine = constructedLine[:retryFrom-1]

			if lastGuess.Guess == defectRune {
				lastGuess.Guess = workingRune
				constructedLine += string(workingRune)
			} else {
				lastGuess.Guess = defectRune
				constructedLine += string(defectRune)
			}
			lastGuess.group.ResetSize()
			groupIdx = slices.Index(foundGroups, *lastGuess.group)
			for i := groupIdx + 1; i < len(foundGroups); i++ {
				foundGroups[i].ResetSize()
			}
		}

	}

	return fmt.Sprint(cnt)
}

func lastMovableDefectGuess(guesses []guess) (*guess, int) {
	for i := len(guesses) - 1; i >= 0; i-- {
		if guesses[i].CanBeChanged() {
			return &guesses[i], i
		}
	}
	return nil, -1
}

func firstMovableDefectGuess(guesses []guess) (*guess, int) {
	for i := 0; i < len(guesses); i++ {
		if guesses[i].CanBeChanged() {
			return &guesses[i], i
		}
	}
	return nil, -1
}

func guessedDefectCnt(guesses []guess) int {
	cnt := 0
	for _, g := range guesses {
		if g.Guess == defectRune {
			cnt++
		}
	}
	return cnt
}

func UNUSED(x ...any) {

}

var testInput12_ = ``

var testInput12 = ``

var realInput12 = ``
