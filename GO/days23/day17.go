package days23

import (
	"aoc/solutions/etc"
	"math"
	"slices"
	"strconv"
	"strings"
)

var Day17 = etc.Solution{Solve1: solve17_1}

func solve17_1() string {
	graph := map[Pos]int{}
	for y, line := range strings.Split(testInput17, "\n") {
		for x, v := range line {
			heatL, _ := strconv.Atoi(string(v))
			graph[Pos{X: x, Y: y}] = heatL
		}
	}
	dijkstra(graph, Pos{X: 0, Y: 0})
	return ""
}

func dijkstra(graph map[Pos]int, start Pos) int {
	vorgaenger := map[Pos]Pos{}
	abstand, q := initDijkstra(graph, Pos{X: 0, Y: 0})
	for len(q) != 0 {
		tmpMin := math.MaxInt
		tmpMinPos := Pos{}
		posInQ := -1
		for idx, qItem := range q {
			v := abstand[qItem]
			if v < tmpMin {
				tmpMin = v
				tmpMinPos = qItem
				posInQ = idx
			}
		}
		q = slices.Delete(q, posInQ, posInQ+1)
		u := tmpMinPos
		// for _, neighbour := range neighbours(u, vorgaenger) {

		// }
		UNUSED(u, vorgaenger)
	}
	return 0
}

func neighbours(u Pos, vorgaenger map[Pos]Pos) []Pos {
	neighb := []Pos{}

	return neighb
}

func initDijkstra(graph map[Pos]int, start Pos) (map[Pos]int, []Pos) {
	q := []Pos{}
	abstand := map[Pos]int{}
	for k, _ := range graph {
		abstand[k] = math.MaxInt
		q = append(q, k)
	}
	abstand[start] = 0
	return abstand, q
}

var testInput17 = ``
