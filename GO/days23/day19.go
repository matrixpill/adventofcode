package days23

import (
	"aoc/solutions/etc"
	"fmt"
	"maps"
	"math"
	"regexp"
	"strconv"
	"strings"
)

var Day19 = etc.Solution{Solve1: solve19_1, Solve2: solve19_2}

var greater = '>'
var smaller = '<'

var accepted = "A"
var rejected = "R"

type rule19inWf struct {
	ruleIdx int
	wf      string
}

type rule19 struct {
	hasComp   bool
	cat       rune
	compValue int
	compType  rune
	next      string
}

type part19 struct {
	values map[rune]int
}

func solve19_1() string {
	input := strings.Split(realInput19, "\n")

	wfs, _, parts := ParseInput19(input)
	sum := 0
	for _, part := range parts {
		wf := wfs["in"]
		for {
			cont := true
			for _, r := range wf {
				if !r.hasComp {
					if r.next == accepted {
						sum += sumPart(part)
					} else {
						if r.next == rejected {
							cont = false
							break
						}
						wf = wfs[r.next]
						break
					}
					cont = false
					break
				} else {
					if r.compType == greater {
						if part.values[r.cat] > r.compValue {
							if r.next == accepted {
								sum += sumPart(part)
								cont = false
								break
							} else {
								if r.next == rejected {
									cont = false
									break
								}
								wf = wfs[r.next]
								break
							}
						}
					} else {
						if part.values[r.cat] < r.compValue {
							if r.next == accepted {
								sum += sumPart(part)
								cont = false
								break
							} else {
								if r.next == rejected {
									cont = false
									break
								}
								wf = wfs[r.next]
								break
							}
						}
					}
				}
			}
			if !cont {
				break
			}
		}
	}

	return fmt.Sprint(sum)
}

func solve19_2() string {
	input := strings.Split(testInput19, "\n")
	rangeMin := map[rune]int{
		'x': 1,
		'm': 1,
		'a': 1,
		's': 1}
	rangeMax := map[rune]int{
		'x': 4000,
		'm': 4000,
		'a': 4000,
		's': 4000}

	minV, maxV := 1, 4000
	rang := maxV - minV + 1

	sum := 0
	wfs, ruleWf, _ := ParseInput19(input)
	maxPerm := int(math.Pow(float64(rang), float64(len(rangeMin))))
	reps := maxPerm / rang
	for wf, wfRules := range wfs {
		for i := len(wfRules) - 1; i >= 0; i-- {
			rule := wfRules[i]
			if rule.next == accepted {
				someRange := buildRanges(wf, i, wfs, ruleWf, maps.Clone(rangeMin), maps.Clone(rangeMax))
				r := calc(someRange, minV, maxV, reps, rang, maxPerm)
				someRange.res = r
				sum += r
			}
		}
	}

	return fmt.Sprint(sum)
}

type someRange19 struct {
	min, max map[rune]int
	res      int
}

func calc(someRange someRange19, rMin, rMax, reps, rang, maxPerm int) int {
	remain := maxPerm
	for k := range someRange.min {
		v := someRange.min[k]
		diff := rang - (someRange.max[k] - v + 1)
		remain -= diff * reps
		d := rang - diff
		reps = reps / rang * d
	}
	return remain
}

func buildRanges(wfN string, rI int, wfs map[string][]rule19, ruleWf map[string]rule19inWf, rangeMin, rangeMax map[rune]int) someRange19 {
	wf := wfs[wfN]
	rule := wf[rI]
	posRule(rule, rangeMin, rangeMax)
	for i := rI - 1; i >= 0; i-- {
		rule = wf[i]
		nRule(rule, rangeMin, rangeMax)
	}
	if wfN != "in" {
		nWf := ruleWf[wfN]
		return buildRanges(nWf.wf, nWf.ruleIdx, wfs, ruleWf, rangeMin, rangeMax)
	}
	return someRange19{min: rangeMin, max: rangeMax}
}

func posRule(rule rule19, rangeMin, rangeMax map[rune]int) {
	if rule.hasComp {
		if rule.compType == greater {
			if rule.compValue > rangeMin[rule.cat] {
				rangeMin[rule.cat] = rule.compValue + 1
			}
		} else {
			if rule.compValue < rangeMax[rule.cat] {
				rangeMax[rule.cat] = rule.compValue - 1
			}
		}
	}
}

func nRule(rule rule19, rangeMin, rangeMax map[rune]int) {
	if rule.hasComp {
		if rule.compType == greater {
			if rule.compValue < rangeMax[rule.cat] {
				rangeMax[rule.cat] = rule.compValue
			}
		} else {
			if rule.cat == 's' && rule.compValue == 537 {
				UNUSED()
			}
			if rule.compValue > rangeMin[rule.cat] {
				rangeMin[rule.cat] = rule.compValue
			}
		}
	}
}

func ParseInput19(input []string) (map[string][]rule19, map[string]rule19inWf, []part19) {
	ruleReg := regexp.MustCompile(`([a-z]+){(.*)}`)
	wfs := map[string][]rule19{}
	parts := []part19{}
	ruleNxtWf := map[string]rule19inWf{}

	wf := true
	for _, line := range input {
		if line == "" {
			wf = false
			continue
		}
		if wf {
			match := ruleReg.FindStringSubmatch(line)
			rules := []rule19{}
			for i, r := range strings.Split(match[2], ",") {
				colonIdx := strings.Index(r, ":")
				if colonIdx == -1 {
					if r != accepted && r != rejected {
						ruleNxtWf[r] = rule19inWf{ruleIdx: i, wf: match[1]}
					}
					rules = append(rules, rule19{hasComp: false, next: r})
					continue
				}
				cat := rune(r[0])
				comp := rune(r[1])
				val, _ := strconv.Atoi(r[2:colonIdx])
				nxt := r[colonIdx+1:]
				newRule := rule19{hasComp: true, cat: cat, compValue: val, compType: comp, next: nxt}
				if nxt != accepted && nxt != rejected {
					ruleNxtWf[nxt] = rule19inWf{ruleIdx: i, wf: match[1]}
				}
				rules = append(rules, newRule)
			}

			wfs[match[1]] = rules
		} else {
			partString := line[1 : len(line)-1]
			part := part19{values: map[rune]int{}}
			for _, p := range strings.Split(partString, ",") {
				for _, v := range strings.Split(p, ",") {
					vals := strings.Split(v, "=")
					vi, _ := strconv.Atoi(vals[1])
					part.values[rune(vals[0][0])] = vi
				}
			}
			parts = append(parts, part)
		}
	}
	return wfs, ruleNxtWf, parts
}

func sumPart(part part19) int {
	s := 0
	for _, v := range part.values {
		s += v
	}
	return s
}

var testInput19 = ``

var realInput19 = ``
