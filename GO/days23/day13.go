package days23

import (
	"aoc/solutions/etc"
	"fmt"
	"strconv"
	"strings"
)

var Day13 = etc.Solution{Solve1: solve13_1, Solve2: solve13_2}

func solve13_1() string {
	sum := 0
	block := []string{}
	for _, line := range strings.Split(realInput13, "\n") {
		if line == "" {
			sum += checkBlock(block)
			block = make([]string, 0)
			continue
		}
		block = append(block, line)
	}
	sum += checkBlock(block)

	return fmt.Sprint(sum)
}

func solve13_2() string {
	sum := 0
	block := []string{}
	for _, line := range strings.Split(realInput13, "\n") {
		if line == "" {
			sum += checkBlock2(block)
			block = make([]string, 0)
			continue
		}
		block = append(block, line)
	}
	sum += checkBlock2(block)

	return fmt.Sprint(sum)
}

func spinBlock(block []string) []string {
	spun := make([]string, len(block[0]))
	for i := 0; i < len(block[0]); i++ {
		for _, line := range block {
			spun[i] += string(line[i])
		}
	}
	return spun
}

func checkBlock(block []string) int {
	cnt, isHori := checkSymm(block)
	if !isHori {
		isVerti := false
		spunBlock := spinBlock(block)
		cnt, isVerti = checkSymm(spunBlock)
		if !isVerti {
			panic("no symmetry found")
		}
	}
	if isHori {
		return cnt * 100
	}
	return cnt
}

func checkSymm(block []string) (int, bool) {
	prevLines := Stack{}
	for l, line := range block {
		if len(prevLines) == 0 {
			prevLines.Push(line)
			continue
		}

		tmpStack := Stack{}
		symm := true
		future := 0
		for {
			tmp, ok := prevLines.Pop()
			if !ok {
				break
			}
			tmpStack.Push(tmp)
			if l+future >= len(block) {
				break
			}
			if tmp != block[l+future] {
				symm = false
				break
			}
			future++
		}
		if symm {
			return l, true
		} else {
			for {
				t, ok := tmpStack.Pop()
				if !ok {
					break
				}
				prevLines.Push(t)
			}
			prevLines.Push(line)
		}

	}
	return 0, false
}

func checkBlock2(block []string) int {
	cnt, isHori := checkSymm2(block)
	if !isHori {
		isVerti := false
		spunBlock := spinBlock(block)
		cnt, isVerti = checkSymm2(spunBlock)
		if !isVerti {
			panic("no symmetry found")
		}
	}
	if isHori {
		return cnt * 100
	}
	return cnt
}

func checkSymm2(block []string) (int, bool) {
	prevLines := Stack{}
	for l, line := range block {
		if len(prevLines) == 0 {
			prevLines.Push(line)
			continue
		}

		tmpStack := Stack{}
		symm := true
		future := 0
		madeFix := false
		for {
			tmp, ok := prevLines.Pop()
			if !ok {
				break
			}
			tmpStack.Push(tmp)
			if l+future >= len(block) {
				break
			}
			test := block[l+future]
			if tmp != test {
				difIdx := singleDifIdx(tmp, test)
				if difIdx == -1 || madeFix {
					symm = false
					break
				}
				madeFix = true
			}
			future++
		}
		if symm && madeFix {
			return l, true
		} else {
			for {
				t, ok := tmpStack.Pop()
				if !ok {
					break
				}
				prevLines.Push(t)
			}
			prevLines.Push(line)
		}

	}
	return 0, false
}

func toBinary(s string) int64 {
	binString := strings.ReplaceAll(s, "#", "1")
	binString = strings.ReplaceAll(binString, ".", "0")
	bin, _ := strconv.ParseInt(binString, 2, 64)
	return bin
}

func singleDifIdx(s1, s2 string) int {
	b1 := toBinary(s1)
	b2 := toBinary(s2)
	bc := b1 ^ b2
	bitLen := len(s1)
	b := fmt.Sprintf("%0"+fmt.Sprint(bitLen)+"b", bc)

	oneIdx := -1
	for i := 0; i < len(b); i++ {
		if b[i] == '1' {
			if oneIdx == -1 {
				oneIdx = i
			} else {
				return -1
			}
		}
	}
	return oneIdx
}

type Stack []string

func (stack *Stack) Push(item string) {
	*stack = append(*stack, item)
}

func (stack *Stack) Pop() (string, bool) {
	if len(*stack) == 0 {
		return "", false
	}
	lastItem := (*stack)[len(*stack)-1]
	*stack = (*stack)[:len(*stack)-1]
	return lastItem, true
}

var testInput13 = ``

var realInput13 = ``
