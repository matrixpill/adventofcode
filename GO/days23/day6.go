package days23

import (
	"aoc/solutions/etc"
	"fmt"
	"math"
	"regexp"
	"strings"
)

var Day6 etc.Solution = etc.Solution{Solve1: solve6_1, Solve2: solve6_2}

func solve6_1() string {
	input := strings.Split(realInput6, "\n")
	numbRegx := regexp.MustCompile(`\d+`)
	times := etc.ToIntArray(numbRegx.FindAllString(input[0], -1))
	recordDists := etc.ToIntArray(numbRegx.FindAllString(input[1], -1))
	margin := 1
	for idx, race := range times {
		cntWins := waysToWinSmort(race, recordDists[idx]+1)
		margin *= cntWins
	}

	return fmt.Sprint(margin)
}

func solve6_2() string {
	input := strings.Split(realInput6, "\n")
	numbRegx := regexp.MustCompile(`\d+`)
	input[0] = strings.ReplaceAll(input[0], " ", "")
	input[1] = strings.ReplaceAll(input[1], " ", "")
	times := etc.ToIntArray(numbRegx.FindAllString(input[0], -1))
	recordDists := etc.ToIntArray(numbRegx.FindAllString(input[1], -1))
	margin := 1
	for idx, race := range times {
		cntWins := waysToWinSmort(race, recordDists[idx]+1)
		margin *= cntWins
	}

	return fmt.Sprint(margin)
}

//lint:ignore U1000 Ignore unused function temporarily for debugging
func waysToWin(time, target int) int {
	cnt := 0
	unEvenTime := time%2 != 0
	for i := 1; i <= time/2; i++ {
		dist := (time - i) * i
		if dist >= target {
			cnt++
		}
	}
	corr := 0
	if !unEvenTime {
		corr = 1
	}
	return cnt*2 - corr
}

func waysToWinSmort(timeI, targetI int) int {
	time, target := float64(timeI), float64(targetI)
	higher := (time + math.Sqrt(math.Pow(time, 2)-4*target)) / 2
	lower := (time - math.Sqrt(math.Pow(time, 2)-4*target)) / 2
	higher = math.Floor(higher)
	lower = math.Ceil(lower)

	return int(higher - lower + 1)
}

//lint:ignore U1000 Ignore unused function temporarily for debugging
var testInput6 string = ``

var realInput6 string = ``
