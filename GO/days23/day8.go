package days23

import (
	"aoc/solutions/etc"
	"fmt"
	"regexp"
	"strings"
)

var Day8 = etc.Solution{Solve1: solve8_1, Solve2: solve8_2}

type node struct {
	name  string
	left  string
	right string
}

var nodeRegx = regexp.MustCompile("[0-9A-Z]{3}")

func parseNodes(input string) (map[string]node, string) {

	nodeMap := map[string]node{}

	lines := strings.Split(input, "\n")

	for _, line := range lines[2:] {
		if !nodeRegx.MatchString(line) {
			continue
		}

		nodes := nodeRegx.FindAllString(line, -1)
		nodeMap[nodes[0]] = node{name: nodes[0], left: nodes[1], right: nodes[2]}
	}

	return nodeMap, lines[0]
}

func follow(nodes map[string]node, start, end, instr string) int {
	cnt := 0

	currentNode := nodes[start]
	for i := 0; i < len(instr); i++ {
		cnt++
		if instr[i] == 'L' {
			currentNode = nodes[currentNode.left]
		} else {
			currentNode = nodes[currentNode.right]
		}

		if currentNode.name == end {
			break
		}

		if i == len(instr)-1 {
			i = -1
		}
	}

	return cnt
}

func getCycleLength(nodes map[string]node, start, instr string) int {
	cnt := 0
	currentNode := nodes[start]
	endIdx := -1
	for i := 0; i < len(instr); i++ {
		cnt++
		if instr[i] == 'L' {
			currentNode = nodes[currentNode.left]
		} else {
			currentNode = nodes[currentNode.right]
		}

		if currentNode.name[2] == 'Z' {
			if endIdx == -1 {
				endIdx = i
				cnt = 0
			} else {
				if endIdx == i {
					break
				}
			}
		}

		if i == len(instr)-1 {
			i = -1
		}
	}
	return cnt
}

func followAsGhost(nodes map[string]node, instr string) int {

	cycles := []int{}
	for k := range nodes {
		if k[2] == 'A' {
			cycles = append(cycles, getCycleLength(nodes, k, instr))
		}
	}

	return etc.Lcm(cycles)
}

func solve8_1() string {
	nodes, instructions := parseNodes(realInput8)
	return fmt.Sprint(follow(nodes, "AAA", "ZZZ", instructions))
}

func solve8_2() string {
	nodes, instructions := parseNodes(realInput8)
	return fmt.Sprint(followAsGhost(nodes, instructions))
}

var testInput8 = ``

var realInput8 = ``
