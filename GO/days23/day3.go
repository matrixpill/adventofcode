package days23

import (
	"aoc/solutions/etc"
	"fmt"
	"strconv"
	"strings"
)

var Day3 etc.Solution = etc.Solution{Solve1: solve3_1, Solve2: solve3_2}

type Pos struct {
	X int
	Y int
}

func solve3_1() string {
	lines := strings.Split(realInput3, "\n")

	var foundNumbs map[Pos]int = make(map[Pos]int)
	var totalSum = 0
	for y, line := range lines {
		for x := 0; x < len(line); x++ {
			c := line[x]

			if c == '.' || isNumb(c) {
				continue
			}

			adjNumbs := allAdjNumbers(lines, Pos{X: x, Y: y}, foundNumbs)
			for _, numb := range adjNumbs {
				totalSum += numb
			}
		}
	}
	return fmt.Sprint(totalSum)
}

func solve3_2() string {
	lines := strings.Split(realInput3, "\n")

	var foundNumbs map[Pos]int = make(map[Pos]int)
	var totalSum = 0
	for y, line := range lines {
		for x := 0; x < len(line); x++ {
			c := line[x]

			if c == '.' || isNumb(c) || c != '*' {
				continue
			}
			adjNumbers := allAdjNumbers(lines, Pos{X: x, Y: y}, foundNumbs)
			if len(adjNumbers) == 2 {
				totalSum += adjNumbers[0] * adjNumbers[1]
			}
		}
	}
	return fmt.Sprint(totalSum)
}

func allAdjNumbers(lines []string, pos Pos, foundNumbs map[Pos]int) []int {
	var adjNumbers []int = []int{}
	for y := pos.Y - 1; y <= (pos.Y + 1); y++ {
		if y < 0 || y >= len(lines) {
			continue
		}
		for x := pos.X - 1; x <= (pos.X + 1); x++ {
			if x < 0 || x >= len(lines[y]) {
				continue
			}
			var _, found = foundNumbs[Pos{X: x, Y: y}]
			if isNumb(lines[y][x]) && !found {
				var numb = getNumber(lines[y], Pos{X: x, Y: y}, foundNumbs)
				adjNumbers = append(adjNumbers, numb)
			}
		}
	}

	return adjNumbers
}

func getNumber(line string, pos Pos, foundNumbs map[Pos]int) int {
	var lastIdx = pos.X
	for x := pos.X + 1; x < len(line); x++ {
		if !isNumb(line[x]) {
			break
		}
		lastIdx = x
	}

	var x = lastIdx
	var numb = 0
	var cnt = 1
	for x >= 0 && isNumb(line[x]) {
		var val, _ = strconv.Atoi(string(line[x]))
		foundNumbs[Pos{X: x, Y: pos.Y}] = val
		numb += cnt * val
		cnt *= 10
		x--
	}
	return numb
}

func isNumb(b byte) bool {
	return b >= '0' && b <= '9'
}

//lint:ignore U1000 Ignore unused function temporarily for debugging
const testInput3 = ``

const realInput3 = ``
