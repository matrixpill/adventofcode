package days23

import (
	"aoc/solutions/etc"
	"fmt"
	"strings"
)

var Day9 = etc.Solution{Solve1: solve9_1, Solve2: solve9_2}

func solve9_1() string {
	lines := strings.Split(realInput9, "\n")
	sum := 0
	for _, line := range lines {
		history := etc.ToIntArray(strings.Split(line, " "))
		sum += history[len(history)-1] + recursFwd(history)
	}
	return fmt.Sprint(sum)
}

func solve9_2() string {
	lines := strings.Split(realInput9, "\n")
	sum := 0
	for _, line := range lines {
		history := etc.ToIntArray(strings.Split(line, " "))
		sum += history[0] - recursBwd(history)
	}
	return fmt.Sprint(sum)
}

func recursFwd(hist []int) int {
	diffs := []int{}
	allZeroes := true
	for i := 0; i < len(hist)-1; i++ {
		diff := hist[i+1] - hist[i]
		diffs = append(diffs, diff)
		if diff != 0 {
			allZeroes = false
		}
	}
	if !allZeroes {
		return recursFwd(diffs) + diffs[len(diffs)-1]
	}

	return 0
}

func recursBwd(hist []int) int {
	diffs := []int{}
	allZeroes := true
	for i := 0; i < len(hist)-1; i++ {
		diff := hist[i+1] - hist[i]
		diffs = append(diffs, diff)
		if diff != 0 {
			allZeroes = false
		}
	}

	if !allZeroes {
		return diffs[0] - recursBwd(diffs)
	}
	return 0
}

var testInput9 = ``

var realInput9 = ``
