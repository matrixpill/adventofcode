package days23

import (
	"aoc/solutions/etc"
	"fmt"
	"math"
	"strings"
)

var Day10 = etc.Solution{Solve1: solve10_1, Solve2: solve10_2}

func solve10_1() string {
	input := realinput10
	inputLines := strings.Split(input, "\n")

	sIdx := strings.Index(strings.ReplaceAll(input, "\n", ""), "S")
	x := sIdx % len(inputLines[0])
	y := int(math.Floor(float64(sIdx) / float64(len(inputLines[0]))))
	currentPos := Pos{X: x, Y: y}
	cnt := 0
	if checkForConnection(currentPos, north, inputLines) {
		cnt = followPipes(currentPos, north, inputLines)
	} else if checkForConnection(currentPos, east, inputLines) {
		cnt = followPipes(currentPos, east, inputLines)
	} else if checkForConnection(currentPos, south, inputLines) {
		cnt = followPipes(currentPos, south, inputLines)
	} else if checkForConnection(currentPos, west, inputLines) {
		cnt = followPipes(currentPos, west, inputLines)
	}

	return fmt.Sprint((cnt + 1) / 2)

}

func followPipes(currentPos, step Pos, inputLines []string) int {
	cnt := 0
	onPip := currentPos.Add(step)
	lastPip := currentPos

	for {
		pip := inputLines[onPip.Y][onPip.X]
		if pip == 'S' {
			break
		}
		cnt++
		if pipMap[pip][0].Add(onPip) != lastPip {
			lastPip = onPip
			onPip = pipMap[pip][0].Add(onPip)
		} else {
			lastPip = onPip
			onPip = pipMap[pip][1].Add(onPip)
		}
	}

	return cnt
}

func solve10_2() string {
	input := realinput10
	inputLines := strings.Split(input, "\n")
	flatInput := strings.ReplaceAll(input, "\n", "")
	sIdx := strings.Index(flatInput, "S")
	x := sIdx % len(inputLines[0])
	y := int(math.Floor(float64(sIdx) / float64(len(inputLines[0]))))
	currentPos := Pos{X: x, Y: y}
	var pipesInSpace map[Pos]pipSpace
	if checkForConnection(currentPos, north, inputLines) {
		pipesInSpace = followPipesAndExpandToSpace(currentPos, north, inputLines)
	} else if checkForConnection(currentPos, east, inputLines) {
		pipesInSpace = followPipesAndExpandToSpace(currentPos, east, inputLines)
	} else if checkForConnection(currentPos, south, inputLines) {
		pipesInSpace = followPipesAndExpandToSpace(currentPos, south, inputLines)
	} else if checkForConnection(currentPos, west, inputLines) {
		pipesInSpace = followPipesAndExpandToSpace(currentPos, west, inputLines)
	}

	maxX, maxY := 0, 0
	for key := range pipesInSpace {
		if key.X > maxX {
			maxX = key.X
		}
		if key.Y > maxY {
			maxY = key.Y
		}
	}

	max := Pos{X: maxX + 1, Y: maxY + 1}
	filled := floodFill(pipesInSpace, max, inputLines)
	result := checkPipSpace(filled, max, inputLines, pipesInSpace)
	return fmt.Sprint(result)
}

func floodFill(pipesInSpace map[Pos]pipSpace, max Pos, real []string) map[Pos]bool {
	filled := map[Pos]bool{}

	for y := 0; y < len(real); y++ {
		for x := 0; x < len(real[y]); x++ {
			if x != 0 && y != 0 && x != len(real[y])-1 && y != len(real)-1 {
				continue
			}
			inPipSpace := toPipSpace(Pos{X: x, Y: y})
			_, ok2 := pipesInSpace[inPipSpace]
			if ok2 {
				continue
			}
			filled[inPipSpace] = false

			filled = fillRec(inPipSpace, pipesInSpace, filled, max)

		}
	}

	return filled
}

func checkPipSpace(filledSpace map[Pos]bool, max Pos, real []string, pipes map[Pos]pipSpace) int {
	cnt := 0
	for y := 0; y <= max.Y; y++ {
		for x := 0; x <= max.X; x++ {
			_, ok := filledSpace[Pos{X: x, Y: y}]
			if ok {
				continue
			}
			_, ok2 := pipes[Pos{X: x, Y: y}]
			if ok2 {
				continue
			}
			_, real := fromPipSpace(Pos{X: x, Y: y})
			if !real {
				continue
			}
			cnt++
		}

	}
	return cnt
}

func fillRec(curr Pos, pipesInSpace map[Pos]pipSpace, filled map[Pos]bool, max Pos) map[Pos]bool {
	filled = fillDirec(north, curr, pipesInSpace, filled, max)
	filled = fillDirec(east, curr, pipesInSpace, filled, max)
	filled = fillDirec(south, curr, pipesInSpace, filled, max)
	filled = fillDirec(west, curr, pipesInSpace, filled, max)
	return filled
}

func fillDirec(direc Pos, curr Pos, piesInSpace map[Pos]pipSpace, filled map[Pos]bool, max Pos) map[Pos]bool {
	n := curr.Add(direc)
	_, alreadyFilled := filled[n]
	_, isPipe := piesInSpace[n]

	if !alreadyFilled && !isPipe && inSpace(n, max) {
		filled[n] = false
		filled = fillRec(n, piesInSpace, filled, max)
	}
	return filled
}

func inSpace(pos Pos, max Pos) bool {
	return pos.X <= max.X && pos.X >= 0 && pos.Y <= max.Y && pos.Y >= 0
}

type pipSpace struct {
	Char byte
	Real bool
}

func printPipesInSpace(pipSpace map[Pos]pipSpace, max Pos) {
	for y := 0; y <= max.Y; y++ {
		for x := 0; x <= max.X; x++ {
			space, ok := pipSpace[Pos{X: x, Y: y}]
			if ok {
				fmt.Print(string(space.Char))
			} else {
				fmt.Print(" ")
			}
		}
		fmt.Println()
	}
}

func printFilledPipSpace(pipSpace map[Pos]bool, max Pos) {
	for y := 0; y <= max.Y; y++ {
		for x := 0; x <= max.X; x++ {
			_, ok := pipSpace[Pos{X: x, Y: y}]
			if ok {
				fmt.Print("O")
			} else {
				fmt.Print(" ")
			}
		}
		fmt.Println()
	}
}

func followPipesAndExpandToSpace(currentPos, step Pos, inputLines []string) map[Pos]pipSpace {
	cnt := 0
	onPip := currentPos.Add(step)
	lastPip := currentPos
	pipSpace := map[Pos]pipSpace{}
	addExtension(currentPos, step, pipSpace, inputLines)
	for {
		pip := inputLines[onPip.Y][onPip.X]
		if pip == 'S' {
			break
		}
		cnt++

		if pipMap[pip][0].Add(onPip) != lastPip {
			addExtension(onPip, pipMap[pip][0], pipSpace, inputLines)
			lastPip = onPip
			onPip = pipMap[pip][0].Add(onPip)
		} else {
			addExtension(onPip, pipMap[pip][1], pipSpace, inputLines)
			lastPip = onPip
			onPip = pipMap[pip][1].Add(onPip)
		}
	}

	return pipSpace
}

func addExtension(from, dir Pos, space map[Pos]pipSpace, inputLines []string) {
	fromInSpace := toPipSpace(from)
	space[fromInSpace] = pipSpace{Char: inputLines[from.Y][from.X], Real: true}
	switch dir {
	case north:
		space[fromInSpace.Add(dir)] = pipSpace{Char: '|', Real: false}
	case east:
		space[fromInSpace.Add(dir)] = pipSpace{Char: '-', Real: false}
	case south:
		space[fromInSpace.Add(dir)] = pipSpace{Char: '|', Real: false}
	case west:
		space[fromInSpace.Add(dir)] = pipSpace{Char: '-', Real: false}
	}
}

func toPipSpace(p Pos) Pos {
	return Pos{X: p.X * 2, Y: p.Y * 2}
}

func fromPipSpace(p Pos) (Pos, bool) {
	real := p.X%2 == 0 && p.Y%2 == 0
	return Pos{X: p.X / 2, Y: p.Y / 2}, real
}

func checkForConnection(cur, dir Pos, inputLines []string) bool {
	n := cur.Add(dir)
	if !inSpace(Pos{X: n.X, Y: n.Y}, Pos{X: len(inputLines[0]), Y: len(inputLines)}) {
		return false
	}
	m := pipMap[inputLines[n.Y][n.X]]
	if m == nil {
		return false
	}
	if m[0].Add(n) == cur || m[1].Add(n) == cur {
		return true
	}
	return false
}

func (p1 Pos) Add(p2 Pos) Pos {
	return Pos{X: p1.X + p2.X, Y: p1.Y + p2.Y}
}

func (p1 Pos) Mult(m Pos) Pos {
	return Pos{X: p1.X * m.X, Y: p1.Y * m.Y}
}

var north = Pos{X: 0, Y: -1}
var east = Pos{X: 1, Y: 0}
var south = Pos{X: 0, Y: 1}
var west = Pos{X: -1, Y: 0}

var pipMap = map[byte][]Pos{
	'|': {north, south},
	'-': {east, west},
	'L': {north, east},
	'J': {north, west},
	'7': {south, west},
	'F': {south, east},
}

var testinput10 = ``

var realinput10 = ``
