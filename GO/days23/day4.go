package days23

import (
	"aoc/solutions/etc"
	"fmt"
	"regexp"
	"slices"
	"strconv"
	"strings"
)

var Day4 etc.Solution = etc.Solution{Solve1: solve4_1, Solve2: solve4_2}

type Card struct {
	Id             int
	WinningNumbers []int
	MyNumbers      []int
}

var idReg *regexp.Regexp = regexp.MustCompile(`(\d+):`)

func splitFunc(c rune) bool {
	return c == ' '
}

func getCards() []Card {
	cards := []Card{}
	cardlines := strings.Split(realInput4, "\n")
	for _, card := range cardlines {
		id, _ := strconv.Atoi(idReg.FindStringSubmatch(card)[1])
		tmpCard := Card{Id: id}
		colIdx := strings.Index(card, ":")
		numbers := card[colIdx+1:]
		numbersArr := strings.Split(numbers, "|")
		winning := strings.FieldsFunc(numbersArr[0], splitFunc)
		my := strings.FieldsFunc(numbersArr[1], splitFunc)
		tmpCard.MyNumbers = etc.ToIntArray(my)
		tmpCard.WinningNumbers = etc.ToIntArray(winning)
		cards = append(cards, tmpCard)
	}
	return cards
}

func solve4_1() string {
	var sum int64 = 0
	for _, card := range getCards() {
		points, _ := card.calcPoints()
		sum += points
	}
	return fmt.Sprint(sum)
}

func solve4_2() string {
	cards := getCards()
	cardCounts := []int{}
	for i := 0; i < len(cards); i++ {
		cardCounts = append(cardCounts, 1)
	}

	for cardIdx, card := range cards {
		_, cnt := card.calcPoints()
		for i := 1; i <= cnt; i++ {
			cardCounts[cardIdx+i] += 1 * cardCounts[cardIdx]
		}
	}

	var sum int = 0
	for _, cnt := range cardCounts {
		sum += cnt
	}
	return fmt.Sprint(sum)
}

func (card Card) calcPoints() (int64, int) {
	var points int64 = 0
	matches := 0
	for _, number := range card.MyNumbers {
		if slices.Contains(card.WinningNumbers, number) {
			if points == 0 {
				points = 1
			} else {
				points *= 2
			}
			matches++
		}
	}
	return points, matches
}

//lint:ignore U1000 Ignore unused function temporarily for debugging
var testInput4 = ``

var realInput4 = ``
