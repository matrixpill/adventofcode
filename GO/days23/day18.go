package days23

import (
	"aoc/solutions/etc"
	"fmt"
	"strconv"
	"strings"
)

var Day18 = etc.Solution{Solve1: solve18_1, Solve2: solve18_2}

func solve18_1() string {
	digPlan := strings.Split(realInput18, "\n")
	min := Pos{}
	max := Pos{}
	perim := []Pos{}
	currentPos := Pos{X: 0, Y: 0}
	for _, op := range digPlan {
		opSplit := strings.Split(op, " ")
		cnt, _ := strconv.Atoi(opSplit[1])
		var direction Pos
		switch opSplit[0] {
		case "U":
			direction = north
		case "R":
			direction = east
		case "D":
			direction = south
		case "L":
			direction = west
		default:
			panic("oh no")
		}

		for i := 0; i < cnt; i++ {
			currentPos = currentPos.Add(direction)
			if currentPos.X > max.X {
				max.X = currentPos.X
			}
			if currentPos.Y > max.Y {
				max.Y = currentPos.Y
			}
			if currentPos.X < min.X {
				min.X = currentPos.X
			}
			if currentPos.Y < min.Y {
				min.Y = currentPos.Y
			}
			perim = append(perim, currentPos)
		}
	}
	//fill18(perim, Pos{X: 1, Y: 1})
	area := shoeLace(perim, min)
	i := int(area - float64(len(perim)/2) + 1)
	// fill18(perim, Pos{X: 1, Y: 1})
	return fmt.Sprint(i + len(perim))
	// return fmt.Sprint(len(perim))
}

func solve18_2() string {
	digPlan := strings.Split(realInput18, "\n")
	min := Pos{}
	perim := []Pos{}
	currentPos := Pos{X: 0, Y: 0}
	for _, op := range digPlan {
		opSplit := strings.Split(op, " ")
		cnt, _ := strconv.ParseInt(opSplit[2][2:7], 16, 64)

		var direction Pos
		switch opSplit[2][7:8] {
		case "3":
			direction = north
		case "0":
			direction = east
		case "1":
			direction = south
		case "2":
			direction = west
		default:
			panic("oh no")
		}
		// perim = append(perim, currentPos.Add(direction))
		// perim = append(perim, currentPos.Add(direction.Mult(Pos{X: int(cnt), Y: int(cnt)})))
		// min = CheckMin(perim[len(perim)-2], min)
		// min = CheckMin(perim[len(perim)-1], min)
		// currentPos = perim[len(perim)-1]
		var i int64 = 0
		for ; i < cnt; i++ {
			currentPos = currentPos.Add(direction)
			if currentPos.X < min.X {
				min.X = currentPos.X
			}
			if currentPos.Y < min.Y {
				min.Y = currentPos.Y
			}
			perim = append(perim, currentPos)
		}
	}
	area := shoeLace(perim, min)
	i := int((area + float64(len(perim)/2) + 1))
	fmt.Printf("%s = %s + (%s/%d) + %d\n", "result", "area", "boundary", 2, 1)
	fmt.Printf("%d = %.f + (%d/%d) + %d\n", i, area, len(perim), 2, 1)
	fmt.Println()
	i2 := int(float64(len(perim)) + area - float64(len(perim)/2) + 1)
	fmt.Printf("%s = %s + %s - (%s/%d) + %d\n", "result", "boundary", "area", "boundary", 2, 1)
	fmt.Printf("%d = %d + %.f - (%d/%d) + %d\n", i2, len(perim), area, len(perim), 2, 1)
	return fmt.Sprint(i)
}

func CheckMin(p, min Pos) Pos {
	if p.X < min.X {
		min.X = p.X
	}
	if p.Y < min.Y {
		min.Y = p.Y
	}
	return min
}

func shoeLace(perim []Pos, min Pos) float64 {
	sum := 0
	minPos := Pos{X: etc.Abs(min.X), Y: etc.Abs(min.Y)}
	for i, p := range perim {
		curr := p.Add(minPos)
		var next Pos
		if i == len(perim)-1 {
			next = perim[0].Add(minPos)
		} else {
			next = perim[i+1].Add(minPos)
		}
		sum += (curr.Y + next.Y) * (curr.X - next.X)
	}
	return float64(sum) / float64(2)
}

func fill18(perim map[Pos]string, pos Pos) {
	fillDirec18(perim, pos, north)
	fillDirec18(perim, pos, east)
	fillDirec18(perim, pos, south)
	fillDirec18(perim, pos, west)
}

func fillDirec18(perim map[Pos]string, pos, direc Pos) {
	n := pos.Add(direc)
	_, isHole := perim[n]
	if !isHole {
		perim[n] = "#"
		fill18(perim, n)
	}
}

func printPerim(perim map[Pos]string, min, max Pos) string {
	var sb strings.Builder
	for y := min.Y; y <= max.Y; y++ {
		for x := min.X; x <= max.X; x++ {
			p, there := perim[Pos{X: x, Y: y}]
			if there {
				sb.WriteString(p)
			} else {
				sb.WriteString(" ")
			}
		}
		sb.WriteString("\n")
	}
	//os.Remove("perim")
	//os.WriteFile("perim", []byte(fileString), os.ModeTemporary)
	return sb.String()
}

var testInput18 = ``

var realInput18 = ``
