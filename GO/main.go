package main

import (
	"aoc/solutions/days23"
	"aoc/solutions/etc"
	"fmt"
)

func main() {
	solve(days23.Day20)
}

func solve(solution etc.Solution) {
	if solution.Solve1 != nil {
		fmt.Println(etc.RunTimed(solution.Solve1))
	}

	if solution.Solve2 != nil {
		fmt.Println(etc.RunTimed(solution.Solve2))
	}
}
