package etc

type Solution struct {
	Solve1 func() string
	Solve2 func() string
}
