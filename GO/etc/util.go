package etc

import (
	"fmt"
	"reflect"
	"runtime"
	"slices"
	"sort"
	"strconv"
	"time"
)

func GetFunctionName(i interface{}) string {
	return runtime.FuncForPC(reflect.ValueOf(i).Pointer()).Name()
}

func RunTimed[T any](fu func() T) (T, string) {
	fmt.Println(GetFunctionName(fu))
	start := time.Now()
	result := fu()
	end := time.Now()
	elapsed := end.UnixMilli() - start.UnixMilli()
	// fmt.Println(fmt.Sprint(elapsed) + " ms\n")
	return result, (fmt.Sprint(elapsed) + " ms")
}

func ToIntArray(stringList []string) []int {
	intList := []int{}
	for _, text := range stringList {
		number, _ := strconv.Atoi(text)
		intList = append(intList, number)
	}
	return intList
}

func Gcd(ints []int) int {
	sort.Slice(ints, func(i, j int) bool { return ints[i] < ints[j] })
	greatest := 1
	for i := 2; i < ints[0]/2; i++ {
		all := true
		for _, numb := range ints {
			if numb%i != 0 {
				all = false
				break
			}
		}
		if all {
			greatest = i
		}
	}

	return greatest

}

func Lcm(ints []int) int {
	intCnt := len(ints)
	if intCnt == 1 {
		return ints[0]
	}
	if intCnt == 0 {
		return 0
	}
	largest := ints[0]
	for i := 1; i < intCnt; i++ {
		a, b := largest, ints[i]
		largest = Abs(a) * (Abs(b) / Gcd([]int{a, b}))
	}
	return largest
}

func Abs(a int) int {
	if a < 0 {
		return -a
	}
	return a
}

func SlowLcm(ints []int) int {

	sort.Slice(ints, func(i, j int) bool { return ints[i] < ints[j] })
	baseNumbs := slices.Clone(ints)
	for {
		same := true
		for _, numb := range ints {
			if ints[0] != numb {
				same = false
				break
			}
		}
		if same {
			break
		}
		for i, numb := range ints {
			if i < len(ints)-1 {
				if ints[i+1] > numb {
					ints[i] += baseNumbs[i]
					break
				}
			} else {
				ints[i] += baseNumbs[i]
			}
		}
	}
	return ints[0]
}
