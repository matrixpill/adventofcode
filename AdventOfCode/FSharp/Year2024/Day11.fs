namespace FSharp.Year2024

open System
open AoCUtils.Interfaces

[<type: Day(11)>]
type Day11() as me =
    inherit Solution()

    let intLen int1 = int64 (Math.Log10(float int1)) + 1L

    let intPow (a: int64) (b: int64) =
        let mutable result = 1L

        for _ in [| 0L .. (b - 1L) |] do
            result <- result * a

        result

    let blinkStone (stoneV: int64) =
        if stoneV = 0 then
            [| 1L |]
        else if intLen stoneV % 2L = 0 then
            let divisor = intPow 10 ((intLen stoneV) / 2L)
            let sec = stoneV % divisor
            let first = Math.Floor(stoneV / divisor) |> int64
            [| first; sec |]
        else
            [| stoneV * 2024L |]
    //https://github.com/Inkkonu/AdventOfCode/blob/master/aoc2024/Day11.py
    let rec addStoneToMap map stone cnt = map |> Map.add stone cnt

    let stonesToMap stones =
        (Map.empty, stones) ||> Array.fold (fun m s -> addStoneToMap m s 1L)

    let existingCountOrZero (num: int64) (map: Map<int64, int64>) =
        if map |> Map.containsKey num then
            map |> Map.find num
        else
            0

    let rec moveStones (stoneMap: Map<int64, int64>) until cnt =
        let cnt = cnt + 1

        let newStones =
            (Map.empty, stoneMap)
            ||> Map.fold (fun state num count ->
                (state, blinkStone num)
                ||> Array.fold (fun state nn -> state |> (Map.add nn ((state |> existingCountOrZero nn) + count))))

        if cnt = until then
            newStones
        else
            moveStones newStones until cnt



    let Task1 () =
        let stones = me.Input[0].Split " " |> Array.map int64
        let stones = moveStones (stonesToMap stones) 25 0
        (0L, stones) ||> Map.fold (fun sum _ count -> sum + count)

    let Task2 () =
        let stones = me.Input[0].Split " " |> Array.map int64
        let stones = moveStones (stonesToMap stones) 75 0
        (0L, stones) ||> Map.fold (fun sum _ count -> sum + count)

    override me.SolvePart1() : string = $"%i{Task1()}"
    override me.SolvePart2() : string = $"%i{Task2()}"
