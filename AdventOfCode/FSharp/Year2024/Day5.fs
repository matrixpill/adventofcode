namespace FSharp.Year2024

open System
open AoCUtils.Interfaces

exception SomError of string

[<type: Day(5)>]
type Day5() as me =
    inherit Solution()

    let getRules allLines =
        allLines
        |> Array.takeWhile (fun x -> x <> "")
        |> Array.map (fun rule ->
            match rule.ToCharArray() with
            | [| n1; n2; _; n3; n4 |] -> ($"{n1}{n2}" |> int, $"{n3}{n4}" |> int)
            | [||] -> raise (SomError(""))
            | _ -> failwith "naah")

    let isCorrect (rules: Set<int * int>) update =
        update
        |> Array.indexed
        |> Array.forall (fun (i, first) ->
            update
            |> Array.skip (i + 1)
            |> Array.forall (fun second -> rules |> Set.contains (first, second)))

    let getMiddle (update: int array) =
        let middle = Math.Floor(decimal update.Length / 2m) |> int
        update[middle]

    let getUpdates (updates: string array) =
        updates
        |> Array.map (fun s ->
            s.Split(',', StringSplitOptions.RemoveEmptyEntries)
            |> Array.map (fun x -> x |> int))

    let getRulesAdnUpdates () =
        let rules = getRules me.Input |> Set.ofArray
        let updates = me.Input |> Array.skip (rules.Count + 1) |> getUpdates
        (rules, updates)

    let countPosInRules rules num =
        ((0, 0), rules)
        ||> Set.fold (fun s x ->
            if fst x = num then ((fst s) + 1, snd s)
            elif snd x = num then (fst s, (snd s) + 1)
            else s)


    let fixUpdate (rules: Set<int * int>) (update: int array) : int array =
        let onlyApplicapleRules =
            rules
            |> Set.filter (fun (r1, r2) -> update |> Array.exists ((=) r1) && update |> Array.exists ((=) r2))

        let count = countPosInRules onlyApplicapleRules
        let counted = update |> Array.map (fun x -> (count x, x))
        let sorted = counted |> Array.sortByDescending (fun (c, _) -> fst c)
        sorted |> Array.map snd


    let Task1 () =
        let rules, updates = getRulesAdnUpdates ()
        updates |> Array.filter (isCorrect rules) |> Array.map getMiddle |> Array.sum

    let Task2 () =
        let rules, updates = getRulesAdnUpdates ()

        updates
        |> Array.filter (fun x -> not (isCorrect rules x))
        |> Array.map (fixUpdate rules)
        |> Array.map getMiddle
        |> Array.sum

    override me.SolvePart1() : string = $"%i{Task1()}"
    override me.SolvePart2() : string = $"%i{Task2()}"
