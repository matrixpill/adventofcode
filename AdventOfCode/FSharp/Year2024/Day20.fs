namespace FSharp.Year2024

open System
open AoCUtils.Interfaces
type RTVec = { X: int; Y: int }

type RaceTrack =
    { End: RTVec
      Racer: RTVec
      Path: Set<RTVec>
      PathSec: Map<RTVec, int>
      Cheats: Map<int, int> }


[<type: Day(20)>]
type Day20() as me =
    inherit Solution()

    let parseTrack (input: string array) : RaceTrack =
        let track =
            { Racer = { X = 0; Y = 0 }
              End = { X = 0; Y = 0 }
              Path = Set.empty
              PathSec = Map.empty
              Cheats = Map.empty }

        (track, input |> Array.indexed)
        ||> Array.fold (fun maz (y, row) ->
            (maz, row.ToCharArray() |> Array.indexed)
            ||> Array.fold (fun tr (x, c) ->

                let tpos = [ { X = x; Y = y } ]

                match c with
                | '.' ->
                    { tr with
                        Path = (tr.Path, tpos) ||> List.fold (fun path p -> path |> Set.add p) }
                | 'S' -> { tr with Racer = tpos[0] }
                | 'E' ->
                    { tr with
                        End = tpos[0]
                        Path = (tr.Path, tpos) ||> List.fold (fun path p -> path |> Set.add p) }
                | _ -> tr))

    let add p1 p2 = { X = p1.X + p2.X; Y = p1.Y + p2.Y }

    let moves =
        [ { X = 0; Y = -1 }; { X = 1; Y = 0 }; { X = 0; Y = 1 }; { X = -1; Y = 0 } ]

    let rec raceAndCheat maxCheat track  =
        let nTrack =
            let pairs = List.allPairs [-maxCheat..maxCheat] [-maxCheat..maxCheat]// |> List.map (fun (x, y) -> {X = x; Y = y})
            (track, pairs)
            ||> List.fold (fun tr (x, y) ->
                let prevP = add {X = x; Y = y} tr.Racer
                let dist = Math.Abs(prevP.X - tr.Racer.X) + Math.Abs(prevP.Y - tr.Racer.Y)
                if tr.PathSec |> Map.containsKey prevP && dist <= maxCheat then
                    let saved = tr.PathSec.Count - tr.PathSec[prevP] - dist
                    if saved > 0 then
                        if tr.Cheats |> Map.containsKey saved then
                            { tr with
                                Cheats = tr.Cheats |> Map.add saved (tr.Cheats[saved] + 1) }
                        else
                            { tr with
                                Cheats = tr.Cheats |> Map.add saved 1 }
                    else
                        tr
                else
                    tr)

        if nTrack.End = nTrack.Racer then
            nTrack
        else
            let nxt =
                try
                    moves
                    |> List.find (fun dir ->
                        let nxt = add dir nTrack.Racer
                        nTrack.Path |> Set.contains nxt && not (nTrack.PathSec |> Map.containsKey nxt))
                with e ->
                    { X = 0; Y = 0 }

            raceAndCheat maxCheat
                { nTrack with
                    Racer = add nxt nTrack.Racer
                    PathSec = nTrack.PathSec |> Map.add track.Racer track.PathSec.Count } 


    let Task1 () =
        let track = parseTrack me.Input |> raceAndCheat 2
        (0, track.Cheats |> Map.filter (fun k v -> k >= 100))
        ||> Map.fold (fun sum ck cv -> sum + cv)

    let Task2 () =
        let track = parseTrack me.Input |> raceAndCheat 20
        (0, track.Cheats |> Map.filter (fun k v -> k >= 100))
        ||> Map.fold (fun sum ck cv -> sum + cv)

    override me.SolvePart1() : string = $"%i{Task1()}"
    override me.SolvePart2() : string = $"%i{Task2()}"
