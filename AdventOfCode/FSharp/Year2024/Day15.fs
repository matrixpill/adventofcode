namespace FSharp.Year2024

open System
open System.Text
open AoCUtils.Interfaces


type WVec = { X: int; Y: int }

type WBox = { poss: WVec list }

type Warehouse =
    { Size: WVec
      Rob: WVec
      Walls: Set<WVec>
      Boxes: Set<WBox>
      BoxMap: Map<WVec, WBox> }

[<type: Day(15)>]
type Day15() as me =
    inherit Solution()

    let parseWareHouse (map: string array) isWide : Warehouse =
        let wareHouse =
            { Size =
                { X = if isWide then map[0].Length * 2 else map[0].Length
                  Y = map.Length }
              Boxes = Set.empty
              Rob = { X = 0; Y = 0 }
              Walls = Set.empty
              BoxMap = Map.empty }

        (wareHouse, map |> Array.indexed)
        ||> Array.fold (fun wh (y, row) ->
            (wh, row.ToCharArray() |> Array.indexed)
            ||> Array.fold (fun wh (x, c) ->

                let boxPositions =
                    if isWide then
                        [ { X = x * 2; Y = y }; { X = x * 2 + 1; Y = y } ]
                    else
                        [ { X = x; Y = y } ]

                match c with
                | '#' ->
                    { wh with
                        Walls = (wh.Walls, boxPositions) ||> List.fold (fun walls bp -> walls |> Set.add bp) }
                | '@' -> { wh with Rob = boxPositions[0] }
                | 'O' ->
                    let newB = { poss = boxPositions }

                    { wh with
                        Boxes = wh.Boxes |> Set.add newB
                        BoxMap = (wh.BoxMap, boxPositions) ||> List.fold (fun map bp -> map |> Map.add bp newB) }
                | _ -> wh

            ))

    let dir c =
        match c with
        | '^' -> { WVec.X = 0; Y = -1 }
        | '>' -> { WVec.X = 1; Y = 0 }
        | 'v' -> { WVec.X = 0; Y = 1 }
        | '<' -> { WVec.X = -1; Y = 0 }
        | _ -> failwith "no direction"

    let addWVec v1 v2 = { X = v1.X + v2.X; Y = v1.Y + v2.Y }

    let printWh wh =
        let mutable sb = StringBuilder()

        for y in [ 0 .. (wh.Size.Y - 1) ] do
            for x in [ 0 .. (wh.Size.X - 1) ] do
                let pos = { X = x; Y = y }

                if wh.Walls.Contains pos then
                    sb <- sb.Append "#"
                else if wh.BoxMap.ContainsKey pos then
                    if wh.BoxMap[pos].poss.Length = 2 then
                        if wh.BoxMap[pos].poss[0] = pos then
                            sb <- sb.Append "["
                        else
                            sb <- sb.Append "]"
                    else
                        sb <- sb.Append "O"
                else if wh.Rob = pos then
                    sb <- sb.Append "@"
                else
                    sb <- sb.Append "."

            sb <- sb.Append "\n"

        Console.CursorTop <- 0
        Console.CursorLeft <- 0
        printfn $"{sb.ToString()}"
        wh

    let moveBox wh curr dir =
        let newBox =
            { curr with
                poss = curr.poss |> List.map (addWVec dir) }

        let newBoxMap =
            (wh.BoxMap, curr.poss) ||> List.fold (fun map bp -> map |> Map.remove bp)

        let newBoxMap =
            (newBoxMap, newBox.poss) ||> List.fold (fun map bp -> map |> Map.add bp newBox)

        { wh with
            Boxes = wh.Boxes |> Set.remove curr |> Set.add newBox
            BoxMap = newBoxMap }

    let rec pushBoxes wh dir curr =
        if wh.Walls |> Set.contains curr then
            (false, wh)
        else if wh.BoxMap |> Map.containsKey curr then
            let box = wh.BoxMap[curr]

            let alteredWh =
                if dir.Y <> 0 then
                    ((true, wh), box.poss)
                    ||> List.fold (fun (f, wh) b -> if f then pushBoxes wh dir (addWVec b dir) else (f, wh))
                else
                    let i = if dir.X = 1 && box.poss.Length = 2 then 1 else 0
                    pushBoxes wh dir (addWVec box.poss[i] dir)

            if fst alteredWh then
                (true, moveBox (snd alteredWh) wh.BoxMap[curr] dir)
            else
                (false, wh)
        else
            (true, wh)

    let rec makeMove wh move =
        let nWh =
            let np = addWVec wh.Rob (dir move)

            if wh.Walls |> Set.contains np then
                wh
            else if wh.BoxMap |> Map.containsKey np then
                let res = (pushBoxes wh (dir move) np)
                if fst res then { snd res with Rob = np } else snd res
            else
                { wh with Rob = np }
        // Task.Delay(50).Wait()
        // printWh nWh
        nWh

    let rec makeMoves wh moves = (wh, moves) ||> Seq.fold makeMove

    let calcGPS (boxes: Set<WBox>) =
        boxes
        |> Set.map (fun b -> 100 * b.poss[0].Y + b.poss[0].X)
        |> Set.toArray
        |> Array.sum


    let waitForRobot isWide =
        let wareHouse =
            parseWareHouse (me.Input |> Array.takeWhile (fun x -> x <> "")) isWide

        let moves =
            me.Input
            |> Array.skip wareHouse.Size.Y
            |> String.concat ""
            |> _.ToCharArray()
            |> Seq.ofArray

        let wh = makeMoves wareHouse moves
        calcGPS wh.Boxes

    let Task1 () = waitForRobot false
    let Task2 () = waitForRobot true


    override me.SolvePart1() : string =
        Console.Clear()
        $"%i{Task1()}"

    override me.SolvePart2() : string =
        Console.Clear()
        $"%i{Task2()}"
