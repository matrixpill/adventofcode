namespace FSharp.Year2024

open System
open AoCUtils.Interfaces

type Plant = { pos: int * int; name: char }

type Region =
    { name: char
      size: int
      circu: Map<int * int, (int * int) List> }

type PlantWall =
    { pos: int * int
      wallDirection: int * int }

[<type: Day(12)>]
type Day12() as me =
    inherit Solution()
    let directions = [ (0, -1); (1, 0); (0, 1); (-1, 0) ]

    let newRegion: Region =
        { name = ' '
          size = 1
          circu = Map.empty }

    let addV v1 v2 = (fst v1 + fst v2, snd v1 + snd v2)
    let subV v1 v2 = (fst v1 - fst v2, snd v1 - snd v2)

    let rec comb n l =
        match n, l with
        | 0, _ -> [ [] ]
        | _, [] -> []
        | k, (x :: xs) -> List.map ((@) [ x ]) (comb (k - 1) xs) @ comb k xs

    let inBounds (bounds: string array) (pos: int * int) =
        let x, y = pos
        x >= 0 && x < bounds[0].Length && y >= 0 && y < bounds.Length

    let rec calcRegion (map: string array) (visited: Set<int * int>) (region: Region) (current: Plant) =
        ((region, visited), directions)
        ||> List.fold (fun (region, visited) dir ->

            let np = addV current.pos dir

            if not (inBounds map np) || map[snd np][fst np] <> current.name then
                let newCir =
                    if region.circu |> Map.containsKey current.pos then
                        region.circu |> Map.find current.pos
                    else
                        []

                ({ region with
                    circu = region.circu |> Map.add current.pos (dir :: newCir) },
                 visited)
            else if map[snd np][fst np] = current.name && not (visited.Contains np) then
                calcRegion map (visited.Add np) { region with size = region.size + 1 } { current with pos = np }
            else
                (region, visited))

    let countItemsInMap (map: Map<int * int, (int * int) List>) =
        (0, map) ||> Map.fold (fun s _ l -> s + l.Length)

    let mapRegions (map: string array) =
        let plants =
            map
            |> Array.mapi (fun y row -> row.ToCharArray() |> Array.mapi (fun x c -> { name = c; pos = (x, y) }))
            |> Array.concat

        let areas =
            ((0, Set.empty), plants)
            ||> Array.fold (fun (sum, visited) plant ->
                if visited.Contains plant.pos then
                    (sum, visited)
                else
                    let regPrice, nVis =
                        calcRegion map (visited.Add plant.pos) { newRegion with name = plant.name } plant

                    (sum + (countItemsInMap regPrice.circu) * regPrice.size, nVis))

        fst areas

    let rec countSides (all: ((int * int) * List<int * int>) list) =
        let allPairs = comb 2 all

        let r =
            allPairs
            |> List.map (fun l ->

                let v1 = l[0]
                let v2 = l[1]
                let diff = subV (fst v1) (fst v2)

                if Math.Abs(fst diff) = 1 && Math.Abs(snd diff) = 1 then
                    if fst diff = snd diff then
                        //X.
                        //.X
                        let top, bottom = if fst (fst v1) < fst (fst v2) then (v1, v2) else (v2, v1)

                        let cnt =
                            if
                                snd top |> List.contains directions[1]
                                && snd bottom |> List.contains directions[0]
                            then
                                1
                            else
                                0

                        let cnt2 =
                            if
                                snd top |> List.contains directions[2]
                                && snd bottom |> List.contains directions[3]
                            then
                                1
                            else
                                0

                        if cnt = 1 && cnt2 = 1 then 0
                        else if cnt = 1 || cnt2 = 1 then 1
                        else 0
                    else
                        //.X
                        //X.
                        let top, bottom = if fst (fst v1) > fst (fst v2) then (v1, v2) else (v2, v1)

                        let cnt =
                            if
                                snd top |> List.contains directions[3]
                                && snd bottom |> List.contains directions[0]
                            then
                                1
                            else
                                0

                        let cnt2 =
                            if
                                snd top |> List.contains directions[2]
                                && snd bottom |> List.contains directions[1]
                            then
                                1
                            else
                                0

                        if cnt = 1 && cnt2 = 1 then 0
                        else if cnt = 1 || cnt2 = 1 then 1
                        else 0

                else
                    0)


        r |> List.sum



    ///   0 1 2 3
    /// 0   X|_
    /// 1     X
    /// 2
    /// 3
    ///
    ///
    let sideS (region: Region) =

        let outerCorners =
            region.circu
            |> Map.map (fun x v ->
                match v.Length with
                | 2 when
                    (directions |> (List.findIndex (fun x -> x = v[0]))) % 2 = 0
                    && (directions |> (List.findIndex (fun x -> x = v[1]))) % 2 = 1
                    || (directions |> (List.findIndex (fun x -> x = v[0]))) % 2 = 1
                       && (directions |> (List.findIndex (fun x -> x = v[1]))) % 2 = 0
                    ->
                    1
                | 3 -> 2
                | 4 -> 4
                | _ -> 0)
            |> Map.fold (fun s _ v -> s + v) 0
        let inner = countSides (region.circu |> Map.toList)
        inner + outerCorners

    let mapRegions2 (map: string array) =

        let plants =
            map
            |> Array.mapi (fun y row -> row.ToCharArray() |> Array.mapi (fun x c -> { name = c; pos = (x, y) }))
            |> Array.concat

        let areas =
            ((0, Set.empty), plants)
            ||> Array.fold (fun (sum, visited) plant ->
                if visited.Contains plant.pos then
                    (sum, visited)
                else
                    let region, nVis =
                        calcRegion map (visited.Add plant.pos) { newRegion with name = plant.name } plant

                    let sides = sideS region
                    (sum + sides * region.size, nVis))

        fst areas

    let Task1 () = mapRegions me.Input
    let Task2 () = mapRegions2 me.Input
    override me.SolvePart1() : string = $"%i{Task1()}"
    override me.SolvePart2() : string = $"%i{Task2()}"
