namespace FSharp.Year2024

open System
open AoCUtils.Interfaces

[<type: Day(1)>]
type Day1() =
    inherit Solution()

    let twoLists (lines: string[]) =
        let g =
            lines
            |> Array.map (fun x -> x.Split(' ', StringSplitOptions.RemoveEmptyEntries) |> Array.map int)

        let first = g |> Array.map (fun x -> x[0]) |> Array.sort
        let second = g |> Array.map (fun x -> x[1]) |> Array.sort
        (first, second)

    let Task1 (d: int array * int array) =
        let first, second = d
        let pairs = first |> Array.indexed |> Array.map (fun (i, x) -> (x, second[i]))

        let diffs = pairs |> Array.map (fun (f, s) -> Math.Abs(f - s))
        diffs |> Array.sum


    let Task2 (d: int array * int array) =
        let first, second = d

        let secondsCounts = second |> Array.countBy id |> Map

        first
        |> Array.sumBy (fun x -> x * (if secondsCounts.ContainsKey x then secondsCounts[x] else 0))

    override me.SolvePart1() : string = $"%i{Task1(twoLists me.Input)}"
    override me.SolvePart2() : string = $"%i{Task2(twoLists me.Input)}"
