namespace FSharp.Year2024

open System
open System.Text.RegularExpressions
open AoCUtils.Interfaces

type MachinePos = { X: int64; Y: int64 }

type Machine =
    { ButtonA: MachinePos
      ButtonB: MachinePos
      Prize: MachinePos }

type V64 = (int64 * int64)

[<type: Day(13)>]
type Day13() as me =
    inherit Solution()


    let buttonAReg =
        Regex(@"Button A: X\+(?<x>\d+), Y\+(?<y>\d+)", RegexOptions.Compiled)

    let buttonBReg =
        Regex(@"Button B: X\+(?<x>\d+), Y\+(?<y>\d+)", RegexOptions.Compiled)

    let prizeReg = Regex(@"Prize: X\=(?<x>\d+), Y\=(?<y>\d+)", RegexOptions.Compiled)

    let rec gcd (a: int64) (b: int64) =
        match a, b with
        | (a, 0L) -> a
        | (a, b) -> gcd b (a % b)

    let lcm a b = a * b / (gcd a b)

    let machines (add: int64) =

        let get (m: Match) (gn: string) = m.Groups[gn].Value |> int64

        seq {
            let all = me.Input |> String.concat "\n"
            let buttonAMatches = all |> buttonAReg.Matches
            let buttonBMatches = all |> buttonBReg.Matches
            let prizeMatches = all |> prizeReg.Matches
            // printfn $"cnt: {prizeMatches.Count}"
            let getA i = get buttonAMatches[i]
            let getB i = get buttonBMatches[i]
            let getP i = get prizeMatches[i]

            for i in [ 0 .. prizeMatches.Count - 1 ] do
                // printfn $"pos {i + 1}"

                yield
                    { Machine.ButtonA = { X = getA i "x"; Y = getA i "y" }
                      Machine.ButtonB = { X = getB i "x"; Y = getB i "y" }
                      Machine.Prize =
                        { X = ((getP i "x") + add)
                          Y = ((getP i "y") + add) } }
        }

    let rec untilSame (x: V64) (xStep: V64) (y: V64) (yStep: V64) =
        if x = y then
            Some(x)
        else if fst x < fst y then




            let times = (fst y - fst x) / fst xStep
            let times = Math.Max(1L, times)

            untilSame (fst x + ((fst xStep) * times), snd x - ((snd xStep) * times)) xStep y yStep
        else
            let times = (fst x - fst y) / fst yStep
            let times = Math.Max(1L, times)
            untilSame x xStep (fst y + ((fst yStep) * times), snd y - ((snd yStep) * times)) yStep


    let rec findFirst (target: int64) (a: int64) b i =
        if i * a > target then
            None
        else if (target - (i * a)) % b = 0L then
            Some(i, (target - (i * a)) / b)
        else
            findFirst target a b (i + 1L)


    let processMachine m : Async<int64> =
        async {
            // printfn $"{m}"

            // https://github.com/janiorca/advent-of-code-2024/blob/main/src/bin/aoc13.rs
            let c = decimal m.Prize.X / decimal m.Prize.Y

            let r =
                (decimal m.ButtonB.Y * c - decimal m.ButtonB.X)
                / (decimal m.ButtonA.X - decimal m.ButtonA.Y * c)

            let xSteps = decimal m.ButtonA.X * r + decimal m.ButtonB.X
            let numBSteps = Math.Round(decimal m.Prize.X / xSteps) |> int64
            let numASteps = Math.Round(decimal m.Prize.X / xSteps * r) |> int64

            let stepPos =
                (m.ButtonA.X * numASteps + m.ButtonB.X * numBSteps, m.ButtonA.Y * numASteps + m.ButtonB.Y * numBSteps)

            if fst stepPos = m.Prize.X && snd stepPos = m.Prize.Y then
                return numASteps * 3L + numBSteps
            else
                return 0L

        // let xGcd = gcd m.ButtonA.X m.ButtonB.X
        // let yGcd = gcd m.ButtonA.Y m.ButtonB.Y
        //
        // let aXstep = m.ButtonA.X / xGcd
        // let aYstep = m.ButtonA.Y / yGcd
        //
        // let bXstep = m.ButtonB.X / xGcd
        // let bYstep = m.ButtonB.Y / yGcd
        //
        // let x = findFirst m.Prize.X m.ButtonA.X m.ButtonB.X 0L
        // let y = findFirst m.Prize.Y m.ButtonA.Y m.ButtonB.Y 0L
        //
        // if x = None || y = None then
        //     return 0L
        // else
        //     let x, y = (x.Value, y.Value)
        //     let same = untilSame x (bXstep, aXstep) y (bYstep, aYstep)
        //
        //     match same with
        //     | Some x -> return (fst x) * 3L + (snd x) * 1L
        //     | _ -> return 0L
        }

    let Task1 () =
        async {
            let! s = machines 0 |> Seq.map processMachine |> Async.Parallel
            return s |> Array.sum
        }

    let Task2 () =
        async {
            let! s = machines 10000000000000L |> Seq.map processMachine |> Async.Parallel
            return s |> Array.sum
        }

    // printfn $"{gcd m.ButtonA.X m.ButtonB.X}"


    override me.SolvePart1() : string =
        $"%i{Task1() |> Async.RunSynchronously}"

    override me.SolvePart2() : string =
        $"%i{Task2() |> Async.RunSynchronously}"


// Target X=8400 Y=5400
// A 94 34
// B 22 67
//
//
// 0 = 8400 - (a * 84) - (b * 22)
// (a * 84) + (b * 22) = 8400
//
//
// (a * 84) + (380 * 22) = 8400
//  (b * 22) = 8400 - (1 * 84)
//  378 * 22 = 8316
//

// x - 3
// y - 3 -> y + 10


// X
// a   b
// 3   369
// 14  322
// 25  275      25 - 3 = 22    369 - 275 = 94
// 80  40
//
// Y
// a   b
// 13  74
//
// 80  40      80 - 13 = 67    74 - 40 = 34
//
//
//
// 38
