namespace FSharp.Year2024

open System
open AoCUtils.Interfaces

[<type: Day(21)>]
type Day21() as me =
    inherit Solution()

    let numericPad =
        [ [ '7'; '8'; '9' ]; [ '4'; '5'; '6' ]; [ '1'; '2'; '3' ]; [ '_'; '0'; 'A' ] ]

    let directionalPad = [ [ '_'; '^'; 'A' ]; [ '<'; 'v'; '>' ] ]

    let mapify inp =
        (Map.empty, inp |> List.indexed)
        ||> List.fold (fun map (y, row) ->
            (map, row |> List.indexed)
            ||> List.fold (fun map (x, char) -> map |> Map.add char (x, y)))

    let calcDist (map: Map<char, int * int>) c1 c2 =
        let x1, y1 = map[c1]
        let x2, y2 = map[c2]
        ((x2 - x1), (y2 - y1))

    let allMoves (calc: char -> char -> int * int) (out: string) =
        out.ToCharArray()
        |> List.ofArray
        |> List.pairwise
        |> List.map (fun cs -> (fst cs, cs ||> calc))

    let rec distribute e =
        function
        | [] -> [ [ e ] ]
        | x :: xs' as xs -> (e :: xs) :: [ for xs in distribute e xs' -> x :: xs ]

    let rec permute =
        function
        | [] -> [ [] ]
        | e :: xs -> List.collect (distribute e) (permute xs)

    let moveToDirs (move: int * int) =
        let x, y = move

        let xDirs =
            [ 1 .. Math.Abs(x) ] |> List.map (fun _ -> if x < 0 then (-1, 0) else (1, 0))

        let yDirs =
            [ 1 .. Math.Abs(y) ] |> List.map (fun _ -> if y < 0 then (0, -1) else (0, 1))

        permute (xDirs @ yDirs) |> List.distinct

    let validate (map: Map<char, int * int>) (inp: char * (int * int) list) =
        let start = map[fst inp]

        not (
            ((false, start), snd inp)
            ||> List.fold (fun (found, pos) x ->
                let n = (fst pos + fst x, snd pos + snd x)
                (found || n = map['_'], n))
            |> fst
        )

    let toChar (coord: int * int) =
        match coord with
        | 0, -1 -> "^"
        | 1, 0 -> ">"
        | 0, 1 -> "v"
        | -1, 0 -> "<"
        | _ -> failwith "todo"

    let toChars (coords: (int * int) list) : string =
        coords |> List.map toChar |> String.concat "" |> (fun x -> x + "A")

    let rec dirLayers dirMap cnt (map: Map<char * char, int>) (out: char * char) =
        let calcDirDist = calcDist dirMap
        let validateDir = validate dirMap
        let dirLayersD = dirLayers dirMap (cnt - 1) map

        let allMoves =
            allMoves calcDirDist $"{fst out}{snd out}"
            |> List.map (fun x ->
                snd x
                |> moveToDirs
                |> List.filter (fun moves -> validateDir (fst x, moves))
                |> List.map toChars)

        let poss =
            (allMoves[0], allMoves[1..])
            ||> List.fold (fun all curr -> curr |> List.map (fun x -> all |> List.map (fun a -> a + x)) |> List.concat)

        if cnt > 0 then
            let x =
                poss
                |> List.map (fun x -> "A" + x)
                |> List.map (fun x -> x.ToCharArray() |> List.ofArray |> List.pairwise |> List.map dirLayersD)

            let map = map |> Map.add out (x |> List.concat |> List.min)
            x |> List.concat |> List.min
        else
            poss |> List.min |> _.Length

    let getComplexity numMap dirMap steps (out: string) =

        let calcNumDist = calcDist numMap
        let validateNum = validate numMap
        let dirLayersD = dirLayers dirMap (steps - 1)

        let allMoves =
            allMoves calcNumDist $"A{out}"
            |> List.map (fun x ->
                snd x
                |> moveToDirs
                |> List.filter (fun moves -> validateNum (fst x, moves))
                |> List.map toChars)

        (allMoves[0], allMoves[1..])
        ||> List.fold (fun all curr -> curr |> List.map (fun x -> all |> List.map (fun a -> a + x)) |> List.concat)
        |> List.map (fun x -> x.ToCharArray() |> List.ofArray |> List.pairwise |> List.map dirLayersD)
        |> List.concat
        |> List.min
        |> (fun x -> (out.Substring(0, out.Length - 1) |> int) * x)


    let Task1 () =
        // let numMap = mapify numericPad
        // let dirMap = mapify directionalPad
        // let calcComp = getComplexity numMap dirMap 2
        // me.Input |> Array.map calcComp |> Array.sum
        0

    let Task2 () =
        let numMap = mapify numericPad
        let dirMap = mapify directionalPad
        let calcComp = getComplexity numMap dirMap 10
        let comp = calcComp "A0"
        printfn $"{comp}"
        me.Input |> Array.map (fun x -> calcComp ("A" + x)) |> Array.sum

    override me.SolvePart1() : string = $"%i{Task1()}"
    override me.SolvePart2() : string = $"%i{Task2()}"

// 029A <<vAA>A>^AvAA<^A>A<<vA>>^AvA^A<vA>^A<<vA>^A>AAvA^A<<vA>A>^AAAvA<^A>A

// A -> 0 = X -1 Y  0   1 2
// 0 -> 2 = X  0 Y -1   1 2
// 2 -> 9 = X  1 Y -2   3 4
// 9 -> A = X  0 Y  3   3 4
//                      8 12
// <A^A>^^AvvvA

// A -> < = X -2 Y  1   3 4
// < -> A = X  2 Y -1   3 4
// A -> ^ = X -1 Y  0   1 2
// ^ -> A = X  1 Y  0   1 2
// A -> > = X  0 Y  1   1 2
// > -> ^ = X -1 Y -1   2 3
// ^ -> ^ = X  0 Y  0   0 1
// ^ -> A = X  1 Y  0   1 2
// A -> v = X -1 Y  1   2 3
// v -> v = X  0 Y  0   0 1
// v -> v = X  0 Y  0   0 1
// v -> A = X  1 Y -1   2 3
//
// v<<A>>^A<A>AvA<^AA>A<vAAA>^A
