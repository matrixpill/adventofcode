namespace FSharp.Year2024

open System
open AoCUtils.Interfaces



[<type: Day(10)>]
type Day10() as me =
    inherit Solution()
    let directions = [ (0, -1); (1, 0); (0, 1); (-1, 0) ]
    let addV v1 v2 = (fst v1 + fst v2, snd v1 + snd v2)

    let inBounds (bounds: int array2d) (pos: int * int) =
        let x, y = pos

        x >= 0
        && x < (bounds |> Array2D.length2)
        && y >= 0
        && y < (bounds |> Array2D.length1)

    let rec trailHead (map: int array2d) (pos: int * int) =
        let x, y = pos
        let currV = Array2D.get map y x

        let nines =
            directions
            |> List.map (fun d ->
                let np = addV pos d

                if inBounds map np then
                    let nv = Array2D.get map (snd np) (fst np)
                    if nv = currV + 1 then
                        if nv = 9 then [ np ] else trailHead map np
                    else
                        []
                else
                    [])
            |> List.concat

        nines

    let Task1 () =
        let map =
            array2D (
                me.Input
                |> Array.map (fun row -> row.ToCharArray() |> Array.map (fun c -> Char.GetNumericValue(c) |> int))
            )

        map
        |> Array2D.mapi (fun y x v ->
            if v = 0 then
                let n = trailHead map (x, y) |> List.distinct
                n |> List.length
            else
                0)
        |> Seq.cast<int>
        |> Seq.sum
        
    let Task2 () =
        let map =
            array2D (
                me.Input
                |> Array.map (fun row -> row.ToCharArray() |> Array.map (fun c -> Char.GetNumericValue(c) |> int))
            )

        map
        |> Array2D.mapi (fun y x v ->
            if v = 0 then
                let n = trailHead map (x, y)
                n |> List.length
            else
                0)
        |> Seq.cast<int>
        |> Seq.sum

    override me.SolvePart1() : string = $"%i{Task1()}"
    override me.SolvePart2() : string = $"%i{Task2()}"
