namespace FSharp.Year2024

open System
open AoCUtils.Interfaces

type OpComp =
    { A: int64
      B: int64
      C: int64
      Pointer: int
      Prg: int64 array
      Out: string }


[<type: Day(17)>]
type Day17() as me =
    inherit Solution()

    let getOperand comp (op: int64) =
        match op with
        | 0L
        | 1L
        | 2L
        | 3L -> op
        | 4L -> comp.A
        | 5L -> comp.B
        | 6L -> comp.C
        | _ -> failwith $"invalid combo opCode {op}"


    let instruction comp =
        let opCode = comp.Prg[comp.Pointer]
        let operand = comp.Prg[comp.Pointer + 1]

        let comboOperand () = getOperand comp operand

        let divide () =
            Math.Floor(decimal comp.A / decimal (Math.Pow(2, comboOperand () |> float)))
            |> int64

        match int opCode with
        | 0 ->
            { comp with
                A = divide ()
                Pointer = comp.Pointer + 2 }
        | 1 ->
            { comp with
                B = comp.B ^^^ operand
                Pointer = comp.Pointer + 2 }
        | 2 ->
            { comp with
                B = comboOperand () % 8L
                Pointer = comp.Pointer + 2 }
        | 3 ->
            if comp.A = 0 then
                { comp with Pointer = comp.Pointer + 2 }
            else
                { comp with Pointer = int operand }
        | 4 ->
            { comp with
                B = comp.B ^^^ comp.C
                Pointer = comp.Pointer + 2 }
        | 5 ->
            { comp with
                Out =
                    comp.Out
                    + (if comp.Out.Length <> 0 then "," else "")
                    + (comboOperand () % 8L).ToString()
                Pointer = comp.Pointer + 2 }
        | 6 ->
            { comp with
                B = divide ()
                Pointer = comp.Pointer + 2 }
        | 7 ->
            { comp with
                C = divide ()
                Pointer = comp.Pointer + 2 }
        | _ -> failwith $"invalid opCode {opCode}"

    let parseInput (inp: string array) =
        let a = (inp[0].Split ":")[1] |> int
        let b = (inp[1].Split ":")[1] |> int
        let c = (inp[2].Split ":")[1] |> int
        let prg = ((inp[4].Split ":")[1]).Split "," |> Array.map int64

        { A = a
          B = b
          C = c
          Pointer = 0
          Out = ""
          Prg = prg }

    let rec run comp =
        if comp.Pointer >= comp.Prg.Length then
            comp
        else
            run (instruction comp)

    let rec find (target: string) (prev: int64 list) comp cnt =
        let possibleInputs =
            prev
            |> List.map (fun pre ->
                let shifted = pre <<< 3

                ([], [ 0..7 ])
                ||> List.fold (fun pos v ->
                    let newBin = shifted + (v |> int64)

                    let nCopm = run { comp with A = newBin }

                    if target = nCopm.Out then newBin :: pos
                    else if target.EndsWith nCopm.Out then newBin :: pos
                    else pos))
            |> List.concat

        if cnt = 0 then
            possibleInputs |> List.min
        else
            find target possibleInputs comp (cnt - 1)

    let Task1 () =
        let comp = parseInput me.Input
        let comp = run comp
        comp.Out


    let Task2 () =
        let comp = parseInput me.Input

        find (comp.Prg |> Array.map string |> String.concat ",") [ 0 ] comp (comp.Prg.Length - 1)

    override me.SolvePart1() : string = $"%s{Task1()}"
    override me.SolvePart2() : string = $"%i{Task2()}"
