namespace FSharp.Year2024

open System
open AoCUtils.Interfaces

[<type: Day(7)>]
type Day7() as me =
    inherit Solution()

    let intLen int1 = int (Math.Log10(float int1)) + 1

    let intPow (a: int64) (b: int64) =
        let mutable result = 1L

        for _ in [| 0L .. (b - 1L) |] do
            result <- result * a

        result

    let combineInts (int1: int64) (int2: int64) = int1 * (intPow 10 (intLen int2)) + int2

    let rec solvable (ops: char array) (result: int64) (numbers: int64 array) (idx: int) (sum: int64) =
        if idx >= numbers.Length then
            result = sum
        elif sum > result then
            false
        else
            let nIdx = idx + 1

            ops
            |> Array.exists (fun op ->
                match op with
                | '*' ->
                    let calc = sum * numbers[idx]
                    solvable ops result numbers nIdx calc
                | '+' ->
                    let calc = sum + numbers[idx]
                    solvable ops result numbers nIdx calc
                | '|' ->
                    let calc = combineInts sum numbers[idx]
                    solvable ops result numbers nIdx calc
                | _ -> failwith "unrecognized operator")

    let solve ops =
        me.Input
        |> Array.map (fun line ->
            let result = (line.Split ':')[0] |> int64

            let numberString =
                ((line.Split ':')[1]).Split(' ', StringSplitOptions.RemoveEmptyEntries)

            let numbers = numberString |> Array.map int64

            if solvable ops result numbers 1 numbers[0] then
                result
            else
                0)
        |> Array.sum


    let Task1 () = solve [| '*'; '+' |]
    let Task2 () = solve [| '*'; '+'; '|' |]

    override me.SolvePart1() : string = $"%i{Task1()}"
    override me.SolvePart2() : string = $"%i{Task2()}"
