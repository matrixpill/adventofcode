namespace FSharp.Year2024

open AoCUtils.Interfaces

type Direction =
    | Up = 0
    | Right = 1
    | Down = 2
    | Left = 3


type GuardPos =
    { pos: int * int; Direction: Direction }

type Map6 =
    { Obstacles: Set<int * int>
      XLength: int
      YLength: int
      GuardPos: GuardPos }


[<type: Day(6)>]
type Day6() as me =
    inherit Solution()
    let moveChange = [| (0, -1); (1, 0); (0, 1); (-1, 0) |]

    let addPos first second =
        ((fst first + fst second), (snd first + snd second))

    let rec move (map: Map6) (visited: Set<GuardPos>) =
        let visited = visited |> Set.add map.GuardPos
        let nextPos = addPos map.GuardPos.pos moveChange[int map.GuardPos.Direction]

        if
            fst nextPos < 0
            || fst nextPos >= map.XLength
            || snd nextPos < 0
            || snd nextPos >= map.YLength
        then
            (visited |> Set.map _.pos |> Set.toArray |> Array.distinct |> Array.length, visited)
        else if map.Obstacles |> Set.contains nextPos then
            move
                { map with
                    Map6.GuardPos.Direction = enum<Direction> (((int map.GuardPos.Direction) + 1) % 4) }
                visited
        else
            move { map with Map6.GuardPos.pos = nextPos } visited

    let printv (visited: Set<GuardPos>) obstacles (map: string array) =
        let vistedArr = visited |> Set.toArray

        for y in [| 0 .. (map.Length - 1) |] do
            for x in [| 0 .. (map[0].Length - 1) |] do
                if map[y][x] = '#' && obstacles |> Set.contains (x, y) then
                    printf "#"
                elif obstacles |> Set.contains (x, y) then
                    printf "X"
                elif visited |> Set.exists (fun g -> g.pos = (x, y)) then
                    match vistedArr |> Array.find (fun g -> g.pos = (x, y)) |> _.Direction with
                    | Direction.Up -> printf "^"
                    | Direction.Right -> printf ">"
                    | Direction.Down -> printf "v"
                    | Direction.Left -> printf "<"
                    | _ -> printf ""
                else
                    printf "."

            printfn ""


    let rec checkLoop (map: Map6) (visited: Set<GuardPos>) print =
        if visited |> Set.contains map.GuardPos then
            if print then
                printfn "true"
                printv visited map.Obstacles me.Input
                true
            else
                true
        else
            let nextPos = addPos map.GuardPos.pos moveChange[int map.GuardPos.Direction]

            if
                fst nextPos < 0
                || fst nextPos >= map.XLength
                || snd nextPos < 0
                || snd nextPos >= map.YLength
            then
                if print then
                    printfn "false"
                    printv visited map.Obstacles me.Input
                    false
                else
                    false
            else if map.Obstacles |> Set.contains nextPos then
                checkLoop
                    { map with
                        Map6.GuardPos.Direction = enum<Direction> (((int map.GuardPos.Direction) + 1) % 4) }
                    (visited |> Set.add map.GuardPos)
                    print
            else
                checkLoop { map with Map6.GuardPos.pos = nextPos } (visited |> Set.add map.GuardPos) print

    let rec move2 (initGuard: int * int) (map: Map6) (visited: Set<GuardPos>) (cnt: int) =
        let nextPos = addPos map.GuardPos.pos moveChange[int map.GuardPos.Direction]
        let vistitedWithNewGuard = visited |> Set.add map.GuardPos

        if
            fst nextPos < 0
            || fst nextPos >= map.XLength
            || snd nextPos < 0
            || snd nextPos >= map.YLength
        then
            cnt
        elif map.Obstacles |> Set.contains nextPos then
            move2
                initGuard
                { map with
                    Map6.GuardPos.Direction = enum<Direction> (((int map.GuardPos.Direction) + 1) % 4) }
                vistitedWithNewGuard
                cnt
        else if
            nextPos = initGuard
            || visited
               |> Set.contains
                   { GuardPos.pos = nextPos
                     Direction = Direction.Up }
            || visited
               |> Set.contains
                   { GuardPos.pos = nextPos
                     Direction = Direction.Right }
            || visited
               |> Set.contains
                   { GuardPos.pos = nextPos
                     Direction = Direction.Down }
            || visited
               |> Set.contains
                   { GuardPos.pos = nextPos
                     Direction = Direction.Left }
        then
            move2 initGuard { map with Map6.GuardPos.pos = nextPos } vistitedWithNewGuard cnt
        else
            let isLoop =
                checkLoop
                    { map with
                        Obstacles = (map.Obstacles.Add nextPos)
                        Map6.GuardPos.Direction = enum<Direction> (((int map.GuardPos.Direction) + 1) % 4) }
                    vistitedWithNewGuard
                    false

            move2
                initGuard
                { map with Map6.GuardPos.pos = nextPos }
                vistitedWithNewGuard
                (if isLoop then (cnt + 1) else cnt)

    let getObstacles () =
        me.Input
        |> Array.mapi (fun y row ->
            row.ToCharArray()
            |> Array.indexed
            |> Array.choose (fun (x, c) -> if c = '#' then Some((x, y)) else None))
        |> Array.concat
        |> Set.ofArray

    let guardPos () =
        me.Input
        |> Array.indexed
        |> Array.pick (fun (y, row) ->
            if row.IndexOf '^' > -1 then
                Some((row.IndexOf '^', y))
            else
                None)

    let map6 () =
        { Obstacles = getObstacles ()
          XLength = me.Input[0].Length
          YLength = me.Input.Length
          GuardPos =
            { pos = guardPos ()
              Direction = Direction.Up } }

    let Task1 () = fst (move (map6 ()) Set.empty)

    let mapWithOpstacleAt (pos: int * int) =
        me.Input
        |> Array.mapi (fun y row ->
            if y <> snd pos then
                row
            else
                row.ToCharArray()
                |> Array.mapi (fun i c -> if i <> fst pos then c else '#')
                |> System.String)

    let Task2 () =
        let map = map6 ()
        move2 map.GuardPos.pos map Set.empty 0

    override me.SolvePart1() : string = $"%i{Task1()}"
    override me.SolvePart2() : string = $"%i{Task2()}"
