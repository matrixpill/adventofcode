namespace FSharp.Year2024

open AoCUtils.Interfaces

[<type: Day(8)>]
type Day8() as me =
    inherit Solution()


    let rec comb n l =
        match n, l with
        | 0, _ -> [ [] ]
        | _, [] -> []
        | k, (x :: xs) -> List.map ((@) [ x ]) (comb (k - 1) xs) @ comb k xs


    let antennas (rows: string array) : Map<char, (int * int) List> =
        (Map.empty, rows |> Array.indexed)
        ||> Array.fold (fun map (y, row) ->
            (map, row.ToCharArray() |> Array.indexed)
            ||> Array.fold (fun map (x, c) ->
                if c = '.' then
                    map
                else if map.ContainsKey c then
                    map |> Map.add c ((x, y) :: map[c])
                else
                    map |> Map.add c [ (x, y) ]))

    let subV v1 v2 = (fst v1 - fst v2, snd v1 - snd v2)
    let addV v1 v2 = (fst v1 + fst v2, snd v1 + snd v2)
    let mulV v1 v2 = (fst v1 * fst v2, snd v1 * snd v2)

    let inBounds (bounds: string array) (pos: int * int) =
        let x, y = pos
        x >= 0 && x < bounds[0].Length && y >= 0 && y < bounds.Length

    let getAntibodies1 (_: char) (positions: (int * int) List) =
        let combinations = comb 2 positions
        let mulV22 = mulV (2, 2)

        let anti v1 v2 = addV (subV v1 v2) v1

        combinations
        |> List.map (fun combs ->
            let f = combs[0]
            let s = combs[1]
            [ anti f s; anti s f ])
        |> List.concat
        |> List.filter (inBounds me.Input)

    let getAntibodies2 (c: char) (positions: (int * int) List) =
        let combinations = comb 2 positions
        let inBounds = inBounds me.Input

        let rec untilBounds list last diff =
            let p = addV last diff
            if inBounds p then untilBounds (p :: list) p diff else list

        let antis =
            combinations
            |> List.map (fun combs ->
                let f = combs[0]
                let s = combs[1]
                (untilBounds [] f (subV f s)) @ untilBounds [] s (subV s f))
            |> List.concat

        antis @ positions

    let Task1 () =
        let allAntennas =
            antennas me.Input
            |> Map.map getAntibodies1
            |> Map.toList
            |> List.map snd
            |> List.concat
            |> List.distinct

        allAntennas.Length

    let Task2 () =
        let allAntennas =
            antennas me.Input
            |> Map.map getAntibodies2
            |> Map.toList
            |> List.map snd
            |> List.concat
            |> List.distinct

        allAntennas.Length

    override me.SolvePart1() : string = $"%i{Task1()}"
    override me.SolvePart2() : string = $"%i{Task2()}"
