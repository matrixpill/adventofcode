namespace FSharp.Year2024

open AoCUtils.Interfaces

[<type: Day(19)>]
type Day19() as me =
    inherit Solution()

    let avaibleTowels (inp: string) = inp.Split "," |> Array.map _.Trim()

    let rec find (avaibleTowels: string array) (target: string) (from: int) (mem: Map<string, int64>) =
        if mem |> Map.containsKey target[from..] then
            if mem[target[from..]] > 0 then
                (mem[target[from..]], mem)
            else
                (0L, mem)
        else
            ((0L, mem), avaibleTowels)
            ||> Array.fold (fun (cnt, mem) tow ->
                let s = target[from .. (from + tow.Length - 1)]

                if s = tow then
                    if from + tow.Length = target.Length then
                        (cnt + 1L, mem)
                    else
                        let res = find avaibleTowels target (from + tow.Length) mem
                        let newMem = snd res |> Map.add target[from..] (fst res + cnt |> int64)
                        (fst res + cnt, newMem)
                else
                    (cnt, mem))

    let Task1 () =
        let av = avaibleTowels me.Input[0] |> Array.sortByDescending _.Length

        ((0, Map.empty), me.Input[2..])
        ||> Array.fold (fun (cnt, mem) design ->
            let found = find av design 0 mem
            if fst found > 0 then (cnt + 1, mem) else (cnt, mem))
        |> fst

    let Task2 () =
        let av = avaibleTowels me.Input[0] |> Array.sortByDescending _.Length

        ((0L, Map.empty), me.Input[2..])
        ||> Array.fold (fun (cnt, mem) design ->
            let found, mem = find av design 0 mem
            (cnt + found, mem))
        |> fst

    override me.SolvePart1() : string = $"%i{Task1()}"
    override me.SolvePart2() : string = $"%i{Task2()}"
