namespace FSharp.Year2024

open AoCUtils.Interfaces

type MatchState =
    { sum: string
      currentAction: string
      enabled: bool
      vals: string array }

[<type: Day(3)>]
type Day3() as me =
    inherit Solution()


    let baseSet (state: MatchState) (elem: char) : MatchState =
        let num1 = if state.vals.Length > 0 then state.vals[0] else ""
        let num2 = if state.vals.Length > 1 then state.vals[1] else ""

        match elem with
        | 'm' ->
            { state with
                currentAction = "m"
                vals = [||] }
        | 'u' when state.currentAction = "m" -> { state with currentAction = "mu" }
        | 'l' when state.currentAction = "mu" -> { state with currentAction = "mul" }
        | '(' when state.currentAction = "mul" -> { state with currentAction = "mul(" }
        | ')' when state.currentAction = "mul(," && num1 <> "" && num2 <> "" ->
            let mult = (num1 |> int64) * (num2 |> int64)
            let sumi = (state.sum |> int64) + mult

            { state with
                sum = sumi |> string
                currentAction = ""
                vals = [||] }
        | ',' when state.currentAction = "mul(" && num1 <> "" ->
            { state with
                currentAction = "mul(,"
                vals = [| num1 |] }
        | '0'
        | '1'
        | '2'
        | '3'
        | '4'
        | '5'
        | '6'
        | '7'
        | '8'
        | '9' when state.currentAction = "mul(" && num1.Length < 3 ->
            { state with
                vals = [| num1 + (elem |> string) |] }
        | '0'
        | '1'
        | '2'
        | '3'
        | '4'
        | '5'
        | '6'
        | '7'
        | '8'
        | '9' when state.currentAction = "mul(," && num2.Length < 3 ->
            { state with
                vals = [| num1; num2 + (elem |> string) |] }
        | _ ->
            { state with
                vals = [||]
                currentAction = "" }

    let task2Set (state: MatchState) (elem: char) : MatchState =

        let num1 = if state.vals.Length > 0 then state.vals[0] else ""
        let num2 = if state.vals.Length > 1 then state.vals[1] else ""

        match elem with
        | 'd' ->
            { state with
                currentAction = "d"
                vals = [||] }
        | 'o' when state.currentAction = "d" -> { state with currentAction = "do" }
        | '(' when state.currentAction = "do" -> { state with currentAction = "do(" }
        | ')' when state.currentAction = "do(" ->
            { state with
                enabled = true
                currentAction = "" }
        | 'n' when state.currentAction = "do" -> { state with currentAction = "don" }
        | ''' when state.currentAction = "don" -> { state with currentAction = "don'" }
        | 't' when state.currentAction = "don'" -> { state with currentAction = "don't" }
        | '(' when state.currentAction = "don't" -> { state with currentAction = "don't(" }
        | ')' when state.currentAction = "don't(" ->
            { state with
                enabled = false
                currentAction = "" }
        | ')' when state.currentAction = "mul(," && num1 <> "" && num2 <> "" ->
            if state.enabled then
                baseSet state elem
            else
                { state with
                    vals = [||]
                    currentAction = "" }
        | _ -> baseSet state elem

    let (|Prefix|_|) (p: string) (s: string) =
        if s.StartsWith(p) then
            Some(s.Substring(p.Length))
        else
            None

    let rec tryTakeNum stop text (num: string) : (int64 * string) =
        match text with
        | Prefix "0" rest
        | Prefix "1" rest
        | Prefix "2" rest
        | Prefix "3" rest
        | Prefix "4" rest
        | Prefix "5" rest
        | Prefix "6" rest
        | Prefix "7" rest
        | Prefix "8" rest
        | Prefix "9" rest ->
            if (num.Length < 3) then
                tryTakeNum stop rest $"{num}{text[0]}"
            else
                (0, text)
        | Prefix stop rest -> (num |> int64, rest)
        | _ -> (0, text)


    let mult text : int64 * string =
        let first, rest = tryTakeNum "," text ""

        if first = 0 then
            (0, text)
        else
            let second, rest = tryTakeNum ")" rest ""
            ((first * second), rest)

    let rec take state text =
        match text with
        | Prefix "mul(" rest ->
            let result, rest = mult rest

            let newSum =
                if state.enabled then
                    ((state.sum |> int64) + result) |> string
                else
                    state.sum

            take { state with sum = newSum } rest
        | Prefix "do()" rest ->
            take
                { state with
                    enabled = true
                    currentAction = "" }
                rest
        | Prefix "don't()" rest ->
            take
                { state with
                    enabled = false
                    currentAction = "" }
                rest
        | _ ->
            if text <> "" then
                take state (text.Substring(1))
            else
                { state with
                    enabled = false
                    currentAction = "" }


    let decorrupt matchStateFunc (corrupt: string) =
        let chars = corrupt.ToCharArray()

        let r =
            ({ sum = "0"
               vals = [||]
               currentAction = ""
               enabled = true },
             chars)
            ||> Array.fold matchStateFunc

        r.sum |> int64


    let takeTest (corrupt: string) =

        let r =
            ({ sum = "0"
               vals = [||]
               currentAction = ""
               enabled = true },
             corrupt)
            ||> take

        r.sum |> int64

    let Task1 () =
        me.Input |> String.concat "" |> decorrupt baseSet

    let Task2 () =
        me.Input |> String.concat "" |> takeTest

    override me.SolvePart1() : string = $"%i{Task1()}"
    override me.SolvePart2() : string = $"%i{Task2()}"
