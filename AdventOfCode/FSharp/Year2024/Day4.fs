namespace FSharp.Year2024

open System.Text.RegularExpressions
open AoCUtils.Interfaces

[<type: Day(4)>]
type Day4() as me =
    inherit Solution()

    let revString (text: string) =
        text.ToCharArray() |> Array.rev |> System.String

    let XMASregex = Regex("(?=(XMAS|SAMX))", RegexOptions.Compiled)

    let matchCount text =
        let matches = XMASregex.Matches text
        matches.Count

    let vertiSlices (arr: string array) =
        [| 0 .. arr[0].Length - 1 |]
        |> Array.map (fun i -> arr |> Array.map (fun x -> x[i]) |> System.String)

    let allDiagonals (arr: string array) =
        let firstHalf =
            [| 0 .. arr[0].Length - 1 |]
            |> Array.map (fun i -> arr |> Array.take (i + 1) |> Array.mapi (fun i2 x -> x[i - i2]) |> System.String)

        let secondHalf: string array =
            [| 1 .. arr.Length - 1 |]
            |> Array.map (fun i ->
                arr
                |> Array.skip i
                |> Array.mapi (fun i2 x -> x[arr.Length - 1 - i2])
                |> System.String)

        Array.concat [| firstHalf; secondHalf |]

    let rot90r (array: string array) =
        array |> vertiSlices |> Array.map (fun c -> c |> revString)

    let count arr =
        let cnt = matchCount
        arr |> Array.map cnt |> Array.sum

    let shrink (arr: string array) =
        arr |> Array.take (arr.Length - 1) |> Array.map (fun x -> x[.. x.Length - 2])

    let MandS first second =
        (first = 'M' && second = 'S') || (first = 'S' && second = 'M')

    let search (arr: string array) =
        let all = arr |> String.concat ""
        let rowLength = arr[0].Length

        [| 0 .. (all.Length - (2 + 2 * rowLength) - 1) |]
        |> Array.map (fun sIdx ->
            let aIdx = sIdx + rowLength + 1

            if sIdx % rowLength > rowLength - 3 then
                0
            elif
                all[aIdx] = 'A'
                && MandS all[sIdx] all[sIdx + 2 + 2 * rowLength]
                && MandS all[sIdx + 2] all[sIdx + 2 * rowLength]
            then
                1
            else
                0)
        |> Array.sum

    let Task1 () =
        let hori = me.Input |> count
        let vert = vertiSlices me.Input |> count
        let diag = me.Input |> allDiagonals |> count
        let rotated = me.Input |> rot90r
        let diag2 = rotated |> allDiagonals |> count
        // not needed anymore because allDiagonals returns all not the first half anymore
        // let rotatedTwice = rotated |> rot90r
        // let diag3 = rotatedTwice |> shrink |> diag1 |> count
        // let rotatedThriceShrunken = rotatedTwice |> rot90r
        // let diag4 = rotatedThriceShrunken |> shrink |> diag1 |> count

        hori + vert + diag + diag2 // + diag3 + diag4

    let Task2 () = search me.Input

    override me.SolvePart1() : string = $"%i{Task1()}"
    override me.SolvePart2() : string = $"%i{Task2()}"
