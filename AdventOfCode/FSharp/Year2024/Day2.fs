namespace FSharp.Year2024

open AoCUtils.Interfaces

[<type: Day(2)>]
type Day2() as me =
    inherit Solution()

    let reports () =
        me.Input |> Array.map (fun x -> x.Split(' ') |> Array.map int)

    let diffMatch diff =
        match diff with
        | 1
        | 2
        | 3 -> true
        | _ -> false

    let checkSafety (report: int array) =
        let pairs = report |> Array.pairwise

        let increasing = not (pairs |> Array.exists (fun (f, s) -> not (diffMatch (s - f))))
        let decreasing = not (pairs |> Array.exists (fun (f, s) -> not (diffMatch (f - s))))

        increasing || decreasing

    let boolToInt v = if v then 1 else 0

    let allWithOneRemoved (report: int array) =
        report
        |> Array.indexed
        |> Array.map (fun (idx, _) -> report |> Array.removeAt idx)

    let checkSafety2 (report: int array) =
        if checkSafety report then
            true
        else
            allWithOneRemoved report |> Array.exists checkSafety


    let Task1 () =
        reports () |> Array.map checkSafety |> Array.map boolToInt |> Array.sum

    let Task2 () =
        reports () |> Array.map checkSafety2 |> Array.map boolToInt |> Array.sum

    override me.SolvePart1() : string = $"%i{Task1()}"
    override me.SolvePart2() : string = $"%i{Task2()}"
