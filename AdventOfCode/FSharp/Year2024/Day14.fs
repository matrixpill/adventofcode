namespace FSharp.Year2024

open System
open System.Text
open System.Text.RegularExpressions
open AoCUtils.Interfaces

type RVec = { X: int; Y: int }

type Robot = { pos: RVec; vel: RVec }

type Quadrants = { tl: int; tr: int; bl: int; br: int }

[<type: Day(14)>]
type Day14() as me =
    inherit Solution()
    let robotReg = Regex(@"p=(?<posX>\d+),(?<posY>\d+) v=(?<velX>-?\d+),(?<velY>-?\d+)")

    let robots () =
        seq {
            for r in me.Input do
                let regexedR = robotReg.Match r
                let getInt (name: string) = regexedR.Groups[name].Value |> int

                yield
                    { Robot.pos = { X = getInt "posX"; Y = getInt "posY" }
                      vel = { X = getInt "velX"; Y = getInt "velY" } }
        }


    let moveRobot times rob =
        let npX = rob.pos.X + (times * rob.vel.X)
        let npY = rob.pos.Y + (times * rob.vel.Y)
        { rob with pos.X = npX; pos.Y = npY }

    let restrictToMap mapSize r =
        let nx = (mapSize.X + (r.pos.X % mapSize.X)) % mapSize.X
        let ny = (mapSize.Y + (r.pos.Y % mapSize.Y)) % mapSize.Y
        { r with pos.X = nx; pos.Y = ny }


    let toQuadrant middles (quads: Quadrants) r =
        if r.pos.X < middles.X && r.pos.Y < middles.Y then
            { quads with tl = quads.tl + 1 }
        else if r.pos.X > middles.X && r.pos.Y < middles.Y then
            { quads with tr = quads.tr + 1 }
        else if r.pos.X < middles.X && r.pos.Y > middles.Y then
            { quads with bl = quads.bl + 1 }
        else if r.pos.X > middles.X && r.pos.Y > middles.Y then
            { quads with br = quads.br + 1 }
        else
            quads

    let Task1 () =
        let size =
            if me.WithSampleData then
                { X = 11; Y = 7 }
            else
                { X = 101; Y = 103 }

        let restrict = restrictToMap size

        let positions = robots () |> Seq.map (moveRobot 100) |> Seq.map restrict

        let toQuadrants =
            toQuadrant
                { X = (size.X - 1) / 2
                  Y = (size.Y - 1) / 2 }

        let quadrants =
            ({ tl = 0; tr = 0; bl = 0; br = 0 }, positions) ||> Seq.fold toQuadrants

        quadrants.tl * quadrants.tr * quadrants.bl * quadrants.br


    let Task2 () =
        let size =
            if me.WithSampleData then
                { X = 11; Y = 7 }
            else
                { X = 101; Y = 103 }

        let restrict = restrictToMap size


        let rec findNonOVerlap iter robots =
            let iter = iter + 1
            let positions = robots |> List.map (moveRobot 1) |> List.map restrict
            let rMap = (Set.empty, positions) ||> Seq.fold (fun m nr -> m |> Set.add nr.pos)

            if rMap.Count = positions.Length then
                iter
            else
                findNonOVerlap iter positions


        let rec printTree iter step robots =
            let iter = (iter + step)
            printfn $"{iter}"
            let nRobots = robots |> Seq.map (moveRobot step) |> Seq.map restrict
            let rMap = (Set.empty, nRobots) ||> Seq.fold (fun m nr -> m |> Set.add nr.pos)

            let mutable sb = StringBuilder()

            for y in [ 0 .. size.Y ] do
                for x in [ 0 .. size.X ] do
                    if rMap |> Seq.contains { X = x; Y = y } then
                        sb <- sb.Append "X"
                    else
                        sb <- sb.Append " "

                sb <- sb.Append "\n"

            printfn $"{sb.ToString()}"
            let pressedKey = Console.ReadKey(true)
            printfn "Space to continue printing"

            match pressedKey.Key with
            | ConsoleKey.Spacebar -> printTree iter step nRobots
            | _ -> ()

        // printTree 2 101 (robots () |> Seq.map (moveRobot 2) |> Seq.map restrict)
        //"Look at the counter"
        findNonOVerlap 0 (robots () |> List.ofSeq)



    override me.SolvePart1() : string = $"%i{Task1()}"

    override me.SolvePart2() : string = $"{Task2()}"
