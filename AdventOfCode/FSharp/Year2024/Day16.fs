namespace FSharp.Year2024

open System
open AoCUtils.Interfaces

type MVec = { X: int; Y: int }

type Movement =
    | Forward
    | Rotate

type Reindeer = { Pos: MVec; DirIdx: int }

type Maze =
    { Reindeer: Reindeer
      Walls: Set<MVec>
      End: MVec }

[<type: Day(16)>]
type Day16() as me =
    inherit Solution()
    let addMVec v1 v2 = { X = v1.X + v2.X; Y = v1.Y + v2.Y }

    let parseMaze (input: string array) : Maze =
        let maze =
            { Reindeer = { Pos = { X = 0; Y = 0 }; DirIdx = 1 }
              End = { X = 0; Y = 0 }
              Walls = Set.empty }

        (maze, input |> Array.indexed)
        ||> Array.fold (fun maz (y, row) ->
            (maz, row.ToCharArray() |> Array.indexed)
            ||> Array.fold (fun m (x, c) ->

                let mpos = [ { X = x; Y = y } ]

                match c with
                | '#' ->
                    { m with
                        Walls = (m.Walls, mpos) ||> List.fold (fun walls bp -> walls |> Set.add bp) }
                | 'S' -> { m with Maze.Reindeer.Pos = mpos[0] }
                | 'E' -> { m with End = mpos[0] }
                | _ -> m))

    let directions =
        [ { X = 0; Y = -1 }; { X = 1; Y = 0 }; { X = 0; Y = 1 }; { X = -1; Y = 0 } ]

    let rec findPath (maze: Maze) (path: Set<MVec>) (movements: List<Movement>) : bool * List<Movement> =
        if maze.End = maze.Reindeer.Pos then
            (true, movements)
        else
            let nPos = addMVec maze.Reindeer.Pos directions[maze.Reindeer.DirIdx]
            let isWall = maze.Walls.Contains(nPos)
            let isPrevPath = path.Contains(nPos)

            
            let fwd =
                if not (isPrevPath) && not (isWall) then
                    findPath { maze with Maze.Reindeer.Pos = nPos } (path |> Set.add nPos) (Forward :: movements)
                else
                    (false, movements)
                    
            let rot =
                if movements.Length > 0 && movements[0] = Rotate then
                    (false, movements)
                else
                    let left =
                        if fst fwd then
                            fwd
                        else
                            findPath
                                { maze with
                                    Maze.Reindeer.DirIdx = (maze.Reindeer.DirIdx + 3) % directions.Length }
                                path
                                (Rotate :: movements)

                    let right =
                        if fst left then
                            left
                        else
                            findPath
                                { maze with
                                    Maze.Reindeer.DirIdx = (maze.Reindeer.DirIdx + 1) % directions.Length }
                                path
                                (Rotate :: movements)
                    right

            rot


    let Task1 () =
        let maze = parseMaze me.Input
        let path = findPath maze Set.empty List.empty

        0

    override me.SolvePart1() : string =
        $"%i{Task1()}"

// override me.SolvePart2() : string =
//     Console.Clear()
//     $"%i{Task2()}"
