namespace FSharp.Year2024

open System
open AoCUtils.Interfaces

type Space =
    { idx: int64
      id: int64
      isFile: bool
      len: int64
      moved: bool }


[<type: Day(9)>]
type Day9() as me =
    inherit Solution()

    let rec fillEmptySpace
        (diskMap: string)
        (checkSum: int64)
        (diskMapPos: int)
        (blockPos: int)
        (endPos: int * int)
        (cnt: int)
        =
        if cnt = 0 then
            (checkSum, diskMapPos, blockPos, endPos)
        else
            let vEnd = Char.GetNumericValue diskMap[fst endPos] |> int
            let fileId = int64 (fst endPos) / int64 2
            let poss = [ blockPos .. ((blockPos + vEnd) - 1 - (snd endPos)) ]
            let poss = poss |> List.take (Math.Min(poss.Length, cnt))
            // let h = poss |> List.map (fun _ -> fileId.ToString()) |> String.concat ""
            // printf $"{h}"
            let mult = poss |> List.sum
            let prod = int64 mult * fileId
            let checkSum = checkSum + prod

            let endPos =
                if (snd endPos) + poss.Length = vEnd then
                    (fst endPos - 2, 0)
                else
                    (fst endPos, snd endPos + poss.Length)

            fillEmptySpace diskMap checkSum diskMapPos (blockPos + poss.Length) endPos (cnt - poss.Length)

    let rec fileMap (diskMap: string) (checkSum: int64) (diskMapPos: int) (blockPos: int) (endPos: int * int) =
        if diskMapPos > fst endPos then
            checkSum
        else
            let v = Char.GetNumericValue(diskMap[diskMapPos]) |> int
            let isFile = int64 diskMapPos % 2L = 0

            if isFile then
                let fileId = int64 diskMapPos / int64 2
                let poss = [ blockPos .. ((blockPos + v) - 1) ]

                let poss =
                    if diskMapPos = fst endPos then
                        poss |> List.take (Math.Min(poss.Length, v - snd endPos))
                    else
                        poss

                // let h = poss |> List.map (fun _ -> fileId.ToString()) |> String.concat ""
                // printf $"{h}"

                let mult = poss |> List.sum
                let prod = int64 mult * fileId
                let checkSum = checkSum + prod

                fileMap diskMap checkSum (diskMapPos + 1) (blockPos + v) endPos
            else
                let ch, dskmp, blckp, endp =
                    fillEmptySpace diskMap checkSum diskMapPos blockPos endPos v

                fileMap diskMap ch (dskmp + 1) blckp endp


    let Task1 () =
        let diskMap = me.Input[0]

        let lastFileIdx =
            if diskMap.Length - 1 % 2 = 0 then
                diskMap.Length - 2
            else
                diskMap.Length - 1

        fileMap diskMap 0 0 0 (lastFileIdx, 0)

    let printDMap dMap =
        let print = false

        if print then
            let s =
                dMap
                |> List.map (fun x ->
                    [ 1 .. int x.len ]
                    |> List.map (fun _ -> if x.isFile then x.id.ToString() else "."))
                |> List.concat
                |> String.concat ""

            printfn $"{s}"
        else
            ()

    let rec moveFile (diskMap: Space list) fileToMoveIdx =
        let fileToMove = diskMap[fileToMoveIdx]

        let foundSpaceIdx =
            try
                diskMap
                |> List.findIndex (fun x ->
                    if x.isFile || x.idx > fileToMove.idx || x.len < fileToMove.len then
                        false
                    else
                        true)
            with ex ->
                -1

        let newD =
            if foundSpaceIdx < 0 then
                diskMap
                |> List.map (fun x ->
                    if x.id = fileToMove.id && x.isFile then
                        { x with moved = true }
                    else
                        x)
            else
                let smallerSpace =
                    { diskMap[foundSpaceIdx] with
                        len = diskMap[foundSpaceIdx].len - fileToMove.len }

                let movedFile = { fileToMove with moved = true }

                let movedTo =
                    [ { id = 0
                        len = 0
                        isFile = false
                        idx = 0
                        moved = false }
                      movedFile
                      smallerSpace ]

                let movedTo =
                    if fileToMoveIdx - 1 = foundSpaceIdx then
                        movedTo[1..]
                        @ [ { smallerSpace with
                                len = smallerSpace.len + diskMap[fileToMoveIdx + 1].len + fileToMove.len } ]
                    else
                        movedTo

                let spaceToAdd =
                    if fileToMoveIdx < (diskMap.Length - 1) then
                        diskMap[fileToMoveIdx + 1].len
                    else
                        0

                let diskMap =
                    if (fileToMoveIdx + 1) >= diskMap.Length then
                        diskMap
                    else
                        diskMap |> List.removeAt (fileToMoveIdx + 1)

                let diskMap = diskMap |> List.removeAt fileToMoveIdx

                let g =
                    { diskMap[fileToMoveIdx - 1] with
                        Space.len = diskMap[fileToMoveIdx - 1].len + fileToMove.len + spaceToAdd }

                let diskMap =
                    if fileToMoveIdx - 1 <> foundSpaceIdx then
                        let diskMap = diskMap |> List.removeAt (fileToMoveIdx - 1)
                        diskMap |> List.insertAt (fileToMoveIdx - 1) g
                    else
                        diskMap

                let diskMap = diskMap |> List.removeAt foundSpaceIdx
                let diskMap = diskMap |> List.insertManyAt foundSpaceIdx movedTo

                diskMap
        // diskMap
        // |> List.mapi (fun i x ->
        //     if i = foundSpaceIdx then
        //         movedTo
        //     else if i = fileToMoveIdx - 1 then
        //         let spaceToAdd =
        //             if fileToMoveIdx < (diskMap.Length - 1) then
        //                 diskMap[fileToMoveIdx + 1].len
        //             else
        //                 0
        //
        //         [ { x with
        //               Space.len = x.len + fileToMove.len + spaceToAdd } ]
        //     else if i = fileToMoveIdx then
        //         []
        //     else if i = fileToMoveIdx + 1 then
        //         []
        //     else
        //         [ x ])
        // |> List.concat

        printDMap newD

        let lastIdx =
            try
                newD |> List.findIndexBack (fun x -> x.isFile && x.moved = false)
            with ex ->
                -1

        if lastIdx >= 0 then moveFile newD lastIdx else newD

    let Task2 () =
        let diskMap =
            me.Input[0].ToCharArray()
            |> List.ofArray
            |> List.mapi (fun i x ->
                match i % 2 with
                | 0 ->
                    { idx = i
                      id = int64 i / 2L
                      isFile = true
                      len = Char.GetNumericValue(x) |> int64
                      moved = false }
                | _ ->
                    { idx = i
                      id = 0
                      isFile = false
                      len = Char.GetNumericValue(x) |> int64
                      moved = false })

        printDMap diskMap

        let nDiskMap = moveFile diskMap (diskMap.Length - 1)

        printDMap nDiskMap

        let result =
            ((0L, 0L), nDiskMap)
            ||> List.fold (fun (idx, sum) x ->
                if x.isFile then
                    let poss = [ idx .. ((idx + x.len) - 1L) ]
                    let mult = poss |> List.sum
                    let prod = mult * x.id
                    //File.AppendAllLines("text.text", [$"{sum+prod}"])
                    (idx + x.len, sum + prod)
                else
                    (idx + x.len, sum))

        snd result

    override me.SolvePart1() : string = $"%i{Task1()}"
    override me.SolvePart2() : string = $"%i{Task2()}"
