﻿namespace AoCUtils.Interfaces
{
    public class DayAttribute : Attribute
    {
        public readonly int DayNr;

        public DayAttribute(int day)
        {
            DayNr = day;
        }
    }
}