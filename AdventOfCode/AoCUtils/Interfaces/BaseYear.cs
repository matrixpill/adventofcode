﻿using System.Reflection;

namespace AoCUtils.Interfaces
{
    public class BaseYear<T> where T : IYear
    {
        public static Solution Implementation(int day)
        {
            var dayType = SearchDay(day);
            if (dayType != null) return BuildDay(dayType);

            var year = GetYearImplementation();
            if (!year.TryGetValue(day, out var value))
                return new EmptySolution();

            dayType = value;
            return BuildDay(dayType);
        }

        private static Type? SearchDay(int day)
        {
            var assembly1 = Assembly.GetAssembly(typeof(T));
            var nameSpace = typeof(T).Namespace;

            return assembly1?.GetTypes().FirstOrDefault(p =>
                p.Namespace == nameSpace &&
                Attribute.GetCustomAttribute(p, typeof(DayAttribute)) != null &&
                ((DayAttribute)Attribute.GetCustomAttribute(p, typeof(DayAttribute))).DayNr == day
            );
        }

        private static Dictionary<int, Type> GetYearImplementation()
        {
            return GetCtor().Implementations ?? new Dictionary<int, Type>();
        }

        private static IYear GetCtor()
        {
            return typeof(T).GetConstructor(Type.EmptyTypes)?.Invoke(Type.EmptyTypes) as IYear;
        }

        private static Solution BuildDay(Type dayImplementation)
        {
            return (Solution)dayImplementation
                .GetConstructor(Type.EmptyTypes)
                ?.Invoke(Type.EmptyTypes);
        }
    }
}