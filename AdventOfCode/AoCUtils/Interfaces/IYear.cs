﻿namespace AoCUtils.Interfaces
{
    public interface IYear
    {
        public Dictionary<int, Type> Implementations => new();
    }
}