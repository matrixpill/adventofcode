﻿namespace AoCUtils.Interfaces
{
    public abstract class Solution : IPart1, IPart2
    {
        public string[] Input { get; protected set; } = [];

        public bool WithSampleData { get; set; }

        public virtual void Initialize(string[] input)
        {
            Input = input;
        }

        public virtual string SolvePart1() => throw new NotImplementedException();

        public virtual string SolvePart2() => throw new NotImplementedException();
    }

    public interface IPart1
    {
        public string SolvePart1();
    }

    public interface IPart2
    {
        public string SolvePart2();
    }
}