﻿using System;
using System.Runtime.Serialization;

namespace AdventOfCode
{
    [Serializable]
    internal class AocException : Exception
    {
        public AocException()
        {
        }

        public AocException(string message) : base(message)
        {
        }

        public AocException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected AocException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}