﻿using System.Linq;
using System.Numerics;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2020
{
    class Day3 : Solution
    {
        private int width;

        public override void Initialize(string[] input)
        {
            //            this.input = input; @"..##.......
            //#...#...#..
            //.#....#..#.
            //..#.#...#.#
            //.#...##..#.
            //..#.##.....
            //.#.#.#....#
            //.#........#
            //#.##...#...
            //#...##....#
            //.#..#...#.#".Split("\r\n");
            this.Input = input;
            width = this.Input[0].Length;
        }


        public override string SolvePart1()
        {
            return countTrees(new IntVec(3, 1)).ToString();
        }

        private long countTrees(IntVec movement)
        {
            var pos = new IntVec(0, 0);
            long trees = CheckForTree(pos) ? 1 : 0;
            while (Move(pos, movement))
            {
                trees = CheckForTree(pos) ? trees + 1 : trees;
            }

            return trees;
        }

        private bool CheckForTree(IntVec pos)
        {
            return Input[pos.Y][pos.X] == '#';
        }

        private bool Move(IntVec pos, IntVec movement)
        {
            pos.Add(movement);

            if (pos.X >= width)
                pos.X %= width;

            if (pos.Y >= Input.Length)
                return false;
            return true;
        }

        public override string SolvePart2()
        {
            var movements = new IntVec[]
            {
                new IntVec(1, 1),
                new IntVec(3, 1),
                new IntVec(5, 1),
                new IntVec(7, 1),
                new IntVec(1, 2)
            };
            return movements.Aggregate((long)1, (acc, x) => acc * countTrees(x)).ToString();
        }
    }

    class IntVec
    {
        private Vector2 vec;

        public int X
        {
            get => (int)vec.X;
            set => vec.X = value;
        }

        public int Y
        {
            get => (int)vec.Y;
            set => vec.Y = value;
        }

        public IntVec(int x = 0, int y = 0)
        {
            vec = new Vector2(x, y);
        }

        public void Add(IntVec intVec)
        {
            vec += intVec.vec;
        }
    }
}