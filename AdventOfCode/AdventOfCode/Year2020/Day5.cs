﻿using System;
using System.Collections.Generic;
using System.Linq;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2020
{
    //F(front) = lower B(back) = upper
    //L(left) = lower R(right) = upper
    class Day5 : Solution
    {
        SortedSet<int> SeatIds = new SortedSet<int>();

        public override void Initialize(string[] input)
        {
            foreach (var line in input)
            {
                var row = getPosition(line[..^3], 0, 127, 'F', 'B');
                var seat = getPosition(line[^3..], 0, 7, 'L', 'R');
                int seatId = row * 8 + seat;

                //alternative

                //var binaryString = Regex.Replace(line, "[B]|[R]", "1");
                //binaryString = Regex.Replace(binaryString, "[F]|[L]", "0");

                //-------
                //var binaryString = line.Replace('B', '1').Replace('R', '1')
                //                    .Replace('F', '0').Replace('L', '0');

                //-----
                //var binaryString = new string(line.ToArray().Select(c => (c == 'B' || c == 'R') ? '1' : '0').ToArray());
                //var seatId = Convert.ToInt32(binaryString, 2);
                SeatIds.Add(seatId);
            }

            base.Initialize(input);
        }


        public override string SolvePart1()
        {
            return SeatIds.Last().ToString();
        }

        public override string SolvePart2()
        {
            var arr = SeatIds.ToArray();

            int id = 0;
            for (var i = 0; i < arr.Length - 1; i++)
            {
                if (arr[i] == arr[i + 1] - 2)
                {
                    id = arr[i] + 1;
                    break;
                }
            }

            return id.ToString();
        }


        private int getPosition(string inp, int min, int max, char lower, char higher)
        {
            foreach (var c in inp)
            {
                if (c == lower)
                    max = (int)Math.Floor((decimal)(min + max) / 2);
                else if (c == higher)
                    min = (int)Math.Ceiling((decimal)(min + max) / 2);
            }

            return min;
        }
    }
}