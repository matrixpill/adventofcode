﻿using System;
using System.Collections.Generic;
using System.Linq;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2020
{
    class Day10 : Solution
    {
        long[] adapters;
        private Dictionary<long, long> dic;

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            adapters = Array.ConvertAll(input, long.Parse);
            Array.Sort(adapters);
            var tmp = adapters.ToList();
            tmp.Insert(0, 0);
            tmp.Add(tmp.Max() + 3);
            adapters = tmp.ToArray();
        }

        public override string SolvePart1()
        {
            var oneDiff = 0;
            var threeDiff = 0;

            for (var i = 0; i < adapters.Length - 1; ++i)
            {
                var thisAdapter = adapters[i];
                var nextAdapter = adapters[i + 1];
                if (nextAdapter - thisAdapter == 1)
                    ++oneDiff;
                else if (nextAdapter - thisAdapter == 3)
                    ++threeDiff;
            }

            return (oneDiff * threeDiff).ToString();
        }

        public override string SolvePart2()
        {
            dic = new Dictionary<long, long>();
            return Dynamic(0).ToString();
        }

        //https://topaz.github.io/paste/#XQAAAQBXAwAAAAAAAAA0m0pnuFI8c9WAoVc3IiG2khdS9RZGAa499EcIVmCwxqrnAJZMsdk+H8l7MiszFTEde8DoV7n+/WefM7nW1Ic3lhsEhf3S4zdgzjX50qIXgQEhSNODCRVZbfHeFM18KWiFhSMfxdu4suT0rN1uZKwETsDfvGHfW6nFu3zQxYSs988dllD2nEjZ+HBPEvkmFunvVYs9sGNbRWRjJiM9LZcZpcIxDwuT1zjVPWLsEULJwXlN9VXcNKGoPfZCZwYD1BksoAJWbfDrJewLUGJiu5D+VLNbh1DzlGOdRbI3HG+SVBHoX5+8O2VGRNKZTwvye1gnJhRWBeGxIVBm4bTuvtx15hJn15NqYWvuBDoBocJaf12l1Tv/7ePY24JUn9m1MBOKMOrkMuFIGk5Z0Nt2tfq3gHjcFb0Jpzgk8R6mc7nb/bV64+HZikk5gt4NgUl7FbYiZ65EmrsqxrBf3IsNiUdVU/HeraeCEXgf0DTgXoR/4pY+QbXkyIU2ArRTtyfaDjCQOx7o5okGZS3/AtiCJlwGko3GLgqMs6U2Jrlq1MS9jOJPynawI5ci5m6e+UOrl8bX0l5hRD9OAUktOtsHZPlscB/c1LZMfmjsmQHAFSkP/NwgGv9rdJQA
        private long Dynamic(long i)
        {
            if (i == adapters.Length - 1)
                return 1;
            if (dic.ContainsKey(i))
                return dic[i];
            long ans = 0;
            for (var j = i + 1; j < adapters.Length; j++)
            {
                if (adapters[j] - adapters[i] <= 3)
                    ans += Dynamic(j);
            }

            dic[i] = ans;
            return ans;
        }
    }
}