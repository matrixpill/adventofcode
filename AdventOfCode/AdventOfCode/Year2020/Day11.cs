﻿using System;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2020
{
    class Day11 : Solution
    {
        private const char EMPTY = 'L';
        private const char OCCUPIED = '#';
        private const char FLOOR = '.';
        private int RowWidth;
        private char[][] CharArray;

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            RowWidth = input[0].Length;
            CharArray = Array.ConvertAll(input, x => x.ToCharArray());
        }

        public override string SolvePart1()
        {
            return Simulate(true).ToString();
        }

        private void clone(char[][] src, char[][] target)
        {
            for (int i = 0; i < src.Length; ++i)
            {
                target[i] = (char[])src[i].Clone();
            }
        }

        private int countSeatsTaken(char[][] plane)
        {
            var cnt = 0;
            foreach (var row in plane)
            {
                foreach (var seat in row)
                {
                    if (seat == OCCUPIED)
                        ++cnt;
                }
            }

            return cnt;
        }

        private int getOccupiedSeats(int row, int seat, char[][] currentPlane, bool part1Rules)
        {
            int tryGetSeat(int rDir, int sDir) =>
                tryGetSeatInDirection(row, seat, rDir, sDir, currentPlane, part1Rules);

            var occupiedSeats = 0;

            //top
            occupiedSeats += tryGetSeat(-1, 0);
            //top right
            occupiedSeats += tryGetSeat(-1, 1);
            //right
            occupiedSeats += tryGetSeat(0, 1);
            //buttom right
            occupiedSeats += tryGetSeat(1, 1);
            //buttom
            occupiedSeats += tryGetSeat(1, 0);
            //buttom left
            occupiedSeats += tryGetSeat(1, -1);
            //left
            occupiedSeats += tryGetSeat(0, -1);
            //top left
            occupiedSeats += tryGetSeat(-1, -1);
            return occupiedSeats;
        }

        private int tryGetSeatInDirection(int currRow, int currSeat, int rowDriection, int seatDirection,
            char[][] currentPlane, bool part1Rules)
        {
            var nxtRow = currRow + rowDriection;
            var nxtSeat = currSeat + seatDirection;

            while (true)
            {
                if (nxtRow >= 0 && nxtRow < currentPlane.Length && nxtSeat >= 0 && nxtSeat < RowWidth)
                {
                    switch (currentPlane[nxtRow][nxtSeat])
                    {
                        case OCCUPIED:
                            return 1;
                        case EMPTY:
                            return 0;
                        case FLOOR:
                            if (part1Rules)
                                return 0;
                            break;
                    }

                    nxtRow += rowDriection;
                    nxtSeat += seatDirection;
                }
                else
                    return 0;
            }
        }

        public override string SolvePart2()
        {
            return Simulate(false).ToString();
        }

        private int Simulate(bool part1Rules)
        {
            var currentPlane = new char[Input.Length][];
            clone(CharArray, currentPlane);
            var seatsTaken = 0;
            var limit = part1Rules ? 4 : 5;
            while (true)
            {
                var tmpPlane = new char[Input.Length][];
                clone(currentPlane, tmpPlane);

                for (var i = 0; i < currentPlane.Length; ++i)
                {
                    var row = currentPlane[i];
                    for (var j = 0; j < row.Length; ++j)
                    {
                        var place = row[j];
                        if (place == FLOOR) continue;
                        var occupiedSeats = getOccupiedSeats(i, j, currentPlane, part1Rules);

                        switch (place)
                        {
                            case EMPTY when occupiedSeats == 0:
                                tmpPlane[i][j] = OCCUPIED;
                                break;
                            case OCCUPIED when occupiedSeats >= limit:
                                tmpPlane[i][j] = EMPTY;
                                break;
                        }
                    }
                }

                clone(tmpPlane, currentPlane);
                var takenSeats = countSeatsTaken(currentPlane);
                if (takenSeats == seatsTaken)
                    break;
                else
                    seatsTaken = takenSeats;
            }

            return seatsTaken;
        }
    }
}