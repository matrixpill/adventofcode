﻿using System;
using System.Collections.Generic;
using System.Linq;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2020
{
    class Day16 : Solution
    {
        private TicketRules TicketRules;
        private int[] MyTicket;
        private Ticket[] OtherTickets;

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            var i = 0;
            TicketRules = new TicketRules();
            var rules = new List<Rule>();
            foreach (var rule in input)
            {
                if (rule == "")
                    break;

                var split1 = rule.Split(':');
                rules.Add(new Rule()
                {
                    Name = split1[0],
                    ValidRanges = new ValidRanges(split1[1])
                });

                i++;
            }

            TicketRules.Rules = rules.ToArray();

            MyTicket = Array.ConvertAll(input[i + 2].Split(','), int.Parse);
            i += 5;
            OtherTickets = new Ticket[input.Length - i];

            for (var j = 0; j < OtherTickets.Length; j++, i++)
            {
                OtherTickets[j] = new Ticket
                {
                    Values = Array.ConvertAll(input[i].Split(','), int.Parse)
                };
            }
        }

        private List<Ticket> validTickets;

        public override string SolvePart1()
        {
            validTickets = new List<Ticket>();
            long sum = 0;
            foreach (var ticket in OtherTickets)
            {
                var validTicket = true;
                for (var i = 0; i < ticket.Values.Length; i++)
                {
                    var tickFeld = ticket.Values[i];
                    var validField = false;
                    foreach (var r in TicketRules.Rules)
                    {
                        validField = r.IsValid(tickFeld);
                        if (validField) break;
                    }

                    if (!validField)
                    {
                        sum += tickFeld;
                        validTicket = false;
                    }
                }

                if (validTicket)
                    validTickets.Add(ticket);
            }

            return sum.ToString();
        }


        public override string SolvePart2()
        {
            var positionToRule = new Dictionary<int, List<string>>();
            for (int i = 0; i < MyTicket.Length; i++)
                positionToRule[i] = new List<string>();

            var first = true;
            foreach (var ticket in validTickets)
            {
                for (var pos = 0; pos < ticket.Values.Length; pos++)
                {
                    var value = ticket.Values[pos];
                    foreach (var r in TicketRules.Rules)
                    {
                        if (r.IsValid(value))
                        {
                            if (first)
                                if (!positionToRule[pos].Contains(r.Name))
                                    positionToRule[pos].Add(r.Name);
                        }
                        else
                        {
                            if (positionToRule[pos].Contains(r.Name))
                                positionToRule[pos].RemoveAll(x => x == r.Name);
                        }
                    }
                }

                first = false;
            }

            while (OneWithMore(positionToRule))
            {
                foreach (var pos in positionToRule)
                {
                    if (pos.Value.Count == 1)
                    {
                        foreach (var m in positionToRule)
                        {
                            if (m.Key != pos.Key)
                                m.Value.RemoveAll(x => x == pos.Value[0]);
                        }
                    }
                }
            }

            var f = positionToRule.Where(x => x.Value[0].StartsWith("departure")).ToList();
            long sum = 1;
            foreach (var dep in f)
            {
                sum *= MyTicket[dep.Key];
            }

            return sum.ToString();
        }

        private bool OneWithMore(Dictionary<int, List<string>> positionToRule)
        {
            foreach (var pos in positionToRule)
            {
                if (pos.Value.Count > 1)
                    return true;
            }

            return false;
        }
    }


    class Ticket
    {
        public int[] Values { get; set; }
    }

    class TicketRules
    {
        public Rule[] Rules { get; set; }
    }

    class Rule
    {
        public string Name { get; set; }
        public ValidRanges ValidRanges { get; set; }
        public int Position { get; set; } = -1;

        public bool IsValid(int value) => ValidRanges.IsValid(value);
    }

    class ValidRanges
    {
        public Range Range1 { get; set; }
        public Range Range2 { get; set; }

        public ValidRanges(string inp)
        {
            var split = inp.Split("or");
            var r1 = split[0].Split('-');
            var r2 = split[1].Split('-');
            Range1 = new Range()
            {
                Min = int.Parse(r1[0]),
                Max = int.Parse(r1[1])
            };
            Range2 = new Range()
            {
                Min = int.Parse(r2[0]),
                Max = int.Parse(r2[1])
            };
        }

        public bool IsValid(int value) => Range1.IsInRange(value) || Range2.IsInRange(value);
    }

    class Range
    {
        public int Min { get; set; }
        public int Max { get; set; }

        public bool IsInRange(int value)
        {
            return value >= Min && value <= Max;
        }
    }
}