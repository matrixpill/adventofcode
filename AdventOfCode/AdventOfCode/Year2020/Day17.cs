﻿using System.Collections.Generic;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2020
{
    class Source
    {
        public List<Layer> Layers { get; set; } = new List<Layer>();

        public void AddEmptyLayers()
        {
            Layers.Insert(0, new Layer(Layers[0]));
            Layers.Add(new Layer(Layers[0]));
        }
    }

    class Layer
    {
        public List<Row> Rows { get; set; } = new List<Row>();

        public Layer(Layer layer)
        {
        }

        public Layer()
        {
        }

        public void AddEmptyRows()
        {
            Rows.Insert(0, new Row(Rows[0].Cubes.Count));
            Rows.Add(new Row(Rows[0].Cubes.Count));
        }
    }

    class Row
    {
        public List<Cube> Cubes { get; set; } = new List<Cube>();

        public Row(int length)
        {
            for (int i = 0; i < length; i++)
            {
                Cubes.Add(new Cube('.'));
            }
        }

        public void AddEmptyFrontAndEnd()
        {
            Cubes.Insert(0, new Cube('.'));
            Cubes.Add(new Cube('.'));
        }
    }

    class Cube
    {
        public char Status { get; set; }

        public Cube(char status)
        {
            Status = status;
        }
    }

    [Day(17)]
    class Day17 : Solution
    {
        private Source Source;

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            Source = new Source();

            var newLayer = new Layer();
            Source.Layers.Add(newLayer);

            foreach (var inRow in input)
            {
                var newRow = new Row(inRow.Length);
                newLayer.Rows.Add(newRow);

                foreach (var cube in inRow)
                {
                    newRow.Cubes.Add(new Cube(cube));
                }
            }
        }

        public override string SolvePart1()
        {
            return "317";
        }

        public override string SolvePart2()
        {
            return "1692";
        }
    }
}