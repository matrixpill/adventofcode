﻿using System;
using System.Collections.Generic;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2020
{
    public class _2020 : IYear
    {
        private static Dictionary<int, Type> ThisYear { get; set; } = new Dictionary<int, Type>()
        {
            { 0, typeof(EmptySolution) },
            { 1, typeof(Day1) },
            { 2, typeof(Day2) },
            { 3, typeof(Day3) },
            { 4, typeof(Day4) },
            { 5, typeof(Day5) },
            { 6, typeof(Day6) },
            { 7, typeof(Day7) },
            { 8, typeof(Day8) },
            { 9, typeof(Day9) },
            { 10, typeof(Day10) },
            { 11, typeof(Day11) },
            { 12, typeof(Day12) },
            { 13, typeof(Day13) },
            { 14, typeof(Day14) },
            { 15, typeof(Day15) },
            { 16, typeof(Day16) },
            { 17, typeof(Day17) },
        };

        public Dictionary<int, Type> Implementations => ThisYear;
    }
}