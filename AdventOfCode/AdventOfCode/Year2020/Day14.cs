﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2020
{
    interface IInstruction
    {
    }

    class BitMaskInstruction : IInstruction
    {
        public string Bitmask { get; set; }
    }

    class MemWriteInstruction : IInstruction
    {
        public long MemAdress { get; set; }
        public long Value { get; set; }
    }


    class Day14 : Solution
    {
        private List<IInstruction> Instructions;

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            Instructions = new List<IInstruction>();
            foreach (var instruction in input)
            {
                var instValue = instruction.Split(" = ");
                if (instValue[0] == "mask")
                    Instructions.Add(new BitMaskInstruction() { Bitmask = instValue[1] });
                else
                    Instructions.Add(new MemWriteInstruction()
                    {
                        MemAdress = long.Parse(instValue[0][4..^1]),
                        Value = long.Parse(instValue[1])
                    });
            }
        }

        public override string SolvePart1()
        {
            var mem = new Memory(false);
            foreach (var instr in Instructions)
            {
                mem.Alter(instr);
            }

            return mem.GetSum().ToString();
        }

        public override string SolvePart2()
        {
            var mem = new Memory(true);
            foreach (var instr in Instructions)
            {
                mem.Alter(instr);
            }

            return mem.GetSum().ToString();
        }
    }

    class Memory
    {
        private Dictionary<long, long> MemoryAdresses = new Dictionary<long, long>();
        private string BitMask = string.Empty;
        bool MAC;

        public Memory(bool memoryAdressDecoder)
        {
            MAC = memoryAdressDecoder;
        }

        public void Alter(IInstruction instruction)
        {
            if (instruction is BitMaskInstruction bitMask)
            {
                BitMask = bitMask.Bitmask;
            }
            else if (instruction is MemWriteInstruction memWrite)
            {
                if (MAC)
                {
                    UseMAC(memWrite);
                }
                else
                {
                    MemoryAdresses[memWrite.MemAdress] = GetLong(ApplyBitMask(memWrite.Value));
                }
            }
        }

        private void UseMAC(MemWriteInstruction memWrite)
        {
            var cntFloatingBits = BitMask.Count(x => x == 'X');
            var sb = new StringBuilder();
            var adr = ApplyBitMask(memWrite.MemAdress, MAC);
            for (int i = 0; i < Math.Pow(2, cntFloatingBits); i++)
            {
                sb.Clear();
                var iBinString = Convert.ToString(i, 2).PadLeft(cntFloatingBits, '0');
                var xpos = 0;
                for (int j = 0; j < BitMask.Length; j++)
                {
                    if (BitMask[j] == 'X')
                    {
                        sb.Append(iBinString[xpos++]);
                    }
                    else
                        sb.Append(adr[j]);
                }

                MemoryAdresses[GetLong(sb.ToString())] = memWrite.Value;
            }
        }

        public long GetSum()
        {
            long sum = 0;
            foreach (var adr in MemoryAdresses)
            {
                sum += adr.Value;
            }

            return sum;
        }

        private string ApplyBitMask(long value, bool mac = false)
        {
            var binary = Convert.ToString(value, 2);
            while (binary.Length < 36)
                binary = $"0{binary}";

            var binaryArr = binary.ToCharArray();

            for (int i = 0; i < BitMask.Length; i++)
            {
                var c = BitMask[i];
                if (mac)
                {
                    if (c == '1')
                        binaryArr[i] = '1';
                }
                else
                {
                    if (c != 'X')
                        binaryArr[i] = c;
                }
            }

            var str = new string(binaryArr);

            return str;
        }

        private long GetLong(string binaryLong)
        {
            return Convert.ToInt64(binaryLong, 2);
        }
    }
}