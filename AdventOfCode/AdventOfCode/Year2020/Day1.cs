﻿using System;
using System.Linq;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2020
{
    public class Day1 : Solution
    {
        private int[] intInput;

        public override void Initialize(string[] rawInput)
        {
            intInput = prepareInput(rawInput);
        }

        public override string SolvePart1()
        {
            for (int i = 0; i < intInput.Length - 1; i++)
            {
                for (int j = i + 1; j < intInput.Length; j++)
                {
                    var val1 = intInput[i];
                    var val2 = intInput[j];
                    if (val1 + val2 == 2020)
                    {
                        return (val1 * val2).ToString();
                    }
                }
            }

            return "Keine Lösung gefunden";
        }

        public override string SolvePart2()
        {
            for (int i = 0; i < intInput.Length - 2; i++)
            {
                for (int j = i + 1; j < intInput.Length - 1; j++)
                {
                    for (int k = j + 1; k < intInput.Length; k++)
                    {
                        var val1 = intInput[i];
                        var val2 = intInput[j];
                        var val3 = intInput[k];
                        if (val1 + val2 + val3 == 2020)
                        {
                            return (val1 * val2 * val3).ToString();
                        }
                    }
                }
            }

            return "Keine Lösung gefunden";
        }

        public string SolvePart2(string[] input)
        {
            throw new NotImplementedException();
        }

        private int[] prepareInput(string[] input)
        {
            return input.Select(x => int.Parse(x)).ToArray();
        }
    }
}