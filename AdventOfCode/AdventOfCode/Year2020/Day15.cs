﻿using System;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2020
{
    class Day15 : Solution
    {
        private int[] StartNumbers;


        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            var firstLine = input[0];
            StartNumbers = Array.ConvertAll(firstLine.Split(','), int.Parse);
        }

        public override string SolvePart1()
        {
            return GetNumberAfterTurns(2020).ToString();
        }

        public override string SolvePart2()
        {
            return GetNumberAfterTurns(30000000).ToString();
        }

        private int GetNumberAfterTurns(int turns)
        {
            //var previousNumbers = new Dictionary<int, int>();
            var previousNumbers = new int[turns];
            for (var i = 0; i < StartNumbers.Length; i++)
            {
                previousNumbers[StartNumbers[i]] = i + 1;
            }

            var turn = StartNumbers.Length + 1;
            var lastNumber = 0;
            while (turn < turns)
            {
                turn++;
                int nextNumber;
                //if (previousNumbers.ContainsKey(lastNumber))
                if (previousNumbers[lastNumber] != 0)
                {
                    nextNumber = (turn - 1) - previousNumbers[lastNumber];
                }
                else
                {
                    nextNumber = 0;
                }

                previousNumbers[lastNumber] = turn - 1;
                lastNumber = nextNumber;
            }

            return lastNumber;
        }
    }
}