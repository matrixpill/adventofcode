﻿using System.Collections.Generic;
using System.Linq;
using AdventOfCode.Utility;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2020
{
    partial class Day13 : Solution
    {
        private int ArrivalAtPort;
        private int[] BusIds;

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            ArrivalAtPort = int.Parse(input[0]);
            var busIds = new List<int>();
            foreach (var b in input[1].Split(','))
            {
                if (b != "x")
                    busIds.Add(int.Parse(b));
                else
                    busIds.Add(-1);
            }

            BusIds = busIds.ToArray();
        }

        public override string SolvePart1()
        {
            var timeTillNextBus = new int[BusIds.Length];
            var smallest = int.MaxValue;
            var nextBus = 0;

            for (var i = 0; i < BusIds.Length; i++)
            {
                if (BusIds[i] == -1) continue;
                var timeTill = BusIds[i] - (ArrivalAtPort % BusIds[i]);
                timeTillNextBus[i] = timeTill;
                if (timeTill < smallest)
                {
                    nextBus = BusIds[i];
                    smallest = timeTill;
                }
            }

            return (nextBus * smallest).ToString();
        }

        public override string SolvePart2()
        {
            var found = false;

            var busses = new List<Bus>();
            for (var i = 0; i < BusIds.Length; i++)
            {
                if (BusIds[i] == -1) continue;
                busses.Add(new Bus
                {
                    Id = (ulong)BusIds[i],
                    offset = (ulong)i
                });
            }

            var ordered = busses.OrderBy(x => x.Id).ToArray();
            var startTime = 10 * ordered[0].Id - ordered[0].offset;

            var increment = ordered[0].Id;
            var lastFound = 0;

            while (!found)
            {
                found = true;
                for (int i = 1; i < ordered.Length; i++)
                {
                    var bus = ordered[i];
                    if ((startTime + bus.offset) % bus.Id == 0)
                    {
                        if (lastFound < i)
                        {
                            var tmp = new List<ulong>();
                            for (int j = 0; j < i; j++)
                                tmp.Add(ordered[j].Id);
                            increment = GFG.LCM(tmp.ToArray());
                            ;
                            lastFound = i;
                        }

                        continue;
                    }

                    found = false;
                    break;
                }

                if (!found)
                    startTime += increment;
            }

            return startTime.ToString();
        }

        class Bus
        {
            public ulong Id { get; set; }
            public ulong offset { get; set; }
        }
    }
}