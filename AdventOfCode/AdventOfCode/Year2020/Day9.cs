﻿using System;
using System.Collections.Generic;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2020
{
    class Day9 : Solution
    {
        long[] numInput;
        long prevResult;

        public override void Initialize(string[] input)
        {
            numInput = Array.ConvertAll(input, x => long.Parse(x));
            base.Initialize(input);
        }

        public override string SolvePart1()
        {
            var preamble = new Queue<long>(25);

            bool IsFull()
            {
                return preamble.Count == 25;
            }

            foreach (var n in numInput)
            {
                if (IsFull())
                {
                    if (!CheckIfValid(n, preamble))
                    {
                        prevResult = n;
                        return n.ToString();
                    }

                    preamble.Dequeue();
                }

                preamble.Enqueue(n);
            }

            return "Something went Wrong!";
        }

        private bool CheckIfValid(long n, Queue<long> preamble)
        {
            var preamblArr = preamble.ToArray();

            for (var i = 0; i < preamblArr.Length - 1; ++i)
            {
                for (var j = i + 1; j < preamblArr.Length; ++j)
                {
                    if (preamblArr[i] + preamblArr[j] == n)
                        return true;
                }
            }

            return false;
        }

        public override string SolvePart2()
        {
            for (int i = 0; i < numInput.Length - 1; ++i)
            {
                var start = numInput[i];
                long sum = start;
                long min = start;
                long max = start;

                for (var j = i + 1; j < numInput.Length; ++j)
                {
                    var nxt = numInput[j];
                    if (nxt < min)
                        min = nxt;
                    else if (nxt > max)
                        max = nxt;
                    sum += nxt;

                    if (sum > prevResult)
                        break;
                    if (sum == prevResult)
                        return (min + max).ToString();
                }
            }

            return "Something went Wrong!";
        }
    }
}