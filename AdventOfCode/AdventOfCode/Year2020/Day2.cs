﻿using System.Linq;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2020
{
    class Day2 : Solution
    {
        public override string SolvePart1()
        {
            var rightPasswords = 0;
            foreach (var line in Input)
            {
                var p = SplitInput(line);

                var cnt = 0;
                foreach (var b in p.Password)
                {
                    if (b == p.PolicyLetter)
                        cnt++;
                }

                if (cnt >= p.Numbers[0] && cnt <= p.Numbers[1])
                {
                    rightPasswords++;
                }
            }

            return rightPasswords.ToString();
        }

        private PasswordAndPolicy SplitInput(string line)
        {
            var result = new PasswordAndPolicy();
            var firstSplit = line.Split(':');
            result.Password = firstSplit[1].Trim();
            var policy = firstSplit[0].Split(' ');
            result.Numbers = policy[0].Split('-').Select(x => int.Parse(x)).ToArray();
            result.PolicyLetter = policy[1].Trim()[0];
            return result;
        }

        public override string SolvePart2()
        {
            var rightCount = 0;
            foreach (var line in Input)
            {
                var pp = SplitInput(line);

                var firstPos = IsLetterAtPosition(pp.Password, pp.Numbers[0] - 1, pp.PolicyLetter);
                var secondPos = IsLetterAtPosition(pp.Password, pp.Numbers[1] - 1, pp.PolicyLetter);

                if (firstPos ^ secondPos)
                {
                    rightCount++;
                }
            }

            return rightCount.ToString();
        }

        private bool IsLetterAtPosition(string pass, int pos, char letter)
        {
            return pass[pos] == letter;
        }


        private class PasswordAndPolicy
        {
            public string Password { get; set; }
            public int[] Numbers { get; set; }
            public char PolicyLetter { get; set; }
        }
    }
}