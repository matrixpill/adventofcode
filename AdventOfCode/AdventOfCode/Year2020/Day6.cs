﻿using System.Collections.Generic;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2020
{
    class Day6 : Solution
    {
        public override void Initialize(string[] input)
        {
            base.Initialize(input);
        }


        public override string SolvePart1()
        {
            long sum = 0;
            HashSet<char> s = new HashSet<char>();

            foreach (var l in Input)
            {
                if (l == "")
                {
                    sum += s.Count;
                    s.Clear();
                }

                foreach (var c in l)
                {
                    s.Add(c);
                }
            }

            sum += s.Count;
            return sum.ToString();
        }

        public override string SolvePart2()
        {
            long sum = 0;
            List<char> hs = new List<char>();
            var first = true;


            foreach (var l in Input)
            {
                if (l == "")
                {
                    sum += hs.Count;
                    hs.Clear();
                    first = true;
                    continue;
                }

                if (first)
                {
                    foreach (var c in l)
                    {
                        if (!hs.Contains(c))
                            hs.Add(c);
                    }

                    first = false;
                }
                else
                {
                    if (hs.Count == 0)
                        continue;

                    for (var i = 0; i < hs.Count; i++)
                    {
                        var c = hs[i];

                        if (!l.Contains(c))
                        {
                            hs.Remove(c);
                            i--;
                        }
                    }
                }
            }

            sum += hs.Count;

            return sum.ToString();
        }
    }
}