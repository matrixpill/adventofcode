﻿using System.Collections.Generic;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2020
{
    public class Day8 : Solution
    {
        const string ACC = "acc";
        const string JMP = "jmp";
        const string NOP = "nop";

        public override string SolvePart1()
        {
            var acc = 0;
            var executedLines = new HashSet<int>();
            var position = 0;

            while (true)
            {
                if (!executedLines.Add(position))
                    break;

                var operation = Input[position].Split(" ");

                switch (operation[0])
                {
                    case ACC:
                        acc += int.Parse(operation[1]);
                        ++position;
                        break;
                    case JMP:
                        position += int.Parse(operation[1]);
                        break;
                    case NOP:
                        ++position;
                        break;
                }
            }

            return acc.ToString();
        }

        public override string SolvePart2()
        {
            for (int i = 0; i < Input.Length; ++i)
            {
                string[] newInput;
                if (Input[i].Contains(NOP))
                {
                    newInput = (string[])Input.Clone();
                    newInput[i] = newInput[i].Replace(NOP, JMP);
                }
                else if (Input[i].Contains(JMP))
                {
                    newInput = (string[])Input.Clone();
                    newInput[i] = newInput[i].Replace(JMP, NOP);
                }
                else
                {
                    continue;
                }

                var acc = CheckForInfiniteLoop(newInput);
                if (acc != -1)
                {
                    return acc.ToString();
                }
            }

            return "nothing";
        }

        private long CheckForInfiniteLoop(string[] operations)
        {
            long acc = 0;
            var executedLines = new HashSet<int>();
            var position = 0;
            while (true)
            {
                if (!executedLines.Add(position))
                    return -1;

                var operation = operations[position].Split(" ");

                switch (operation[0])
                {
                    case ACC:
                        acc += int.Parse(operation[1]);
                        ++position;
                        break;
                    case JMP:
                        position += int.Parse(operation[1]);
                        break;
                    case NOP:
                        ++position;
                        break;
                }

                if (position >= operations.Length || position < 0)
                    break;
            }

            return acc;
        }
    }
}