﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2020
{
    class Day7 : Solution
    {
        Dictionary<string, Dictionary<string, int>> Bags = new Dictionary<string, Dictionary<string, int>>();
        Dictionary<string, List<string>> bagsIn = new Dictionary<string, List<string>>();
        const string MYBAGCOLOR = "shiny gold";

        public override void Initialize(string[] input)
        {
//            var demoInput = @"shiny gold bags contain 2 dark red bags.
//dark red bags contain 2 dark orange bags.
//dark orange bags contain 2 dark yellow bags.
//dark yellow bags contain 2 dark green bags.
//dark green bags contain 2 dark blue bags.
//dark blue bags contain 2 dark violet bags.
//dark violet bags contain no other bags.".Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries);


//            var demoInput = @"light red bags contain 1 bright white bag, 2 muted yellow bags.
//dark orange bags contain 3 bright white bags, 4 muted yellow bags.
//bright white bags contain 1 shiny gold bag.
//muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
//shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
//dark olive bags contain 3 faded blue bags, 4 dotted black bags.
//vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
//faded blue bags contain no other bags.
//dotted black bags contain no other bags.".Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries);
//            input = demoInput;

            var bagsRegex = new Regex("(?<count>[0-9] )(?<name>.*)( bag)");

            foreach (var line in input)
            {
                var first = line.Split("contain");
                var mainBag = first[0][..^6];

                var innerBags = first[1].Split(",");

                foreach (var x in innerBags)
                {
                    var f = bagsRegex.Match(x);
                    if (f.Success)
                    {
                        var subName = f.Groups["name"].Value;
                        var subCount = int.Parse(f.Groups["count"].Value);

                        if (!Bags.ContainsKey(mainBag))
                            Bags.Add(mainBag, new Dictionary<string, int>());

                        Bags[mainBag].Add(subName, subCount);

                        if (!bagsIn.ContainsKey(subName))
                            bagsIn.Add(subName, new List<string>());

                        bagsIn[subName].Add(mainBag);
                    }
                }
            }

            base.Initialize(input);
        }

        public override string SolvePart1()
        {
            var cnt = RecursivePart1(MYBAGCOLOR, new List<string>());
            return cnt.ToString();
        }

        private int RecursivePart1(string name, List<string> alreadyCounted)
        {
            var cnt = 0;
            if (bagsIn.ContainsKey(name))
                foreach (var b in bagsIn[name])
                {
                    if (!alreadyCounted.Contains(b))
                    {
                        cnt++;
                        alreadyCounted.Add(b);
                        cnt += RecursivePart1(b, alreadyCounted);
                    }
                }

            return cnt;
        }

        public override string SolvePart2()
        {
            var cnt = RecursivePart2(MYBAGCOLOR);
            return (cnt - 1).ToString();
        }

        private int RecursivePart2(string name)
        {
            var cnt = 1;
            if (Bags.ContainsKey(name))
                foreach (var bag in Bags[name])
                {
                    cnt += bag.Value * RecursivePart2(bag.Key);
                }

            return cnt;
        }
    }
}