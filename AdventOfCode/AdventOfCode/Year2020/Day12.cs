﻿using System;
using System.Drawing;
using System.IO;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2020
{
    class Day12 : Solution
    {
        Action[] Actions;

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            Actions = Array.ConvertAll(input, Action.ToAction);
        }

        public override string SolvePart1()
        {
            var ship = new ShipPart1(ShouldDraw());
            foreach (var action in Actions)
            {
                ship.Move(action);
            }

            return (Math.Abs(ship.NorthPosition) + Math.Abs(ship.EastPosition)).ToString();
        }

        public override string SolvePart2()
        {
            var ship2 = new ShipPart2(ShouldDraw());

            foreach (var action in Actions)
                ship2.Move(action);

            return (Math.Abs(ship2.NorthPosition) + Math.Abs(ship2.EastPosition)).ToString();
        }

        private bool ShouldDraw()
        {
            return false;
#pragma warning disable CS0162 // Unerreichbarer Code wurde entdeckt.
            Console.WriteLine("Wollen diesen part malen? (j/n)");
            var inp = Console.ReadKey().KeyChar;
            return char.ToLower(inp) == 'j';
#pragma warning restore CS0162 // Unerreichbarer Code wurde entdeckt.
        }
    }


    class ShipPart1
    {
        public int NorthPosition { get; set; } = 0;
        public int EastPosition { get; set; } = 0;
        public int Facing { get; set; } = 90;

        public Drawer im;

        public ShipPart1(bool shoudlDraw)
        {
            if (shoudlDraw)
                im = new ImageMaker(5000, 2000, "part1");
            else
                im = new EmptyDrawer();
        }

        public Direction MovingDirection
        {
            get
            {
                return Facing switch
                {
                    0 => Direction.NORTH,
                    90 => Direction.EAST,
                    180 => Direction.SOUTH,
                    270 => Direction.WEST,
                    _ => throw new Exception("OH NO"),
                };
            }
        }

        public void Move(Action action)
        {
            switch (action.Type)
            {
                case Direction.NORTH:
                    NorthPosition += action.Value;
                    break;
                case Direction.EAST:
                    EastPosition += action.Value;
                    break;
                case Direction.SOUTH:
                    NorthPosition -= action.Value;
                    break;
                case Direction.WEST:
                    EastPosition -= action.Value;
                    break;
                case Direction.LEFT:
                    var tmp = (Facing - action.Value);
                    Facing = tmp < 0 ? 360 + tmp : tmp;
                    break;
                case Direction.RIGHT:
                    Facing = (Facing + action.Value) % 360;
                    break;
                case Direction.FORWARD:
                    var movDirect = MovingDirection;
                    switch (movDirect)
                    {
                        case Direction.NORTH:
                            NorthPosition += action.Value;
                            break;
                        case Direction.EAST:
                            EastPosition += action.Value;
                            break;
                        case Direction.SOUTH:
                            NorthPosition -= action.Value;
                            break;
                        case Direction.WEST:
                            EastPosition -= action.Value;
                            break;
                    }

                    break;
                default:
                    throw new Exception("OH NO");
            }

            im.DrawLine(NorthPosition, EastPosition);
        }
    }

    class ShipPart2
    {
        public int NorthPosition { get; set; } = 0;
        public int EastPosition { get; set; } = 0;

        public int WayPointNorth { get; set; } = 1;
        public int WayPointEast { get; set; } = 10;
        public Drawer im;

        public ShipPart2(bool shouldDraw)
        {
            if (shouldDraw)
                im = new ImageMaker(5000, 6000, "part2");
            else
                im = new EmptyDrawer();
        }

        public void Move(Action action)
        {
            switch (action.Type)
            {
                case Direction.NORTH:
                    WayPointNorth += action.Value;
                    break;
                case Direction.EAST:
                    WayPointEast += action.Value;
                    break;
                case Direction.SOUTH:
                    WayPointNorth -= action.Value;
                    break;
                case Direction.WEST:
                    WayPointEast -= action.Value;
                    break;
                case Direction.LEFT:
                    for (int i = 0; i < (action.Value / 90); i++)
                    {
                        var north = WayPointNorth;
                        WayPointNorth = WayPointEast;
                        WayPointEast = north * -1;
                    }

                    break;
                case Direction.RIGHT:
                    for (int i = 0; i < (action.Value / 90); i++)
                    {
                        var east = WayPointEast;
                        WayPointEast = WayPointNorth;
                        WayPointNorth = east * -1;
                    }

                    break;
                case Direction.FORWARD:
                    NorthPosition += (action.Value * WayPointNorth);
                    EastPosition += (action.Value * WayPointEast);
                    break;
                default:
                    break;
            }

            im.DrawLine(NorthPosition, EastPosition);
        }
    }

    interface Drawer
    {
        public void DrawLine(int north, int east);
    }

    class EmptyDrawer : Drawer
    {
        public void DrawLine(int north, int east)
        {
        }
    }

    class ImageMaker : Drawer
    {
        int width;
        int height;
        double lastNorth;
        double lastEast;
        int step = -1;
        string location = @".\Day10Output\";

        private Bitmap bitmap;
        Graphics grx;
        readonly Pen Pen = new Pen(Color.Red);

        public ImageMaker(int width, int height, string nameAddidion)
        {
            this.width = width;
            this.height = height;
            lastNorth = 0.2 * height;
            lastEast = width - (width / 8);
            location += nameAddidion + @"\";
            manageDirectory();
            bitmap = new Bitmap(this.width, this.height);
            grx = Graphics.FromImage(bitmap);
            grx.Clear(Color.LightGray);
        }

        private void manageDirectory()
        {
            Directory.CreateDirectory(location);
            var dirInfo = new DirectoryInfo(location);
            foreach (FileInfo file in dirInfo.GetFiles())
            {
                file.Delete();
            }
        }

        public void DrawLine(int north, int east)
        {
            var nNorth = north * 0.01;
            var nEast = east * 0.01;
            grx.DrawLine(Pen, (int)lastEast, (int)lastNorth, (int)(lastEast + nEast), (int)(lastNorth + nNorth));
            lastEast += nEast;
            lastNorth += nNorth;
            if (++step % 10 != 0) return;
            bitmap.Save($"{location}{step}.png");
        }
    }


    class Action
    {
        public Direction Type { get; set; }
        public int Value { get; set; }

        public static Action ToAction(string inp)
        {
            return new Action()
            {
                Type = CharToDirection(inp[0]),
                Value = int.Parse(inp[1..])
            };
        }

        private static Direction CharToDirection(char c) =>
            c switch
            {
                'N' => Direction.NORTH,
                'E' => Direction.EAST,
                'S' => Direction.SOUTH,
                'W' => Direction.WEST,
                'L' => Direction.LEFT,
                'R' => Direction.RIGHT,
                'F' => Direction.FORWARD,
                _ => throw new Exception("OH NO")
            };
    }

    enum Direction
    {
        NORTH,
        EAST,
        SOUTH,
        WEST,
        LEFT,
        RIGHT,
        FORWARD
    }
}