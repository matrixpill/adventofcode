﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AdventOfCode.Year2020.Day4Util;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2020
{
    class Day4 : Solution
    {
        private Passport[] Passports;

        public override void Initialize(string[] input)
        {
            Passports = prepareInput(input);
        }

        public override string SolvePart1()
        {
            var validPassports = 0;
            foreach (var passport in Passports)
            {
                if (PasswordValidator.HasRequiredFields(passport))
                    validPassports++;
            }

            return validPassports.ToString();
        }

        public override string SolvePart2()
        {
            var validPassports = 0;
            foreach (var passport in Passports)
            {
                if (PasswordValidator.IsValid(passport))
                    validPassports++;
            }

            return validPassports.ToString();
        }

        private Passport[] prepareInput(string[] input)
        {
            var passports = new List<Passport>();

            var readFields = new StringBuilder();

            foreach (var line in input)
            {
                if (string.IsNullOrWhiteSpace(line) && readFields.Length > 0)
                {
                    passports.Add(buildPassport(readFields.ToString()));
                    readFields.Clear();
                    continue;
                }

                readFields.Append(" " + line);
            }

            passports.Add(buildPassport(readFields.ToString()));

            return passports.ToArray();
        }

        private Passport buildPassport(string fields)
        {
            return new Passport()
            {
                Fields = fields.Replace("\n", "")
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => x.Split(':'))
                    .ToDictionary(mx => mx[0], mx => mx[1])
            };
        }
    }
}