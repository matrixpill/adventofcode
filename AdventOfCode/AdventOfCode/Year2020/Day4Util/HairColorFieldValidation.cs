﻿using System.Text.RegularExpressions;

namespace AdventOfCode.Year2020.Day4Util
{
    internal class HairColorFieldValidation : FieldValidation
    {
        Regex alphaNumber = new Regex("([^0-9a-f])", RegexOptions.Singleline);

        public HairColorFieldValidation(string name) : base(name) { }

        public override bool IsValid(string rawValue)
        {
            if (rawValue[0] == '#' && LengthValidation(rawValue, 7))
                return !alphaNumber.IsMatch(rawValue.Substring(1, 6));
            return false;
        }
    }

}
