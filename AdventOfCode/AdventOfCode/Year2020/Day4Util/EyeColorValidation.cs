﻿using System.Linq;

namespace AdventOfCode.Year2020.Day4Util
{
    internal class EyeColorValidation : FieldValidation
    {

        private readonly string[] eyeColors = new[] { "amb", "blu", "brn", "gry", "grn", "hzl", "oth" };

        public EyeColorValidation(string name) : base(name) { }

        public override bool IsValid(string rawValue) => eyeColors.Contains(rawValue);
    }

}
