﻿namespace AdventOfCode.Year2020.Day4Util
{
    internal class HeightFieldValidation : FieldValidation
    {
        public HeightFieldValidation(string name) : base(name) { }

        public override bool IsValid(string rawValue)
        {
            return rawValue[^2..] switch
            {
                "cm" => RangeValidation(rawValue[0..^2], 150, 193),
                "in" => RangeValidation(rawValue[0..^2], 59, 76),
                _ => false,
            };
        }
    }

}
