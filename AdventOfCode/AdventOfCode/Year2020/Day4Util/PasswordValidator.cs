﻿using System.Linq;

namespace AdventOfCode.Year2020.Day4Util
{
    internal class PasswordValidator
    {
        static FieldValidation[] requiredFields = new FieldValidation[] {
                new NumberFieldValidation("byr", 4, 1920, 2002),
                new NumberFieldValidation("iyr", 4, 2010, 2020),
                new NumberFieldValidation("eyr", 4, 2020, 2030),
                new HeightFieldValidation("hgt"),
                new HairColorFieldValidation("hcl"),
                new EyeColorValidation("ecl"),
                new NumberFieldValidation("pid", 9, 0, 999999999)};

        string[] optional = new[] { "cid" };

        public static bool IsValid(Passport passport)
        {
            foreach (var validator in requiredFields)
            {
                string value = HasKey(passport, validator.Name);
                if (value == null || passport.Fields.Count < 7) return false;

                if (!validator.IsValid(passport.Fields[validator.Name]))
                    return false;
            }
            return true;
        }

        public static bool HasRequiredFields(Passport passport)
        {
            return !requiredFields.Any(x => HasKey(passport, x.Name) == null);
        }

        private static string HasKey(Passport passport, string key)
        {
            passport.Fields.TryGetValue(key, out string value);
            return value;
        }
    }

}
