﻿namespace AdventOfCode.Year2020.Day4Util
{
    internal interface IValid
    {
        public bool IsValid(string rawValue);
    }

}
