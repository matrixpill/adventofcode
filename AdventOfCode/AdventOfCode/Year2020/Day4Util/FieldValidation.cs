﻿namespace AdventOfCode.Year2020.Day4Util
{
    internal abstract class FieldValidation : IValid
    {
        public string Name { get; set; }

        protected FieldValidation(string name)
        {
            Name = name;
        }

        protected bool LengthValidation(string value, int length)
        {
            return length == value.Length;
        }

        protected bool RangeValidation(string rawValue, int min, int max)
        {
            if (int.TryParse(rawValue, out int value))
                if (value >= min && value <= max)
                    return true;
            return false;
        }

        public abstract bool IsValid(string rawValue);
    }
}
