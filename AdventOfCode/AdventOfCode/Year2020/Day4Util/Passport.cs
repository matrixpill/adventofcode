﻿using System.Collections.Generic;

namespace AdventOfCode.Year2020.Day4Util
{
    internal class Passport
    {
        public Dictionary<string, string> Fields { get; set; }
    }

}
