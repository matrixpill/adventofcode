﻿namespace AdventOfCode.Year2020.Day4Util
{

    internal class NumberFieldValidation : FieldValidation
    {
        private readonly int min;
        private readonly int max;
        private readonly int length;
        public NumberFieldValidation(string name, int length, int min = 0, int max = 0) : base(name)
        {
            this.min = min;
            this.max = max;
            this.length = length;
        }

        public override bool IsValid(string rawValue) => RangeValidation(rawValue, min, max) && LengthValidation(rawValue, length);
    }

}
