using System;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2023
{
    [Day(2)]
    internal class Day2 : Solution
    {
        public override string SolvePart1()
        {
            var gameCubes = new GameCubes(12, 13, 14);
            var sum = 0;
            foreach (var game in Input)
            {
                var colonIdx = game.IndexOf(':');
                var gameId = int.Parse(game[5..colonIdx]);
                if (isGamePossiblie(game[(colonIdx + 1)..], gameCubes))
                {
                    sum += gameId;
                }
            }

            return sum.ToString();
        }


        private bool isGamePossiblie(string game, GameCubes gameCubes)
        {
            var sets = game.Split(";");
            foreach (var set in sets)
            {
                var colors = set.Split(",", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
                foreach (var color in colors)
                {
                    var cnt = int.Parse(color[0..color.IndexOf(" ")]);
                    if (color.EndsWith("red"))
                    {
                        if (cnt > gameCubes.Red) return false;
                    }
                    else if (color.EndsWith("green"))
                    {
                        if (cnt > gameCubes.Green) return false;
                    }
                    else if (color.EndsWith("blue"))
                    {
                        if (cnt > gameCubes.Blue) return false;
                    }
                }
            }

            return true;
        }

        public override string SolvePart2()
        {
            var sum = 0;
            foreach (var game in Input)
            {
                var colonIdx = game.IndexOf(':');
                var gameId = int.Parse(game[5..colonIdx]);
                sum += powerOfGame(game[(colonIdx + 1)..]);
            }

            return sum.ToString();
        }

        private int powerOfGame(string game)
        {
            var sets = game.Split(";");
            var minR = 0;
            var minG = 0;
            var minB = 0;
            foreach (var set in sets)
            {
                var colors = set.Split(",", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
                foreach (var color in colors)
                {
                    var cnt = int.Parse(color[0..color.IndexOf(" ")]);
                    if (color.EndsWith("red"))
                    {
                        if (cnt > minR) minR = cnt;
                    }
                    else if (color.EndsWith("green"))
                    {
                        if (cnt > minG) minG = cnt;
                    }
                    else if (color.EndsWith("blue"))
                    {
                        if (cnt > minB) minB = cnt;
                    }
                }
            }

            return minR * minG * minB;
        }
    }

    record GameCubes(int Red, int Green, int Blue);
}