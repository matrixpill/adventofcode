﻿using System.Collections.Generic;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2023
{
    [Day(1)]
    internal class Day1 : Solution
    {
        Dictionary<string, int> numNames = new Dictionary<string, int>()
        {
            { "1", 1 },
            { "2", 2 },
            { "3", 3 },
            { "4", 4 },
            { "5", 5 },
            { "6", 6 },
            { "7", 7 },
            { "8", 8 },
            { "9", 9 },
            { "one", 1 },
            { "two", 2 },
            { "three", 3 },
            { "four", 4 },
            { "five", 5 },
            { "six", 6 },
            { "seven", 7 },
            { "eight", 8 },
            { "nine", 9 }
        };


        public override string SolvePart1()
        {
            var sum = 0;
            foreach (var line in Input)
            {
                var val = GetCalibrationVal(line);
                sum += val;
            }

            return sum.ToString();
        }

        public override string SolvePart2()
        {
            var sum = 0;
            foreach (var line in Input)
            {
                int val = GetCalibrationVal(line, true);
                sum += val;
            }

            return sum.ToString();
        }

        private int GetCalibrationVal(string line, bool writtenNumbers = false)
        {
            var firstIdx = int.MaxValue;
            var fistVal = 0;
            var lastIdx = int.MinValue;
            var lastVal = 0;
            foreach (var numText in numNames)
            {
                if (!writtenNumbers && numText.Key == "one") break;
                var fisrtTmpIdx = line.IndexOf(numText.Key);
                var lastTmpIdx = line.LastIndexOf(numText.Key);
                if (fisrtTmpIdx < firstIdx)
                {
                    firstIdx = fisrtTmpIdx;
                    fistVal = numText.Value;
                }

                if (lastTmpIdx > lastIdx)
                {
                    lastIdx = lastTmpIdx;
                    lastVal = numText.Value;
                }
            }

            var val = fistVal * 10 + lastVal;
            return val;

            //var first = -100;
            //var last = 0;
            //for (int i = 0; i < line.Length; i++)
            //{
            //    char c = line[i];
            //    if (char.IsDigit(c))
            //    {
            //        var numVal = (int)char.GetNumericValue(c);
            //        last = numVal;
            //        if (first == -100)
            //            first = numVal;

            //        continue;
            //    }
            //    if (!writtenNumbers) continue;
            //    foreach (var numText in numNames)
            //    {
            //        if (line.Substring(i).StartsWith(numText.Key))
            //        {
            //            var numVal = numText.Value;
            //            last = numVal;
            //            if (first == -100)
            //                first = numVal;
            //            continue;
            //        }
            //    }
            //}

            //var val = first * 10 + last;
            //return val;
        }
    }
}