﻿using AdventOfCode.Year2020;
using AdventOfCode.Year2021;
using AdventOfCode.Year2022;
using AdventOfCode.Year2023;
using AoCUtils.Interfaces;
using FSharp.Year2024;

namespace AdventOfCode.Base
{
    public static class ImplementationContext
    {
        public static Solution GetSolution(int year, int day)
            => year switch
            {
                2020 => BaseYear<_2020>.Implementation(day),
                2021 => BaseYear<_2021>.Implementation(day),
                2022 => BaseYear<_2022>.Implementation(day),
                2023 => BaseYear<_2023>.Implementation(day),
                2024 => BaseYear<_2024>.Implementation(day),
                _ => new EmptySolution()
            };
    }
}