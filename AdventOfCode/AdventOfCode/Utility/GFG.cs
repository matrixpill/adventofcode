﻿using System.Collections.Generic;

namespace AdventOfCode.Utility
{
    public class GFG
    {
        // Returns LCM of arr[0..n-1]  
        public static ulong LCM(ulong[] arr)
        {
            var n = (ulong)arr.Length;
            // Find the maximum value in arr[]  
            ulong max_num = 0;
            for (ulong i = 0; i < n; i++)
            {
                if (max_num < arr[i])
                {
                    max_num = arr[i];
                }
            }

            // Initialize result  
            ulong res = 1;

            // Find all factors that are present  
            // in two or more array elements.  
            ulong x = 2; // Current factor.  
            while (x <= max_num)
            {
                // To store indexes of all array  
                // elements that are divisible by x.  
                var indexes = new List<ulong>();
                for (ulong j = 0; j < n; j++)
                {
                    if (arr[j] % x == 0)
                    {
                        indexes.Add(j);
                    }
                }

                // If there are 2 or more array elements  
                // that are divisible by x.  
                if (indexes.Count >= 2)
                {
                    // Reduce all array elements divisible  
                    // by x.  
                    for (int j = 0; j < indexes.Count; j++)
                    {
                        arr[indexes[j]] = arr[indexes[j]] / x;
                    }

                    res = res * x;
                }
                else
                {
                    x++;
                }
            }

            // Then multiply all reduced  
            // array elements  
            for (ulong i = 0; i < n; i++)
            {
                res = res * arr[i];
            }

            return res;
        }
    }
}
