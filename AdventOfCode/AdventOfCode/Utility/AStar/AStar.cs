﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace AdventOfCode.Utility.AStar
{
    internal class AStar
    {
        public Vector2 Start { get; set; }
        public Vector2 End { get; set; }
        //private List<Node> openList = new List<Node>();
        private MyPriorityList openList = new MyPriorityList();
        private HashSet<Node> closedList = new HashSet<Node>();
        private Node[][] Levels;
        public AStar(Node[][] levels, Vector2 start, Vector2 end)
        {
            Levels = levels;
            Start = start;  
            End = end;
        }
        public long SolveAStar()
        {
            openList.Enqueue(Levels[(int)Start.X][(int)Start.Y], 0);
            do
            {
                var currentNode = openList.Dequeue();
                if (currentNode.Position == End) return currentNode.G;

                closedList.Add(currentNode);
                ExpandNode(currentNode);

            } while (openList.Count != 0);

            throw new Exception("No Way Found");
        }

        public void ExpandNode(Node currentNode)
        {
            foreach (var successor in currentNode.Successors)
            {
                var successorNode = Levels[(int)successor.X][(int)successor.Y];

                if (closedList.Contains(successorNode))
                    continue;

                var cost = successorNode.Cost;

                var tentativeG = currentNode.G + cost;

                var isInOpenList = openList.Contains(successorNode);
                if (isInOpenList && tentativeG >= successorNode.G)
                    continue;

                successorNode.G = tentativeG;


                var x = Vector2.Subtract(End, successor);
                var s = (long)(x.X + x.Y);
                var f = tentativeG + s;

                if (isInOpenList)
                {
                    openList.Update(successorNode, f);
                }
                else
                {
                    openList.Enqueue(successorNode, f);
                }
            }
        }
    }

    public class Node
    {
        public Vector2 Position { get; }
        public List<Vector2> Successors = new();
        public long G { get; set; }
        public long Cost { get; }

        public Node(Vector2 pos, long prio, long dang)
        {
            Cost = dang;
            Position = pos;
        }


        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj is not Node) return false;
            return ((Node)obj).GetHashCode() == GetHashCode();
        }

        public override int GetHashCode()
        {
            return Position.GetHashCode();
        }

        public Node Copy()
        {
            return new Node(new Vector2(Position.X, Position.Y), 0, Cost)
            {
                Successors = Successors.Select(x => new Vector2(x.X, x.Y)).ToList()
            };
        }
    }

    public class MyPriorityList
    {
        private readonly List<Node> Items = new();
        private readonly List<long> Prio = new();

        public void Enqueue(Node newItem, long priority)
        {
            if (Prio.Count == 0)
            {
                Items.Add(newItem);
                Prio.Add(priority);
                return;
            }
            var mid = Items.Count / 2;
            var dist = mid;
            while (true)
            {
                if (priority > Prio[mid])
                {
                    mid += dist / 2;
                    dist /= 2;
                }
                else if (priority < Prio[mid])
                {
                    mid -= dist / 2;
                    dist /= 2;
                }
                if (priority == Prio[mid] || dist == 0 || mid == 0 || mid == Prio.Count - 1)
                {

                    if (priority > Prio[mid])
                    {
                        Items.Insert(mid + 1, newItem);
                        Prio.Insert(mid + 1, priority);
                    }
                    else if (priority <= Prio[mid])
                    {
                        Items.Insert(mid, newItem);
                        Prio.Insert(mid, priority);
                    }

                    break;
                }
            }
        }

        public Node Dequeue()
        {
            var n = Items[0];

            Items.RemoveAt(0);
            Prio.RemoveAt(0);
            return n;
        }

        public void Clear()
        {
            Items.Clear();
            Prio.Clear();
        }

        public bool Contains(Node node) => Items.Contains(node);
        public int Count => Items.Count;

        public void Update(Node successorNode, long prio)
        {
            var idx = Items.IndexOf(successorNode);
            Items.RemoveAt(idx);
            Prio.RemoveAt(idx);

            Enqueue(successorNode, prio);
        }
    }
}
