﻿namespace AdventOfCode
{
    public enum SolutionLevel
    {
        Result1 = 1,
        Result2 = 2
    }
}
