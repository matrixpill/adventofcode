﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using AdventOfCode.Base;
using AoCUtils.Interfaces;
using HtmlAgilityPack;

namespace AdventOfCode
{
    public partial class Aoc
    {
        private const string HOST = "https://adventofcode.com";
        private readonly Stopwatch sw = new();
        private int year;
        private int day;
        readonly TimeZoneInfo estTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
        private readonly HttpClient http = new();

        private static string Cookie => "session=" + File.ReadAllText(@".\user.auth");
        private string InputDirectory => @$".\Inputs\{year}";
        private string InputFile => @$"{InputDirectory}\Day{day}.txt";
        private string SampleInputFile => @$"{InputDirectory}\Sample{day}.txt";
        private string ResultDirectory => @$".\Results\{year}";

        private string ResultFile(SolutionLevel level)
        {
            return @$"{ResultDirectory}\Day{day}_{(int)level}.result";
        }

        private string TrysFile(SolutionLevel level)
        {
            return @$"{ResultDirectory}\Day{day}_{(int)level}.trys";
        }

        private string BaseAdress => $"{HOST}/{year}/day/{day}";
        private string InputAdress => $"{BaseAdress}/input";
        private string AnswerAdress => $"{BaseAdress}/answer";


        public Aoc()
        {
            http.DefaultRequestHeaders.Add("Cookie", Cookie);
        }

        public Task Start()
        {
            while (true)
            {
                try
                {
                    GetYear();
                    GetDayOfMonth();
                    ExecuteImplementation();
                    MoreOptions();
                }
                catch (AocException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private void ExecuteImplementation()
        {
            if (TryGetSampleInput(out var sample))
            {
                Execute(sample, true);
            }

            var input = GetInput();
            Execute(input);
        }

        private void Execute(string[] input, bool isSample = false)
        {
            Console.WriteLine($"WITH {(isSample ? "SAMPLE" : "MAIN")} INPUT \n");
            var implementation = ImplementationContext.GetSolution(year, day);
            implementation.WithSampleData = isSample;
            sw.Restart();
            implementation.Initialize(input);
            sw.Stop();
            Console.WriteLine("Initialization: " + sw.ElapsedMilliseconds + " ms and " + sw.ElapsedTicks + " ticks\n");

            ExecuteSolution(SolutionLevel.Result1, implementation, isSample);
            Console.WriteLine();
            ExecuteSolution(SolutionLevel.Result2, implementation, isSample);
            Console.WriteLine("\n#######################################\n");
        }

        private void ExecuteSolution(SolutionLevel level, Solution implementation, bool isSample = false)
        {
            try
            {
                var solution = level == SolutionLevel.Result1
                    ? implementation.SolvePart1
                    : (Func<string>)implementation.SolvePart2;
                sw.Restart();
                var result = solution();
                sw.Stop();
                Console.WriteLine(
                    $"Part{(int)level}: {result} \nMilliseconds: {sw.ElapsedMilliseconds} ms and {sw.ElapsedTicks} ticks");
                if (!isSample)
                {
                    if (string.IsNullOrWhiteSpace(result) &&
                        ShouldCountinue("Your result is empty. Do you want to input the reult manually?"))
                    {
                        Console.Write("Your input: ");
                        result = Console.ReadLine();
                    }

                    if (!string.IsNullOrEmpty(result))
                        TestResult(level, result);
                }
            }
            catch (Exception e)
            {
                sw.Stop();
                if (e is NotImplementedException)
                {
                    Console.WriteLine("Part" + (int)level + ": Not implemented");
                }
                else
                    Console.WriteLine(e);
            }
        }

        private void GetYear()
        {
            year = DateTime.Now.Year;
            Console.Write("Which year? default(" + year + "): ");

            if (int.TryParse(Console.ReadLine(), out int inputYear))
            {
                if (inputYear > 2015 && inputYear < year)
                    year = inputYear;
            }
        }

        private void GetDayOfMonth()
        {
            var estTime = TimeZoneInfo.ConvertTime(DateTime.Now, estTimeZoneInfo);
            day = estTime.Day;
            Console.Write("Which day? default(" + day + "): ");
            if (int.TryParse(Console.ReadLine(), out int inputDay))
            {
                if (inputDay >= 1 && inputDay <= 31 && (inputDay <= day || year < DateTime.Now.Year))
                {
                    day = inputDay;
                }
                else
                    Console.WriteLine($"Unknown Date. Now Using Day: {day}/{year}");
            }

            Console.WriteLine();
        }

        private bool TryGetSampleInput(out string[] input)
        {
            if (File.Exists(SampleInputFile))
            {
                input = File.ReadAllLines(SampleInputFile);
                return true;
            }

            input = null;
            return false;
        }

        private string[] GetInput()
        {
            if (File.Exists(InputFile))
                return File.ReadAllLines(InputFile);

            try
            {
                var response = http.GetStringAsync(InputAdress);
                response.Wait();
                var lines = response.Result.Split("\n").ToList();
                if (lines[^1] == string.Empty)
                {
                    lines.RemoveAt(lines.Count - 1);
                }

                Directory.CreateDirectory(InputDirectory);
                File.WriteAllLines(InputFile, lines);
                return lines.ToArray();
            }
            catch (Exception ex)
            {
                throw new AocException("Error on GetInput '" + InputAdress + "'", ex);
            }
        }

        private bool ShouldCountinue(string question)
        {
            Console.Write(question + " (y/n): ");
            var yOrNo = Console.ReadLine() ?? "n";
            if (yOrNo.StartsWith("y", StringComparison.CurrentCultureIgnoreCase))
                return true;
            return false;
        }

        private void TestResult(SolutionLevel level, string result)
        {
            Directory.CreateDirectory(ResultDirectory);

            if (File.Exists(ResultFile(level)))
            {
                var rightAnswer = File.ReadAllText(ResultFile(level));
                if (rightAnswer == result)
                {
                    Console.WriteLine("You had the right answer already");
                    return;
                }
                else
                {
                    Console.WriteLine("You had the right answer already but it was " + rightAnswer);
                    return;
                }
            }

            if (File.Exists(TrysFile(level)))
            {
                var trys = File.ReadAllLines(TrysFile(level)).Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
                var tried = trys.FirstOrDefault(x => x.Split(";")[0] == result);
                if (tried != null)
                {
                    Console.WriteLine("You tried this answer already: " +
                                      (tried.Split(";").Length > 1 ? tried.Split(";")[1] : ""));
                    return;
                }
            }

            if (!ShouldCountinue("Do you want to submit and save your Answer?"))
                return;

            var parameters = new Dictionary<string, string>
            {
                { "level", ((int)level).ToString() },
                { "answer", result }
            };

            var c = new FormUrlEncodedContent(parameters);
            var request = http.PostAsync(AnswerAdress, c);
            request.Wait();
            var response = request.Result;
            var responseHtml = response.Content.ReadAsStringAsync().Result;
            var pageDocument = new HtmlDocument();
            pageDocument.LoadHtml(responseHtml);
            var node = pageDocument.DocumentNode.SelectSingleNode("//main/article");
            var text = node.InnerText;


            switch (text)
            {
                case var _ when text.Contains("That's the right answer"):
                    Console.WriteLine("That's the right answer");
                    File.WriteAllText(ResultFile(level), result);
                    break;
                case var _ when text.Contains("Did you already complete it"):
                    Console.WriteLine("Did you already complete it");
                    File.WriteAllText(ResultFile(level), result);
                    break;
                case var _ when text.Contains("That's not the right answer"):
                    var semi = text.IndexOf(";");
                    var colon = text.IndexOf(".");
                    var info = "";
                    if (semi >= 0 && colon >= 0 && colon > semi)
                        info = text.Substring(semi + 1, colon - semi);

                    Console.WriteLine("That's not the right answer " + info);
                    File.AppendAllText(TrysFile(level), result + ";" + info + Environment.NewLine);
                    break;
                case var _ when text.Contains("You gave an answer too recently"):
                    Console.WriteLine(text);
                    break;
                default:
                    Console.WriteLine("Unrecognized response: " + text);
                    break;
            }
        }

        private void ReadTestData()
        {
            if (File.Exists(SampleInputFile) && !ShouldCountinue("Do you want do overwrite an existing Sampleinput?"))
                return;
            var request = http.GetAsync(BaseAdress);
            request.Wait();
            var response = request.Result;
            var responseHtml = response.Content.ReadAsStringAsync().Result;
            var pageDocument = new HtmlDocument();
            pageDocument.LoadHtml(responseHtml);
            var nodes = pageDocument.DocumentNode.SelectNodes("//main/article");

            List<HtmlNode> sampleNodes = new();
            foreach (var node in nodes)
            {
                var codeNodes = node.SelectNodes("pre/code");
                if (codeNodes != null)
                    sampleNodes.AddRange(codeNodes);
            }


            if (sampleNodes == null)
            {
                Console.WriteLine("No Nodes found.");
                return;
            }

            var selectedNode = 0;
            var rowsToClear = 0;
            while (true)
            {
                var innerText = HttpUtility.HtmlDecode(sampleNodes[selectedNode].InnerText);
                if (innerText.EndsWith("\n"))
                    innerText = innerText[..^1];
                PrintNode(innerText, rowsToClear);
                rowsToClear = sampleNodes[selectedNode].InnerText.Split("\n").Length;
                var key = Console.ReadKey(true);
                switch (key.Key)
                {
                    case ConsoleKey.UpArrow:
                        if (selectedNode > 0)
                            selectedNode--;
                        break;
                    case ConsoleKey.DownArrow:
                        if (selectedNode < sampleNodes.Count - 1)
                            selectedNode++;
                        break;
                    case ConsoleKey.Escape:
                        return;
                    case ConsoleKey.Enter:
                        File.WriteAllText(SampleInputFile, innerText);
                        return;
                }
            }
        }

        private void PrintNode(string text, int rowsToClear)
        {
            if (rowsToClear > 0)
                rowsToClear += 4;
            for (var i = 0; i < rowsToClear; i++)
            {
                Console.CursorTop -= 1;
                Console.CursorLeft = 0;
                Console.Write(new string(' ', Console.WindowWidth));
                Console.CursorLeft = 0;
            }

            Console.WriteLine("------------------------------------------------------");
            Console.WriteLine();
            Console.WriteLine(text);
            Console.WriteLine();
            Console.WriteLine("-- enter = select, esc = cancel, ^ = prev, v = next --");
            Console.WriteLine();
        }

        private void MoreOptions()
        {
            Console.WriteLine(
                "you have following options: 'sample' -> load sample data; 'bench' -> benchmark day; Enter(or everything else) -> start over;");
            var userInput = Console.ReadLine()?.Trim().ToLower();
            if (userInput == null) return;
            var options = userInput.Split(" ");
            if (options.Length < 1) return;
            switch (options[0])
            {
                case "sample":
                    ReadTestData();
                    break;
                case "bench":
                {
                    var n = 1000L;
                    if (options.Length == 2)
                        if (long.TryParse(options[1], out var parsed))
                            n = parsed;

                    var input = GetInput();
                    var implementation = ImplementationContext.GetSolution(year, day);
                    implementation.WithSampleData = false;
                    Console.WriteLine($"Benchmarking with {n} excecutions...");
                    Bench("Initializing", () => implementation.Initialize(input));
                    Bench("Task 1", () => implementation.SolvePart1());
                    Bench("Task 2", () => implementation.SolvePart2());
                    break;


                    void Bench(string name, Action action)
                    {
                        var percentNotice = 5;
                        var percentStep = Math.Max(n / (100 / percentNotice), 1);
                        var sum = 0L;
                        Console.Write($"\r{name}: 0%");
                        for (var i = 1; i <= (100 / percentNotice); i++)
                        {
                            sw.Restart();
                            for (var j = 0; j < percentStep; j++)
                                action();
                            sw.Stop();
                            sum += sw.ElapsedMilliseconds;

                            // if (i % percentStep != 0) continue;
                            var percent = i * percentNotice;

                            Console.Write($"\r{name}: {percent}% - avg: {(sum / (decimal)(percentStep * i)):F4}    ");
                        }

                        Console.WriteLine();
                    }
                }
                default: break;
            }

            Console.WriteLine();
        }
    }
}