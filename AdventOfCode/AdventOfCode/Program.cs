﻿using System;

namespace AdventOfCode
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            new Aoc().Start();
        }
    }
}
