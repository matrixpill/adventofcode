﻿using System.Collections.Generic;
using System.Numerics;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2021
{
    [Day(25)]
    internal class Day25 : Solution
    {
        private Board Floor;

        public override void Initialize(string[] input)
        {
            Floor = new Board(input[0].Length, input.Length);
            base.Initialize(input);
            for (int y = 0; y < input.Length; y++)
            {
                for (int x = 0; x < input[y].Length; x++)
                {
                    var c = input[y][x];
                    if (c == '>')
                    {
                        Floor.AddCucumber(new Vector2(x, y), Direction.EAST);
                    }
                    else if (c == 'v')
                    {
                        Floor.AddCucumber(new Vector2(x, y), Direction.SOUTH);
                    }
                }
            }
        }

        public override string SolvePart1()
        {
            var step = 0;
            while (true)
            {
                step++;
                if (!Floor.OneMove())
                    break;
            }

            return step.ToString();
        }

        private class Board
        {
            public int Height { get; set; }
            public int Width { get; set; }

            private HashSet<Vector2> EastMovingCucumbers = new HashSet<Vector2>();
            private HashSet<Vector2> SouthMovingCucumbers = new HashSet<Vector2>();

            public Board(int width, int height)
            {
                Width = width;
                Height = height;
            }

            public bool OneMove()
            {
                var eastHasMoved = DoMove(Direction.EAST);
                var southhasMoved = DoMove(Direction.SOUTH);
                return eastHasMoved || southhasMoved;
            }


            private bool DoMove(Direction direction)
            {
                bool hasMoved = false;
                var newPositions = new HashSet<Vector2>();

                foreach (var cucumber in (direction == Direction.EAST) ? EastMovingCucumbers : SouthMovingCucumbers)
                {
                    var newPos = NewPos(cucumber, direction);
                    if (Contains(newPos))
                        newPositions.Add(cucumber);
                    else
                    {
                        hasMoved = true;
                        newPositions.Add(newPos);
                    }
                }

                if (direction == Direction.EAST)
                    EastMovingCucumbers = newPositions;
                else
                    SouthMovingCucumbers = newPositions;
                return hasMoved;
            }


            private bool Contains(Vector2 vec)
            {
                return EastMovingCucumbers.Contains(vec) || SouthMovingCucumbers.Contains(vec);
            }

            private Vector2 NewPos(Vector2 pos, Direction dir)
            {
                var nV = new Vector2(dir == Direction.EAST ? pos.X + 1 : pos.X,
                    dir == Direction.SOUTH ? pos.Y + 1 : pos.Y);
                if (nV.X >= Width)
                    nV.X = 0;

                if (nV.Y >= Height)
                    nV.Y = 0;

                return nV;
            }

            public void AddCucumber(Vector2 position, Direction direction)
            {
                if (direction == Direction.EAST)
                    EastMovingCucumbers.Add(position);
                else if (direction == Direction.SOUTH)
                    SouthMovingCucumbers.Add(position);
            }
        }

        private enum Direction
        {
            EAST,
            SOUTH
        }
    }
}