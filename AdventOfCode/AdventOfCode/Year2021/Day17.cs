﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2021
{
    [Day(17)]
    internal class Day17 : Solution
    {
        private int xMin, xMax, yMin, yMax;

        public override void Initialize(string[] input)
        {
            base.Initialize(input);

            var rightFromColon = input[0].Split(":")[1].Trim();
            var area = rightFromColon.Split(",");
            var xArea = area[0].Split('=')[1];
            var yArea = area[1].Split('=')[1];
            xMin = int.Parse(xArea.Split("..")[0]);
            xMax = int.Parse(xArea.Split("..")[1]);
            yMin = int.Parse(yArea.Split("..")[0]);
            yMax = int.Parse(yArea.Split("..")[1]);
        }

        public override string SolvePart1()
        {
            var possibleXs = GetPossibleXRange();
            var possibleSteps = new List<int>();
            foreach (var possibleX in possibleXs)
                possibleSteps.AddRange(possibleX.Item2);

            var yPossibles = CheckPossibleStepsWithY(possibleSteps.Distinct().ToArray());

            var highest = int.MinValue;
            foreach (var xes in possibleXs)
            {
                foreach (var yes in yPossibles)
                {
                    var steps = xes.Item2.Where(x => yes.Item2.Contains(x)).ToArray();
                    foreach (var step in steps)
                    {
                        var xpos = XPosForStep(step, xes.Item1);
                        var ypos = YPosForStep(step, yes.Item1);
                        if (xpos >= xMin && xpos <= xMax && ypos >= yMin && ypos <= yMax)
                        {
                            var maxH = YPosForStep(yes.Item1, yes.Item1);

                            if (maxH > highest)
                                highest = maxH;
                        }
                    }
                }
            }

            return highest.ToString();
        }

        public override string SolvePart2()
        {
            var possibleXs = GetPossibleXRange();
            var possibleSteps = new List<int>();
            foreach (var possibleX in possibleXs)
                possibleSteps.AddRange(possibleX.Item2);

            var yPossibles = CheckPossibleStepsWithY(possibleSteps.Distinct().ToArray());

            var unique = new List<Vector2>();
            foreach (var xes in possibleXs)
            {
                foreach (var yes in yPossibles)
                {
                    var steps = xes.Item2.Where(x => yes.Item2.Contains(x)).ToArray();
                    foreach (var step in steps)
                    {
                        var xpos = XPosForStep(step, xes.Item1);
                        var ypos = YPosForStep(step, yes.Item1);

                        if (!unique.Contains(new Vector2(xes.Item1, yes.Item1)))
                        {
                            unique.Add(new Vector2(xes.Item1, yes.Item1));
                        }
                    }
                }
            }

            return unique.Count.ToString();
        }

        private (int, int[])[] CheckPossibleStepsWithY(int[] steps)
        {
            var yPlusSteps = new List<(int, int[])>();

            for (int y = -10000; y < 10000; y++)
            {
                var possibleSteps = new List<int>();

                foreach (var step in steps)
                {
                    var pos = YPosForStep(step, y);
                    if (pos >= yMin && pos <= yMax)
                    {
                        possibleSteps.Add(step);
                    }
                }

                if (possibleSteps.Count > 0)
                    yPlusSteps.Add((y, possibleSteps.ToArray()));
            }

            return yPlusSteps.ToArray();
        }

        private (int, int[])[] GetPossibleXRange()
        {
            var xPlusSteps = new List<(int, int[])>();
            for (int x = 0; x <= xMax; x++)
            {
                var possibleSteps = new List<int>();
                var pos = XPosForStep(x, x);
                if (pos < xMin) continue;
                for (int step = 1000; step > 0; step--)
                {
                    var xPos = XPosForStep(step, x);
                    if (xPos >= xMin && xPos <= xMax)
                    {
                        possibleSteps.Add(step);
                    }
                    else if (xPos < xMin)
                    {
                        break;
                    }
                }

                if (possibleSteps.Count > 0)
                    xPlusSteps.Add((x, possibleSteps.ToArray()));
            }

            return xPlusSteps.ToArray();
        }


        private int XPosForStep(int n, int stepSize)
        {
            var step = n - (((n - stepSize) > 0) ? (n - stepSize) : 0);
            var tmp = step * stepSize;
            var m = MissingX(step);
            return tmp - m;
        }

        private int YPosForStep(int n, int stepSize)
        {
            var tmp = n * stepSize;
            var m = MissingX(n);
            return tmp - m;
        }

        private int MissingX(int n)
        {
            return (n - 1) * n / 2;
        }
    }
}