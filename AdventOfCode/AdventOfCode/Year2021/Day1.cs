﻿using System;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2021
{
    [Day(1)]
    internal class Day1 : Solution
    {
        private long[] numericInputs;

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            numericInputs = Array.ConvertAll(input, long.Parse);
        }


        public override string SolvePart1()
        {
            long inc = 0;
            var prev = long.MaxValue;
            foreach (var line in numericInputs)
            {
                if (line > prev)
                    inc++;
                prev = line;
            }

            return inc.ToString();
        }

        public override string SolvePart2()
        {
            long inc = 0;
            var lastSum = long.MaxValue;

            for (var i = 0; i < numericInputs.Length - 2; i++)
            {
                var sum = numericInputs[i] + numericInputs[i + 1] + numericInputs[i + 2];
                if (sum > lastSum)
                    inc++;
                lastSum = sum;
            }

            return inc.ToString();
        }
    }
}