﻿using System;
using System.Collections.Generic;
using System.Linq;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2021
{
    [Day(24)]
    internal class Day24 : Solution
    {
        private ALU Alu;

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            Alu = BuildAlu();
        }

        private ALU BuildAlu()
        {
            var instructions = new List<Instruction>();
            var newAlu = new ALU();
            foreach (var instruction in Input)
            {
                var desc = instruction.Split(' ');
                switch (desc[0])
                {
                    case "inp":
                        instructions.Add(new Inp(desc[1][0], newAlu));
                        break;
                    case "add":
                        instructions.Add(new Add(desc[1][0], desc[2], newAlu));
                        break;
                    case "mul":
                        instructions.Add(new Mul(desc[1][0], desc[2], newAlu));
                        break;
                    case "div":
                        instructions.Add(new Div(desc[1][0], desc[2], newAlu));
                        break;
                    case "mod":
                        instructions.Add(new Mod(desc[1][0], desc[2], newAlu));
                        break;
                    case "eql":
                        instructions.Add(new Eql(desc[1][0], desc[2], newAlu));
                        break;
                }
            }

            newAlu.Setinstructions(instructions);
            return newAlu;
        }

        public override string SolvePart1()
        {
            RunRange();

            return "".ToString();
        }

        private string RunRange()
        {
            var queue = new Queue<int>();
            var set = new HashSet<LongWithZOut>
            {
                new LongWithZOut
                {
                    Z = 0,
                }
            };
            var dMain = new Dictionary<int, Dictionary<long, List<long>>>();

            for (int idx = 14; idx > 0; idx--)
            {
                var nextLookup = new HashSet<LongWithZOut>();
                Console.Write(idx + " ");
                var d = new Dictionary<long, List<long>>();
                for (int i = 9; i > 0; i--)
                {
                    for (int zIn = 0; zIn < 1000000; zIn++)
                    {
                        queue.Clear();
                        queue.Enqueue(i);
                        Alu.SetInput(queue);
                        var res = Alu.ExecuteSection(idx, zIn);
                        if (res && set.Any(x => x.Z == Alu.Z))
                        {
                            if (!nextLookup.Any(x => x.Z == zIn))
                            {
                                var g = new LongWithZOut
                                {
                                    Z = zIn,
                                };
                                g.Outs.Add(i);
                                nextLookup.Add(g);
                            }
                            else
                            {
                                nextLookup.First(x => x.Z == zIn).Outs.Add(i);
                            }

                            if (!d.ContainsKey(zIn))
                            {
                                d.Add(zIn, new List<long>() { i });
                            }
                            else
                                d[zIn].Add(i);
                        }
                    }
                }

                set = nextLookup;
                dMain.Add(idx, d);
            }

            return "";
        }
    }

    class LongWithZOut
    {
        public long Z;
        public List<int> Outs = new List<int>();

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj.GetType() != typeof(LongWithZOut)) return false;
            return (obj as LongWithZOut).GetHashCode() == GetHashCode();
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Z);
        }
    }

    class ALU
    {
        public long W { get; private set; } = 0;
        public long X { get; private set; } = 0;
        public long Y { get; private set; } = 0;
        public long Z { get; private set; } = 0;

        private List<Instruction> instructions = new List<Instruction>();
        private Queue<int> input;

        public void Setinstructions(List<Instruction> instructions)
        {
            this.instructions = instructions;
        }

        public bool Execute(int untilNr)
        {
            var success = true;

            var nr = 0;
            //Console.WriteLine($"init: w:{W} x:{X} y:{Y} z:{Z}");

            for (int currInstr = 0; currInstr < instructions.Count; currInstr++)
            {
                var nxtI = instructions[currInstr];
                if (nxtI is Inp)
                    nr++;
                if (nr > untilNr) break;
                success = nxtI.Execute();
                //Console.WriteLine($"after {nxtI} \t w:{W} x:{X} y:{Y} z:{Z}");
                if (!success) break;
            }

            return success;
        }

        internal bool ExecuteSection(int section, long z)
        {
            Z = z;
            var success = true;

            var nr = section - 1;
            //Console.WriteLine($"init: w:{W} x:{X} y:{Y} z:{Z}");

            for (int currInstr = (section - 1) * 18; currInstr < instructions.Count; currInstr++)
            {
                var nxtI = instructions[currInstr];
                if (nxtI is Inp)
                    nr++;
                if (nr > section) break;
                success = nxtI.Execute();
                //Console.WriteLine($"after {nxtI} \t w:{W} x:{X} y:{Y} z:{Z}");
                if (!success) break;
            }

            return success;
        }


        public long this[char index]
        {
            get => GetValue(index);
            set => SetValue(index, value);
        }

        public int AskForNextInput()
        {
            if (input.Count > 0)
                return input.Dequeue();
            throw new Exception("now more inputs");
        }


        public void Reset()
        {
            W = 0;
            X = 0;
            Y = 0;
            Z = 0;
        }

        public void SetInput(Queue<int> inp)
        {
            input = inp;
            Reset();
        }

        private long GetValue(char c) => c switch
        {
            'w' => W,
            'x' => X,
            'y' => Y,
            'z' => Z,
            _ => throw new KeyNotFoundException(c + " not found. Couldnt return value"),
        };

        private void SetValue(char c, long v)
        {
            switch (c)
            {
                case 'w':
                    W = v;
                    break;
                case 'x':
                    X = v;
                    break;
                case 'y':
                    Y = v;
                    break;
                case 'z':
                    Z = v;
                    break;
                default:
                    throw new KeyNotFoundException(c + " not found. Couldnt set value");
            }
        }
    }


    abstract class Instruction
    {
        public readonly char A;
        public readonly char B;
        protected int bVal;
        protected ALU alu;

        public Instruction(char a, ALU alu, string b = null)
        {
            this.alu = alu;
            A = a;
            if (b == null) return;

            if (!int.TryParse(b, out bVal))
            {
                B = b[0];
            }
        }

        public override string ToString()
        {
            return $"{GetType().Name} {A} {(GetType().Name != "Inp" ? ((B == '\0') ? bVal.ToString() : B) : "")}"
                .PadRight(10);
        }

        protected long GetB()
        {
            return (B == '\0') ? bVal : alu[B];
        }

        public abstract bool Execute();
    }

    class Value : Instruction
    {
        private long value;

        public Value(long value, ALU alu) : base('\0', alu)
        {
            this.value = value;
        }

        public override bool Execute()
        {
            //return value;
            return false;
        }
    }

    class Inp : Instruction
    {
        public Inp(char a, ALU alu) : base(a, alu)
        {
        }

        public override bool Execute()
        {
            alu[A] = alu.AskForNextInput();
            return true;
        }
    }

    class Add : Instruction
    {
        public Add(char a, string b, ALU alu) : base(a, alu, b)
        {
        }

        public override bool Execute()
        {
            alu[A] += GetB();
            return true;
        }
    }

    class Mul : Instruction
    {
        public Mul(char a, string b, ALU alu) : base(a, alu, b)
        {
        }

        public override bool Execute()
        {
            alu[A] *= GetB();
            return true;
        }
    }

    class Div : Instruction
    {
        public Div(char a, string b, ALU alu) : base(a, alu, b)
        {
        }

        public override bool Execute()
        {
            if (GetB() == 0) return false;
            alu[A] /= GetB();
            return true;
        }
    }

    class Mod : Instruction
    {
        public Mod(char a, string b, ALU alu) : base(a, alu, b)
        {
        }

        public override bool Execute()
        {
            if (alu[A] < 0 || GetB() <= 0) return false;
            alu[A] %= GetB();
            return true;
        }
    }

    class Eql : Instruction
    {
        public Eql(char a, string b, ALU alu) : base(a, alu, b)
        {
        }

        public override bool Execute()
        {
            alu[A] = (alu[A] == GetB()) ? 1 : 0;
            return true;
        }
    }
}