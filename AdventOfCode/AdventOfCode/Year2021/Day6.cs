﻿using System;
using System.Collections.Generic;
using System.Linq;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2021
{
    [Day(6)]
    class Day6 : Solution
    {
        private List<int> Fish = new List<int>();

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            Fish = Array.ConvertAll(Input[0].Split(","), int.Parse).ToList();
        }

        ulong daysPart1 = 80;

        public override string SolvePart1()
        {
            var tmpFish = Fish;
            for (ulong i = 0; i < daysPart1; i++)
            {
                var newFish = new List<int>();
                foreach (var fi in tmpFish)
                {
                    if (fi == 0)
                    {
                        newFish.Add(6);
                        newFish.Add(8);
                        continue;
                    }

                    newFish.Add(fi - 1);
                }

                tmpFish = newFish;
            }

            return tmpFish.Count.ToString();
        }

        ulong daysPart2 = 256;
        Dictionary<ulong, ulong> dic = new Dictionary<ulong, ulong>();

        public override string SolvePart2()
        {
            var sumOfFish = (ulong)Fish.Count;
            foreach (var fi in Fish)
            {
                sumOfFish += SpawnFish((ulong)fi, 0);
            }

            return sumOfFish.ToString();
        }

        private ulong SpawnFish(ulong curVal, ulong curDay)
        {
            ulong willSpawn = (daysPart2 - (curVal + curDay) - 1) / 7 + 1;
            for (var i = willSpawn; i > 0; i--)
            {
                var atDay = curVal + curDay + 1 + (7 * (i - 1));
                ulong tmp = 0;
                if (dic.ContainsKey(atDay))
                {
                    tmp = dic[atDay];
                }
                else
                {
                    if (8 + atDay < daysPart2)
                    {
                        tmp = SpawnFish(8, atDay);
                        dic.Add(atDay, tmp);
                    }
                }

                willSpawn += tmp;
            }

            return willSpawn;
        }
    }
}