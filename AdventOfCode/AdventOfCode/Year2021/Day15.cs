﻿using System;
using System.Numerics;
using AdventOfCode.Utility.AStar;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2021
{
    [Day(15)]
    internal class Day15 : Solution
    {
        private Node[][] Levels;

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            Levels = new Node[input.Length][];
            for (int x1 = 0; x1 < input[0].Length; x1++)
            {
                Levels[x1] = new Node[input.Length];
            }

            var start = new Vector2(0, 0);
            var end = new Vector2(Levels[0].Length - 1, Levels.Length - 1);
            var y = 0;
            foreach (string row in input)
            {
                var x = 0;
                foreach (var level in row)
                {
                    var pos = new Vector2(x, y);
                    var newNode = new Node(pos, 0, Convert.ToInt32(char.GetNumericValue(level)));
                    Levels[x++][y] = newNode;

                    var tmpPos = Vector2.Add(pos, new Vector2(0, -1));
                    if (!IsNotInCave(tmpPos, start, end))
                        newNode.Successors.Add(tmpPos);

                    tmpPos = Vector2.Add(pos, new Vector2(1, 0));
                    if (!IsNotInCave(tmpPos, start, end))
                        newNode.Successors.Add(tmpPos);

                    tmpPos = Vector2.Add(pos, new Vector2(0, 1));
                    if (!IsNotInCave(tmpPos, start, end))
                        newNode.Successors.Add(tmpPos);

                    tmpPos = Vector2.Add(pos, new Vector2(-1, 0));
                    if (!IsNotInCave(tmpPos, start, end))
                        newNode.Successors.Add(tmpPos);
                }

                y++;
            }
        }

        public override string SolvePart1()
        {
            var astar = new AStar(Levels, Vector2.Zero, new Vector2(Input[0].Length - 1, Input.Length - 1));
            return astar.SolveAStar().ToString();
        }

        //0,10 - 10,10 = -10,0 > +1 ---
        //10,10 - 10,10 = 0,0 > +2
        //10,0 - 10,10 = 0,-10 > +1
        //20,20 - 10,10 = 10,10 > +4
        //30,30 - 10,10 = 20,20 > +6
        //0,30 - 10,10 = -10,20 > +3
        public override string SolvePart2()
        {
            var origWIdth = Input[0].Length;
            var origHeight = Input.Length;

            var largerCave = new Node[origWIdth * 5][];
            for (int i = 0; i < largerCave.Length; i++)
            {
                largerCave[i] = new Node[origHeight * 5];
            }

            Levels = largerCave;
            var start = new Vector2(0, 0);
            var end = new Vector2(Levels[0].Length - 1, Levels.Length - 1);
            var astar = new AStar(Levels, start, end);
            var xSrc = 0;
            for (var x = 0; x < Levels[0].Length; x++)
            {
                var ySrc = 0;
                for (var y = 0; y < Levels.Length; y++)
                {
                    var pos = new Vector2(x, y);
                    var xDif = (int)Math.Floor(x / (decimal)origWIdth);
                    var yDif = (int)Math.Floor(y / (decimal)origHeight);
                    var newVal = Convert.ToInt32(char.GetNumericValue(Input[ySrc][xSrc])) + yDif + xDif;
                    if (newVal > 9)
                        newVal -= 9;
                    var newNode = new Node(pos, 0, newVal);
                    Levels[x][y] = newNode;

                    var tmpPos = Vector2.Add(pos, new Vector2(0, -1));
                    if (!IsNotInCave(tmpPos, start, end))
                        newNode.Successors.Add(tmpPos);

                    tmpPos = Vector2.Add(pos, new Vector2(1, 0));
                    if (!IsNotInCave(tmpPos, start, end))
                        newNode.Successors.Add(tmpPos);

                    tmpPos = Vector2.Add(pos, new Vector2(0, 1));
                    if (!IsNotInCave(tmpPos, start, end))
                        newNode.Successors.Add(tmpPos);

                    tmpPos = Vector2.Add(pos, new Vector2(-1, 0));
                    if (!IsNotInCave(tmpPos, start, end))
                        newNode.Successors.Add(tmpPos);


                    if (ySrc >= origHeight - 1)
                        ySrc = 0;
                    else
                        ySrc++;
                }

                if (xSrc >= origWIdth - 1)
                    xSrc = 0;
                else
                    xSrc++;
            }


            return astar.SolveAStar().ToString();
        }

        private bool IsNotInCave(Vector2 node, Vector2 start, Vector2 end)
        {
            return node.X > end.X || node.Y > end.Y ||
                   node.X < start.X || node.Y < start.Y;
        }
    }
}