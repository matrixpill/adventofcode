﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2021
{
    [Day(9)]
    class Day9 : Solution
    {
        public override void Initialize(string[] input)
        {
            base.Initialize(input);
        }

        public override string SolvePart1()
        {
            long sum = 0;
            var row = 0;
            foreach (var line in Input)
            {
                for (var col = 0; col < line.Length; col++)
                {
                    if (row > 0)
                    {
                        if (Input[row - 1][col] <= Input[row][col])
                        {
                            continue;
                        }
                    }

                    if (col < line.Length - 1)
                    {
                        if (Input[row][col + 1] <= Input[row][col])
                        {
                            continue;
                        }
                    }

                    if (row < Input.Length - 1)
                    {
                        if (Input[row + 1][col] <= Input[row][col])
                        {
                            continue;
                        }
                    }

                    if (col > 0)
                    {
                        if (Input[row][col - 1] <= Input[row][col])
                        {
                            continue;
                        }
                    }

                    sum += (int)char.GetNumericValue(Input[row][col]) + 1;
                }

                row++;
            }

            return sum.ToString();
        }

        public override string SolvePart2()
        {
            var row = 0;
            var x = new List<long>();
            var visitedVectors = new List<Vector2>();
            foreach (var line in Input)
            {
                for (var col = 0; col < line.Length; col++)
                {
                    if (Input[row][col] != '9' && !visitedVectors.Contains(new Vector2(row, col)))
                    {
                        var basin = new List<Vector2>();
                        CheckBasinSize(row, col, basin);
                        visitedVectors.AddRange(basin);
                        x.Add(basin.Count);
                    }
                }

                row++;
            }

            x = x.OrderByDescending(x => x).Take(3).ToList();

            return (x[0] * x[1] * x[2]).ToString();
        }

        private void CheckBasinSize(int row, int col, List<Vector2> currentBasin)
        {
            var vec = new Vector2(row, col);
            if (Input[row][col] == '9')
            {
                currentBasin.Remove(vec);
                return;
            }

            if (currentBasin.Contains(vec)) return;
            currentBasin.Add(new Vector2(row, col));
            if (row > 0)
            {
                CheckBasinSize(row - 1, col, currentBasin);
            }

            if (col < Input[0].Length - 1)
            {
                CheckBasinSize(row, col + 1, currentBasin);
            }

            if (row < Input.Length - 1)
            {
                CheckBasinSize(row + 1, col, currentBasin);
            }

            if (col > 0)
            {
                CheckBasinSize(row, col - 1, currentBasin);
            }
        }
    }
}