﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2021
{
    [Day(16)]
    class Day16 : Solution
    {
        private string bits;

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            var sb = new StringBuilder();

            foreach (var hex in input[0])
            {
                sb.Append(Convert.ToString(Convert.ToInt32(hex.ToString(), 16), 2).PadLeft(4, '0'));
            }

            bits = sb.ToString();
        }

        public override string SolvePart1()
        {
            var p = Decoder.Decode(bits);
            var sum = SumVersions(p).ToString();
            return sum;
        }

        public override string SolvePart2()
        {
            return Decoder.Decode(bits).GetValue().ToString();
        }

        private long SumVersions(Packet packet)
        {
            long sum = packet.Version;

            if (packet is OperatorPacket op && op.InnerPackets.Count > 0)
            {
                foreach (var inner in op.InnerPackets)
                {
                    sum += SumVersions(inner);
                }
            }

            return sum;
        }
    }

    static class Decoder
    {
        public static Packet Decode(string bits)
        {
            var versionsBits = bits[..3];
            var typeBits = bits[3..6];
            var version = Convert.ToInt32(versionsBits, 2);
            var typeId = Convert.ToInt32(typeBits, 2);
            var remainingBits = bits[6..];
            if (typeId == 4)
                return new LiteralPacket(version, remainingBits);

            return new OperatorPacket(version, typeId, remainingBits);
        }
    }

    class Packet
    {
        public int Version { get; }
        public int TypeId { get; }
        public int PacketLength { get; internal set; }

        protected Packet(int version, int typeId)
        {
            Version = version;
            TypeId = typeId;
        }

        internal IEnumerable<string> ChunkBits(string source, int groupSize)
        {
            var s = source;
            while (s.Length >= groupSize)
            {
                yield return s[..groupSize];
                s = s[groupSize..];
            }
        }

        public virtual long GetValue()
        {
            throw new NotImplementedException();
        }
    }

    internal class LiteralPacket : Packet
    {
        private string innerBits;
        private readonly List<string> bitPackages = new();

        public LiteralPacket(int version, string innerBits) : base(version, 4)
        {
            this.innerBits = innerBits;
            var chunkBits = ChunkBits(innerBits, 5).ToList();
            foreach (var bitpack in chunkBits)
            {
                bitPackages.Add(bitpack[1..]);
                if (bitpack[0] != '1')
                    break;
            }

            PacketLength = 3 + 3 + bitPackages.Count * 5;
        }

        public override long GetValue()
        {
            return GetLongValue();
        }

        private long GetLongValue()
        {
            var x = bitPackages.Aggregate((cur, next) => cur + next);
            return Convert.ToInt64(x, 2);
        }
    }

    internal class OperatorPacket : Packet
    {
        public List<Packet> InnerPackets { get; set; }
        private readonly string innerBits;
        private readonly LengthType lengthType;
        private readonly long lengthTypeValue;

        public OperatorPacket(int version, int typeId, string innerBits) : base(version, typeId)
        {
            InnerPackets = new List<Packet>();
            lengthType = (LengthType)int.Parse(innerBits[0].ToString());
            switch (lengthType)
            {
                case LengthType.TotalLength:
                    lengthTypeValue = Convert.ToInt64(innerBits[1..16], 2);
                    var until = 16 + (int)lengthTypeValue;
                    this.innerBits = innerBits[16..until];
                    PacketLength = 3 + 3 + 1 + 15;
                    break;
                case LengthType.SubPackagesCnt:
                    lengthTypeValue = Convert.ToInt64(innerBits[1..12], 2);
                    this.innerBits = innerBits[12..];
                    PacketLength = 3 + 3 + 1 + 11;
                    break;
            }

            BuildSubPackets();
        }

        public override long GetValue()
        {
            switch (TypeId)
            {
                case 0:
                    return InnerPackets.Sum(x => x.GetValue());
                case 1:
                    return InnerPackets.Aggregate((long)1, (prev, nxt) => prev * nxt.GetValue());
                case 2:
                    return InnerPackets.Min(x => x.GetValue());
                case 3:
                    return InnerPackets.Max(x => x.GetValue());
                case 5:
                    return InnerPackets[0].GetValue() > InnerPackets[1].GetValue() ? 1 : 0;
                case 6:
                    return InnerPackets[0].GetValue() < InnerPackets[1].GetValue() ? 1 : 0;
                case 7:
                    return InnerPackets[0].GetValue() == InnerPackets[1].GetValue() ? 1 : 0;
                default:
                    throw new ArgumentOutOfRangeException(nameof(TypeId), $"Id {TypeId} not found");
            }
        }

        private void BuildSubPackets()
        {
            var nextBits = innerBits;
            var countedBits = 0;
            var countedPackages = 0;
            while (true)
            {
                var x = Decoder.Decode(nextBits);
                nextBits = nextBits[x.PacketLength..];
                countedBits += x.PacketLength;
                countedPackages += 1;
                InnerPackets.Add(x);

                if (lengthType == LengthType.TotalLength && countedBits >= lengthTypeValue)
                    break;
                if (lengthType == LengthType.SubPackagesCnt && countedPackages >= lengthTypeValue)
                    break;
            }

            PacketLength += countedBits;
        }
    }

    enum LengthType
    {
        TotalLength,
        SubPackagesCnt
    }
}