﻿using System;
using System.Collections.Generic;
using System.Linq;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2021
{
    [Day(3)]
    class Day3 : Solution
    {
        public override string SolvePart1()
        {
            var gammaString = "";
            var epsilonString = "";
            for (var i = 0; i < Input[0].Length; i++)
            {
                var cnt0 = Count0BitsAtPosition(i, Input.ToList());
                if (cnt0 >= Input.Length / 2)
                {
                    gammaString += "0";
                    epsilonString += "1";
                }
                else
                {
                    gammaString += "1";
                    epsilonString += "0";
                }
            }

            var gammaValue = Convert.ToInt64(gammaString, 2);
            var epsilonValue = Convert.ToInt64(epsilonString, 2);

            return (gammaValue * epsilonValue).ToString();
        }

        public override string SolvePart2()
        {
            var oxyList = new List<string>();
            var c02List = new List<string>();
            foreach (var s in Input)
            {
                oxyList.Add(s);
                c02List.Add(s);
            }

            for (var i = 0; i < Input[0].Length; i++)
            {
                if (oxyList.Count != 1)
                {
                    var cnt0 = Count0BitsAtPosition(i, oxyList);
                    if (cnt0 > oxyList.Count / 2)
                    {
                        oxyList = oxyList.Where(x => x[i] == '0').ToList();
                    }
                    else
                    {
                        oxyList = oxyList.Where(x => x[i] == '1').ToList();
                    }
                }

                if (c02List.Count != 1)
                {
                    var cnt0 = Count0BitsAtPosition(i, c02List);
                    if (cnt0 > c02List.Count / 2)
                    {
                        c02List = c02List.Where(x => x[i] == '1').ToList();
                    }
                    else
                    {
                        c02List = c02List.Where(x => x[i] == '0').ToList();
                    }
                }

                if (c02List.Count == 1 && oxyList.Count == 1)
                    break;
            }

            var oxy = Convert.ToInt64(oxyList[0], 2);
            var c02 = Convert.ToInt64(c02List[0], 2);

            return (oxy * c02).ToString();
        }

        private int Count0BitsAtPosition(int i, List<string> list)
        {
            var cnt0 = 0;
            foreach (var line in list)
            {
                if ((int)char.GetNumericValue(line[i]) == 0)
                    cnt0++;
            }

            return cnt0;
        }
    }
}