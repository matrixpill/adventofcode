﻿using System;
using System.Linq;
using System.Numerics;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2021
{
    [Day(20)]
    class Day20 : Solution
    {
        private string enhancement;
        private char[,] startImage;

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            enhancement = input[0];
            startImage = new char[input[2].Length, input.Length - 2];
            for (var y = 2; y < input.Length; y++)
            {
                var x = 0;
                foreach (var c in input[y])
                {
                    startImage[x, y - 2] = c;
                    x++;
                }
            }
        }

        public override string SolvePart1()
        {
            return CountOfLit(IterateOnStartImage(2)).ToString();
        }

        public override string SolvePart2()
        {
            return CountOfLit(IterateOnStartImage(50)).ToString();
        }

        private int CountOfLit(char[,] arr)
        {
            return arr.Cast<char>().Count(x => x == '#');
        }

        private char[,] IterateOnStartImage(int times)
        {
            var tmpBase = (char[,])startImage.Clone();
            PrintPic(tmpBase);

            for (var i = 0; i < times; i++)
            {
                var nextImage = new char[tmpBase.GetLength(0) + 2, tmpBase.GetLength(0) + 2];
                CopyToMiddle(tmpBase, nextImage);
                tmpBase = (char[,])nextImage.Clone();

                PrintPic(tmpBase);
            }

            return tmpBase;
        }


        private void PrintPic(char[,] pic)
        {
            return;
#pragma warning disable CS0162 // Unerreichbarer Code wurde entdeckt.
            for (var y = 0; y < pic.GetLength(1); y++)
            {
                for (var x = 0; x < pic.GetLength(0); x++)
                {
                    Console.Write(pic[x, y]);
                }

                Console.WriteLine();
            }
#pragma warning restore CS0162 // Unerreichbarer Code wurde entdeckt.
            Console.WriteLine();
        }

        private char infinite = '.';

        private void CopyToMiddle(char[,] source, char[,] target)
        {
            for (var y = 0; y < target.GetLength(1); y++)
            {
                for (var x = 0; x < target.GetLength(0); x++)
                {
                    var f = Convert.ToInt32(GetSurrounding(new Vector2(x, y), source), 2);
                    target[x, y] = enhancement[f];
                }
            }

            if (enhancement[0] != '#') return;
            infinite = infinite == '.' ? '#' : '.';
        }

        private string GetSurrounding(Vector2 src, char[,] srcArr)
        {
            var res = "";
            res += GetBinaryValue(new Vector2(src.X - 1, src.Y - 1), srcArr);
            res += GetBinaryValue(new Vector2(src.X, src.Y - 1), srcArr);
            res += GetBinaryValue(new Vector2(src.X + 1, src.Y - 1), srcArr);
            res += GetBinaryValue(new Vector2(src.X - 1, src.Y), srcArr);
            res += GetBinaryValue(new Vector2(src.X, src.Y), srcArr);
            res += GetBinaryValue(new Vector2(src.X + 1, src.Y), srcArr);
            res += GetBinaryValue(new Vector2(src.X - 1, src.Y + 1), srcArr);
            res += GetBinaryValue(new Vector2(src.X, src.Y + 1), srcArr);
            res += GetBinaryValue(new Vector2(src.X + 1, src.Y + 1), srcArr);
            return res;
        }

        private int GetBinaryValue(Vector2 vec, char[,] srcArr)
        {
            return GetVal(vec, srcArr) == '#' ? 1 : 0;
        }

        private char GetVal(Vector2 vec, char[,] srcArr)
        {
            vec = Vector2.Subtract(vec, Vector2.One);
            if (vec.X >= 0 && vec.X < srcArr.GetLength(1) && vec.Y >= 0 && vec.Y < srcArr.GetLength(0))
                return srcArr[(int)vec.X, (int)vec.Y];
            return infinite;
        }
    }
}