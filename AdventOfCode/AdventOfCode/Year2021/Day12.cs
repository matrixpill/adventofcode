﻿using System;
using System.Collections.Generic;
using System.Linq;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2021
{
    [Day(12)]
    internal class Day12 : Solution
    {
        private Dictionary<string, Cave> Caves = new Dictionary<string, Cave>();

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            foreach (var connection in input)
            {
                Cave GetCave(string name)
                {
                    if (!Caves.ContainsKey(name))
                        Caves.Add(name, new Cave
                        {
                            Name = name,
                            Small = char.IsLower(name[0])
                        });
                    return Caves[name];
                }


                var leftRight = connection.Split("-");
                var leftCave = GetCave(leftRight[0]);
                var rightCave = GetCave(leftRight[1]);
                leftCave.AddCave(rightCave);
                rightCave.AddCave(leftCave);
            }
        }

        public override string SolvePart1()
        {
            var start = Caves["start"];
            var routes = BuildRoute1(start, start.Name);
            return routes.Count.ToString();
        }


        public override string SolvePart2()
        {
            var start = Caves["start"];
            var routes = BuildRoute1(start, start.Name, true);
            return routes.Count.ToString();
        }


        private List<string> BuildRoute1(Cave cave, string visited = null, bool allowDouble = false)
        {
            var tmpRoutes = new List<string>();
            foreach (var way in cave.ConnectedCaves.Values)
            {
                var newList = visited ?? "";
                if (allowDouble)
                {
                    if ((newList.Contains(way.Name) && way.Small && VisitedOneSmallAlreadyDouble(newList)) ||
                        way.Name == "start")
                        continue;
                }
                else
                {
                    if (newList.Contains(way.Name) && way.Small)
                        continue;
                }

                newList += "," + way.Name;
                if (way.Name != "end")
                    tmpRoutes.AddRange(BuildRoute1(way, newList, allowDouble));
                else
                {
                    tmpRoutes.Add(newList);
                }
            }

            return tmpRoutes;
        }

        private bool VisitedOneSmallAlreadyDouble(string route)
        {
            var small = route.Split(",").Where(x => char.IsLower(x[0])).ToList();
            var distinct = small.Distinct().ToList();
            return distinct.Count != small.Count;
        }
    }

    class Cave
    {
        public bool Small { get; set; }
        public string Name { get; set; }
        public Dictionary<string, Cave> ConnectedCaves { get; } = new Dictionary<string, Cave>();

        public void AddCave(Cave cave)
        {
            if (!ConnectedCaves.ContainsKey(cave.Name))
                ConnectedCaves.Add(cave.Name, cave);
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj is Cave c)
                return c.Name == Name;
            return false;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name);
        }
    }
}