﻿using System;
using System.Collections.Generic;
using System.Linq;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2021
{
    [Day(8)]
    internal class Day8 : Solution
    {
        public override string SolvePart1()
        {
            var cntOf = 0;

            foreach (var line in Input)
            {
                var output = line.Split("|")[1].Split(" ");

                foreach (var digit in output)
                {
                    if (digit.Length == 2 || digit.Length == 3 || digit.Length == 4 || digit.Length == 7)
                        cntOf++;
                }
            }

            return cntOf.ToString();
        }

        public override string SolvePart2()
        {
            var pos2 = Utils.one.Select(x => x).ToArray();
            var pos3 = Utils.seven.Select(x => x).ToArray();
            var pos4 = Utils.four.Select(x => x).ToArray();
            var pos5 = Utils.two.Select(x => x).Concat(Utils.three.Select(x => x)).Concat(Utils.five.Select(x => x))
                .Distinct().ToArray();
            var pos6 = Utils.zero.Select(x => x).Concat(Utils.six.Select(x => x)).Concat(Utils.nine.Select(x => x))
                .Distinct().ToArray();
            var pos7 = Utils.eight.Select(x => x).ToArray();
            long sum = 0;
            foreach (var line in Input)
            {
                var signalAndOutput = line.Split("|", StringSplitOptions.RemoveEmptyEntries);
                var signals = signalAndOutput[0].Split(" ", StringSplitOptions.RemoveEmptyEntries);
                var outputs = signalAndOutput[1].Split(" ", StringSplitOptions.RemoveEmptyEntries);
                var dic = new Dictionary<string, string[]>();
                foreach (var digit in signals)
                {
                    switch (digit.Length)
                    {
                        case 2:
                            FillIt(pos2, dic, digit);
                            break;
                        case 3:
                            FillIt(pos3, dic, digit);
                            break;
                        case 4:
                            FillIt(pos4, dic, digit);
                            break;
                        case 5:
                            FillIt(pos5, dic, digit);
                            break;
                        case 6:
                            FillIt(pos6, dic, digit);
                            break;
                        case 7:
                            FillIt(pos7, dic, digit);
                            break;
                        default:
                            throw new ArgumentException("not Possible");
                    }
                }

                var twosegs = signals.Where(x => x.Length == 2).ToList();
                var threesegs = signals.Where(x => x.Length == 3).ToList();
                var x = threesegs[0].Where(x => !twosegs[0].Contains(x)).First();
                dic[x.ToString()] = new string[] { "a" };

                foreach (var item in dic.Keys.ToArray())
                {
                    if (item != x.ToString())
                    {
                        dic[item] = dic[item].Where(f => f != "a").ToArray();
                    }
                }

                var fivs = signals.Where(x => x.Length == 5).Distinct().ToList();
                if (fivs.Count == 3)
                {
                    var same = fivs[0].Where(x => fivs[1].Contains(x) && fivs[2].Contains(x));
                    foreach (var s in same)
                    {
                        dic[s.ToString()] = dic[s.ToString()].Where(x => "adg".Contains(x)).ToArray();
                        if (dic[s.ToString()].Length == 1)
                        {
                            foreach (var item in dic.Keys.ToArray())
                            {
                                if (item != s.ToString())
                                {
                                    dic[item] = dic[item].Where(f => f != dic[s.ToString()][0]).ToArray();
                                }
                            }
                        }
                    }
                }


                var one = signals.Where(x => x.Length == 2).Distinct().First();
                var four = signals.Where(x => x.Length == 4).Distinct().First();
                var diff = four.Where(x => !one.Contains(x)).Where(x => dic[x.ToString()].Length != 1).First();
                dic[diff.ToString()] = new string[] { "b" };
                RemoveFromDic(dic, "b");


                foreach (var s in dic.Where(x => x.Value.Length == 1).ToArray())
                    RemoveFromDic(dic, s.Value[0]);


                foreach (var s in dic.Where(x => x.Value.Length == 3).ToArray())
                    dic[s.Key] = new string[] { "e" };


                var stillDouble = string.Join("", dic.Where(x => x.Value.Length == 2).Select(x => x.Key).ToArray());
                var six = signals.Where(x => x.Length == 6)
                    .Where(x => !x.Contains(stillDouble[0]) || !x.Contains(stillDouble[1])).First();
                if (six.Contains(stillDouble[0]))
                {
                    dic[stillDouble[0].ToString()] = new string[] { "f" };
                    dic[stillDouble[1].ToString()] = new string[] { "c" };
                }

                if (six.Contains(stillDouble[1]))
                {
                    dic[stillDouble[0].ToString()] = new string[] { "c" };
                    dic[stillDouble[1].ToString()] = new string[] { "f" };
                }


                var num = "";
                foreach (var o in outputs)
                {
                    var connectedNodes = "";

                    foreach (var item in o)
                    {
                        connectedNodes += dic[item.ToString()][0];
                    }

                    connectedNodes = new string(connectedNodes.OrderBy(x => x).ToArray());
                    num += Utils.LitSegments[connectedNodes];
                }

                sum += long.Parse(num);
            }

            return sum.ToString();
        }

        private static void FillIt(string[] pos2, Dictionary<string, string[]> dic, string digit)
        {
            foreach (var item in digit)
            {
                if (dic.ContainsKey(item.ToString()))
                {
                    dic[item.ToString()] = dic[item.ToString()].Where(x => pos2.Contains(x)).ToArray();
                }
                else
                {
                    dic.Add(item.ToString(), pos2);
                }
            }
        }

        private void RemoveFromDic(Dictionary<string, string[]> dic, string c)
        {
            foreach (var item in dic.Keys.ToArray())
            {
                if (dic[item].Length > 1)
                {
                    dic[item] = dic[item].Where(f => f != c).ToArray();
                }
            }
        }
    }


    class Utils
    {
        public static readonly Dictionary<string, int> LitSegments = new Dictionary<string, int>()
        {
            { "abcefg", 0 },
            { "cf", 1 },
            { "acdeg", 2 },
            { "acdfg", 3 },
            { "bcdf", 4 },
            { "abdfg", 5 },
            { "abdefg", 6 },
            { "acf", 7 },
            { "abcdefg", 8 },
            { "abcdfg", 9 }
        };

        public static string[] zero => GetArrayFromDict(0);
        public static string[] one => GetArrayFromDict(1);
        public static string[] two => GetArrayFromDict(2);
        public static string[] three => GetArrayFromDict(3);
        public static string[] four => GetArrayFromDict(4);
        public static string[] five => GetArrayFromDict(5);
        public static string[] six => GetArrayFromDict(6);
        public static string[] seven => GetArrayFromDict(7);
        public static string[] eight => GetArrayFromDict(8);
        public static string[] nine => GetArrayFromDict(9);


        private static string[] GetArrayFromDict(int v)
        {
            return LitSegments.First(x => x.Value == v).Key.Select(x => x.ToString()).Distinct().ToArray();
        }
    }
}