﻿using System;
using System.Linq;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2021
{
    [Day(7)]
    internal class Day7 : Solution
    {
        private int[] intInput;

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            intInput = Array.ConvertAll(input[0].Split(","), int.Parse);
        }

        public override string SolvePart1()
        {
            var ordered = new int[intInput.Length];
            Array.Copy(intInput, ordered, intInput.Length);
            Array.Sort(ordered);
            decimal median;
            if (ordered.Length % 2 == 0)
            {
                median = (ordered[ordered.Length / 2] + ordered[ordered.Length / 2 - 1]) / 2;
            }
            else
            {
                median = ordered[(int)Math.Floor((double)ordered.Length / 2)];
            }

            var sum = 0;
            Console.WriteLine("Median is: " + median);
            foreach (var i in intInput)
            {
                sum += (int)Math.Abs(i - median);
            }

            return sum.ToString();
        }

        public override string SolvePart2()
        {
            decimal sum = decimal.MaxValue;
            int lastMid = 0;
            for (var mid = intInput.Min(); mid < intInput.Max(); mid++)
            {
                var tmpSum = 0;
                foreach (var i in intInput)
                {
                    var moves = Math.Abs(i - mid);
                    for (int m = 1; m <= moves; m++)
                    {
                        tmpSum += m;
                    }
                }

                if (tmpSum < sum)
                {
                    sum = tmpSum;
                    lastMid = mid;
                }
            }

            Console.WriteLine("Computed: " + lastMid);

            //Es hätte doch funktioniert hätte ich direkt abgerunded
            //decimal average = 0;
            //foreach (var item in intInput)
            //{
            //    average += item;
            //}
            //average = Math.Floor(average / intInput.Length);


            //decimal sum = 0;
            //Console.WriteLine("floored average is: " + average);
            //foreach (var i in intInput)
            //{
            //    var moves = (int)Math.Abs(i - average);
            //    for (int m = 1; m <= moves; m++)
            //    {
            //        sum += m;
            //    }
            //}
            return sum.ToString();
        }
    }
}