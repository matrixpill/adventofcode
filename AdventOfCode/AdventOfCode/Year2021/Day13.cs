﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2021
{
    [Day(13)]
    class Day13 : Solution
    {
        private List<Vector2> Dots = new List<Vector2>();
        private List<Fold> Folds = new List<Fold>();

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            var row = 0;
            while (input[row] != "")
            {
                Dots.Add(new Vector2(int.Parse(input[row].Split(",")[0]), int.Parse(input[row].Split(",")[1])));
                row++;
            }

            row++;
            for (; row < input.Length; row++)
            {
                Folds.Add(new Fold()
                {
                    AtLine = int.Parse(input[row].Split("=")[1]),
                    Direction = input[row].Split("=")[0][^1].ToString()
                });
            }
        }

        public override string SolvePart1()
        {
            OneFold(Folds[0]);
            return Dots.Count.ToString();
        }

        public override string SolvePart2()
        {
            for (var i = 1; i < Folds.Count; i++)
                OneFold(Folds[i]);

            var xMax = Dots.Max(x => x.X);
            var yMax = Dots.Max(x => x.Y);
            for (var y = 0; y <= yMax; y++)
            {
                for (var x = 0; x <= xMax; x++)
                    Console.Write(Dots.Contains(new Vector2(x, y)) ? "#" : " ");
                Console.WriteLine();
            }

            return Dots.Count.ToString();
        }

        private void OneFold(Fold fold)
        {
            var newDots = new List<Vector2>();
            var oldDots = new List<Vector2>();
            foreach (var dot in Dots)
            {
                if (fold.Direction == "x")
                {
                    if (!(dot.X > fold.AtLine)) continue;
                    var newX = fold.AtLine - (dot.X - fold.AtLine);
                    newDots.Add(new Vector2(newX, dot.Y));
                    oldDots.Add(dot);
                }
                else
                {
                    if (!(dot.Y > fold.AtLine)) continue;
                    var newY = fold.AtLine - (dot.Y - fold.AtLine);
                    newDots.Add(new Vector2(dot.X, newY));
                    oldDots.Add(dot);
                }
            }

            foreach (var oldDot in oldDots)
            {
                Dots.Remove(oldDot);
            }

            foreach (var newDot in newDots)
            {
                if (!Dots.Contains(newDot))
                    Dots.Add(newDot);
            }
        }
    }


    class Fold
    {
        public string Direction { get; set; }
        public int AtLine { get; set; }
    }
}