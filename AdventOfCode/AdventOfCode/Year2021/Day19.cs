﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2021
{
    [Day(19)]
    internal class Day19 : Solution
    {
        private List<Scanner> Scanners = new List<Scanner>();


        public override void Initialize(string[] input)
        {
            base.Initialize(input);

            var scannerIdx = 0;
            var scanner = new Scanner()
            {
                number = scannerIdx++
            };
            foreach (var line in input)
            {
                if (string.IsNullOrWhiteSpace(line))
                {
                    Scanners.Add(scanner);
                    scanner = new Scanner()
                    {
                        number = scannerIdx++
                    };
                    continue;
                }

                if (line.Contains("---")) continue;


                var stringParams = line.Split(',');
                scanner.Beacons.Add(new Vector3(long.Parse(stringParams[0]), long.Parse(stringParams[1]),
                    long.Parse(stringParams[2])));
            }

            Scanners.Add(scanner);
        }

        public override string SolvePart1()
        {
            var ortedScanners = new Dictionary<int, (Vector3, int)>();
            ortedScanners.Add(0, (new Vector3(0, 0, 0), 12));
            while (ortedScanners.Count < Scanners.Count)
            {
                foreach (var ortedScanner in ortedScanners.Keys.ToArray())
                {
                    foreach (var unortedScanner in Scanners.Where(x => !ortedScanners.ContainsKey(x.number)))
                    {
                        var baseScanner = Scanners[ortedScanner];
                        var compareTo = unortedScanner;


                        var variationCounts = new Dictionary<Vector3, (int, int)>();
                        foreach (var item in baseScanner.Beacons)
                        {
                            for (int varIdx = 1; varIdx <= 24; varIdx++)
                            {
                                foreach (var comp in compareTo.Beacons)
                                {
                                    var variation = GetRotation(comp).Take(varIdx).Last();

                                    var possiblePosition1 = Vector3.Subtract(item, variation);

                                    if (variationCounts.ContainsKey(possiblePosition1))
                                    {
                                        variationCounts[possiblePosition1] = (
                                            variationCounts[possiblePosition1].Item1 + 1, varIdx);
                                    }
                                    else
                                        variationCounts.Add(possiblePosition1, (1, varIdx));
                                }
                            }
                        }

                        var ordered = variationCounts.OrderByDescending(x => x.Value).ToList();
                        if (ordered[0].Value.Item1 >= 12)
                        {
                            if (ortedScanner == 0)
                            {
                                var sub = Vector3.Add(ortedScanners[ortedScanner].Item1, ordered[0].Key);
                                ortedScanners.Add(compareTo.number, (sub, ordered[0].Value.Item2));
                            }
                            else
                            {
                                var x = ortedScanners[ortedScanner].Item1;
                                var variations1 = GetRotation(x).ToList();
                                var variations2 = GetRotation(ordered[0].Key).ToList();
                                var v = GetRotation(x).Take(ortedScanners[ortedScanner].Item2).Last();
                                var t = GetRotation(ordered[0].Key).Take(ortedScanners[ortedScanner].Item2).Last();
                                var t2 = GetRotation(ordered[0].Key).Take(ordered[0].Value.Item2).Last();


                                var sub = Vector3.Subtract(x, ordered[0].Key);
                                sub.Y *= -1;
                                ortedScanners.Add(compareTo.number, (sub, ordered[0].Value.Item2));
                            }
                        }
                    }
                }
            }


            return "";
        }

        private IEnumerable<Vector3> GetRotation(Vector3 vec)
        {
            Vector3 Roll(Vector3 v)
            {
                return new Vector3(v.X, v.Z, -v.Y);
            }

            Vector3 Turn(Vector3 v)
            {
                return new Vector3(-v.Y, v.X, v.Z);
            }

            for (int cycle = 0; cycle < 2; cycle++)
            {
                for (int step = 0; step < 3; step++)
                {
                    vec = Roll(vec);
                    yield return vec;
                    for (var i = 0; i < 3; i++)
                    {
                        vec = Turn(vec);
                        yield return vec;
                    }
                }

                vec = Roll(Turn(Roll(vec)));
            }
        }


        private class Scanner
        {
            public int number;
            public List<Vector3> Beacons { get; set; } = new List<Vector3>();

            public int Rotation { get; set; }
        }
    }
}