﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2021
{
    [Day(11)]
    internal class Day11 : Solution
    {
        private int[][] Octopuses;

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            Octopuses = new int[input.Length][];

            for (var r = 0; r < input.Length; r++)
                Octopuses[r] = input[r].Select(x => int.Parse(x.ToString())).ToArray();
        }

        public override string SolvePart1()
        {
            long sum = 0;
            for (int step = 1; step <= 100; step++)
                sum += OneStep();

            return sum.ToString();
        }

        public override string SolvePart2()
        {
            var step = 101;
            while (OneStep() != 100)
                step++;

            return step.ToString();
        }

        private long OneStep()
        {
            var wasLit = new List<Vector2>();

            for (var row = 0; row < Octopuses.Length; row++)
            for (var col = 0; col < Octopuses[row].Length; col++)
                Influence(new Vector2(row, col), wasLit);

            foreach (var hasLit in wasLit)
                Octopuses[(int)hasLit.X][(int)hasLit.Y] = 0;

            return wasLit.Count;
        }

        private void Influence(Vector2 source, List<Vector2> wasLit)
        {
            if (source.X >= Input.Length ||
                source.X < 0 ||
                source.Y >= Input[0].Length ||
                source.Y < 0) return;

            Octopuses[(int)source.X][(int)source.Y]++;

            if (Octopuses[(int)source.X][(int)source.Y] <= 9 || wasLit.Contains(source))
                return;

            wasLit.Add(source);
            foreach (var direction in Neighbours)
                Influence(Vector2.Add(source, direction), wasLit);
        }

        private readonly Vector2[] Neighbours = new Vector2[]
        {
            new Vector2(-1, -1),
            new Vector2(-1, 0),
            new Vector2(-1, 1),
            new Vector2(0, -1),
            new Vector2(0, 1),
            new Vector2(1, -1),
            new Vector2(1, 0),
            new Vector2(1, 1)
        };
    }
}