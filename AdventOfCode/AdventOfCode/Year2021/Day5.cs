﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2021
{
    [Day(5)]
    class Day5 : Solution
    {
        public Vent[] Vents;

        public override void Initialize(string[] input)
        {
            Vents = new Vent[input.Length];
            base.Initialize(input);
            for (int i = 0; i < Vents.Length; i++)
            {
                var s = Array.ConvertAll(input[i].Split("->")[0].Split(","), int.Parse);
                var e = Array.ConvertAll(input[i].Split("->")[1].Split(","), int.Parse);
                Vents[i] = new Vent
                {
                    Start = new Vector2(s[0], s[1]),
                    End = new Vector2(e[0], e[1])
                };
            }
        }

        public override string SolvePart1()
        {
            var dangerousParts = new Dictionary<Vector2, long>();
            var veryDangerous = new List<Vector2>();
            foreach (var vent in Vents)
            {
                if (!IsHorizontal(vent)) continue;

                var max = Vector2.Max(vent.Start, vent.End);
                var min = Vector2.Min(vent.Start, vent.End);
                if (vent.Start.X == vent.End.X)
                {
                    for (var i = min.Y; i <= max.Y; i++)
                    {
                        var tmp = new Vector2(vent.Start.X, i);
                        AddToDangerous(dangerousParts, veryDangerous, tmp);
                    }
                }
                else
                {
                    for (var i = min.X; i <= max.X; i++)
                    {
                        var tmp = new Vector2(i, vent.Start.Y);
                        AddToDangerous(dangerousParts, veryDangerous, tmp);
                    }
                }
            }

            return veryDangerous.Count().ToString();
        }


        public override string SolvePart2()
        {
            var dangerousParts = new Dictionary<Vector2, long>();
            var veryDangerous = new List<Vector2>();
            foreach (var vent in Vents)
            {
                if (!IsHorizontal(vent) && !IsDiagonal(vent)) continue;

                var max = Vector2.Max(vent.Start, vent.End);
                var min = Vector2.Min(vent.Start, vent.End);
                if (vent.Start.X == vent.End.X)
                {
                    for (var i = min.Y; i <= max.Y; i++)
                    {
                        var tmp = new Vector2(vent.Start.X, i);
                        AddToDangerous(dangerousParts, veryDangerous, tmp);
                    }
                }
                else if (vent.Start.Y == vent.End.Y)
                {
                    for (var i = min.X; i <= max.X; i++)
                    {
                        var tmp = new Vector2(i, vent.Start.Y);
                        AddToDangerous(dangerousParts, veryDangerous, tmp);
                    }
                }
                else
                {
                    for (var i = 0; i <= Math.Abs(max.X - min.X); i++)
                    {
                        var tmp = new Vector2();
                        if (vent.Start.X < vent.End.X)
                            tmp.X = vent.Start.X + i;
                        else
                            tmp.X = vent.Start.X - i;

                        if (vent.Start.Y < vent.End.Y)
                            tmp.Y = vent.Start.Y + i;
                        else
                            tmp.Y = vent.Start.Y - i;
                        AddToDangerous(dangerousParts, veryDangerous, tmp);
                    }
                }
            }

            return veryDangerous.Count().ToString();
        }

        private void AddToDangerous(Dictionary<Vector2, long> dangerousParts, List<Vector2> veryDangerous, Vector2 tmp)
        {
            if (dangerousParts.ContainsKey(tmp))
            {
                dangerousParts[tmp]++;
                if (dangerousParts[tmp] == 2)
                    veryDangerous.Add(tmp);
                return;
            }

            dangerousParts.Add(tmp, 1);
        }


        private bool IsHorizontal(Vent vent)
        {
            var diff = Vector2.Subtract(vent.Start, vent.End);
            return diff.X == 0 || diff.Y == 0;
        }

        private bool IsDiagonal(Vent vent)
        {
            var diff = Vector2.Subtract(vent.Start, vent.End);
            return Math.Abs(diff.X) == Math.Abs(diff.Y);
        }
    }

    class Vent
    {
        public Vector2 Start { get; set; }
        public Vector2 End { get; set; }
    }
}