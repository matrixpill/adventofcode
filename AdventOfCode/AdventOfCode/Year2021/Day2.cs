﻿using AoCUtils.Interfaces;

namespace AdventOfCode.Year2021
{
    [Day(2)]
    internal class Day2 : Solution
    {
        public override string SolvePart1()
        {
            long depth = 0;
            long position = 0;

            foreach (var cmd in Input)
            {
                var splittedCMd = cmd.Split(' ');
                var direction = splittedCMd[0].Trim();
                var amount = long.Parse(splittedCMd[1]);
                switch (direction)
                {
                    case "forward":
                        position += amount;
                        break;
                    case "up":
                        depth -= amount;
                        break;
                    case "down":
                        depth += amount;
                        break;
                }
            }

            return (depth * position).ToString();
        }

        public override string SolvePart2()
        {
            long depth = 0;
            long hPosition = 0;
            long aim = 0;
            foreach (var cmd in Input)
            {
                var splittedCmd = cmd.Split(' ');
                var direction = splittedCmd[0].Trim();
                var amount = long.Parse(splittedCmd[1]);
                switch (direction)
                {
                    case "forward":
                        hPosition += amount;
                        depth += aim * amount;
                        break;
                    case "up":
                        aim -= amount;
                        break;
                    case "down":
                        aim += amount;
                        break;
                }
            }

            return (depth * hPosition).ToString();
        }
    }
}