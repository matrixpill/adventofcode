﻿using System;
using System.Collections.Generic;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2021
{
    [Day(18)]
    internal class Day18 : Solution
    {
        private List<NumberPair> numbers = new List<NumberPair>();


        public override void Initialize(string[] input)
        {
            foreach (var line in input)
            {
                numbers.Add(NumberBuilder.BuildNumber(line) as NumberPair);
            }

            base.Initialize(input);
        }

        public override string SolvePart1()
        {
            var basePair = numbers[0].Copy();
            NumberBuilder.ReduceNumber(basePair);
            for (var i = 1; i < numbers.Count; i++)
            {
                basePair = basePair.Add(numbers[i].Copy());
                NumberBuilder.ReduceNumber(basePair);
            }

            return basePair.Magnitude.ToString();
        }

        public override string SolvePart2()
        {
            var maxMagnitude = long.MinValue;
            for (var i = 0; i < numbers.Count - 1; i++)
            {
                for (var j = i + 1; j < numbers.Count; j++)
                {
                    var tmpPair = numbers[i].Copy().Add(numbers[j].Copy());
                    NumberBuilder.ReduceNumber(tmpPair);
                    if (tmpPair.Magnitude > maxMagnitude)
                        maxMagnitude = tmpPair.Magnitude;

                    tmpPair = numbers[j].Copy().Add(numbers[i].Copy());
                    NumberBuilder.ReduceNumber(tmpPair);
                    if (tmpPair.Magnitude > maxMagnitude)
                        maxMagnitude = tmpPair.Magnitude;
                }
            }

            return maxMagnitude.ToString();
        }
    }

    class NumberBuilder
    {
        public static SnailfishNumber BuildNumber(string input)
        {
            var openStack = new Stack<char>();
            if (input.Length == 1)
                return new NumberValue() { Value = int.Parse(input) };

            for (var i = 0; i < input.Length; i++)
            {
                var c = input[i];
                switch (c)
                {
                    case '[':
                        openStack.Push(']');
                        break;
                    case ']':
                        openStack.Pop();
                        break;
                    case ',':
                        if (openStack.Count == 1)
                        {
                            var left = input[1..i];
                            var right = input[(i + 1)..^1];
                            var lPair = BuildNumber(left);
                            var rPair = BuildNumber(right);
                            var newPair = new NumberPair() { Left = lPair, Right = rPair };
                            lPair.Parent = newPair;
                            rPair.Parent = newPair;
                            return newPair;
                        }

                        break;
                    default:
                        break;
                }
            }

            return null;
        }

        public static void ReduceNumber(SnailfishNumber number)
        {
            if (number is NumberPair np)
            {
                while (np.Explode())
                {
                }

                while (np.Split(true))
                {
                }

                //this takes double the time
                //while (true)
                //{
                //    if (np.Explode()) continue;
                //    if (np.Split()) continue;
                //    break;
                //}
            }
        }

        public static bool AddToNeighbour(NumberPair parent, SnailfishNumber source, int orig, bool left)
        {
            if ((left ? parent.Left : parent.Right) != source)
            {
                var outerValue = OuterMostValue(left ? parent.Left : parent.Right, !left);
                outerValue.Value += orig;
                return true;
            }
            else
            {
                if (parent.Parent != null)
                {
                    return AddToNeighbour(parent.Parent, parent, orig, left);
                }

                return false;
            }
        }

        public static NumberValue OuterMostValue(SnailfishNumber curr, bool left)
        {
            if (curr is NumberPair np)
                return OuterMostValue(left ? np.Left : np.Right, left);
            else if (curr is NumberValue nv)
                return nv;
            return null;
        }
    }


    abstract class SnailfishNumber
    {
        public int NestingLevel => Parent != null ? Parent.NestingLevel + 1 : 1;

        public abstract string RawValue { get; }
        public NumberPair Parent { get; internal set; }
        public abstract bool Split(bool checkForExplode = false);
        public abstract long Magnitude { get; }

        public abstract SnailfishNumber Copy();
    }


    class NumberPair : SnailfishNumber
    {
        public SnailfishNumber Left { get; set; }
        public SnailfishNumber Right { get; set; }
        public override string RawValue => $"[{Left.RawValue},{Right.RawValue}]";
        public override long Magnitude => 3 * Left.Magnitude + 2 * Right.Magnitude;

        public bool Explode()
        {
            if (Left is NumberValue lnv && Right is NumberValue rnv && NestingLevel > 4)
            {
                NumberBuilder.AddToNeighbour(Parent, this, lnv.Value, true);
                NumberBuilder.AddToNeighbour(Parent, this, rnv.Value, false);
                var newZero = new NumberValue()
                {
                    Parent = Parent,
                    Value = 0
                };

                if (Parent.Right == this)
                    Parent.Right = newZero;
                else
                    Parent.Left = newZero;
                return true;
            }

            var exploded = false;
            if (Left != null && Left is NumberPair nl)
            {
                exploded = nl.Explode();
            }

            if (exploded) return true;

            if (Right != null && Right is NumberPair nr)
            {
                exploded = nr.Explode();
            }

            return exploded;
        }

        public override bool Split(bool checkForExplode = false)
        {
            var splitted = false;
            if (Left != null)
            {
                splitted = Left.Split(checkForExplode);
            }

            if (splitted) return true;

            if (Right != null)
            {
                splitted = Right.Split(checkForExplode);
            }

            return splitted;
        }


        public NumberPair Add(NumberPair addedPair)
        {
            var newPair = new NumberPair()
            {
                Left = this,
                Right = addedPair,
            };
            newPair.Left.Parent = newPair;
            newPair.Right.Parent = newPair;
            return newPair;
        }

        public override NumberPair Copy()
        {
            var n = new NumberPair()
            {
                Left = Left.Copy(),
                Right = Right.Copy(),
            };
            n.Left.Parent = n;
            n.Right.Parent = n;
            return n;
        }
    }


    class NumberValue : SnailfishNumber
    {
        public int Value { get; set; }

        public override string RawValue => Value.ToString();

        public override long Magnitude => Value;

        public override NumberValue Copy()
        {
            return new NumberValue()
            {
                Value = Value
            };
        }

        public override bool Split(bool checkForExplode)
        {
            if (Value <= 9) return false;
            var newSplit = new NumberPair()
            {
                Parent = Parent
            };

            newSplit.Left = new NumberValue()
            {
                Value = (int)Math.Floor(Value / (decimal)2),
                Parent = newSplit
            };

            newSplit.Right = new NumberValue()
            {
                Value = Value - ((NumberValue)newSplit.Left).Value,
                Parent = newSplit
            };

            if (Parent.Right == this)
                Parent.Right = newSplit;
            else
                Parent.Left = newSplit;
            if (checkForExplode)
                newSplit.Explode();
            return true;
        }
    }
}