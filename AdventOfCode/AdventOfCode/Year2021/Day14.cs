﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2021
{
    [Day(14)]
    internal class Day14 : Solution
    {
        private Dictionary<string, string> Rules = new Dictionary<string, string>();

        public override void Initialize(string[] input)
        {
            for (var i = 2; i < input.Length; i++)
            {
                Rules.Add(input[i].Split(" -> ")[0],
                    input[i].Split(" -> ")[1]);
            }

            base.Initialize(input);
        }


        public override string SolvePart1()
        {
            var polymer = Input[0];
            var sb = new StringBuilder(polymer);
            var tmpSb = new StringBuilder();
            for (var step = 1; step <= 10; step++)
            {
                tmpSb.Clear();

                for (var i = 0; i < sb.Length - 1; i++)
                {
                    var pair = sb[i].ToString() + sb[i + 1].ToString();
                    if (Rules.ContainsKey(pair))
                    {
                        tmpSb.Append(pair[0]);
                        tmpSb.Append(Rules[pair]);
                    }
                    else
                        tmpSb.Append(pair[0]);
                }

                tmpSb.Append(polymer[^1]);
                sb.Clear();
                sb.Append(tmpSb);
            }

            var s = sb.ToString();
            var elements = s.Distinct().ToArray();

            var mostCommon = long.MinValue;
            var leastCommon = long.MaxValue;

            foreach (var element in elements)
            {
                var cnt = s.Count(x => x == element);
                if (cnt < leastCommon)
                    leastCommon = cnt;
                if (cnt > mostCommon)
                    mostCommon = cnt;
            }

            return (mostCommon - leastCommon).ToString();
        }

        public override string SolvePart2()
        {
            var pairCount = new Dictionary<string, long>();
            var charCount = new Dictionary<char, long>();
            var start = Input[0];
            for (var i = 0; i < start.Length - 1; i++)
            {
                UpdateCharCount(charCount, start[i], 1);

                var pair = start[i].ToString() + start[i + 1].ToString();
                if (pairCount.ContainsKey(pair))
                    pairCount[pair]++;
                else
                    pairCount.Add(pair, 1);
            }

            UpdateCharCount(charCount, start[^1], 1);

            for (var step = 1; step <= 40; step++)
            {
                var pairCountDiff = new Dictionary<string, long>();
                foreach (var pair in pairCount.Keys.ToArray())
                {
                    if (pairCount[pair] <= 0) continue;
                    var cnt = pairCount[pair];
                    if (Rules.ContainsKey(pair))
                    {
                        if (pairCountDiff.ContainsKey(pair))
                            pairCountDiff[pair] -= cnt;
                        else
                            pairCountDiff.Add(pair, cnt * -1);

                        var newPair1 = pair[0] + Rules[pair];
                        var newPair2 = Rules[pair] + pair[1];
                        TryAddToPairDict(newPair1, pairCountDiff, cnt);
                        TryAddToPairDict(newPair2, pairCountDiff, cnt);
                        UpdateCharCount(charCount, Rules[pair][0], cnt);
                    }
                }

                foreach (var item in pairCountDiff.Keys.ToArray())
                {
                    if (pairCount.ContainsKey(item))
                        pairCount[item] += pairCountDiff[item];
                    else
                        pairCount.Add(item, pairCountDiff[item]);
                }
            }

            var mostCommon = charCount.Max(x => x.Value);
            var leastCommon = charCount.Min(x => x.Value);

            return (mostCommon - leastCommon).ToString();
        }

        private void UpdateCharCount(Dictionary<char, long> dic, char c, long cnt)
        {
            if (dic.ContainsKey(c))
                dic[c] += cnt;
            else
                dic.Add(c, cnt);
        }

        private void TryAddToPairDict(string pair, Dictionary<string, long> dic, long cnt)
        {
            if (dic.ContainsKey(pair))
            {
                dic[pair] += cnt;
            }
            else
            {
                dic.Add(pair, cnt);
            }
        }
    }
}