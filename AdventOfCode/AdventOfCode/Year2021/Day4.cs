﻿using System;
using System.Collections.Generic;
using System.Linq;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2021
{
    [Day(4)]
    class Day4 : Solution
    {
        private int[] pulledNumbers;
        private List<Board> boards;

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            pulledNumbers = input[0].Split(",").Select(int.Parse).ToArray();

            boards = new List<Board>();
            var board = new Board();
            var rowIdx = 0;
            for (var i = 1; i < input.Length; i++)
            {
                if (string.IsNullOrWhiteSpace(input[i]))
                {
                    board = new Board();
                    boards.Add(board);
                    rowIdx = 0;
                    i++;
                }

                var row = input[i];

                var col = 0;
                foreach (var numb in Array.ConvertAll(row.Split(' ', options: StringSplitOptions.RemoveEmptyEntries),
                             int.Parse))
                {
                    var n = new Number
                    {
                        Value = numb,
                        Board = board,
                        Row = rowIdx,
                        Column = col
                    };


                    board.Numbers.Add(numb, n);
                    if (!board.RowNumbers.ContainsKey(rowIdx))
                        board.RowNumbers.Add(rowIdx, new List<Number> { n });
                    else
                        board.RowNumbers[rowIdx].Add(n);

                    if (!board.ColumnNumbers.ContainsKey(col))
                        board.ColumnNumbers.Add(col, new List<Number>() { n });
                    else
                        board.ColumnNumbers[col].Add(n);
                    col++;
                }

                rowIdx++;
            }
        }

        public override string SolvePart1()
        {
            var winningBoard = GetSolution(SolutionLevel.Result1);
            var sum = winningBoard.Numbers.Sum(x => x.Value.Pulled ? 0 : x.Key);
            return (sum * winningBoard.WonOnNumber).ToString();
        }


        public override string SolvePart2()
        {
            var winningBoard = GetSolution(SolutionLevel.Result2);
            var sum = winningBoard.Numbers.Sum(x => x.Value.Pulled ? 0 : x.Key);
            return (sum * winningBoard.WonOnNumber).ToString();
        }

        public Board GetSolution(SolutionLevel level)
        {
            var i = 0;
            foreach (var number in pulledNumbers)
            {
                i++;
                var remainingBoards = boards.Where(x => !x.Won).ToList();
                foreach (var board in remainingBoards)
                {
                    if (!board.Numbers.ContainsKey(number))
                        continue;

                    board.Numbers[number].Pulled = true;
                    if (!board.RowNumbers[board.Numbers[number].Row].All(x => x.Pulled) &&
                        !board.ColumnNumbers[board.Numbers[number].Column].All(x => x.Pulled))
                        continue;

                    board.Won = true;
                    board.WonOnNumber = number;
                    board.NmbrCnt = i;
                    if (level == SolutionLevel.Result1)
                        return board;

                    if (level == SolutionLevel.Result2 && i == pulledNumbers.Length || remainingBoards.Count() == 1)
                        return board;
                }
            }

            return default;
        }
    }

    class Board
    {
        public Dictionary<int, Number> Numbers { get; set; } = new Dictionary<int, Number>();
        public Dictionary<int, List<Number>> RowNumbers { get; set; } = new Dictionary<int, List<Number>>();
        public Dictionary<int, List<Number>> ColumnNumbers { get; set; } = new Dictionary<int, List<Number>>();
        public bool Won { get; set; }
        public int NmbrCnt { get; set; }
        public int WonOnNumber { get; set; }
    }

    class Number
    {
        public int Value { get; set; }
        public int Row { get; set; }
        public int Column { get; set; }
        public Board Board { get; set; }
        public bool Pulled { get; set; }
    }
}