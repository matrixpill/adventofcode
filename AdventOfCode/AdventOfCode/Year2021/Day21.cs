﻿using System.Linq;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2021
{
    [Day(21)]
    internal class Day21 : Solution
    {
        private Player player1;
        private Player player2;
        private DeterministicDice dice;


        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            dice = new DeterministicDice();
            player1 = new Player(input[0], dice);
            player2 = new Player(input[1], dice);
        }

        public override string SolvePart1()
        {
            while (true)
            {
                if (player1.MakeMove(1000))
                    break;
                if (player2.MakeMove(1000))
                    break;
            }

            var minScore = new int[] { player1.Score, player2.Score }.Min();
            return (minScore * dice.DicesRolled).ToString();
        }

        public override string SolvePart2()
        {
            return base.SolvePart2();
        }

        private class DeterministicDice
        {
            private readonly int Max = 100;
            private readonly int Min = 1;

            private int LastDice = 0;
            public int DicesRolled { get; private set; } = 0;

            public int RollDice()
            {
                var result = LastDice + 1;
                if (result > Max)
                    result = Min;
                LastDice = result;
                DicesRolled++;
                return result;
            }
        }


        private class Player
        {
            public string Name { get; private set; }
            public int Score { get; private set; }
            private int Position;
            private readonly DeterministicDice dice;

            public Player(string input, DeterministicDice dice)
            {
                Name = input.Split("starting")[0];
                Position = int.Parse(input.Split("starting")[1].Split(":")[1]);
                this.dice = dice;
            }

            public bool MakeMove(int maxScore)
            {
                var rolledSum = 0;
                for (int i = 0; i < 3; i++)
                {
                    rolledSum += dice.RollDice();
                }

                Position += rolledSum;
                if (Position % 10 == 0)
                {
                    Position = 10;
                }
                else
                {
                    Position %= 10;
                }

                Score += Position;
                if (Score >= maxScore)
                    return true;
                return false;
            }
        }
    }
}