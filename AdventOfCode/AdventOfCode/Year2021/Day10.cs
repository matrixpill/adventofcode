﻿using System;
using System.Collections.Generic;
using System.Linq;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2021
{
    [Day(10)]
    class Day10 : Solution
    {
        private const string OpenBrackets = "([{<";
        private const string ClosingBrackets = ")]}>";
        private readonly int[] Points = { 3, 57, 1197, 25137 };


        private readonly List<string> CorruptedLines = new List<string>();

        public override string SolvePart1()
        {
            var corrCnt = new int[] { 0, 0, 0, 0 };

            foreach (var line in Input)
            {
                var stack = new Stack<int>();

                foreach (var c in line)
                {
                    if (OpenBrackets.Contains(c))
                    {
                        stack.Push(OpenBrackets.IndexOf(c));
                    }
                    else
                    {
                        if (stack.Peek() == ClosingBrackets.IndexOf(c))
                        {
                            stack.Pop();
                        }
                        else
                        {
                            corrCnt[ClosingBrackets.IndexOf(c)]++;
                            CorruptedLines.Add(line);
                            break;
                        }
                    }
                }
            }

            long sum = 0;
            for (var i = 0; i < corrCnt.Length; i++)
            {
                sum += corrCnt[i] * Points[i];
            }

            return sum.ToString();
        }

        public override string SolvePart2()
        {
            var incompletePoints = new[] { 1, 2, 3, 4 };
            var incompleteLines = Input.Where(x => !CorruptedLines.Contains(x));
            var scores = new List<long>();

            foreach (var line in incompleteLines)
            {
                var openBrackets = new Stack<int>();

                foreach (var c in line)
                {
                    if (openBrackets.Count > 0 && (openBrackets.Peek() == ClosingBrackets.IndexOf(c)))
                    {
                        openBrackets.Pop();
                        continue;
                    }

                    openBrackets.Push(OpenBrackets.IndexOf(c));
                }

                long score = 0;

                while (openBrackets.TryPop(out var result))
                {
                    score *= 5;
                    score += incompletePoints[result];
                }

                scores.Add(score);
            }

            scores.Sort();
            var mid = (int)Math.Floor(scores.Count / 2m);
            return scores[mid].ToString();
        }
    }
}