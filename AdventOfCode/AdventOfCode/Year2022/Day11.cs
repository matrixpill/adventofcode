﻿using System;
using System.Collections.Generic;
using System.Linq;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2022
{
    [Day(11)]
    internal class Day11 : Solution
    {
        public override string SolvePart1()
        {
            var list = new MonkeyList();
            list.AddMonkeys(Input);
            list.DoRounds(20);

            var most = list.Monkeys.OrderByDescending(x => x.InspectedItems).Take(2).ToList();
            return (most[0].InspectedItems * most[1].InspectedItems).ToString();
        }

        public override string SolvePart2()
        {
            var list = new MonkeyList();
            list.AddMonkeys(Input);
            list.DoRounds(10000, false);

            var most = list.Monkeys.OrderByDescending(x => x.InspectedItems).Take(2).ToList();
            return (most[0].InspectedItems * most[1].InspectedItems).ToString();
        }
    }


    class MonkeyList
    {
        public List<Monkey> Monkeys { get; set; } = new List<Monkey>();
        public long Wrap { get; set; } = 1;

        public void AddMonkeys(string[] input)
        {
            Monkey newMonk = new Monkey(this);
            foreach (string line in input)
            {
                var trimmed = line.Trim();
                if (trimmed.StartsWith("Monkey"))
                {
                    newMonk = new Monkey(this);
                    Monkeys.Add(newMonk);
                }
                else if (trimmed.StartsWith("Starting"))
                {
                    var splt = trimmed.Split(':')[1].Split(',');
                    foreach (var i in splt)
                    {
                        newMonk.Items.Enqueue(long.Parse(i));
                    }
                }
                else if (trimmed.StartsWith("Operation"))
                {
                    newMonk.Calculation = trimmed.Split("=")[1];
                }
                else if (trimmed.StartsWith("Test"))
                {
                    newMonk.DivisibleBy = int.Parse(trimmed.Split("by")[1]);
                    Wrap *= newMonk.DivisibleBy;
                }
                else if (trimmed.StartsWith("If true"))
                {
                    newMonk.TrueMonkey = int.Parse(trimmed.Split("monkey")[1]);
                }
                else if (trimmed.StartsWith("If false"))
                {
                    newMonk.FalseMonkey = int.Parse(trimmed.Split("monkey")[1]);
                }
            }
        }

        public void TossItemTo(long item, int monkey)
        {
            Monkeys[monkey].Items.Enqueue(item);
        }

        public void DoRounds(int cnt, bool div3 = true)
        {
            for (var i = 0; i < cnt; i++)
            {
                foreach (var m in Monkeys)
                {
                    m.InspectItems(div3);
                }
            }
        }
    }

    class Monkey
    {
        public Queue<long> Items { get; set; } = new Queue<long>();
        private MonkeyList MonkeyList { get; }

        public long InspectedItems = 0;

        public int DivisibleBy { get; set; }

        public int TrueMonkey { get; set; }
        public int FalseMonkey { get; set; }
        public string Calculation { get; set; }

        public Monkey(MonkeyList monkeyList)
        {
            MonkeyList = monkeyList;
        }

        public void InspectItems(bool div3 = true)
        {
            while (Items.TryDequeue(out var i))
            {
                InspectedItems++;
                var newItemValue = new Calc(Calculation.Replace("old", i.ToString())).DoCalc();

                if (div3)
                    newItemValue = (long)Math.Floor(newItemValue / 3m);
                else
                    newItemValue %= MonkeyList.Wrap;
                if (newItemValue % DivisibleBy == 0)
                    MonkeyList.TossItemTo(newItemValue, TrueMonkey);
                else
                    MonkeyList.TossItemTo(newItemValue, FalseMonkey);
            }
        }
    }

    public class Calc
    {
        public long Left { get; set; }
        public long Right { get; set; }
        public CalcType CalcType { get; set; }

        public Calc(string c)
        {
            var spl = c.Trim().Split(' ');
            Left = long.Parse(spl[0]);
            Right = long.Parse(spl[2]);
            CalcType = spl[1] switch
            {
                "+" => CalcType.Plus,
                "-" => CalcType.Minus,
                "*" => CalcType.Mult,
                "/" => CalcType.Div,
                _ => throw new Exception()
            };
        }

        public long DoCalc()
        {
            return CalcType switch
            {
                CalcType.Plus => Left + Right,
                CalcType.Minus => Left - Right,
                CalcType.Div => Left / Right,
                CalcType.Mult => Left * Right,
                _ => throw new Exception(),
            };
        }
    }

    public enum CalcType
    {
        Plus,
        Minus,
        Div,
        Mult
    }
}