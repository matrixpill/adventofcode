﻿using System;
using System.Linq;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2022
{
    [Day(3)]
    internal class Day3 : Solution
    {
        public override string SolvePart1()
        {
            var sum = 0L;
            foreach (var item in Input)
            {
                var firstHalf = item[..(item.Length / 2)];
                var secondHalf = item.Substring(item.Length / 2, item.Length / 2);
                foreach (var c in firstHalf)
                {
                    if (secondHalf.Contains(c))
                    {
                        sum += GetValueOfChar(c);
                        break;
                    }
                }
            }

            return sum.ToString();
        }

        public override string SolvePart2()
        {
            var sum = 0L;
            var tmpString = "";
            for (var i = 0; i < Input.Length; i++)
            {
                tmpString += string.Join("", Input[i].ToHashSet());

                if (i % 3 == 2)
                {
                    foreach (var c in tmpString)
                    {
                        if (tmpString.Count(x => c == x) == 3)
                        {
                            sum += GetValueOfChar(c);
                            break;
                        }
                    }

                    tmpString = "";
                }
            }

            return sum.ToString();
        }


        private int GetValueOfChar(char c)
        {
            if (c >= 'a')
            {
                return c - 96;
            }

            if (c >= 'A' && c <= 'Z')
                return c - 38;
            throw new Exception();
        }
    }
}