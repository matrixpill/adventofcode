﻿using System;
using System.Collections.Generic;
using System.Linq;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2022
{
    [Day(6)]
    internal class Day6 : Solution
    {
        char[] signal;

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            signal = input[0].ToCharArray();
        }

        public override string SolvePart1()
        {
            return FindConsecutiveDifferent(4).ToString();
        }

        public override string SolvePart2()
        {
            return FindConsecutiveDifferent(14).ToString();
        }

        private int FindConsecutiveDifferent(int cnt)
        {
            HashSet<char> set;
            var tmpArr = new char[cnt];
            for (var i = 0; i < signal.Length - cnt; i++)
            {
                Array.Copy(signal, i, tmpArr, 0, cnt);
                set = tmpArr.ToHashSet();
                if (set.Count == cnt)
                {
                    return (i + cnt);
                }
            }

            throw new Exception();
        }
    }
}