﻿using System.Collections.Generic;
using System.Numerics;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2022
{
    [Day(14)]
    internal class Day14 : Solution
    {
        HashSet<Vector2> Walls;
        HashSet<Vector2> Sand;
        int maxY = 0;

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            Walls = new HashSet<Vector2>();

            for (var i = 0; i < Input.Length; i++)
            {
                var points = Input[i].Split("->");
                for (var p = 0; p < points.Length - 1; p++)
                {
                    var p1 = points[p].Split(",");
                    var start = new Vector2(int.Parse(p1[0]), int.Parse(p1[1]));
                    var p2 = points[p + 1].Split(",");
                    var end = new Vector2(int.Parse(p2[0]), int.Parse(p2[1]));
                    for (var x = start.X; x <= end.X; x++)
                    {
                        var v = new Vector2(x, start.Y);
                        Walls.Add(v);
                    }

                    for (var y = start.Y; y <= end.Y; y++)
                    {
                        var v = new Vector2(start.X, y);
                        Walls.Add(v);
                    }

                    for (var x = end.X; x <= start.X; x++)
                    {
                        var v = new Vector2(x, start.Y);
                        Walls.Add(v);
                    }

                    for (var y = end.Y; y <= start.Y; y++)
                    {
                        var v = new Vector2(start.X, y);
                        Walls.Add(v);
                    }

                    if (start.Y > maxY) maxY = (int)start.Y;
                    if (end.Y > maxY) maxY = (int)end.Y;
                }
            }
        }

        public override string SolvePart1()
        {
            Sand = new HashSet<Vector2>();
            var newSand = new Vector2(500, 0);
            while (true)
            {
                var fallen = NextPosPosition(newSand);
                if (fallen == newSand)
                {
                    Sand.Add(fallen);
                    newSand = new Vector2(500, 0);
                    continue;
                }

                if (fallen.Y == int.MaxValue)
                {
                    break;
                }

                newSand = fallen;
            }

            return Sand.Count.ToString();
        }

        public override string SolvePart2()
        {
            Sand = new HashSet<Vector2>();
            var newSand = new Vector2(500, 0);
            while (true)
            {
                var fallen = NextPosPosition(newSand, true);
                if (fallen == newSand)
                {
                    Sand.Add(fallen);
                    if (fallen.Y == 0) break;
                    newSand = new Vector2(500, 0);
                    continue;
                }

                if (fallen.Y == int.MaxValue)
                {
                    break;
                }

                newSand = fallen;
            }

            return Sand.Count.ToString();
        }

        private Vector2 NextPosPosition(Vector2 curr, bool infinFloor = false)
        {
            var posToTry = new Vector2(curr.X, curr.Y + 1);
            if (posToTry.Y > maxY && !infinFloor) return new Vector2(curr.X, int.MaxValue);
            if (!Walls.Contains(posToTry) && !Sand.Contains(posToTry) && !InFloor(posToTry, infinFloor))
                return posToTry;
            posToTry = new Vector2(curr.X - 1, curr.Y + 1);
            if (!Walls.Contains(posToTry) && !Sand.Contains(posToTry) && !InFloor(posToTry, infinFloor))
                return posToTry;
            posToTry = new Vector2(curr.X + 1, curr.Y + 1);
            if (!Walls.Contains(posToTry) && !Sand.Contains(posToTry) && !InFloor(posToTry, infinFloor))
                return posToTry;
            return curr;
        }

        private bool InFloor(Vector2 pos, bool infinFloor = false)
        {
            if (!infinFloor) return false;
            if (pos.Y >= maxY + 2) return true;
            return false;
        }
    }
}