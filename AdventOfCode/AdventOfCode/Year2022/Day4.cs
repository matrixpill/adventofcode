﻿using System.Collections.Generic;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2022
{
    [Day(4)]
    public class Day4 : Solution
    {
        List<Pair> ElvPairs = new List<Pair>();

        public class Pair
        {
            public AocRange First { get; }
            public AocRange Second { get; }

            public Pair(AocRange first, AocRange second)
            {
                First = first;
                Second = second;
            }

            public bool NeedsAttention()
            {
                return First.IsCompletelyInRange(Second) || Second.IsCompletelyInRange(First);
            }

            public bool Overlaps()
            {
                return First.Overlaps(Second);
            }
        }

        public class AocRange
        {
            public long Start;
            public long End;

            public AocRange(long start, long end)
            {
                Start = start;
                End = end;
            }

            public AocRange(string input)
            {
                var splitInput = input.Split('-');
                Start = long.Parse(splitInput[0]);
                End = long.Parse(splitInput[1]);
            }

            public bool IsCompletelyInRange(AocRange other)
            {
                return other.Start >= Start && other.End <= End;
            }

            public bool Overlaps(AocRange other)
            {
                return (other.Start <= Start && other.End >= Start) || (other.Start >= Start && other.Start <= End);
            }
        }

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            foreach (var pair in Input)
            {
                var split = pair.Split(",");
                ElvPairs.Add(new Pair(new AocRange(split[0]), new AocRange(split[1])));
            }
        }

        public override string SolvePart1()
        {
            var sum = 0L;
            foreach (var pair in ElvPairs)
            {
                if (pair.NeedsAttention()) sum++;
            }

            return sum.ToString();
        }

        public override string SolvePart2()
        {
            var sum = 0L;
            foreach (var pair in ElvPairs)
            {
                if (pair.Overlaps()) sum++;
            }

            return sum.ToString();
        }
    }
}