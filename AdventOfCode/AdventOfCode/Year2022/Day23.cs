﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2022
{
    [Day(23)]
    internal class Day23 : Solution
    {
        HashSet<Elve> Elves = new();

        public override void Initialize(string[] input)
        {
            for (var y = 0; y < input.Length; y++)
            {
                var line = input[y];
                for (var x = 0; x < line.Length; x++)
                {
                    if (line[x] == '#')
                        Elves.Add((Elve)new Vector2(x, y));
                }
            }

            base.Initialize(input);
        }

        public override string SolvePart1()
        {
            var proposedMoves = new Dictionary<Vector2, Elve>();
            //Display();

            for (var i = 0; i < 10; i++)
            {
                foreach (var elve in Elves)
                {
                    elve.ProposeMove(Elves);
                    if (elve.ProposedMove != Vector2.Zero)
                    {
                        if (!proposedMoves.TryAdd(elve.ProposedMove, elve))
                        {
                            proposedMoves[elve.ProposedMove].ProposedMove = Vector2.Zero;
                            elve.ProposedMove = Vector2.Zero;
                        }
                    }
                }

                foreach (var elve in Elves)
                {
                    elve.Move();
                }

                Directions.MoveDirections();
                proposedMoves.Clear();
                //Display();
                //Console.Read();
            }

            Display();
            var sortedByX = Elves.OrderBy(x => x.Position.X);
            var sortedByY = Elves.OrderBy(x => x.Position.Y);
            var xLength = sortedByX.Last().Position.X - sortedByX.First().Position.X + 1;
            var yLength = sortedByY.Last().Position.Y - sortedByY.First().Position.Y + 1;
            return (xLength * yLength).ToString();
        }

        private void Display()
        {
            var sortedByX = Elves.OrderBy(x => x.Position.X);
            var sortedByY = Elves.OrderBy(x => x.Position.Y);
            var min = new Vector2(sortedByX.First().Position.X, sortedByY.First().Position.Y);
            var max = new Vector2(sortedByX.Last().Position.X, sortedByY.Last().Position.Y);
            for (var y = (int)min.Y; y <= max.Y; y++)
            {
                for (var x = (int)min.X; x <= max.X; x++)
                {
                    if (x == 4 && y == -1) ;
                    if (Elves.Any(e => e.Position == new Vector2(x, y)))
                        Console.Write("#");
                    else
                        Console.Write(".");
                }

                Console.WriteLine();
            }

            Console.WriteLine();
        }
    }

    class Elve
    {
        public Vector2 Position;

        public Vector2 ProposedMove = Vector2.Zero;

        public static explicit operator Elve(Vector2 v)
        {
            return new Elve { Position = v };
        }

        public void Move()
        {
            if (ProposedMove != Vector2.Zero)
                Position = ProposedMove;
            ProposedMove = Vector2.Zero;
        }

        public void ProposeMove(HashSet<Elve> allEleves)
        {
            var tmpProposed = Vector2.Zero;
            var allClear = true;
            foreach (var direction in Directions.Main)
            {
                var occupied = false;
                foreach (var pos in direction)
                {
                    if (allEleves.Any(e => e.Position == (Position + pos)))
                    {
                        occupied = true;
                        allClear = false;
                    }
                }

                if (!occupied && tmpProposed == Vector2.Zero)
                {
                    tmpProposed = direction[1];
                }
            }

            if (allClear)
            {
                tmpProposed = Vector2.Zero;
            }

            ProposedMove = Position + tmpProposed;
        }

        public override bool Equals(object obj)
        {
            return obj is Elve elve &&
                   Position.Equals(elve.Position);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Position.X, Position.Y);
        }
    }

    static class Directions
    {
        public static Vector2 N = new(0, -1);
        public static Vector2 NE = new(1, -1);
        public static Vector2 E = new(1, 0);
        public static Vector2 SE = new(1, 1);
        public static Vector2 S = new(0, 1);
        public static Vector2 SW = new(-1, 1);
        public static Vector2 W = new(-1, 0);
        public static Vector2 NW = new(-1, -1);

        public static Vector2[] North = { NW, N, NE };
        public static Vector2[] South = { SE, S, SW };
        public static Vector2[] West = { SW, W, NW };
        public static Vector2[] East = { NE, E, SE };

        public static Vector2[][] Main = { North, South, West, East };

        public static void MoveDirections()
        {
            var tmp = Main[0];
            for (int i = 1; i < Main.Length; i++)
            {
                Main[i - 1] = Main[i];
            }

            Main[3] = tmp;


            //Main = new Vector2[][]{ Main[1], Main[2], Main[3], Main[0] };
        }
    }
}