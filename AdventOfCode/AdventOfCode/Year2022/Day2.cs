﻿using System;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2022
{
    [Day(2)]
    public class Day2 : Solution
    {
        private RPC[] order = new RPC[] { RPC.Rock, RPC.Scissors, RPC.Paper };

        public override string SolvePart1()
        {
            long sum = 0;
            foreach (var game in Input)
            {
                var opp = Opponent(game[0]);
                var self = Self(game[2]);
                sum += Game1(opp, self);
            }

            return sum.ToString();
        }

        public override string SolvePart2()
        {
            long sum = 0;
            foreach (var game in Input)
            {
                var opp = Opponent(game[0]);
                var outcome = TargetOutcome(game[2]);
                sum += Game2(opp, outcome);
            }

            return sum.ToString();
        }

        private RPC Opponent(char type) => type switch
        {
            'A' => RPC.Rock,
            'B' => RPC.Paper,
            'C' => RPC.Scissors,
            _ => throw new Exception(),
        };

        private RPC Self(char type) => type switch
        {
            'X' => RPC.Rock,
            'Y' => RPC.Paper,
            'Z' => RPC.Scissors,
            _ => throw new Exception(),
        };

        private Outcome TargetOutcome(char type) => type switch
        {
            'X' => Outcome.Lose,
            'Y' => Outcome.Draw,
            'Z' => Outcome.Win,
            _ => throw new Exception(),
        };

        private int RPCPoints(RPC rpc) => rpc switch
        {
            RPC.Rock => 1,
            RPC.Paper => 2,
            RPC.Scissors => 3,
            _ => throw new Exception(),
        };

        private RPC IsWinningOver(RPC choice)
        {
            var idx = Array.IndexOf(order, choice);
            if (idx == order.Length - 1)
                idx = 0;
            else
                idx += 1;
            return order[idx];
        }

        private RPC IsLosingOver(RPC choice)
        {
            var idx = Array.IndexOf(order, choice);
            if (idx == 0)
                idx = order.Length - 1;
            else
                idx -= 1;
            return order[idx];
        }

        private int Game1(RPC opp, RPC self)
        {
            if (opp == self)
                return 3 + RPCPoints(self);
            if (IsWinningOver(self) == opp)
                return 6 + RPCPoints(self);
            else
                return 0 + RPCPoints(self);
        }

        private int Game2(RPC opp, Outcome outcome) => outcome switch
        {
            Outcome.Draw => 3 + RPCPoints(opp),
            Outcome.Lose => 0 + RPCPoints(IsWinningOver(opp)),
            Outcome.Win => 6 + RPCPoints(IsLosingOver(opp)),
            _ => throw new Exception(),
        };

        enum RPC
        {
            Rock,
            Paper,
            Scissors
        }

        enum Outcome
        {
            Draw,
            Win,
            Lose
        }
    }
}