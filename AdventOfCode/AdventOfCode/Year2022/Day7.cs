﻿using System;
using System.Collections.Generic;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2022
{
    [Day(7)]
    internal class Day7 : Solution
    {
        Storage storage;

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            storage = new Storage();
            storage.BuildStorage(Input);
        }

        public override string SolvePart1()
        {
            var sum = 0L;
            foreach (var f in storage.AllFolders)
            {
                if (f.Size <= 100000)
                    sum += f.Size;
            }

            return sum.ToString();
        }

        public override string SolvePart2()
        {
            var spaceToDelete = storage.Root.Size - 40000000;
            var smallest = long.MaxValue;
            foreach (var f in storage.AllFolders)
            {
                if (f.Size >= spaceToDelete && f.Size < smallest)
                    smallest = f.Size;
            }

            return smallest.ToString();
        }
    }

    class Storage
    {
        public Folder Root { get; set; } = new Folder("/", null);
        private Folder CurrentFolder;
        public List<Folder> AllFolders { get; set; } = new List<Folder>();

        public Storage()
        {
            CurrentFolder = Root;
        }

        public void BuildStorage(string[] input)
        {
            for (var i = 1; i < input.Length; i++)
            {
                i += RunCmd(input[i], input, i);
            }
        }

        public int RunCmd(string cmd, string[] input, int index)
        {
            var split = cmd.Split(' ');
            if (split[0] != "$") return 0;

            switch (split[1])
            {
                case "cd":
                    Cd(split[2]);
                    return 0;
                case "ls": return Ls(input, index);

                default: return 0;
            }
        }

        public void Cd(string param)
        {
            switch (param)
            {
                case "..":
                    CurrentFolder = CurrentFolder.Parent;
                    return;
                case "/":
                    CurrentFolder = Root;
                    return;
                default:
                    foreach (var f in CurrentFolder.Children)
                    {
                        if (f.Name == param && f is Folder folder)
                        {
                            CurrentFolder = folder;
                            return;
                        }
                    }

                    throw new Exception(param + " Folder doesnt exist");
            }
        }

        public int Ls(string[] input, int index)
        {
            var cnt = 0;
            var i = index + 1;

            while (!input[i].StartsWith("$"))
            {
                cnt++;
                var split = input[i].Split(" ");
                switch (split[0])
                {
                    case "dir":
                        AllFolders.Add(CurrentFolder.AddChild(split[1]));
                        break;
                    default:
                        CurrentFolder.AddChild(split[1], long.Parse(split[0]));
                        break;
                }

                i++;
                if (input.Length == i) break;
            }

            return cnt;
        }
    }


    class StorageObject
    {
        public string Name { get; set; }
        public long Size { get; set; }
        public Folder Parent { get; set; }

        public StorageObject(string name, Folder parent)
        {
            Name = name;
            Parent = parent;
        }

        public void AddSize(long size)
        {
            Size += size;
            Parent?.AddSize(size);
        }
    }

    class Folder : StorageObject
    {
        public Folder(string name, Folder folder) : base(name, folder)
        {
        }

        public List<StorageObject> Children { get; set; } = new List<StorageObject>();

        public void AddChild(string name, long size)
        {
            var file = new File(name, this);
            file.AddSize(size);
            Children.Add(file);
        }

        public Folder AddChild(string name)
        {
            var folder = new Folder(name, this);
            Children.Add(folder);
            return folder;
        }
    }

    class File : StorageObject
    {
        public File(string name, Folder parent) : base(name, parent)
        {
        }
    }
}