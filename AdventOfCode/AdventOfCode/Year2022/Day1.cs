﻿using System.Collections.Generic;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2022
{
    [Day(1)]
    public class Day1 : Solution
    {
        public override string SolvePart1()
        {
            long maxCal = 0;
            long tmpSum = 0;
            foreach (var cal in Input)
            {
                if (string.IsNullOrWhiteSpace(cal))
                {
                    if (tmpSum > maxCal)
                    {
                        maxCal = tmpSum;
                    }

                    tmpSum = 0;
                    continue;
                }

                tmpSum += long.Parse(cal);
            }

            return maxCal.ToString();
        }

        public override string SolvePart2()
        {
            SortedSet<long> maxCal = new(new LongComparer());
            long tmpSum = 0;
            foreach (var cal in Input)
            {
                if (string.IsNullOrWhiteSpace(cal))
                {
                    maxCal.Add(tmpSum);
                    tmpSum = 0;
                    continue;
                }

                tmpSum += long.Parse(cal);
            }

            long[] ar = new long[3];
            maxCal.CopyTo(ar, 0, 3);
            return (ar[0] + ar[1] + ar[2]).ToString();
        }
    }

    class LongComparer : IComparer<long>
    {
        public int Compare(long x, long y)
        {
            if (x == y) return 0;
            if (x < y) return 1;
            return -1;
        }
    }
}