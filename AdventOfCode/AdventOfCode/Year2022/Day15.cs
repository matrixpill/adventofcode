﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using AoCUtils.Interfaces;
using static AdventOfCode.Year2022.Day4;

namespace AdventOfCode.Year2022
{
    class Sensor
    {
        public Sensor(Vector2 position, Vector2 beacon)
        {
            Position = position;
            Beacon = beacon;
            Manhatten = (int)Math.Abs(Position.X - Beacon.X) + (int)Math.Abs(Position.Y - Beacon.Y);
        }

        public Vector2 Position { get; }
        public Vector2 Beacon { get; }
        public int Manhatten { get; }
    }

    [Day(15)]
    internal class Day15 : Solution
    {
        List<Sensor> Sensors;

        public override void Initialize(string[] input)
        {
            Sensors = new List<Sensor>();
            base.Initialize(input);

            for (var i = 0; i < Input.Length; i++)
            {
                var split1 = input[i].Split(":");
                var sensorBase = split1[0].Split(",");

                var sensorPos = new Vector2
                {
                    X = int.Parse(sensorBase[0].Split("=")[1]),
                    Y = int.Parse(sensorBase[1].Split("=")[1])
                };

                var beaconBase = split1[1].Split(",");
                var beaconPos = new Vector2
                {
                    X = int.Parse(beaconBase[0].Split("=")[1]),
                    Y = int.Parse(beaconBase[1].Split("=")[1])
                };
                var newS = new Sensor(sensorPos, beaconPos);
                Sensors.Add(newS);
            }
        }

        public override string SolvePart1()
        {
            var targetRow = WithSampleData ? 10 : 2000000;
            var alreadyInRow = new HashSet<Vector2>();
            var row = new Row();

            foreach (var sensor in Sensors)
            {
                var distToTarget = Math.Abs(sensor.Position.Y - targetRow);
                if (distToTarget > sensor.Manhatten) continue;
                var disLeftRight = sensor.Manhatten - distToTarget;
                var startX = sensor.Position.X - disLeftRight;
                var endX = sensor.Position.X + disLeftRight;
                row.AddRange((long)startX, (long)endX);

                if (sensor.Position.Y == targetRow)
                    alreadyInRow.Add(sensor.Position);
                if (sensor.Beacon.Y == targetRow)
                    alreadyInRow.Add(sensor.Beacon);
            }

            var range = row.Scanned.First().Value;
            return ((range.End - range.Start + 1) - alreadyInRow.Count).ToString();
        }

        public override string SolvePart2()
        {
            var maxXY = WithSampleData ? 20 : 4000000;
            var currRow = new Row();
            for (var targetRow = 0; targetRow <= maxXY; targetRow++)
            {
                currRow.SetRow(targetRow);
                foreach (var sensor in Sensors)
                {
                    var distToTarget = Math.Abs(sensor.Position.Y - targetRow);
                    if (distToTarget > sensor.Manhatten) continue;
                    var disLeftRight = sensor.Manhatten - distToTarget;
                    var startX = sensor.Position.X - disLeftRight;
                    var endX = sensor.Position.X + disLeftRight;
                    currRow.AddRange((long)startX, (long)endX);
                }

                if (currRow.Scanned.Count >= 2)
                    break;
            }

            var firstMissing = currRow.Scanned.First().Value.End + 1;
            var bi = new BigInteger(firstMissing);
            bi *= 4000000;
            bi += currRow.SelectedRow;
            return bi.ToString();
        }
    }


    public class Row
    {
        public SortedList<long, AocRange> Scanned = new();
        public long SelectedRow = 0;

        public void AddRange(long start, long end)
        {
            var newRange = new AocRange(start, end);
            var toDelete = new HashSet<int>();
            var overlapped = false;

            for (int i = 0; i < Scanned.Count; i++)
            {
                var key = Scanned.Keys[i];
                var range = Scanned[key];
                if (range.IsCompletelyInRange(newRange)) return;
                if (end < range.Start) break;
                if (!range.Overlaps(newRange)) continue;
                overlapped = true;
                if (range.Start > newRange.Start)
                    range.Start = newRange.Start;
                if (range.End < newRange.End)
                    range.End = newRange.End;

                for (var n = i + 1; n < Scanned.Count; n++)
                {
                    var k2 = Scanned.Keys[n];
                    var nxtRange = Scanned[k2];

                    if (nxtRange.Start > range.End)
                        break;

                    if (range.End >= nxtRange.Start && range.End <= nxtRange.End)
                    {
                        range.End = nxtRange.End;
                        toDelete.Add(n);
                        break;
                    }

                    if (range.End > nxtRange.End)
                        toDelete.Add(n);
                }
            }

            var array = toDelete.ToArray();
            for (var rIdx = array.Length - 1; rIdx >= 0; rIdx--)
                Scanned.RemoveAt(array[rIdx]);
            if (!overlapped)
                Scanned.Add(start, new AocRange(start, end));
        }

        internal void SetRow(int targetRow)
        {
            SelectedRow = targetRow;
            Scanned.Clear();
        }
    }
}