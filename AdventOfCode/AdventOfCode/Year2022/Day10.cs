﻿using System;
using System.Text;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2022
{
    [Day(10)]
    internal class Day10 : Solution
    {
        public Operation[] Operations { get; set; }

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            Operations = new Operation[input.Length];
            for (var i = 0; i < Operations.Length; i++)
            {
                Operations[i] = Operation.Create(input[i]);
            }
        }

        public override string SolvePart1()
        {
            var circuit = new Circuit(Operations);
            var sum = 0L;
            for (var i = 20; i <= 220; i += 40)
            {
                var register = circuit.RunUntilCycle(i);
                sum += i * register;
            }

            return sum.ToString();
        }

        public override string SolvePart2()
        {
            var sb = new StringBuilder();
            var circuit = new Circuit(Operations);

            for (var i = 1; i <= 240; i += 1)
            {
                var register = circuit.RunUntilCycle(i);
                var pos = (i % 40) - 1;

                if (pos == register || pos == register - 1 || pos == register + 1)
                    sb.Append('#');
                else
                    sb.Append('.');

                if (i % 40 == 0)
                    sb.AppendLine();
            }

            Console.WriteLine(sb.ToString());
            return string.Empty;
        }
    }

    public class Circuit
    {
        public int Cycles { get; set; } = 1;
        public int OpIdx { get; set; } = 0;
        public Operation[] Operations { get; set; }

        public long Register { get; set; } = 1;

        public Circuit(Operation[] ops)
        {
            Operations = new Operation[ops.Length];
            for (int i = 0; i < ops.Length; i++)
            {
                Operations[i] = ops[i].Copy();
            }
        }

        public long RunUntilCycle(int cycle)
        {
            while (Cycles < cycle)
            {
                if (Operations[OpIdx].RunOnceOnCircuit(this))
                {
                    OpIdx++;
                }

                Cycles++;
            }

            return Register;
        }
    }


    public abstract class Operation
    {
        public int Cylcles { get; set; }
        public string BaseString { get; set; }

        public Operation(int cycles)
        {
            Cylcles = cycles;
        }

        public static Operation Create(string line)
        {
            var splt = line.Split(" ");
            Operation newOp = splt[0] switch
            {
                "noop" => new Noop(),
                "addx" => new AddX(int.Parse(splt[1])),
                _ => throw new Exception("unknown operation"),
            };
            newOp.BaseString = line;
            return newOp;
        }

        public Operation Copy()
        {
            return Create(BaseString);
        }

        public abstract bool RunOnceOnCircuit(Circuit circuit);
    }

    public class AddX : Operation
    {
        public AddX(int val) : base(2)
        {
            Val = val;
        }

        public int Val { get; }

        public override bool RunOnceOnCircuit(Circuit circuit)
        {
            Cylcles--;
            if (Cylcles == 0)
            {
                circuit.Register += Val;

                return true;
            }

            return false;
        }
    }

    public class Noop : Operation
    {
        public Noop() : base(1)
        {
        }

        public override bool RunOnceOnCircuit(Circuit circuit)
        {
            Cylcles--;
            return true;
        }
    }
}