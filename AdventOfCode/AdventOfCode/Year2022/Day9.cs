﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2022
{
    [Day(9)]
    internal class Day9 : Solution
    {
        public override string SolvePart1()
        {
            var rope = new Vector2[] { Vector2.Zero, Vector2.Zero };
            return SimulateRope(rope).ToString();
        }

        public override string SolvePart2()
        {
            var rope = new Vector2[10];
            for (int i = 0; i < rope.Length; i++)
                rope[i] = Vector2.Zero;

            return SimulateRope(rope).ToString();
        }

        private int SimulateRope(Vector2[] rope, bool anim = false)
        {
            var visited = new HashSet<Vector2>();

            foreach (var mv in Input)
            {
                var dir = mv.Split(" ")[0];
                var cnt = int.Parse(mv.Split(" ")[1]);

                var dirVec = Vector2.Zero;
                switch (dir)
                {
                    case "U":
                        dirVec.Y = 1;
                        break;
                    case "R":
                        dirVec.X = 1;
                        break;
                    case "D":
                        dirVec.Y = -1;
                        break;
                    case "L":
                        dirVec.X = -1;
                        break;
                }

                for (var i = 0; i < cnt; i++)
                {
                    rope[0] += dirVec;
                    for (var j = 0; j < rope.Length - 1; j++)
                    {
                        var div = rope[j] - rope[j + 1];
                        if (Math.Abs(div.X) > 1 || Math.Abs(div.Y) > 1)
                        {
                            var clamped = Vector2.Clamp(div, new Vector2(-1, -1), Vector2.One);
                            rope[j + 1] += clamped;
                        }
                    }

                    visited.Add(rope[^1]);
                }

                if (anim)
                    DisplayRope(rope);
            }

            return visited.Count;
        }

        private void DisplayRope(Vector2[] OrigRope)
        {
            var rope = new Vector2[OrigRope.Length];
            Array.Copy(OrigRope, rope, OrigRope.Length);
            var minX = rope.Min(x => x.X);
            var minY = rope.Min(x => x.Y);

            var maxX = rope.Max(x => x.X) + minX < 0 ? Math.Abs(minX) : 0;
            var maxY = rope.Max(x => x.Y) + minY < 0 ? Math.Abs(minY) : 0;

            for (var i = 0; i < rope.Length; i++)
            {
                rope[i].X += minX < 0 ? Math.Abs(minX) : 0;
                rope[i].Y += minY < 0 ? Math.Abs(minY) : 0;
            }

            Console.Clear();
            for (int i = rope.Length - 1; i >= 0; i--)
            {
                Vector2 k = rope[i];
                Console.SetCursorPosition((int)k.X, (int)k.Y);
                if (i == 0)
                    Console.Write("H");
                else
                    Console.Write(i);
            }

            Thread.Sleep(25);
        }
    }
}