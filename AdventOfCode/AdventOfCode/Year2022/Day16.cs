﻿using System.Collections.Generic;
using System.Linq;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2022
{
    [Day(16)]
    internal class Day16 : Solution
    {
        public Dictionary<string, Valve> Valves = new();

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            foreach (var valve in Input)
            {
                var splt = valve.Split(" ");
                var name = splt[1];
                var rate = int.Parse(splt[4].Split("=")[1].Replace(";", ""));
                var names = new List<string>();
                for (var i = 9; i < splt.Length; i++)
                {
                    names.Add(splt[i].Replace(",", ""));
                }

                Valves.Add(name, new Valve(name, rate, names));
            }

            foreach (var v in Valves)
            {
                v.Value.Connect(Valves);
            }

            foreach (var v in Valves)
            {
                v.Value.CalcDistances(Valves);
            }
        }

        public override string SolvePart1()
        {
            var f = Open(Valves["AA"],
                Valves.Where(x => x.Value.FlowRate > 0).OrderByDescending(x => x.Value.FlowRate).Select(x => x.Value)
                    .ToList());
            return f.ToString();
        }

        public override string SolvePart2()
        {
            return base.SolvePart2();
        }

        public long Open(Valve from, List<Valve> stillClosed, int remaining = 30)
        {
            var sum = 0L;
            stillClosed.Remove(from);
            List<(long Flow, Valve Valve, int Distance)> nxt = new();
            foreach (var v in stillClosed.ToArray())
            {
                var d = from.GetDistanceTo(v);
                var remain = remaining - (d + 1);
                if (remain <= 0)
                    continue;

                var tmpSum = (long)v.FlowInMinutes(remain);
                nxt.Add((tmpSum, v, d));
            }

            var ordered = nxt.OrderByDescending(x => x.Flow);
            foreach (var n in ordered)
            {
                var tmpSum = n.Flow;
                tmpSum += Open(n.Valve, stillClosed, remaining - n.Distance - 1);
                if (tmpSum > sum)
                    sum = tmpSum;
            }

            stillClosed.Add(from);
            return sum;
        }
    }

    class Valve
    {
        public List<Valve> ConnectedValves { get; set; } = new();
        public int FlowRate { get; set; }
        public string Name { get; set; }
        public bool Open { get; set; } = false;
        public List<string> ConnectedNames { get; set; } = new();
        public Dictionary<Valve, int> Distances { get; set; } = new();

        public Valve(string name, int fLowRate, List<string> connectedNames)
        {
            Name = name;
            FlowRate = fLowRate;
            ConnectedNames = connectedNames;
        }

        public void Connect(Dictionary<string, Valve> allValves)
        {
            ConnectedNames.ForEach(name => { ConnectedValves.Add(allValves[name]); });
        }

        public void CalcDistances(Dictionary<string, Valve> allValves)
        {
            foreach (var v in allValves.Values)
            {
                if (v != this)
                    GetDistanceTo(v);
            }
        }

        public int FlowInMinutes(int minutes)
        {
            return minutes * FlowRate;
        }

        public int GetDistanceTo(Valve v)
        {
            if (Distances.ContainsKey(v)) return Distances[v];
            if (v.Distances.ContainsKey(this))
            {
                Distances.Add(v, v.Distances[this]);
                return v.Distances[this];
            }

            var dist = DistanceTo(v);

            Distances.Add(v, dist);
            return dist;
        }

        private int DistanceTo(Valve target, int currMin = int.MaxValue, List<Valve> visited = null)
        {
            visited ??= new();
            visited.Add(this);
            if (visited.Count > currMin) return currMin;
            foreach (var valve in ConnectedValves)
            {
                if (visited.Contains(valve)) continue;
                if (target == valve)
                    return visited.Count;

                var d = valve.DistanceTo(target, currMin, visited);
                visited.Remove(valve);
                if (d > 0 && d < currMin)
                    currMin = d;
            }

            return currMin;
        }
    }
}