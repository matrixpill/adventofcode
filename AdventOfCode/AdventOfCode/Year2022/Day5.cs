﻿using System.Collections.Generic;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2022
{
    [Day(5)]
    internal class Day5 : Solution
    {
        int bottomContainerLayer;
        int stackCount;

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            stackCount = (Input[0].Length + 1) / 4;
            for (var i = 0; i < Input.Length; i++)
            {
                if (Input[i][1] == '1')
                {
                    bottomContainerLayer = i - 1;
                    break;
                }
            }
        }

        public override string SolvePart1()
        {
            var stacks = BuildStacks();

            var move = new Move();
            for (var i = bottomContainerLayer + 3; i < Input.Length; i++)
            {
                move.FillWithNewValues(Input[i]);
                for (var c = 0; c < move.Count; c++)
                {
                    stacks[move.Target - 1].Push(stacks[move.Source - 1].Pop());
                }
            }

            return ReadTops(stacks);
        }

        public override string SolvePart2()
        {
            var stacks = BuildStacks();

            var move = new Move();
            for (var i = bottomContainerLayer + 3; i < Input.Length; i++)
            {
                move.FillWithNewValues(Input[i]);
                var tmpStack = new Stack<string>();
                for (var c = 0; c < move.Count; c++)
                {
                    tmpStack.Push(stacks[move.Source - 1].Pop());
                }

                while (tmpStack.Count > 0)
                {
                    stacks[move.Target - 1].Push(tmpStack.Pop());
                }
            }

            return ReadTops(stacks);
        }

        private List<Stack<string>> BuildStacks()
        {
            var stacks = new List<Stack<string>>();

            for (var i = 0; i < stackCount; i++)
            {
                stacks.Add(new Stack<string>());
            }

            for (var i = 0; i < stackCount; i++)
            {
                var col = i * 4 + 1;
                for (var r = bottomContainerLayer; r >= 0; r--)
                {
                    if (string.IsNullOrWhiteSpace(Input[r][col].ToString()))
                        break;
                    stacks[i].Push(Input[r][col].ToString());
                }
            }

            return stacks;
        }

        private string ReadTops(List<Stack<string>> stacks)
        {
            var top = "";
            foreach (var stack in stacks)
            {
                top += stack.Peek();
            }

            return top;
        }
    }

    public class Move
    {
        public int Count { get; set; }
        public int Source { get; set; }
        public int Target { get; set; }

        public void FillWithNewValues(string move)
        {
            var splitted = move.Split(' ');
            Count = int.Parse(splitted[1]);
            Source = int.Parse(splitted[3]);
            Target = int.Parse(splitted[5]);
        }
    }
}