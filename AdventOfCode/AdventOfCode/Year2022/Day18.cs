﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2022
{
    [Day(18)]
    internal class Day18 : Solution
    {
        Grid Grid = new Grid();

        public override string SolvePart1()
        {
            int cubeIdx = 0;
            foreach (var line in Input)
            {
                var split = line.Split(',');
                var vec = new Vector3(int.Parse(split[0]), int.Parse(split[1]), int.Parse(split[2]));
                Grid.TryAddCubeAt(vec, cubeIdx++);
            }

            return Grid.TotalFaces.Count.ToString();
        }

        public override string SolvePart2()
        {
            var currFace = Grid.FaceOnXAxis.First().Value.First();
            var check = new HashSet<Face>() { currFace };

            CountConnected(currFace, check);
            currFace = Grid.FaceOnYAxis.First().Value.First();
            if (!check.Contains(currFace))
            {
                check.Add(currFace);
                CountConnected(currFace, check);
            }

            return check.Count().ToString();
        }

        private void CountConnected(Face from, HashSet<Face> check)
        {
            foreach (var edge in from.Edges)
            {
                var connected = Grid.TotalFaces.Where(x => !check.Contains(x) && x.Edges.Contains(edge)).ToList();
                if (connected.Count > 1)
                {
                    connected = connected.Where(x => x.CubeIdx != from.CubeIdx && x.Type != from.Type).ToList();
                }

                if (connected.Count == 0)
                    continue;
                var next = connected.First();
                check.Add(next);
                CountConnected(next, check);
            }
        }
    }

    public class Edge
    {
        private Vector3[] Points = new Vector3[2];
        public Vector3 First => Points[0];
        public Vector3 Second => Points[1];

        public Edge(Vector3 first, Vector3 second)
        {
            Points[0] = first;
            Points[1] = second;
        }

        public override bool Equals(object obj)
        {
            return obj is Edge edge && (
                (First.Equals(edge.First) && Second.Equals(edge.Second)) ||
                (First.Equals(edge.Second) && Second.Equals(edge.First)));
        }

        public override int GetHashCode()
        {
            var ordered = Points.OrderBy(x => x.X).ThenBy(x => x.Y).ThenBy(x => x.Z).ToArray();
            return HashCode.Combine(ordered[0], ordered[1]);
        }
    }

    public struct Face
    {
        public Vector3 FirstCorner { get; set; }
        public Vector3 SecondCorner { get; set; }
        public FaceType Type { get; set; }
        public List<Edge> Edges { get; set; } = new List<Edge>();
        public int CubeIdx { get; set; }

        public Face(Vector3 first, Vector3 second, FaceType type, int cubeIdx)
        {
            CubeIdx = cubeIdx;
            FirstCorner = first;
            SecondCorner = second;
            Type = type;

            switch (Type)
            {
                case FaceType.X:
                    Edges.Add(new Edge(first, first + new Vector3(0, 1, 0)));
                    Edges.Add(new Edge(first, first + new Vector3(0, 0, 1)));

                    Edges.Add(new Edge(second, second + new Vector3(0, -1, 0)));
                    Edges.Add(new Edge(second, second + new Vector3(0, 0, -1)));
                    break;
                case FaceType.Y:
                    Edges.Add(new Edge(first, first + new Vector3(1, 0, 0)));
                    Edges.Add(new Edge(first, first + new Vector3(0, 0, 1)));

                    Edges.Add(new Edge(second, second + new Vector3(-1, 0, 0)));
                    Edges.Add(new Edge(second, second + new Vector3(0, 0, -1)));
                    break;
                case FaceType.Z:
                    Edges.Add(new Edge(first, first + new Vector3(1, 0, 0)));
                    Edges.Add(new Edge(first, first + new Vector3(0, 1, 0)));

                    Edges.Add(new Edge(second, second + new Vector3(-1, 0, 0)));
                    Edges.Add(new Edge(second, second + new Vector3(0, -1, 0)));
                    break;
            }
        }

        public override bool Equals(object obj)
        {
            return obj is Face side &&
                   FirstCorner.Equals(side.FirstCorner) && SecondCorner.Equals(side.SecondCorner);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(FirstCorner, SecondCorner);
        }
    }

    public enum FaceType
    {
        X,
        Y,
        Z
    }

    public class Grid
    {
        public List<Face> TotalFaces { get; set; } = new List<Face>();
        public SortedDictionary<int, HashSet<Face>> FaceOnXAxis { get; set; } = new();
        public SortedDictionary<int, HashSet<Face>> FaceOnYAxis { get; set; } = new();
        public SortedDictionary<int, HashSet<Face>> FaceOnZAxis { get; set; } = new();


        public void TryAddCubeAt(Vector3 source, int cubeIdx)
        {
            var diag = new Vector3(source.X + 1, source.Y + 1, source.Z + 1);

            TryAddFacesOnX(source, diag, cubeIdx);
            TryAddFacesOnY(source, diag, cubeIdx);
            TryAddFacesOnZ(source, diag, cubeIdx);
        }

        public void TryAddFacesOnX(Vector3 source, Vector3 diag, int cubeIdx)
        {
            var face1 = new Vector3(source.X, source.Y + 1, source.Z + 1);
            var face2 = new Vector3(source.X + 1, source.Y, source.Z);
            TryAdd(new Face(source, face1, FaceType.X, cubeIdx), FaceOnXAxis, face1.X);
            TryAdd(new Face(face2, diag, FaceType.X, cubeIdx), FaceOnXAxis, face2.X);
        }

        public void TryAddFacesOnY(Vector3 source, Vector3 diag, int cubeIdx)
        {
            var face1 = new Vector3(source.X + 1, source.Y, source.Z + 1);
            var face2 = new Vector3(source.X, source.Y + 1, source.Z);
            TryAdd(new Face(source, face1, FaceType.Y, cubeIdx), FaceOnYAxis, face1.Y);
            TryAdd(new Face(face2, diag, FaceType.Y, cubeIdx), FaceOnYAxis, face2.Y);
        }

        public void TryAddFacesOnZ(Vector3 source, Vector3 diag, int cubeIdx)
        {
            var face1 = new Vector3(source.X + 1, source.Y + 1, source.Z);
            var face2 = new Vector3(source.X, source.Y, source.Z + 1);
            TryAdd(new Face(source, face1, FaceType.Z, cubeIdx), FaceOnZAxis, face1.Z);
            TryAdd(new Face(face2, diag, FaceType.Z, cubeIdx), FaceOnZAxis, face2.Z);
        }

        private void TryAdd(Face face, SortedDictionary<int, HashSet<Face>> list, float layer)
        {
            var lIdx = (int)layer;
            if (!list.ContainsKey(lIdx)) list.Add(lIdx, new HashSet<Face>());
            if (list[lIdx].Add(face))
            {
                TotalFaces.Add(face);
            }
            else
            {
                TotalFaces.Remove(face);
                list[lIdx].Remove(face);
            }
        }
    }
}