﻿using System;
using System.Collections.Generic;
using System.Linq;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2022
{
    [Day(17)]
    internal class Day17 : Solution
    {
        Rock[] Rocks = new Rock[5];
        string[] Rockstrings = new string[] { "####", ".#.\n###\n.#.", "..#\n..#\n###", "#\n#\n#\n#", "##\n##" };
        string Winds;

        public override void Initialize(string[] input)
        {
            for (var i = 0; i < Rocks.Length; i++)
                Rocks[i] = new Rock(Rockstrings[i]);
            base.Initialize(input);
            Winds = input[0];
        }

        public override string SolvePart1()
        {
            var windIdx = 0;
            var field = new Field();
            for (var r = 0; r < 2022; r++)
            {
                var rockIdx = r % Rocks.Length;
                var rock = Rocks[rockIdx];
                rock.Position = new LargeVector(3, field.HeighestPiece.Y + 3 + rock.Height);
                var falling = true;
                while (falling)
                {
                    var tmpWindIdx = windIdx++ % Winds.Length;
                    //Console.Write(rock.Position + " -> ");
                    rock.TryMoveOnField(Vectors.GetDirection(Winds[tmpWindIdx]), field);
                    //Console.Write(rock.Position);
                    //Console.WriteLine();
                    if (!rock.TryMoveOnField(MoveDirection.Down, field))
                    {
                        field.PlaceOnField(rock);
                        falling = false;
                        //field.Draw();
                    }
                }
                //Console.ReadLine();
            }

            return field.HeighestPiece.Y.ToString();
        }

        public override string SolvePart2()
        {
            var dropped = new Dictionary<Check, CircleStats>();
            var windIdx = 0;
            var field = new Field();
            var max = 1000000000000;
            for (var r = 0L; r < max; r++)
            {
                var rockIdx = r % Rocks.Length;
                var rock = Rocks[rockIdx];
                rock.Position = new LargeVector(3, field.HeighestPiece.Y + 3 + rock.Height);

                for (var f = 0; true; f++)
                {
                    var tmpWindIdx = windIdx++ % Winds.Length;

                    rock.TryMoveOnField(Vectors.GetDirection(Winds[tmpWindIdx]), field);

                    if (!rock.TryMoveOnField(MoveDirection.Down, field))
                    {
                        field.PlaceOnField(rock);
                        var newCheck = new Check() { WindIdx = tmpWindIdx, RockIdx = rockIdx, MaxFall = f };
                        var newCircleStats = new CircleStats
                        {
                            DroppedRocks = field.DroppedRocks,
                            TowerHeight = field.HeighestPiece.Y
                        };
                        if (!dropped.TryAdd(newCheck, newCircleStats))
                        {
                            var g = dropped.OrderByDescending(x => x.Key.MaxFall).ToList();
                            var old = dropped[newCheck];
                            var toDropBlocks = max - field.DroppedRocks;
                            var openCylcles = toDropBlocks / (newCircleStats.DroppedRocks - old.DroppedRocks);
                            var addedHeight = openCylcles * (newCircleStats.TowerHeight - old.TowerHeight);
                            return (newCircleStats.TowerHeight + addedHeight).ToString();
                        }

                        break;
                    }
                }
            }

            return field.HeighestPiece.Y.ToString();
        }
    }

    public struct Check
    {
        public long WindIdx { get; set; }
        public long RockIdx { get; set; }
        public long MaxFall { get; set; }

        public override bool Equals(object obj)
        {
            return obj is Check check &&
                   WindIdx == check.WindIdx &&
                   RockIdx == check.RockIdx &&
                   MaxFall == check.MaxFall;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(WindIdx, RockIdx, MaxFall);
        }
    }

    public class CircleStats
    {
        public long DroppedRocks { get; set; } = 0;
        public long TowerHeight { get; set; } = 0;
    }

    public class Rock
    {
        public LargeVector Position { get; set; }
        public List<LargeVector> Pieces { get; set; } = new();
        public string[] BaseString;
        public int Width => BaseString[0].Length;
        public int Height => BaseString.Length;

        public Rock(string layout)
        {
            var layoutLines = layout.Split('\n');
            BaseString = layoutLines;
            for (var y = 0; y < layoutLines.Length; y++)
            {
                var line = layoutLines[y];
                for (var x = 0; x < line.Length; x++)
                {
                    if (line[x] != '#') continue;
                    var piece = new LargeVector(x, y * -1);
                    Pieces.Add(piece);
                }
            }
        }

        public bool TryMoveOnField(MoveDirection direction, Field field)
        {
            var moveVector = Vectors.GetMoveVector(direction);
            var canMove = true;
            foreach (var piece in Pieces)
            {
                if (field.IsSpaceOccupied(piece + moveVector + Position))
                {
                    canMove = false;
                    break;
                }
            }

            if (canMove)
                Position += moveVector;
            return canMove;
        }
    }

    public struct LargeVector
    {
        public long X { get; set; }
        public long Y { get; set; }

        public LargeVector(long x, long y)
        {
            X = x;
            Y = y;
        }

        public override bool Equals(object obj)
        {
            return obj is LargeVector vector &&
                   X == vector.X &&
                   Y == vector.Y;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(X, Y);
        }

        public static LargeVector operator +(LargeVector left, LargeVector right) =>
            new LargeVector(left.X + right.X, left.Y + right.Y);

        public static LargeVector Zero => new LargeVector(0, 0);
    }

    public class Field
    {
        public long DroppedRocks { get; set; } = 0;

        public Dictionary<LargeVector, PieceType> OccupiedSpaces { get; set; } =
            new Dictionary<LargeVector, PieceType>();

        public LargeVector HeighestPiece { get; set; } = LargeVector.Zero;

        public bool IsSpaceOccupied(LargeVector position)
        {
            if (OccupiedSpaces.ContainsKey(position)) return true;
            if (position.X <= 0 || position.X >= 8) return true;
            if (position.Y <= 0) return true;
            return false;
        }

        public void PlaceOnField(Rock rock)
        {
            DroppedRocks++;
            foreach (var rockPiece in rock.Pieces)
            {
                var newPiece = rockPiece + rock.Position;
                if (newPiece.Y > HeighestPiece.Y)
                    HeighestPiece = newPiece;
                OccupiedSpaces.Add(newPiece, PieceType.SetPiece);
            }
        }

        internal void Draw()
        {
            for (var y = (long)HeighestPiece.Y; y >= 0; y--)
            {
                for (var x = 0; x < 9; x++)
                {
                    char c = '.';
                    if (y == 0) c = '-';
                    if (x == 0 || x == 8) c = '|';
                    if (OccupiedSpaces.ContainsKey(new LargeVector(x, y))) c = '#';
                    Console.Write(c);
                }

                Console.WriteLine();
            }

            Console.WriteLine();
            Console.WriteLine();
        }
    }

    static class Vectors
    {
        static LargeVector LEFT = new LargeVector(-1, 0);
        static LargeVector RIGHT = new LargeVector(1, 0);
        static LargeVector DOWN = new LargeVector(0, -1);

        public static LargeVector GetMoveVector(MoveDirection direction)
        {
            switch (direction)
            {
                case MoveDirection.Left:
                    return LEFT;
                case MoveDirection.Right:
                    return RIGHT;
                case MoveDirection.Down:
                    return DOWN;
                default:
                    throw new Exception("unknown direction");
            }
        }

        public static MoveDirection GetDirection(char c)
        {
            switch (c)
            {
                case '<': return MoveDirection.Left;
                case '>': return MoveDirection.Right;
            }

            throw new Exception("unknown direction");
        }
    }


    public enum MoveDirection
    {
        Left,
        Right,
        Down
    }

    public enum PieceType
    {
        Wall,
        SetPiece
    }
}