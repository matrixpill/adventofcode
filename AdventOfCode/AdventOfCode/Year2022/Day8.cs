﻿using AoCUtils.Interfaces;

namespace AdventOfCode.Year2022
{
    [Day(8)]
    internal class Day8 : Solution
    {
        //r,c
        int[,] treeField;

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            treeField = new int[input.Length, input[0].Length];
            for (var r = 0; r < input.Length; r++)
            {
                var row = input[r];
                for (var c = 0; c < row.Length; c++)
                {
                    treeField[r, c] = (int)char.GetNumericValue(row[c]);
                }
            }
        }

        public override string SolvePart1()
        {
            var visibleTrees = treeField.GetLongLength(0) * 2 + treeField.GetLongLength(1) * 2 - 4;
            //dumb version
            for (var r = 1; r < treeField.GetLength(0) - 1; r++)
            {
                for (var c = 1; c < treeField.GetLength(1) - 1; c++)
                {
                    if (IsVisible(r, c)) visibleTrees++;
                }
            }

            return visibleTrees.ToString();
        }

        public override string SolvePart2()
        {
            var bestScenicScore = 0L;
            for (var r = 1; r < treeField.GetLength(0) - 1; r++)
            {
                for (var c = 1; c < treeField.GetLength(1) - 1; c++)
                {
                    var score = ScenicScore(r, c);
                    if (score > bestScenicScore)
                    {
                        bestScenicScore = score;
                    }
                }
            }

            return bestScenicScore.ToString();
        }

        private bool IsVisible(int r, int c)
        {
            var height = treeField[r, c];

            //check top
            var tmp = r - 1;
            while (tmp >= 0)
            {
                if (treeField[tmp, c] >= height)
                    break;
                if (tmp == 0)
                    return true;
                tmp--;
            }

            //check right
            tmp = c + 1;
            while (tmp < treeField.GetLength(1))
            {
                if (treeField[r, tmp] >= height)
                    break;
                if (tmp == treeField.GetLength(1) - 1)
                    return true;
                tmp++;
            }


            //check bottom
            tmp = r + 1;
            while (tmp < treeField.GetLength(0))
            {
                if (treeField[tmp, c] >= height)
                    break;
                if (tmp == treeField.GetLength(0) - 1)
                    return true;
                tmp++;
            }

            //check left
            tmp = c - 1;
            while (tmp >= 0)
            {
                if (treeField[r, tmp] >= height)
                    break;
                if (tmp == 0)
                    return true;
                tmp--;
            }

            return false;
        }

        private long ScenicScore(int r, int c)
        {
            var height = treeField[r, c];
            var score = 1L;
            var tmpScore = 0;
            //check top
            var tmp = r - 1;
            while (tmp >= 0)
            {
                tmpScore++;
                if (treeField[tmp, c] >= height)
                    break;
                tmp--;
            }

            score *= tmpScore;

            //check right
            tmpScore = 0;
            tmp = c + 1;
            while (tmp < treeField.GetLength(1))
            {
                tmpScore++;
                if (treeField[r, tmp] >= height)
                    break;

                tmp++;
            }

            score *= tmpScore;

            //check bottom
            tmpScore = 0;
            tmp = r + 1;
            while (tmp < treeField.GetLength(0))
            {
                tmpScore++;
                if (treeField[tmp, c] >= height)
                    break;
                tmp++;
            }

            score *= tmpScore;

            //check left
            tmpScore = 0;
            tmp = c - 1;
            while (tmp >= 0)
            {
                tmpScore++;
                if (treeField[r, tmp] >= height)
                    break;
                tmp--;
            }

            score *= tmpScore;

            return score;
        }
    }
}