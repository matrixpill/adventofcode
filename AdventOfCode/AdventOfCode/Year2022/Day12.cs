﻿using System.Numerics;
using AdventOfCode.Utility.AStar;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2022
{
    [Day(12)]
    internal class Day12 : Solution
    {
        private Node[][] Levels;
        Vector2 Start = new Vector2(0, 0);
        Vector2 End = new Vector2(0, 0);

        public override void Initialize(string[] input)
        {
            base.Initialize(input);
            Levels = new Node[input[0].Length][];
            for (int x1 = 0; x1 < input[0].Length; x1++)
            {
                Levels[x1] = new Node[input.Length];
            }

            var min = new Vector2(0, 0);
            var max = new Vector2(input[0].Length - 1, input.Length - 1);
            var y = 0;
            foreach (string row in input)
            {
                var x = 0;
                foreach (var level in row)
                {
                    var pos = new Vector2(x, y);
                    var newNode = new Node(pos, 0, 1);
                    if (level == 'S')
                    {
                        Start.X = pos.X;
                        Start.Y = pos.Y;
                    }

                    if (level == 'E')
                    {
                        End.X = pos.X;
                        End.Y = pos.Y;
                    }

                    Levels[x++][y] = newNode;

                    var tmpPos = Vector2.Add(pos, new Vector2(0, -1));
                    if (!IsNotOnCard(tmpPos, min, max))
                    {
                        if (Elevation(input[(int)tmpPos.Y][(int)tmpPos.X]) <=
                            Elevation(input[(int)pos.Y][(int)pos.X]) + 1)
                            newNode.Successors.Add(tmpPos);
                    }

                    tmpPos = Vector2.Add(pos, new Vector2(1, 0));
                    if (!IsNotOnCard(tmpPos, min, max))
                    {
                        if (Elevation(input[(int)tmpPos.Y][(int)tmpPos.X]) <=
                            Elevation(input[(int)pos.Y][(int)pos.X]) + 1)
                            newNode.Successors.Add(tmpPos);
                    }

                    tmpPos = Vector2.Add(pos, new Vector2(0, 1));
                    if (!IsNotOnCard(tmpPos, min, max))
                    {
                        if (Elevation(input[(int)tmpPos.Y][(int)tmpPos.X]) <=
                            Elevation(input[(int)pos.Y][(int)pos.X]) + 1)
                            newNode.Successors.Add(tmpPos);
                    }

                    tmpPos = Vector2.Add(pos, new Vector2(-1, 0));
                    if (!IsNotOnCard(tmpPos, min, max))
                    {
                        if (Elevation(input[(int)tmpPos.Y][(int)tmpPos.X]) <=
                            Elevation(input[(int)pos.Y][(int)pos.X]) + 1)
                            newNode.Successors.Add(tmpPos);
                    }
                }

                y++;
            }
        }

        private long Elevation(char c)
        {
            if (c == 'S')
                return 1;
            if (c == 'E')
                return 26;
            return c - 'a' + 1;
        }

        public override string SolvePart1()
        {
            var astar = new AStar(Copy(), Start, End);
            var res = astar.SolveAStar();
            return res.ToString();
        }

        public Node[][] Copy()
        {
            var l = new Node[Levels.Length][];
            for (var i = 0; i < Levels.Length; i++)
            {
                l[i] = new Node[Levels[i].Length];
            }

            for (var x = 0; x < l.Length; x++)
            {
                for (var y = 0; y < l[x].Length; y++)
                {
                    l[x][y] = Levels[x][y].Copy();
                }
            }

            return l;
        }

        public override string SolvePart2()
        {
            var min = long.MaxValue;
            for (var x = 0; x < Levels.Length; x++)
            {
                for (var y = 0; y < Levels[x].Length; y++)
                {
                    if (Input[y][x] == 'a' || Input[y][x] == 'S')
                    {
                        var astar = new AStar(Copy(), new Vector2(x, y), End);
                        try
                        {
                            var res = astar.SolveAStar();
                            if (res < min)
                                min = res;
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
            }

            return min.ToString();
        }

        private bool IsNotOnCard(Vector2 node, Vector2 start, Vector2 end)
        {
            return node.X > end.X || node.Y > end.Y ||
                   node.X < start.X || node.Y < start.Y;
        }
    }
}