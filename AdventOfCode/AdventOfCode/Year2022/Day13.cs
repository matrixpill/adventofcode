﻿using System;
using System.Collections.Generic;
using System.Text;
using AoCUtils.Interfaces;

namespace AdventOfCode.Year2022
{
    [Day(13)]
    internal class Day13 : Solution
    {
        public override string SolvePart1()
        {
            var sum = 0;
            var leftPacket = new Packet();
            var rightPacket = new Packet();
            var pairIdx = 0;
            for (var i = 0; i < Input.Length - 1; i += 3)
            {
                pairIdx++;
                leftPacket.FillQueue(Input[i]);
                rightPacket.FillQueue(Input[i + 1]);

                var r = leftPacket.Pack.Compare(rightPacket.Pack);

                if (r > 0)
                    sum += pairIdx;
            }

            return sum.ToString();
        }

        public override string SolvePart2()
        {
            var packets = new List<Packet>();
            for (var i = 0; i < Input.Length; i++)
            {
                if (string.IsNullOrEmpty(Input[i])) continue;
                var packet = new Packet();
                packet.FillQueue(Input[i]);
                packets.Add(packet);
            }

            var custom1 = "[[2]]";
            var custom2 = "[[6]]";
            var p1 = new Packet();
            p1.FillQueue(custom1);

            var p2 = new Packet();
            p2.FillQueue(custom2);
            packets.Add(p1);
            packets.Add(p2);

            packets.Sort((x, y) => x.Pack.Compare(y.Pack) * -1);

            var idx1 = packets.FindIndex(x => x.SoureString == custom1) + 1;
            var idx2 = packets.FindIndex(x => x.SoureString == custom2) + 1;

            return (idx1 * idx2).ToString();
        }
    }

    abstract class Item
    {
        public abstract int Compare(Item item);
    }

    class ValueItem : Item
    {
        public int Value { get; set; }

        public override int Compare(Item item)
        {
            if (item is ValueItem vi)
            {
                if (vi.Value == Value) return 0;
                if (vi.Value > Value) return 1;
                return -1;
            }
            else if (item is ArrayItem ai)
            {
                var i = new ArrayItem
                {
                    Items = new List<Item> { new ValueItem() { Value = Value } }
                };
                return i.Compare(ai);
            }

            throw new Exception();
        }
    }

    class ArrayItem : Item
    {
        public List<Item> Items { get; set; } = new();

        public override int Compare(Item item)
        {
            if (item is ValueItem vi)
            {
                var i = new ArrayItem
                {
                    Items = new List<Item> { new ValueItem() { Value = vi.Value } }
                };
                return Compare(i);
            }
            else if (item is ArrayItem other)
            {
                for (int i1 = 0; i1 < Items.Count; i1++)
                {
                    if (i1 < other.Items.Count)
                    {
                        var res = Items[i1].Compare(other.Items[i1]);
                        if (res != 0)
                        {
                            return res;
                        }
                    }
                }

                if (other.Items.Count > Items.Count)
                    return 1;
                if (other.Items.Count < Items.Count)
                    return -1;


                if (other.Items.Count == Items.Count)
                    return 0;
                return 1;
            }

            throw new Exception();
        }

        internal void Fill(string v)
        {
            if (v == "[]") return;
            v = v[1..^1];
            if (string.IsNullOrEmpty(v))
                return;

            var openBrackets = new Stack<char>();
            var sb = new StringBuilder();
            for (var i = 0; i < v.Length; i++)
            {
                var c = v[i];

                if (c == '[')
                    openBrackets.Push(c);
                else if (c == ']')
                    openBrackets.Pop();

                if (c == ',' && sb.Length == 0)
                    continue;

                sb.Append(c);
                if (char.IsNumber(c))
                    continue;
                if (openBrackets.Count > 0)
                    continue;

                if (sb[^1] == ',')
                {
                    var newItem = new ValueItem();
                    newItem.Value = int.Parse(sb.ToString()[..^1]);
                    Items.Add(newItem);
                }
                else
                {
                    var newItem = new ArrayItem();
                    newItem.Fill(sb.ToString());
                    Items.Add(newItem);
                }

                sb.Clear();
            }

            if (sb.Length > 0)
            {
                var newItem = new ValueItem();
                newItem.Value = int.Parse(sb.ToString());
                Items.Add(newItem);
            }
        }

        internal void Clear()
        {
            Items.Clear();
        }
    }

    class Packet
    {
        public ArrayItem Pack { get; set; } = new ArrayItem();
        public string SoureString { get; set; }

        internal void FillQueue(string v)
        {
            Pack.Clear();
            SoureString = v;
            Pack.Fill(v);
        }
    }
}