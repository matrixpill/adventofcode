﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace day5
{
    class Program
    {
        static void Main(string[] args)
        {
            var cnt1 = 0;
            var cnt2 = 0;
            foreach (string line in File.ReadAllLines("INPUT.txt"))
            {
                var r1 = RULES.Rule1(line);
                var r2 = RULES.Rule2(line);
                var r3 = RULES.Rule3(line);
                if (r1 && r2 && r3) cnt1++;
                var r21 = RULES.Rule2_1(line);
                var r22 = RULES.Rule2_2(line);
                if (r21 && r22) cnt2++;
            }
            Console.WriteLine("Part1: " + cnt1 + " Part2: " + cnt2);
        }

    }

    public static class RULES
    {
        public static bool Rule1(string input)
        {
            var regexRes = Regex.Matches(input, "[aeiou]");
            return regexRes.Count >= 3;
        }
        public static bool Rule2(string input)
        {
            var regexRes = Regex.Matches(input, "([a-z])\\1");
            return regexRes.Count >= 1;
        }
        public static bool Rule3(string input)
        {
            var regexRex = Regex.Matches(input, "ab|cd|pq|xy");
            return regexRex.Count == 0;
        }

        //Part2
        public static bool Rule2_1(string input)
        {
            var regeRes = Regex.Matches(input, "([a-z][a-z]).*\\1");
            return regeRes.Count >= 1;
        }

        public static bool Rule2_2(string input)
        {
            var regeRes = Regex.Matches(input, "([a-z])[a-z]\\1");
            return regeRes.Count >= 1;
        }
    }
}


