﻿using System;
using System.Collections.Generic;
using System.IO;

namespace day6
{
    class Program
    {
        enum TODO
        {
            TOGGLE,
            ON,
            OFF
        }
        static void Main()
        {

            (int, int)[,] lights = new (int, int)[1000, 1000];
            foreach (var line in File.ReadAllLines("INPUT.txt"))
            {
                var start = 0;
                TODO todo;
                if (line.StartsWith("toggle"))
                {
                    start = 6;
                    todo = TODO.TOGGLE;
                }
                else if (line.StartsWith("turn off"))
                {
                    start = 8;
                    todo = TODO.OFF;
                }
                else if (line.StartsWith("turn on"))
                {
                    start = 7;
                    todo = TODO.ON;
                }
                else
                    throw new Exception("OH NO");

                var coordPart = line.Substring(start).Split("through");
                (int, int) xyStart = (int.Parse(coordPart[0].Split(',')[0]), int.Parse(coordPart[0].Split(',')[1]));
                (int, int) xyEnd = (int.Parse(coordPart[1].Split(',')[0]), int.Parse(coordPart[1].Split(',')[1]));

                for (int y = xyStart.Item2; y <= xyEnd.Item2; y++)
                {
                    for (int x = xyStart.Item1; x <= xyEnd.Item1; x++)
                    {
                        switch (todo)
                        {
                            case TODO.TOGGLE:
                                //Part1
                                lights[y, x].Item1 = lights[y, x].Item1 == 0 ? 1 : 0;
                                //Part2
                                lights[y, x].Item2 += 2;
                                break;
                            case TODO.ON:
                                //Part1
                                lights[y, x].Item1 = 1;
                                //Part2
                                lights[y, x].Item2 += 1;
                                break;
                            case TODO.OFF:
                                //Part1
                                lights[y, x].Item1 = 0;
                                //Part2
                                if (lights[y, x].Item2 > 0)
                                    lights[y, x].Item2 -= 1;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            var cnt1 = 0;
            var cnt2 = 0;
            for (int y = 0; y <= 999; y++)
            {
                for (int x = 0; x <= 999; x++)
                {   //Part1
                    if (lights[y, x].Item1 == 1)
                        cnt1++;            
                    //Part2
                    cnt2 += lights[y, x].Item2;
                }
            }
            Console.WriteLine("Part 1: " + cnt1 + " Part2: " + cnt2);

        }
    }
}
