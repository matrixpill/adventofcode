﻿using System;
using System.Text;

namespace day10
{

    class ElvesSay
    {
        public void Start()
        {
            var start = "1113122113";
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < 50; i++)
            {
                var startPos = 0;
                sb.Clear();
                while (startPos < start.Length)
                {
                    var cnt = 1;
                    char last = start[startPos];
                    for (var ci = startPos + 1; ci < start.Length; ci++)
                    {
                        if (start[ci] == last)
                            cnt++;
                        else
                            break;
                    }
                    startPos += cnt;
                    sb.Append(cnt);
                    sb.Append(last);
                }
                start = sb.ToString();
            }
            Console.WriteLine($"Länge: {start.Length}");

        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            new ElvesSay().Start();
        }
    }
}
