﻿using System;
using System.IO;

namespace day2
{
    class Program
    {
        static void Main(string[] args)
        {
            var paperSum = 0;
            var ribbonSum = 0;
            foreach (string line in File.ReadLines("INPUT.txt"))
            {
                var lwh = Array.ConvertAll(line.Split('x'), (i) => int.Parse(i));

                //Part 1
                var a = 2 * lwh[0] * lwh[1];
                var b = 2 * lwh[1] * lwh[2];
                var c = 2 * lwh[2] * lwh[0];
                paperSum += a + b + c + (Math.Min(a, Math.Min(b, c)) / 2);

                //Part 2
                Array.Sort(lwh);
                ribbonSum += (lwh[0] * 2) + (lwh[1] * 2) + (lwh[0] * lwh[1] * lwh[2]);
            }
            Console.WriteLine("A: " + paperSum);
            Console.WriteLine("B: " + ribbonSum);
        }
    }
}
