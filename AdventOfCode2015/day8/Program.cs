﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;

namespace day8
{
    class Program
    {
        static void Main(string[] args)
        {
            var totalLength = 0;
            var storedLength = 0;
            var newTotalLength = 0;

            foreach (string line in File.ReadAllLines("INPUT.txt"))
            {
                totalLength += line.Length;
                var cntSlash = Slash(line[1..^1]); //line.Substring(1,line.Length-2)
                var cntQuote = Quotes(line[1..^1]);
                var cntHexa = Hexa(line[1..^1]);
                newTotalLength += line.Length + 4 + cntSlash * 2 + cntQuote * 2 + cntHexa;
                storedLength += line.Length - cntSlash - cntQuote - (cntHexa * 3) - 2;
            }

            Console.WriteLine("Part1: " + (totalLength - storedLength));
            Console.WriteLine("Part2: " + (newTotalLength - totalLength));
        }

        static int Slash(string line)
        {
            return Regex.Matches(line, @"[\\][\\]").Count;
        }

        static int Quotes(string line)
        {
            var pattern = Regex.Escape("\\") + Regex.Escape("\"");
            return Regex.Matches(line, pattern).Count;
        }

        static int Hexa(string line)
        {
            var pattern = Regex.Escape("\\") + "x[0-9a-f][0-9a-f]";
            return Regex.Matches(line, pattern).Count;
        }
    }
}
