﻿using System;
using System.Collections.Generic;
using System.IO;

namespace day3
{
    class Program
    {
        static void Main()
        {
            string moves = File.ReadAllText("INPUT.txt");
            Dictionary<(int, int), int> houses = new Dictionary<(int, int), int>();
            houses.TryAdd((0, 0), 1);
            /*int sx = 0, sy = 0;
            int rx = 0, ry = 0;
             */
            (int, int) s = (0, 0);
            (int, int) r = (0, 0);
            var santa = true;
            foreach (char move in moves)
            {
                /*int x = santa ? sx : rx;
                int y = santa ? sy : ry;*/
                (int, int) d = santa ? s : r;
                switch (move)
                {
                    case '^':
                        d.Item2++;
                        add(d, houses);
                        break;
                    case '>':
                        d.Item1++;
                        add(d, houses);
                        break;
                    case 'v':
                        d.Item2--;
                        add(d, houses);
                        break;
                    case '<':
                        d.Item1--;
                        add(d, houses);
                        break;

                }
                if (santa) 
                    s = d;
                else
                    r = d;


                santa = !santa;
            }
            Console.WriteLine(houses.Count);


        }

        static void add((int, int) par, Dictionary<(int, int), int> houses)
        {
            var x = par.Item1;
            var y = par.Item2;
            if (!houses.TryGetValue((x, y), out int presentCount))
            {
                houses.TryAdd((x, y), ++presentCount);
            }
            else
            {
                houses[(x, y)] = ++presentCount;
            }
        }
    }
}
