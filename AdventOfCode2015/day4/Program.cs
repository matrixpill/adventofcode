﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace day4
{
    class Program
    {
        static void Main(string[] args)
        {
            const string SECRET = "yzbqklnj";
            var found = false;
            int number = 0;
            while (!found)
            {
                using (MD5 hash = MD5.Create())
                {
                    byte[] b = hash.ComputeHash(Encoding.UTF8.GetBytes(SECRET + number.ToString()));
                    StringBuilder sbuilder = new StringBuilder();
                    for(int i = 0; i < b.Length; i++)
                    {
                        sbuilder.Append(b[i].ToString("x2"));
                    }
                    if (sbuilder.ToString().StartsWith("000000"))
                    {
                        found = true;
                        Console.WriteLine(number);
                    }
                }
                number++;
            }
        }
    }
}
