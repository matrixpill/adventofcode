﻿using System;
using System.Collections.Generic;
using System.IO;

namespace day7
{
    class Program
    {
        static void Main()
        {

            var all = new Dictionary<string, Operation>();

            foreach (var line in File.ReadAllLines("INPUT.txt"))
            {
                var tmp = line.Split("->");
                var dest = tmp[1].Trim();

                var left = tmp[0].Split(" ", StringSplitOptions.RemoveEmptyEntries);
                //VAL
                if (left.Length == 1)
                {
                    all.TryAdd(dest, new VAL(left[0]));
                }
                //NOT
                else if (left.Length == 2)
                {
                    all.TryAdd(dest, new NOT(left[1]));
                }
                //AND OR LSHIFT RSHIFT
                else if (left.Length == 3)
                {
                    switch (left[1])
                    {
                        case "AND":
                            all.TryAdd(dest, new AND(left[0], left[2]));
                            break;
                        case "OR":
                            all.TryAdd(dest, new OR(left[0], left[2]));
                            break;
                        case "LSHIFT":
                            all.TryAdd(dest, new LSHIFT(left[0], left[2]));
                            break;
                        case "RSHIFT":
                            all.TryAdd(dest, new RSHIFT(left[0], left[2]));
                            break;
                    }
                }
                else
                {
                    throw new Exception("WOW");
                }
            }

            var end = all["a"];
            Console.WriteLine(end.execute(all));

        }
    }

    // "VAL", "AND", "OR", "LSHIFT", "RSHIFT", "NOT" };
    public abstract class Operation
    {
        abstract public ulong execute(Dictionary<string, Operation> operations);
    }

    public class VAL : Operation
    {
        string value;
        public VAL(string _value) : base()
        {
            value = _value;
        }
        public override ulong execute(Dictionary<string, Operation> operations)
        {
            //Console.WriteLine("Executing " + " VAL " + value);
            if (ulong.TryParse(value, out ulong result))
            {
                return result;
            }
            else
            {
                var v = operations[value].execute(operations);
                operations[value] = new VAL(v.ToString());
                return v;
            }
        }
    }

    public class AND : Operation
    {
        string a, b;
        public AND(string _a, string _b) : base()
        {
            a = _a;
            b = _b;
        }

        public override ulong execute(Dictionary<string, Operation> operations)
        {
            //Console.WriteLine("Executing " + a +  " AND "  + b);

            if (!ulong.TryParse(a, out ulong ia))
            {
                ia = operations[a].execute(operations);
                operations[a] = new VAL(ia.ToString());
            }

            if (!ulong.TryParse(b, out ulong ib))
            {
                ib = operations[b].execute(operations);
                operations[b] = new VAL(ib.ToString());
            }

            return ia & ib;
        }
    }

    public class OR : Operation
    {
        string a, b;
        public OR(string _a, string _b) : base()
        {
            a = _a;
            b = _b;
        }
        public override ulong execute(Dictionary<string, Operation> operations)
        {
            //Console.WriteLine("Executing " + a + " OR " + b);
            if (!ulong.TryParse(a, out ulong ia))
            {
                ia = operations[a].execute(operations);
                operations[a] = new VAL(ia.ToString());
            }
            if (!ulong.TryParse(b, out ulong ib))
            {
                ib = operations[b].execute(operations);
                operations[b] = new VAL(ib.ToString());
            }
            return ia | ib;
        }
    }

    public class LSHIFT : Operation
    {
        string a;
        int shift;
        public LSHIFT(string _a, string shiftby) : base()
        {
            a = _a;
            shift = int.Parse(shiftby);
        }
        public override ulong execute(Dictionary<string, Operation> operations)
        {
            //Console.WriteLine("Executing " + a + " LSHIFT " + shift);
            var ia = operations[a].execute(operations);
            operations[a] = new VAL(ia.ToString());
            return ia << shift;
        }
    }

    public class RSHIFT : Operation
    {
        string a;
        int shift;
        public RSHIFT(string _a, string shiftBy) : base()
        {
            a = _a;
            shift = int.Parse(shiftBy);
        }
        public override ulong execute(Dictionary<string, Operation> operations)
        {
            //Console.WriteLine("Executing " + a + " RSHIFT " + shift);
            var ia = operations[a].execute(operations);
            operations[a] = new VAL(ia.ToString());

            return ia >> shift;
        }
    }
    public class NOT : Operation
    {
        string a;
        public NOT(string _var) : base()
        {
            a = _var;
        }
        public override ulong execute(Dictionary<string, Operation> operations)
        {
            //Console.WriteLine("Executing " + "NOT " + a);
            var ia = operations[a].execute(operations);
            operations[a] = new VAL(ia.ToString());

            return ~ia;
        }
    }

}
