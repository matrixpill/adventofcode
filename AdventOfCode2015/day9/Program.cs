﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace day9
{
    class Program
    {
        //Thanks Stranger
        static IEnumerable<IEnumerable<T>> GetPermutations<T>(IEnumerable<T> list, int length)
        {
            if (length == 1) return list.Select(t => new T[] { t });

            return GetPermutations(list, length - 1)
                .SelectMany(t => list.Where(e => !t.Contains(e)),
                    (t1, t2) => t1.Concat(new T[] { t2 }));
        }
        static void Main(string[] args)
        {
            var conections = new Dictionary<(string, string), int>();
            var places = new HashSet<string>();
            foreach (var line in File.ReadAllLines("INPUT.txt"))
            {
                var splitted = line.Split('=');
                var AB = splitted[0].Split("to");
                var A = AB[0].Trim();
                var B = AB[1].Trim();
                places.Add(A);
                places.Add(B);
                conections.Add((A, B), int.Parse(splitted[1]));
            }

            
            List<IEnumerable<int>> perm = GetPermutations(Enumerable.Range(1, places.Count), places.Count).ToList();
            
            var max = int.MinValue;
            var min = int.MaxValue;

            foreach (var comb in perm)
            {
                var sum = 0;
                string str = "";
                foreach (var c in comb)
                    str += c;
                for(int i = 1; i < comb.Count(); i++)
                {
                    var A = places.ElementAt((int) char.GetNumericValue(str[i-1])-1);
                    var B = places.ElementAt((int)char.GetNumericValue(str[i])-1);
                    if (conections.Keys.Contains((A, B)))
                        sum += conections[(A, B)];
                    else
                        sum += conections[(B, A)];
                }
                if (sum > max)
                    max = sum;
                if (sum < min)
                    min = sum;
            }

            
            Console.WriteLine("Min: " + min + " Max: " + max);
        }
    }
}
